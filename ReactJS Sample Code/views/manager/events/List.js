import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { withRouter } from 'react-router';
import { Link } from 'react-router-dom';
import styled from 'styled-components';
import moment from 'moment-timezone';
import _ from 'lodash';

import * as UiActionCreators from '../../../redux/ui';
import * as MeActionCreators from '../../../redux/me';
import * as ManagerActionCreators from '../../../redux/manager';

import ManagerContainer from '../../../components/layouts/ManagerContainer';
import ConfirmModal from '../../../components/common/ConfirmModal';
import EventTile from '../../../components/common/tiles/EventTile';

import eventActionImage from '../../../assets/common/actions/events.svg';

import { t as translate } from '../../../../i18n';

const t = a => translate(a, 'manager/events/index');

const EventTilesWrapper = styled.div`
  display: flex;
  flex-wrap: wrap;
`;

const AddEventTile = styled.div`
  position: relative;
  flex: 0 0 30%;
  margin: 0 3.3333% 25px 0;
  border-radius: 5px;
  background-color: white;
  box-shadow: 0 0 6px 0 rgba(0, 0, 0, 0.16);

  &:before {
    padding-top: 70%;
    display: block;
    content: '';
  }

  & > a {
    display: block;
    position: absolute;
    padding: 20px 15px;
    top: 0;
    bottom: 0;
    left: 0;
    right: 0;
  }
`;

const AddEventTileContent = styled.div`
  position: absolute;
  left: 50%;
  top: 50%;
  padding-bottom: 20px;
  transform: translate(-50%, -50%);
  text-align: center;

  & img {
    width: 90px;
  }

  & h4 {
    color: ${ props => props.theme.colors.mainBlue };

    & i {
      margin-right: 10px;
    }
  }

  &:hover,
  &:active {
    h4 {
      color: ${ props => props.theme.colors.darkBlue };
    }
  }
`;

const mapStateToProps = state => ({
  user: state.me.user,
  organization: state.manager.organization,
  events: state.manager.events,
});
const ActionCreators = Object.assign(
  {},
  UiActionCreators,
  MeActionCreators,
  ManagerActionCreators,
);
const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators(ActionCreators, dispatch),
});

class EventsListView extends Component {
  state = {
    deletingEvent: null,
  };

  async componentDidMount () {
    const { actions, match } = this.props;
    const { oId } = match.params;

    actions.setLoading(true);

    try {
      await actions.getOrganizationEvents(oId);
    } catch (e) {
      console.error('ERROR: EventsListView', e);
    } finally {
      actions.setLoading(false);
    }
  }


  setDeletingEvent = (deletingEvent = null) => this.setState({ deletingEvent });

  onConfirm = event => async () => {
    try {
      const { actions, organization } = this.props;
      await actions.deleteEvent(event.id);
      await Promise.all([actions.getOrganizationEvents(organization.id)]);
      this.setDeletingEvent(null);
    } catch (error) {
      return Promise.reject(error);
    }
  };

  onCancel = () => this.setDeletingEvent(null);

  render () {
    const { organization, events } = this.props;
    const { deletingEvent } = this.state;

    // Sort event
    const localTZ = moment.tz.guess();
    const currentTime = moment.now();
    let futureEvent = events.filter(event => event.start_date
      && (moment
        .utc(event.start_date)
        .tz(event.timezone || localTZ)
        .isAfter(currentTime)
      || moment
        .utc(event.start_date)
        .tz(event.timezone || localTZ)
        .isSame(currentTime)
      ));

    let passEvent = events.filter(event => event.start_date
        && moment
          .utc(event.start_date)
          .tz(event.timezone || localTZ)
          .isBefore(currentTime));
    const undefinedEvent = events.filter(event => !event.start_date);
    futureEvent = _.sortBy(futureEvent, 'start_data').reverse();
    passEvent = _.sortBy(passEvent, 'start_date').reverse();
    // Map EventTiles
    const EventTiles = [...futureEvent, ...undefinedEvent, ...passEvent].map((e) => {
      const routeToEvent = `/${ organization.id }/events/${ e.id }`;
      const routeToEventPage = `/${ e.id }`;

      return (
        <EventTile
          event={ e }
          pageLink={ routeToEventPage }
          manageLink={ routeToEvent }
          onDelete={ this.setDeletingEvent.bind(this, e) }
        />
      );
    });

    return (
      <ManagerContainer>
        <EventTilesWrapper>
          { EventTiles }
          <AddEventTile>
            <Link to={ `/${ organization.id }/events/new` }>
              <AddEventTileContent>
                <img alt='' src={ eventActionImage } />
                <h4>{ t('Create New Event') }</h4>
              </AddEventTileContent>
            </Link>
          </AddEventTile>
        </EventTilesWrapper>
        { deletingEvent ? (
          <ConfirmModal
            warning
            message={ `${ t('Are you sure you want to delete') } ${
              deletingEvent.name
            }?` }
            onConfirm={ this.onConfirm(deletingEvent).bind(this) }
            onCancel={ this.onCancel }
          />
        ) : null }
      </ManagerContainer>
    );
  }
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(EventsListView));
