import React from 'react';
import { BrowserRouter } from 'react-router-dom';
import { createStore, applyMiddleware } from 'redux';
import { Provider } from 'react-redux';
import thunk from 'redux-thunk';
import { hot } from 'react-hot-loader';

import rootReducer from './redux';
// import 'custom.d';
import AppView from './AppView';

const store = createStore(rootReducer, applyMiddleware(thunk));

const Index = () => (
  <Provider store={ store }>
    <BrowserRouter>
      <AppView />
    </BrowserRouter>
  </Provider>
);

export default hot(module)(Index);
