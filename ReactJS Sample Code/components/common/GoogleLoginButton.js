import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import styled from 'styled-components';

import Button from './Button';

import * as MeActionCreators from '../../redux/me';
import * as ManagerActionCreators from '../../redux/manager';

import googleLoginIcon from '../../assets/common/icon_google_signin.png';

import i18n from '../../../i18n';

const t = a => i18n.t([`components/common:${ a }`]);

const { GOOGLE_SIGNIN_CLIENT_ID } = process.env;

const GoogleLoginStyledButton = styled(Button)`
  box-shadow: 0 6px 6px 0 rgba(0, 0, 0, 0.16);
  background-color: white;
  color: ${ props => props.theme.colors.darkGray };
  border-radius: 21px;
  padding: 0;

  display: flex;
  align-items: center;
  justify-content: center;

  &:hover,
  &:active {
    background-color: white;
    color: ${ props => props.theme.colors.mainGray };
  }

  @media (max-width: 900px) {
    height: 42px;
    line-height: 42px;
  }
`;

const GoogleIcon = styled.img`
  height: 18px;
  margin-right: 20px;
`;

const SmallScreenHiddenText = styled.span`
  @media (max-width: 450px) {
    display: none;
  }
`;

const ActionCreators = Object.assign(
  {},
  MeActionCreators,
  ManagerActionCreators,
);

const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators(ActionCreators, dispatch),
});

class GoogleLoginButton extends Component {
  componentDidMount () {
    if (window.auth2) {
      this.attachSignin();
    } else {
      this.loadAuth2();
    }
  }

  loadAuth2 () {
    // try again after 500ms if gapi is not loaded yet
    if (typeof gapi === 'undefined') {
      return setTimeout(this.loadAuth2, 500);
    }

    gapi.load('auth2', () => {
      // Retrieve the singleton for the GoogleAuth library and set up the client.
      window.auth2 = gapi.auth2.init({
        client_id: GOOGLE_SIGNIN_CLIENT_ID,
        cookiepolicy: 'single_host_origin',
      });

      this.attachSignin();
    });
  }

  attachSignin () {
    const { actions, onFinish } = this.props;

    window.auth2.attachClickHandler(
      document.getElementById('google-signin'),
      {},
      async (googleUser) => {
        const idToken = googleUser.getAuthResponse().id_token;
        if (!idToken) return;

        const data = { id_token: idToken };
        await actions.loginWithGoogle(data);
        await actions.getMyManagedOrganizations();
        await actions.getMe();
        if (onFinish) await onFinish();
      },
      error => console.error(error),
    );
  }

  render () {
    const { signup, ...props } = this.props;

    return (
      <GoogleLoginStyledButton
        fullWidth
        type='button'
        id='google-signin'
        className='_google'
        { ...props }
      >
        <GoogleIcon alt='' src={ googleLoginIcon } />
        <p>
          <SmallScreenHiddenText>{ `${ signup ? t('Sign up with') : t('Log in with') } ` }</SmallScreenHiddenText>
          <span>{ t('Google') }</span>
        </p>
      </GoogleLoginStyledButton>
    );
  }
}

export default connect(null, mapDispatchToProps)(GoogleLoginButton);
