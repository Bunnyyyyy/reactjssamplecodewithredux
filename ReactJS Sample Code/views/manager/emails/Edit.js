import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { withRouter } from 'react-router';
import styled from 'styled-components';

import { validateEmail, validateURL } from '../../../utils';
import * as ManagerActionCreators from '../../../redux/manager';
import * as EmailTemplatesActionCreators from '../../../redux/emailTemplates';
import * as UiActionCreators from '../../../redux/ui';

import ManagerContainer from '../../../components/layouts/ManagerContainer';
import ElementsBuilder from '../../../components/manager/ElementsBuilder';
import Input from '../../../components/common/Input';
import Label from '../../../components/common/inputs/Label';
import Button from '../../../components/common/Button';

import { t as translate } from '../../../../i18n';

const t = a => translate(a, 'manager/emails/edit');

const ActionWrapper = styled.div`
  margin-bottom: 40px;
`;

const InstructionText = styled.p`
  font-size: 12px;
  line-height: 14px;
  margin: 10px 0;

  ${ props => props.customCss || '' };
`;

const ELEMENTS_BUILDER_NAME = 'ELEMENTS_BUILDER_NAME';
const TYPES = [
  {
    type: 'logo',
    label: t('Organization Logo'),
  },
  {
    type: 'title',
    label: t('Title'),
    validate: e => e.text && e.text.length,
  },
  {
    type: 'greeting',
    label: t('Greeting'),
  },
  {
    type: 'text',
    label: t('Text Paragraph'),
    validate: e => e.text && e.text.length,
  },
  {
    type: 'image',
    label: t('Image'),
    validate: e => e.file || e.image_url,
  },
  {
    type: 'link_button',
    label: t('Link Button'),
    validate: e => e.text && e.text.length && e.text.length < 50 && e.url && e.url.length && validateURL(e.url),
  },
];

// HACK: keep reference to same array for new email
const NEW_EMAIL_ELEMENTS = [];


const mapStateToProps = state => ({
  organization: state.manager.organization,
  template: state.emailTemplates.template,
});

const ActionCreators = Object.assign(
  {},
  ManagerActionCreators,
  EmailTemplatesActionCreators,
  UiActionCreators,
);
const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators(ActionCreators, dispatch),
});

class EmailsEditView extends Component {
  constructor () {
    super();

    this.state = {
      title: '',
      testingEmail: null,
      testingEmailSuccess: false,
      testingEmailError: false,
    };
  }

  async componentDidMount () {
    const { actions, match } = this.props;
    const { oId, emId } = match.params;

    actions.setLoading(true);

    try {
      if (emId) {
        await actions.getEmailTemplate(oId, emId);

        const { template: { title } } = this.props;
        this.setState({ title });
      } else {
        actions.setEmailTemplate({});
        actions.addSaveAction(null, ELEMENTS_BUILDER_NAME);
      }
    } catch (e) {
      console.error('ERROR: EmailsEditView', e);
    } finally {
      actions.setLoading(false);
    }
  }

  componentWillUnmount () {
    const { actions } = this.props;
    actions.resetSaveActions();
  }

  async onFinishSaving () {
    try {
      const {
        actions, match: { params: { emId } }, history, organization, template,
      } = this.props;
      await actions.getEmailTemplate(organization.id, template.id);
      if (!emId) {
        return history.push(`/${ organization.id }/emails/${ template.id }`);
      }
    } catch (e) {
      console.error(e);
    }
  }

  onCancelSaving () {
    const { history, organization, template } = this.props;

    if (template.id) {
      this.setState({
        title: template.title,
      });
    } else {
      history.push(`/${ organization.id }/emails`);
    }
  }

  async createEmailAction (data) {
    try {
      const { actions, organization } = this.props;
      const oId = organization.id;

      const email = await actions.createEmailTemplate(oId, data);
      await actions.getEmailTemplate(oId, email.id);

      return;
    } catch (e) {
      throw e;
    }
  }

  changeTitle (field, value) {
    this.setState({ title: value }, this.updateTitle);
  }

  updateTitle () {
    const { actions, organization, template } = this.props;
    const { title } = this.state;

    const isInvalid = !title || !title.length;

    if (isInvalid) {
      actions.addSaveAction(null, ELEMENTS_BUILDER_NAME);
      // TODO: make it more universal for different types
    } else {
      const action = template.id
        ? actions.updateEmailTemplate.bind(this, organization.id, template.id, {
          title,
        })
        : this.createEmailAction.bind(this, { title });

      actions.addSaveAction(action, ELEMENTS_BUILDER_NAME, null, 0);
    }
  }

  changeTestingEmail (field, value) {
    this.setState({
      testingEmail: value,
      testingEmailSuccess: false,
      testingEmailError: false,
    });
  }

  async sendTestingEmail () {
    const { actions, organization, template } = this.props;
    const { testingEmail } = this.state;

    if (!validateEmail(testingEmail)) return;

    try {
      const sendData = {
        testing: true,
        emails: [testingEmail],
      };
      await actions.sendTemplateEmails(organization.id, template.id, sendData);
      this.setState({ testingEmailSuccess: true }, () => {
        setTimeout(() => this.setState({ testingEmailSuccess: false }), 5000);
      });
    } catch (e) {
      console.error(e);
      this.setState({ testingEmailError: true });
    }
  }

  createElementAction (element) {
    const { actions, organization, template } = this.props;

    return actions.createEmailTemplateElement(
      organization.id,
      template.id,
      element,
    );
  }

  updateElementAction (eleId, element) {
    const { actions, organization, template } = this.props;

    return actions.updateEmailTemplateElement(
      organization.id,
      template.id,
      eleId,
      element,
    );
  }

  removeElementAction (eleId) {
    const { actions, organization, template } = this.props;

    return actions.removeEmailTemplateElement(
      organization.id,
      template.id,
      eleId,
    );
  }

  render () {
    const { organization, template } = this.props;
    const {
      title,
      testingEmail,
      testingEmailSuccess,
      testingEmailError,
    } = this.state;

    const LeftComponents = [
      <Input
        field='title'
        label={ t('Title') }
        value={ title }
        onChange={ this.changeTitle.bind(this) }
        invalid={ !!template.id && (!title || !title.length) }
      />,
      template.id ? (
        <ActionWrapper>
          <Label label={ t('Send Sample Email') } customCss='opacity: 1;' />
          <InstructionText>
            { t(
              'You may send a sample email of this template to your email address for preview.',
            ) }
          </InstructionText>
          <Input
            field='testing_email'
            value={ testingEmail }
            onChange={ this.changeTestingEmail.bind(this) }
            placeholder={ t('Enter your email address here') }
            customCss='width: 100%; margin-bottom: 10px;'
          />
          <Button
            small
            fullWidth
            disabled={ !validateEmail(testingEmail) }
            onClick={ this.sendTestingEmail.bind(this) }
            customCss='margin: 0;'
          >
            { t('Send') }
          </Button>
          { testingEmailSuccess ? (
            <InstructionText customCss='color: rgb(0, 167, 255);'>
              { t('The sample email was successfully sent.') }
            </InstructionText>
          ) : null }
          { testingEmailError ? (
            <InstructionText customCss='color: rgb(255, 108, 108);'>
              { t(
                'There was an error when sending the sample email. Please try again.',
              ) }
            </InstructionText>
          ) : null }
        </ActionWrapper>
      ) : null,
    ];

    return (
      <ManagerContainer noSubheader contentCss='padding: 0;'>
        <ElementsBuilder
          isEmail
          organization={ organization }
          newObject={ !template.id }
          LeftComponents={ LeftComponents }
          elementTypes={ TYPES }
          elements={ template.id ? template.elements : NEW_EMAIL_ELEMENTS }
          createElementAction={ this.createElementAction.bind(this) }
          updateElementAction={ this.updateElementAction.bind(this) }
          removeElementAction={ this.removeElementAction.bind(this) }
          onFinish={ this.onFinishSaving.bind(this) }
          onCancel={ this.onCancelSaving.bind(this) }
          emptyText={ `${ t(
            'There are no Elements in this Email yet.\nYou may start adding elements by selecting one of the Element Types on the left and drag it here.',
          ) }` }
          saveActionsCss='
            position: absolute;
            z-index: 799;
            width: 100%;
            top: 0;
            right: 0;
            box-shadow: 0 0 6px 0 rgba(0, 0, 0, 0.15);
        '
        />
      </ManagerContainer>
    );
  }
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(EmailsEditView));
