import React from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { withRouter } from 'react-router';
import { Route, Switch } from 'react-router-dom';

import SubpageMenu from '../../../../../components/manager/SubpageMenu';
import ManagerContainer from '../../../../../components/layouts/ManagerContainer';

import EventTicketingTypesListView from './TypesList';
import EventTicketingTypesEditView from './TypesEdit';
import EventTicketingPackagesListView from './PackagesList';
import EventTicketingPackagesEditView from './PackagesEdit';
import EventTicketingPromoCodesListView from './PromoCodesList';
import EventTicketingPromoCodesEditView from './PromoCodesEdit';
import EventTicketingDataCollectionView from './DataCollection';
import EventTicketingAdditionalInfoView from './AdditionalInfo';
import EventTicketingLabelView from './Label';

import * as ManagerActionCreators from '../../../../../redux/manager';
import { t as translate } from '../../../../../../i18n';

const t = a => translate(a, 'manager/events/ticketing');

const mapStateToProps = state => ({
  organization: state.manager.organization,
  events: state.manager.events,
  event: state.manager.event,
});
const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators(ManagerActionCreators, dispatch),
});

const EventIndexView = ({ location, organization, event }) => {
  const baseUrl = `/${ organization.id }/events/${ event.id }/ticketing`;
  const pages = [
    {
      title: t('Ticket Types'),
      route: `${ baseUrl }/types`,
    },
    {
      title: t('Ticket Packages'),
      route: `${ baseUrl }/packages`,
    },
    {
      title: t('Promo Codes'),
      route: `${ baseUrl }/promo-codes`,
    },
    {
      title: t('Guest Data Collection'),
      route: `${ baseUrl }/data-collection`,
    },
    {
      title: t('Additional Information'),
      route: `${ baseUrl }/additional-info`,
    },
    {
      title: t('Customized Label'),
      route: `${ baseUrl }/label`,
    },
  ];

  const contentCss = location.pathname.indexOf(`/${ event.id }/ticketing/data-collection`) > 0
    ? 'padding-bottom: 0;overflow: hidden;'
    : '';

  return (
    <ManagerContainer
      noLoading
      contentCss={ contentCss }
      subpageMenu={ <SubpageMenu pages={ pages } /> }
    >
      <Switch>
        <Route exact path='/:oId/events/:eId/ticketing/types' component={ EventTicketingTypesListView } />
        <Route exact path='/:oId/events/:eId/ticketing/types/new' component={ EventTicketingTypesEditView } />
        <Route exact path='/:oId/events/:eId/ticketing/types/:tkId' component={ EventTicketingTypesEditView } />
        <Route exact path='/:oId/events/:eId/ticketing/packages' component={ EventTicketingPackagesListView } />
        <Route exact path='/:oId/events/:eId/ticketing/packages/new' component={ EventTicketingPackagesEditView } />
        <Route exact path='/:oId/events/:eId/ticketing/packages/:pkId' component={ EventTicketingPackagesEditView } />
        <Route exact path='/:oId/events/:eId/ticketing/promo-codes' component={ EventTicketingPromoCodesListView } />
        <Route exact path='/:oId/events/:eId/ticketing/promo-codes/new' component={ EventTicketingPromoCodesEditView } />
        <Route exact path='/:oId/events/:eId/ticketing/promo-codes/:pcId' component={ EventTicketingPromoCodesEditView } />
        <Route exact path='/:oId/events/:eId/ticketing/data-collection' component={ EventTicketingDataCollectionView } />
        <Route exact path='/:oId/events/:eId/ticketing/additional-info' component={ EventTicketingAdditionalInfoView } />
        <Route exact path='/:oId/events/:eId/ticketing/label' component={ EventTicketingLabelView } />
      </Switch>
    </ManagerContainer>
  );
};

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(EventIndexView));
