import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { withRouter } from 'react-router';
import { Link } from 'react-router-dom';
import styled from 'styled-components';
import queryString from 'query-string';
import MobileDetect from 'mobile-detect';
import { Formik } from 'formik';
import * as Yup from 'yup';

import { validateEmail } from '../../utils';

import * as MeActionCreators from '../../redux/me';
import * as ManagerActionCreators from '../../redux/manager';

import Input from '../../components/common/Input';
import PasswordMaskInput from '../../components/common/inputs/PasswordMaskInput';
import Button from '../../components/common/Button';
import Divider from '../../components/common/Divider';
import FacebookLoginButton from '../../components/common/FacebookLoginButton';
import GoogleLoginButton from '../../components/common/GoogleLoginButton';
import mobileDesktopViewImage from '../../assets/manager/mobile-desktop-view.svg';

import { t as translate } from '../../../i18n';

const t = a => translate(a, 'manager/index');

const { FRONTEND_URL } = process.env;

const Title = styled.h3`
  text-align: center;
  margin-bottom: 30px;
`;

const StyledInput = styled(Input)`
  margin-bottom: 30px;
`;

const InputGroup = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: space-evenly;

  > * {
    margin-right: 0;
    flex-basis: 50%;

    &:nth-child(1) {
      padding-right: 0.5rem;
    }

    &:nth-child(2) {
      padding-left: 0.5rem;
    }
  }
`;

const ToggleWrapper = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  margin: 30px 0;
  
  p {
    margin-right: 10px;
  }

  a {
    color: ${ props => props.theme.colors.mainBlue };
  }
`;

const TermsMessage = styled.p`
  text-align: center;
  color: #7d909a;
  margin: 10px 0;
  padding: 0 40px;
  font-size: 12px;
  line-height: 18px;

  a {
    font-size: 12px;
    line-height: 18px;
    text-decoration: underline;
    color: #7d909a;
  }
`;

const PromptWrapper = styled.div`
  text-align: center;
`;

const StyledDiv = styled.div`
  margin-top: 20px;
  margin-bottom: 20px;
  line-height: 24px;
`;

const mapStateToProps = state => ({
  user: state.me.user,
});

const ActionCreators = Object.assign(
  {},
  MeActionCreators,
  ManagerActionCreators,
);
const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators(ActionCreators, dispatch),
});

const SIGNUP_ERROR_MESSAGE = t('There was an error when you were signing up.');

class AuthSignupView extends Component {
  constructor () {
    super();

    this.state = {
      mobilePrompt: false,
      mobileEmailSent: null,
      mobilePromptError: null,
      signupError: null,
    };
  }

  componentDidMount () {
    const md = new MobileDetect(window.navigator.userAgent);
    if (md.mobile() !== null) {
      return this.toggleMobilePrompt();
    }

    if (this.input) {
      this.input.focus();
    }
  }

  onSignedUp () {
    const { location, history } = this.props;
    const { redirect } = queryString.parse(location.search);

    const { user } = this.props;
    const targetUrl = user.verified ? (redirect || '/') : '/verify';

    return history.push(targetUrl);
  }

  toggleMobilePrompt () {
    const { mobilePrompt } = this.state;

    this.setState({
      mobilePrompt: !mobilePrompt,
    });
  }

  async signup (state) {
    const { actions, location } = this.props;
    const { invite_token } = queryString.parse(location.search);

    try {
      this.setState({ signupError: null });

      if (invite_token) state.invite_token = invite_token;
      await actions.signup(state);
      await actions.getMe();
      await actions.getMyManagedOrganizations();

      this.onSignedUp();
    } catch (e) {
      const signupError = e.message || SIGNUP_ERROR_MESSAGE;

      return this.setState({ signupError });
    }
  }

  async sendMobileEmail (state) {
    const { actions } = this.props;

    try {
      await actions.sendDesktopSignupLink(state);
      this.setState({
        mobileEmailSent: state.email,
      });
    } catch (e) {
      const mobilePromptError = e.message;
      return this.setState({ mobilePromptError });
    }
  }

  render () {
    const { location } = this.props;
    const {
      signupError, mobilePrompt, mobilePromptError, mobileEmailSent,
    } = this.state;
    const { email, invite_token } = queryString.parse(location.search);

    const SignupFormSchema = Yup.object().shape({
      first_name: Yup.string()
        .nullable()
        .required(t('Please fill in your first name')),
      last_name: Yup.string()
        .nullable()
        .required(t('Please fill in your last name')),
      email: Yup.string()
        .nullable()
        .test(
          'is-valid-email',
          t('Please fill in a valid email'),
          v => !v || validateEmail(v),
        ),
      password: Yup.string()
        .nullable()
        .required(t('Please fill in a password that is at least 8 characters long'))
        .min(8, t('Your password must be at least 8 characters long')),
    });

    const signupFormInitialValues = {};
    if (email) signupFormInitialValues.email = email;

    const SignupForm = (
      <Formik
        initialValues={ signupFormInitialValues }
        validationSchema={ SignupFormSchema }
        onSubmit={ this.signup.bind(this) }
      >
        { (props) => {
          const {
            values, errors, setFieldValue, handleSubmit, submitCount,
          } = props;
          const validateOnChange = submitCount > 0;

          return (
            <div>
              <Title>{ t('Create your free Juven account') }</Title>
              <div>
                <FacebookLoginButton onFinish={ this.onSignedUp.bind(this) } />
                <GoogleLoginButton onFinish={ this.onSignedUp.bind(this) } />
                <Divider text='OR' />
              </div>
              <InputGroup>
                <StyledInput
                  field='first_name'
                  label={ t('First name') }
                  inputRef={ input => (this.input = input) }
                  placeholder={ t('Mayanne') }
                  name='first_name'
                  value={ values.first_name }
                  errorMessage={ errors.first_name }
                  onChange={ (_, value) => setFieldValue('first_name', value, validateOnChange) }
                  customCss='flex: 0 0 50%; padding-right: 10px;'
                />
                <StyledInput
                  field='last_name'
                  label={ t('Last name') }
                  placeholder={ t('Byne') }
                  name='last_name'
                  value={ values.last_name }
                  errorMessage={ errors.last_name }
                  onChange={ (_, value) => setFieldValue('last_name', value, validateOnChange) }
                  customCss='flex: 0 0 50%; padding-left: 10px;'
                />
              </InputGroup>
              <StyledInput
                field='email'
                fullWidth
                label={ t('Email Address') }
                onChange={ (_, value) => setFieldValue('email', value, validateOnChange) }
                value={ values.email }
                errorMessage={ errors.email }
                readOnly={ !!invite_token }
              />
              <PasswordMaskInput
                type='password'
                key='password'
                field='password'
                fullWidth
                label={ t('Password') }
                name='password'
                value={ values.password }
                errorMessage={ signupError || errors.password }
                onChange={ (_, value) => setFieldValue('password', value, validateOnChange) }
              />
              <TermsMessage>
                { `${ t('By creating an account, you agree to Juven\'s') } ` }
                <br />
                <a href={ `${ FRONTEND_URL }/terms` } target='_blank' rel='noopener noreferrer'>
                  { t('Terms of Service') }
                </a>
                <span>{ ` ${ t('and') } ` }</span>
                <a href={ `${ FRONTEND_URL }/privacy` } target='_blank' rel='noopener noreferrer'>
                  { t('Privacy Policy') }
                </a>
                <span>.</span>
              </TermsMessage>
              <Button fullWidth onClick={ handleSubmit }>
                { t('Sign up now') }
              </Button>
              { !invite_token ? (
                <ToggleWrapper>
                  <p>{ t('Already have an account?') }</p>
                  <Link to='/login'>
                    { t('Log in now') }
                  </Link>
                </ToggleWrapper>
              ) : null }
            </div>
          );
        } }
      </Formik>
    );

    if (invite_token && email) return SignupForm;

    const MobileEmailFormSchema = Yup.object().shape({
      email: Yup.string()
        .nullable()
        .test(
          'is-valid-email',
          t('Please fill in a valid email'),
          v => !v || validateEmail(v),
        ),
    });

    const MobileEmailForm = (
      <PromptWrapper>
        <Title>{ t('It\'s better on a larger screen') }</Title>
        <div>
          <img alt='' src={ mobileDesktopViewImage } />
        </div>
        <StyledDiv>
          { t(
            'Juven Manager experience is best on larger screens. Do you want to continue to sign up on this device?',
          ) }
        </StyledDiv>
        <p>
          <Button fullWidth onClick={ this.toggleMobilePrompt.bind(this) }>
            { t('Continue anyway') }
          </Button>
          <Divider text='OR' />
        </p>
        <StyledDiv>
          { t('Receive a quick signup link via email to sign up on desktop.') }
        </StyledDiv>
        <Formik
          validationSchema={ MobileEmailFormSchema }
          initialValues={ {} }
          onSubmit={ this.sendMobileEmail.bind(this) }
        >
          { (props) => {
            const {
              values, errors, setFieldValue, handleSubmit, submitCount,
            } = props;
            const validateOnChange = submitCount > 0;
            return (
              <div>
                <StyledInput
                  field='email'
                  fullWidth
                  label={ t('Email') }
                  name='email'
                  type='email'
                  value={ values.email }
                  errorMessage={ mobilePromptError || errors.email }
                  onChange={ (_, value) => setFieldValue('email', value, validateOnChange) }
                />
                <Button fullWidth light onClick={ handleSubmit }>
                  { t('Send Signup Link') }
                </Button>
              </div>
            );
          } }
        </Formik>
      </PromptWrapper>
    );

    const MobileEmailSentScreen = (
      <PromptWrapper>
        <Title>{ t('Signup link sent') }</Title>
        <p>
          { t('We have sent an email with the signup link to ') }
          <b>
            { mobileEmailSent }
          </b>
        </p>
        <StyledDiv>
          { t('We are looking forward for you to sign up on Juven!') }
        </StyledDiv>
        <p>
          <a href={ `${ FRONTEND_URL }/` }>
            <Button fullWidth>{ t('Learn about Juven') }</Button>
          </a>
          <a href={ `${ FRONTEND_URL }/events` }>
            <Button fullWidth light>
              { t('Discover events') }
            </Button>
          </a>
        </p>
      </PromptWrapper>
    );

    if (mobileEmailSent) {
      return MobileEmailSentScreen;
    } if (mobilePrompt) {
      return MobileEmailForm;
    }
    return SignupForm;
  }
}

export default withRouter(
  connect(mapStateToProps, mapDispatchToProps)(AuthSignupView),
);
