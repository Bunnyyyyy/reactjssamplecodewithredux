import React from 'react';
import styled from 'styled-components';

import { BOLD, ITALIC, UNDERLINED } from '../constants';

const Icon = styled.i`
  display: inline-block;
  cursor: pointer;
  text-align: center;
  height: 44px;
  width: 40px;
  font-size: 14px;
  line-height: 44px;
  color: ${ props => (
    props.selected ? props.theme.colors.mainBlue : props.theme.colors.darkGray
  ) };

  &:hover {
    color: ${ props => props.theme.colors.mainBlue };
  }
`;

function Typographical ({ onClick, selected }) {
  const icons = [
    {
      className: 'fas fa-bold',
      type: BOLD,
    },
    {
      className: 'fas fa-italic',
      type: ITALIC,
    },
    {
      className: 'fas fa-underline',
      type: UNDERLINED,
    },
  ];
  return icons.map(({ className, type }) => (
    <Icon
      onClick={ () => onClick(type) }
      className={ className }
      selected={ selected(type) }
    />
  ));
}

export default Typographical;
