import React from 'react';
import styled from 'styled-components';

const Wrapper = styled.ol`
  list-style-type: decimal;
  list-style-position: inside;
`;

function OrderedList ({ children, attributes }) {
  return (
    <Wrapper { ...attributes }>
      { children }
    </Wrapper>
  );
}

export default OrderedList;
