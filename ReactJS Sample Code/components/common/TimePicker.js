import React, { Component } from 'react';
import moment from 'moment-timezone';
import styled from 'styled-components';

import TimePicker from 'rc-time-picker';
import Label from './inputs/Label';
import InvalidMessage from './InvalidMessage';

const TimePickerDiv = styled.div`
  position: relative;
  display: inline-block;
  width: ${ props => (props.fullWidth ? '100%' : '270px') };
  margin-top: 20px;
  margin-bottom: 20px;
  margin-right: 20px;
  border-radius: 0;

  .rc-time-picker {
    width: 100%;
    height: 32px;
    background: none;
    border-bottom: 1px solid rgba(47, 46, 46, 0.25);

    &-input {
      border: none;
      height: 31px;
      font-size: 15px;
      font-weight: 400;
      line-height: 31px;
      padding: 0;
      background: none;
      color: ${ props => props.theme.colors.darkGray };

      &[disabled] {
        border: none;
      }
    }
  }

  ${ props => props.customCss || '' };
`;

const format = 'h:mm a';

const now = moment()
  .minute(0)
  .second(0)
  .millisecond(0);

export default class TimePickerNew extends Component {
  changeDate (date) {
    if (date) {
      const { defaultDate, timezone, onChange } = this.props;
      const dateObj = date && timezone ? date.tz(timezone) : date;
      if (onChange) onChange(dateObj, defaultDate, timezone);
    }
  }

  render () {
    const {
      field,
      label,
      date,
      timezone,
      fullWidth,
      divStyle,
      customCss,
      readOnly,
      required,
      errorMessage,
      onChange,
      defaultDate,
      ...props
    } = this.props;
    const dateObj = date
      ? timezone ? moment.tz(date, timezone) : moment(date).local()
      : null;
    return (
      <TimePickerDiv fullWidth={ fullWidth } style={ divStyle } customCss={ customCss }>
        { label ? (
          <Label field={ field } label={ label } required={ required } />
        ) : null }
        <TimePicker
          showSecond={ false }
          defaultValue={ defaultDate || null }
          defaultOpenValue={ now }
          value={ dateObj || null }
          onChange={ this.changeDate.bind(this) }
          format={ format }
          use12Hours
          inputReadOnly
          disabled={ readOnly }
          minuteStep={ 5 }
          { ...props }
        />
        { errorMessage ? <InvalidMessage>{ errorMessage }</InvalidMessage> : null }
      </TimePickerDiv>
    );
  }
}
