import React from 'react';
import styled from 'styled-components';
import { ToastContainer as Toast, cssTransition } from 'react-toastify';

import toastifyCss from '../../assets/ReactToastify.css';

const ToastContainer = styled.div`
  ${ toastifyCss } .Toastify__toast {
    min-height: 40px;
    background-color: #80e18f;
    border-radius: 5px;
    width: 185px;
    text-align: center;
    left: calc(50% - 90px);
    top: 36px;
  }
`;

const ToastBodyWrapper = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
`;

const ToastMessage = styled.span`
  font-size: 12px;
  padding-left: 5px;
  font-weight: bold;
`;

export const ToastBody = ({ notification }) => {
  const { text } = notification;

  // TODO: add styles for non-success types later
  return (
    <ToastBodyWrapper>
      <i className='fas fa-check' />
      <ToastMessage>{ text }</ToastMessage>
    </ToastBodyWrapper>
  );
};

const slideTransition = cssTransition({
  enter: 'Toastify__slide-enter',
  exit: 'Toastify__slide-exit',
  duration: [100, 750],
  appendPosition: true,
});

const ToastNotification = () => (
  <ToastContainer>
    <Toast
      transition={ slideTransition }
      autoClose={ 2000 }
      closeButton={ false }
      hideProgressBar
      newestOnTop
    />
  </ToastContainer>
);

export default ToastNotification;
