import React, { Component } from 'react';
import styled from 'styled-components';
import timer from 'react-timer-hoc';

const Time = styled.div`
  font-size: 18px;

  & i {
    margin-right: 5px;
  }
`;

function twoDigits (n) {
  return n > 9 ? '' + n : '0' + n;
}

// endAt is either a moment object or null
const Timer = ({ endAt, onEnd, timer, active, style }) => {
  if (!active || !endAt) return <Time>--:--</Time>;

  const minutesLeft = Math.max(0, endAt.diff(new Date(), 'minute'));
  const secondsLeft = Math.max(0, endAt.diff(new Date(), 'second') % 60);

  const timeStyle = {};
  if (!minutesLeft && !secondsLeft) timeStyle.color = 'rgb(255, 108, 108)';

  return (
    <Time style={ { ...timeStyle, ...style } }>
      <i className='far fa-clock' />
      { twoDigits(minutesLeft) + ':' + twoDigits(secondsLeft) }
    </Time>
  );
};

export default timer(1000)(Timer);
