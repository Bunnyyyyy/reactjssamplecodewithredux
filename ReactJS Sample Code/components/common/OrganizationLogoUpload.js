import React, { Component } from 'react';
import Dropzone from 'react-dropzone';
import ImageLoader from 'react-image-file';
import styled from 'styled-components';

import Modal from './ModalV2';
import Label from './inputs/Label';
import ImageCroppie from './ImageCroppie';

import defaultOrgImage from '../../assets/manager/default-organization-icon.svg';

require('formdata-polyfill');

const Container = styled.div`
  border-radius: 5px;
  border: 1px solid ${ props => props.theme.colors.lightGray };
  width: 100%;
  height: 178px;
  padding: 20px 30px 20px 30px;
`;

const ContentWrapper = styled.div`
  display: flex;
  margin-top: 20px;
`;

const UploaderWrapper = styled.div`
  margin-left: 30px;
  display: flex;
  flex-direction: column;
  justify-content: center;
  font-size: 12px;
`;

const DefaultOrg = styled.img`
  width: 100px;
  height: 100px;
  border-radius: 50px;
  border: 1px solid ${ props => props.theme.colors.lightGray };
`;

const DropzoneWrapper = styled.div`
  cursor: pointer;
  width: 84px;
  height: 30px;
  border-radius: 42px;
  border: 1px solid ${ props => props.theme.colors.mainBlue };
  color: ${ props => props.theme.colors.mainBlue };
  justify-content: center;
  align-items: center;
  display: flex;
  font-weight: bold;
  font-size: 12px;
  margin-top: 10px;
  > div {
    display: flex;
    justify-content: center;
    align-items: center;
  }
  :hover {
    border: 1px solid ${ props => props.theme.colors.cerulean };
    color: ${ props => props.theme.colors.cerulean };
  }
`;

const ImageLoaderWrapper = styled.div`
  width: 100px;
  height: 100px;
  border-radius: 50px;
  border: 1px solid  ${ props => props.theme.colors.lightGray };
`;

const ModalDiv = styled(Modal)`
  overflow: hidden;

  & > div:nth-child(1) {
    background-color: white;
    padding-top: 15px;
    padding-bottom: 15px;
    margin-bottom: 0;
  }

  & > div:nth-child(1) > button {
    font-size: 26px;
    font-weight: normal;
    top: 6px;
  }

  & > div:nth-child(1) > h4 {
    font-size: 15px;
    text-align: left;
  }

  & > div:nth-child(2) {
    padding-top: 0;
    padding-left: 0;
    padding-right: 0;
    overflow: hidden;
  }
`;

export default class OrganizationLogoUpload extends Component {
  constructor () {
    super();

    this.state = {
      isCroppieModalOpen: false,
      previewImageFile: null,
      uploadedImageFile: null,
    };
  }

  // hack to enable its parent to make the ImageUpload component reset
  componentWillReceiveProps (nextProps) {
    const { reset } = this.props;
    if (nextProps.reset === true && reset !== true) {
      this.discardImage();
    }
  }

  onDrop (acceptedFiles) {
    const previewImageFile = acceptedFiles[0];
    this.setState({
      isCroppieModalOpen: true,
      previewImageFile,
    });
  }

  onCroppieSave (field, blob) {
    const { previewImageFile } = this.state;
    const uploadedImageFile = new File(
      [blob],
      previewImageFile.name,
      {
        type: 'image/png',
      },
    );

    this.setState({
      uploadedImageFile,
      isCroppieModalOpen: false,
    });

    const { onChange } = this.props;
    onChange(field, uploadedImageFile);
  }

  onCroppieClose () {
    this.setState({ isCroppieModalOpen: false });
  }

  editImage () {
    this.setState({
      isCroppieModalOpen: true,
    });
  }

  discardImage () {
    this.setState({
      previewImageFile: null,
      uploadedImageFile: null,
    });
  }

  render () {
    const {
      label,
      modaltitle = 'Crop image',
      field,
      width = 150,
      height = 150,
      viewportType = 'square',
      enableResize,
      required,
      subLabel,
    } = this.props;
    const {
      isCroppieModalOpen,
      previewImageFile,
      uploadedImageFile,
    } = this.state;

    const dropzoneStyle = {
      width: '100%',
      height: '100%',
      cursor: 'pointer',
    };
    const ImageDropzone = (
      <DropzoneWrapper circle={ viewportType === 'circle' }>
        <Dropzone
          style={ dropzoneStyle }
          multiple={ false }
          onDrop={ this.onDrop.bind(this) }
        >
          Browse
        </Dropzone>
      </DropzoneWrapper>
    );

    return (
      <Container>
        { label ? (
          <Label
            field={ field }
            label={ label }
            required={ required }
            subLabel={ subLabel }
          />
        ) : null }
        <ContentWrapper>
          { uploadedImageFile ? (
            <ImageLoaderWrapper>
              <ImageLoader file={ uploadedImageFile } height={ 100 } />
            </ImageLoaderWrapper>
          ) : (
            <DefaultOrg src={ defaultOrgImage } />
          ) }
          <UploaderWrapper>
            Upload new organization logo or image
            { ImageDropzone }
          </UploaderWrapper>
        </ContentWrapper>
        <ModalDiv
          large
          isOpen={ isCroppieModalOpen }
          onClose={ this.onCroppieClose.bind(this) }
          title={ modaltitle }
          customCss='
                width: 80vw;
                height: 80vh;
              '
        >
          <ImageCroppie
            field={ field }
            viewportWidth={ width }
            viewportHeight={ height }
            viewportType={ viewportType }
            enableResize={ enableResize }
            previewImageFile={ previewImageFile }
            onCroppieSave={ this.onCroppieSave.bind(this) }
            onClose={ this.onCroppieClose.bind(this) }
          />
        </ModalDiv>
      </Container>
    );
  }
}
