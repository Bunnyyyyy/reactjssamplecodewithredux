import React from 'react';
import styled from 'styled-components';
import Card from './Card';

const CardWrapper = styled.div`
  padding: 0 30px;
`;

const ITEM_PER_ROW = 3;

const NumberDiv = styled.div`
  flex-grow: 1;
  border-right: 1px solid ${ props => props.theme.colors.lightGray };
  min-height: 55px;
  margin: 30px 0px;
  padding: 0px 0px 0px 30px;
  min-width: ${ Math.floor(10000 / ITEM_PER_ROW) / 100 }%;
  &:last-child {
    border-right: none;
  }

  &:first-child {
    padding-left: 0px;
  }
`;

const NumberTitle = styled.h6`
  font-weight: 400;
  margin-bottom: 10px;
`;

const NumberContent = styled.div`
  display: flex;
  align-items: center;
`;

const NumberAmount = styled.p`
  font-size: 20px;
  font-weight: 700;
  line-height: 1.25;
`;

const NumberRow = styled.div`
  display: flex;
  border-bottom: 1px solid ${ props => props.theme.colors.lightGray };

  &:last-child {
    border-bottom: none;
  }
`;

const AdditionComponent = styled.span`
  margin-left: 10px;

  > p {
    font-size: 12px;
  }
`;

const NumbersCard = ({ title, items = [], ...props }) => {
  const Number = ({ item = {} }) => {
    const {
      number,
      format = v => (v !== undefined ? v.toLocaleString() : '-'),
      additionComponent,
      renderComponent,
      type,
    } = item;

    return (
      <React.Fragment>
        { type === 'custom' ? <NumberDiv>{ renderComponent() }</NumberDiv> : (
          <NumberDiv>
            <NumberTitle>{ item.title }</NumberTitle>
            <NumberContent>
              <NumberAmount>{ format(number) }</NumberAmount>
              { additionComponent !== undefined ? (
                <AdditionComponent>{ additionComponent }</AdditionComponent>
              ) : null }
            </NumberContent>
          </NumberDiv>
        ) }
      </React.Fragment>
    );
  };

  const rowedItems = [];
  for (let i = 0; i < items.length; i += ITEM_PER_ROW) {
    const newRow = items.slice(i, i + ITEM_PER_ROW);
    rowedItems.push(newRow);
  }
  const Rows = rowedItems.map((ritems = [], i) => (
    <NumberRow key={ `number-row-${ i }` }>
      { ritems.map((item, j) => (
        <Number key={ `number-row-item-${ i }-${ j }` } item={ item } />
      )) }
    </NumberRow>
  ));

  return (
    <Card title={ title } { ...props }>
      <CardWrapper>{ Rows }</CardWrapper>
    </Card>
  );
};

export default NumbersCard;
