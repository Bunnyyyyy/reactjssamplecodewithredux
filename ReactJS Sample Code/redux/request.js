import axios from 'axios';

const { API_URL } = process.env;
const SET_REQUEST = 'SET_REQUEST';
const defaultState = null;

export default function reducer (state = defaultState, action) {
  switch (action.type) {
    case SET_REQUEST:
      return action.payload;

    default:
      return state;
  }
}

export function createRequest (cookie) {
  return dispatch => {
    const instance = axios.create({
      baseURL: API_URL
    });

    if (cookie) {
      // workaround for lack of deep cloning in axios defaults
      const defaultHeaders = JSON.parse(JSON.stringify(axios.defaults.headers));
      defaultHeaders.cookie = cookie;
      instance.defaults.headers = defaultHeaders;
    }

    dispatch(setRequest(instance));
  };
}

export function setRequest (axiosInstance) {
  return {
    type: SET_REQUEST,
    payload: axiosInstance
  };
}
