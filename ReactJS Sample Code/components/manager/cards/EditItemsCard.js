import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import styled from 'styled-components';

import * as ActionCreators from '../../../redux/manager';

import Card from './Card';
import Form from '../../common/inputs/Form';
import Button from '../../common/Button';

const Row = styled.div`
  display: flex;
`;

const CardWrapper = styled.div`
  margin-bottom: 40px;
`;

const RowWrapper = styled.div`
  display: flex;
  padding-bottom: 20px;
`;

const ItemWrapper = styled.div`
  display: flex;
  flex-grow: 1;
  flex-direction: column;
`;

const NewItemsWrapper = styled.div`
  margin-top: 20px;
`;

const ActionButton = styled(Button)`
  width: 40px;
  height: 32px;
  margin: 38px 0 0 15px;
  flex-shrink: 0;
  opacity: 0.25;

  & i {
    margin-right: 0;
  }

  &:hover,
  &:active {
    opacity: 1;
    color: ${ props => props.activeColor || props.theme.colors.mainBlue };
  }

  ${ props => (props.alwaysActive
    ? `
    opacity: 1;
    color: ${ props.activeColor || props.theme.colors.mainBlue };
  `
    : '') };
`;

const ActionButtonsContainer = styled.div`
  display: flex;
  flex-direction: column;
`;

const AddButton = styled(Button)`
  width: 25px;
  height: 25px;
  border-radius: 12.5px;
  line-height: 25px;
  background-color: ${ props => props.theme.colors.darkBlue };
  color: white;

  &:hover,
  &:active {
    background-color: white;
    color: ${ props => props.theme.colors.mainBlue };
    box-shadow: 0 0 6px 0 rgba(0, 0, 0, 0.15);
  }
`;

const ItemNumber = styled.div`
  width: 25px;
  height: 25px;
  border: 2px solid ${ props => props.theme.colors.mainBlue };
  border-radius: 50%;
  display: flex;
  justify-content: center;
  align-items: center;
  color: rgb(0, 167, 255);
  font-weight: bold;
  margin: 22px 10px 0 0;
  font-size: 12px;
`;

const ItemNumberDiv = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  margin: 20px 0;
`;

const mapStateToProps = state => ({
  saveActions: state.manager.saveActions,
});

const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators(ActionCreators, dispatch),
});

class EditItemsCard extends Component {
  constructor () {
    super();

    this.state = {
      resetForms: false,
      resetItemForms: {},
      newItems: {},
      removeItems: {},
      expandItems: {},
    };
  }

  componentWillReceiveProps (nextProps) {
    const { name, saveActions } = this.props;
    if (
      saveActions !== nextProps.saveActions
      && !nextProps.saveActions[name]
    ) {
      this.resetForms();
    }
  }

  onAddItem (state, order) {
    try {
      const {
        actions, fields, name, createAction, actionOrder,
      } = this.props;
      if (!createAction) return;

      let isInvalid = false;
      fields
        .filter(f => (typeof f.readOnly === 'function'
          ? !f.readOnly(state)
          : !f.readOnly))
        .forEach((f) => {
          const {
            field, label, required, getErrorMessage,
          } = f;

          if (
            (getErrorMessage && getErrorMessage(state, state[field], label))
            || (required && state[field] === null)
          ) {
            isInvalid = true;
          }
        });

      const stateData = Object.assign({}, state);
      delete stateData._iceredIdAvailable;

      fields.forEach((f) => {
        const readOnly = typeof f.readOnly === 'function' ? f.readOnly(state) : f.readOnly;

        if (readOnly) delete stateData[f.field];
      });

      if (!isInvalid) {
        actions.addSaveAction(
          createAction.bind(this, { ...stateData, order }),
          name,
          // hack to keep track of different new items
          `new-${ order }`,
          actionOrder,
        );
      } else {
        actions.addSaveAction(
          null,
          name,
          // hack to keep track of different new items
          `new-${ order }`,
          actionOrder,
        );
      }
    } catch (e) {
      throw e;
    }
  }

  onUpdateItem (state, item) {
    try {
      const {
        actions, fields, name, updateAction, actionOrder,
      } = this.props;
      if (!updateAction) return;

      let isInvalid = false;
      fields
        .filter(f => (typeof f.readOnly === 'function'
          ? !f.readOnly(state, item)
          : !f.readOnly))
        .forEach((f) => {
          const {
            field, label, required, getErrorMessage,
          } = f;

          if (
            (getErrorMessage && getErrorMessage(state, state[field], label))
            || (required && state[field] === null)
          ) {
            isInvalid = true;
          }
        });

      const stateData = Object.assign({}, state);
      delete stateData._iceredIdAvailable;

      fields.forEach((f) => {
        const readOnly = typeof f.readOnly === 'function'
          ? f.readOnly(state, item)
          : f.readOnly;

        if (readOnly) delete stateData[f.field];
      });

      if (!isInvalid) {
        actions.addSaveAction(
          updateAction.bind(this, stateData, item),
          name,
          item.id,
          actionOrder,
        );
      } else {
        actions.addSaveAction(null, name, item.id, actionOrder);
      }
    } catch (e) {
      throw e;
    }
  }

  onRemoveItem (item) {
    try {
      const {
        actions, name, removeAction, actionOrder,
      } = this.props;
      const { removeItems } = this.state;
      if (removeAction) {
        actions.addSaveAction(
          removeAction.bind(this, item),
          name,
          item.id,
          actionOrder,
        );

        removeItems[item.id] = true;
        this.setState({ removeItems });
        this.resetItemForm(item.id);
      }
    } catch (e) {
      throw e;
    }
  }

  onToggleExpandItem (item) {
    try {
      const { expandItems } = this.state;
      expandItems[item.id] = !(expandItems && expandItems[item.id]);
      this.setState({ expandItems });
    } catch (e) {
      console.error(e);
    }
  }

  // hack to make the Forms reset
  resetForms () {
    this.setState(
      {
        resetForms: true,
        resetItemForms: {},
        newItems: {},
        removeItems: {},
        expandItems: {},
      },
      () => {
        this.setState({ resetForms: false });
      },
    );
  }

  resetItemForm (iId) {
    const { resetItemForms } = this.state;
    resetItemForms[iId] = true;

    this.setState({ resetItemForms }, () => {
      resetItemForms[iId] = false;
      this.setState({ resetItemForms });
    });
  }

  addNewItem () {
    const { items } = this.props;
    const { newItems } = this.state;

    // eslint-disable-next-line
    const itemOrders = items.filter(i => !isNaN(i.order)).map(i => i.order);
    const newItemOrders = Object.keys(newItems).map(o => Number(o));

    const newItemOrder = itemOrders.length || newItemOrders.length
      ? Math.max(...itemOrders, ...newItemOrders) + 1
      : 0;

    newItems[newItemOrder] = true;
    this.setState({ newItems });
  }

  removeNewItem (order) {
    try {
      const { actions, name } = this.props;
      const { newItems } = this.state;

      actions.removeSaveAction(name, `new-${ order }`);
      delete newItems[order];

      this.setState({ newItems });
    } catch (e) {
      throw e;
    }
  }

  undoRemoveItem (item) {
    try {
      const { actions, name } = this.props;
      const { removeItems } = this.state;

      actions.removeSaveAction(name, item.id);

      delete removeItems[item.id];
      this.setState({ removeItems });
    } catch (e) {
      throw e;
    }
  }

  render () {
    const {
      name,
      title,
      items = [],
      fields = [],
      itemActions = [],
      onUpdateForm = () => {},
      formCss = '',
      children,
    } = this.props;
    const {
      resetForms,
      resetItemForms,
      newItems,
      removeItems,
      expandItems,
    } = this.state;

    const ItemForms = items.map((i, index) => {
      const isExpanded = expandItems[i.id];

      let itemFields = fields.map(f => Object.assign({}, f, {
        default: f.default ? f.default(i) : null,
      }));

      if (!isExpanded) {
        itemFields = itemFields.filter(f => !f.collapsed);
      }

      const isCollapsible = fields.filter(f => f.collapsed).length;
      const isRemoving = !!removeItems[i.id];

      const onUpdate = async (state) => {
        this.onUpdateItem(state, i);
        await onUpdateForm(state);
      };

      return (
        <Row collapsible={ isCollapsible }>
          <ItemNumberDiv>
            <ItemNumber>{ index + 1 }</ItemNumber>
          </ItemNumberDiv>
          <ItemWrapper>
            <RowWrapper>
              <Form
                validate
                name={ `${ name }-item-${ i.id }` }
                key={ `${ name }-item-${ i.id }` }
                item={ i }
                reset={ resetForms || resetItemForms[i.id] }
                fields={ itemFields }
                onUpdate={ onUpdate }
                customCss={ `${ isRemoving ? 'opacity: .25;' : '' }${ formCss }` }
              />
              <ActionButtonsContainer>
                { isRemoving ? (
                  <ActionButton
                    plain
                    alwaysActive
                    activeColor='rgb(255, 108, 108)'
                    onClick={ this.undoRemoveItem.bind(this, i) }
                  >
                    <i className='fas fa-undo' aria-hidden='true' />
                  </ActionButton>
                ) : (
                  <Row collapsible={ isCollapsible }>
                    { isCollapsible ? (
                      <ActionButton
                        plain
                        activeColor='rgb(0, 167, 255)'
                        onClick={ this.onToggleExpandItem.bind(this, i) }
                      >
                        <i
                          className={ `fas fa-sort-amount-${
                            isExpanded ? 'up' : 'down'
                          }` }
                          aria-hidden='true'
                        />
                      </ActionButton>
                    ) : null }
                    <ActionButton
                      plain
                      activeColor='rgb(255, 108, 108)'
                      onClick={ this.onRemoveItem.bind(this, i) }
                    >
                      <i className='fas fa-trash' aria-hidden='true' />
                    </ActionButton>
                  </Row>
                ) }
                { itemActions
                  .filter(a => (a.condition ? a.condition(i) : true))
                  .map(({
                    icon, activeColor, onClick,
                  }) => (
                    <ActionButton
                      plain
                      alwaysActive
                      activeColor={ activeColor }
                      onClick={ onClick.bind(this, i) }
                    >
                      <i className={ `fas ${ icon }` } aria-hidden='true' />
                    </ActionButton>
                  )) }
              </ActionButtonsContainer>
            </RowWrapper>
          </ItemWrapper>
        </Row>
      );
    });

    const NewItemForms = [];
    let newItemIndex = ItemForms.length;
    for (const order in newItems) {
      const itemFields = fields.map(f => Object.assign({}, f, {
        default: f.default ? f.default({}) : null,
      }));

      const onUpdate = async (state) => {
        this.onAddItem(state, order);
        await onUpdateForm(state);
      };

      newItemIndex++;

      NewItemForms.push(
        <Row>
          <ItemNumberDiv>
            <ItemNumber>{ newItemIndex }</ItemNumber>
          </ItemNumberDiv>
          <ItemWrapper>
            <RowWrapper>
              <Form
                name={ `${ name }-new-item-${ order }` }
                key={ `${ name }-new-item-${ order }` }
                item={ {} }
                fields={ itemFields }
                onUpdate={ onUpdate }
                customCss={ formCss }
              />
              <ActionButton
                plain
                onClick={ this.removeNewItem.bind(this, order) }
              >
                <i className='fas fa-trash' aria-hidden='true' />
              </ActionButton>
            </RowWrapper>
          </ItemWrapper>
        </Row>,
      );
    }

    return (
      <CardWrapper>
        <Card edit title={ title } customCss='width: 100%;'>
          { children }
          <div>{ ItemForms }</div>
          <NewItemsWrapper>
            { NewItemForms.length ? NewItemForms : null }
            <AddButton plain onClick={ this.addNewItem.bind(this) }>
              +
            </AddButton>
          </NewItemsWrapper>
        </Card>
      </CardWrapper>
    );
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(EditItemsCard);
