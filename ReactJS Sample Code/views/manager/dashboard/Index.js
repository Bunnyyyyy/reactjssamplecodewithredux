import React from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router';
import { Route, Switch } from 'react-router-dom';
import { Helmet } from 'react-helmet';

import Header from '../../../components/header/Header';
import HomeNavBar from '../../../components/manager/navigation/HomeNavBar';

import DashboardHomeView from './Home';
import DashboardContactsView from './Contacts';
import DashboardIncomeView from './Income';

import { t as translate } from '../../../../i18n';

const t = a => translate(a, 'manager/home');

const mapStateToProps = state => ({
  organization: state.manager.organization,
});

const DashboardIndexView = ({ match, organization }) => {
  const { url } = match;

  const navPages = [
    {
      title: t('Dashboard'),
    },
  ];
  if (url.indexOf('/income') > 0) {
    navPages[0].url = `/${ organization.id }`;
    navPages.push({
      title: t('Income'),
    });
  } else if (url.indexOf('/contacts') > 0) {
    navPages[0].url = `/${ organization.id }`;
    navPages.push({
      title: t('Contacts'),
    });
  }

  const pageTitle = navPages
    .map(p => p.title)
    .reverse()
    .concat('Juven Manager')
    .join(' - ');

  return (
    <div>
      <Helmet>
        <title>{ pageTitle }</title>
        <meta property='og:title' content={ pageTitle } />
      </Helmet>
      <Header navPages={ navPages } />
      <HomeNavBar organization={ organization } />
      <Switch>
        <Route exact path='/:oId' component={ DashboardHomeView } />
        <Route exact path='/:oId/home/contacts' component={ DashboardContactsView } />
        <Route exact path='/:oId/home/income' component={ DashboardIncomeView } />
      </Switch>
    </div>
  );
};

export default withRouter(connect(mapStateToProps)(DashboardIndexView));
