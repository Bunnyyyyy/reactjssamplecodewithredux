import React from 'react';
import styled from 'styled-components';
import Select, { components } from 'react-select';
import Color from 'color';

import Label from './inputs/Label';
import InvalidMessage from './InvalidMessage';
import { nowrap } from '../../utils/styles';

const Wrapper = styled.div`
  position: relative;
  display: inline-block;
  width: ${ props => (props.fullWidth ? '100%' : '400px') };
  margin-top: 20px;
  margin-bottom: 20px;

  &__single-value {
    position: absolute;
    top: 50%;
    max-width: calc(100% - 8px);
    -webkit-transform: translateY(-50%);
    -ms-transform: translateY(-50%);
    transform: translateY(-50%);
    font-size: 15px;
    line-height: 1.4;
    font-weight: 400;
    margin: 0;
    ${ nowrap };
  }

  ${ props => props.customCss || '' };
`;

const InnerWrapper = styled.div`
  .control__control,
  .control__control:focus,
  .control__control--is-focused {
    min-height: 32px;
    background-color: transparent;
    border: none;
    border-bottom: 1px solid rgba(47, 46, 46, 0.25);
    border-radius: 0px;
    box-shadow: none;
    padding: 0px;
  }

  .control__control--is-focused {
    border-color: ${ props => props.theme.colors.mainBlue } !important;
  }

  .control__control .control__value-container {
    padding: 0px;
    max-width: calc(100% - 10px);
    display: flex;

    > div {
      max-width: 100%;
      height: 26px;
      margin-top: 0;
      margin-bottom: 2px;
      padding: 5px;

      > div:first-child {
        padding-left: 5px;
        padding-right: 5px;
        ${ nowrap };
      }
    }
  }

  .control__control .control__multi-value__remove {
    color: #6b8dad;
    background: none;
  }

  .control__input {
    height: 32px;
    padding: 0 !important;
  }

  .control__control input {
    font-size: 12px !important;
    min-width: 50px !important;
    caret-color: ${ ({ theme }) => theme.colors.gray };
  }
`;

const EmptyComponent = () => null;

const MultiValueContainer = styled.div`
  background-color: #f2f6fa;
  display: flex;
  padding: 5px 15px;
  border-radius: 5px;
  margin: 0px 5px;
  border: 1px solid #adceef;
  cursor: pointer;
`;

const MultiValueLabel = styled.div`
  font-size: 13px;
  padding-left: 15px;
  color: ${ ({ theme }) => Color(theme.lightGray)
    .fade(0.3)
    .string() };
`;

export default ({
  underline,
  options = [],
  placeholder,
  value = [],
  fullWidth,
  field,
  label,
  customCss,
  errorMessage,
  required,
  onChange,
  ...rest
}) => {
  const defaultValue = value
    .map((v) => {
      const option = options.find(o => o.value === v);
      return option ? { label: option.label, value: v } : null;
    })
    .filter(v => v !== null);

  const onChangeSelect = (selectedOptions = []) => {
    if (onChange) {
      onChange(selectedOptions.map(o => o.value));
    }
  };

  const Input = props => (
    <components.Input
      { ...props }
      placeholder={ placeholder }
      isHidden={ false }
      style={ {
        fontSize: 12,
        minWidth: 50,
        padding: value.length > 0 ? '5px 0' : '',
        paddingLeft: value.length > 0 ? '5px' : '',
      } }
    />
  );

  return (
    <Wrapper customCss={ customCss } fullWidth={ fullWidth }>
      { label ? (
        <Label
          field={ field }
          label={ label }
          invalid={ !!errorMessage }
          required={ required }
        />
      ) : null }
      <InnerWrapper underline={ underline }>
        <Select
          classNamePrefix='control'
          isMulti
          name='colors'
          options={ options }
          placeholder=''
          defaultValue={ defaultValue }
          isClearable={ false }
          components={ {
            DropdownIndicator: EmptyComponent,
            IndicatorSeparator: EmptyComponent,
            Input,
            MultiValueContainer,
            MultiValueLabel,
          } }
          onChange={ onChangeSelect.bind(this) }
          { ...rest }
        />
      </InnerWrapper>
      { errorMessage ? <InvalidMessage>{ errorMessage }</InvalidMessage> : null }
    </Wrapper>
  );
};
