import React, { Component } from 'react';
import ReactCrop, { makeAspectCrop } from 'react-image-crop';
import Dropzone from 'react-dropzone';

import Button from './Button';
import i18n from '../../../i18n';

require('formdata-polyfill');

const t = a => i18n.t([`components/common:${ a }`]);

export default class ImageUpload extends Component {
  constructor () {
    super();

    this.state = {
      isUpdatingImage: false,
      previewImageFile: null,
      croppedImage: null,
      uploadedImageFile: null,
      crop: {},
      error: null,
    };
  }

  onDrop (acceptedFiles) {
    const previewImageFile = acceptedFiles[0];
    this.setState({ previewImageFile });
  }

  onChange (crop) {
    this.setState({
      crop,
      error: null,
    });
  }

  onImageLoaded (image) {
    const { aspect } = this.props;
    const originalAspect = image.naturalWidth / image.naturalHeight;

    const cropConfig = {
      x: 0,
      y: 0,
    };

    if (aspect) {
      cropConfig.aspect = aspect;
      if (originalAspect > aspect) {
        cropConfig.height = 100;
      } else {
        cropConfig.width = 100;
      }
    } else {
      cropConfig.width = 100;
      cropConfig.height = 100;
    }

    const crop = aspect
      ? makeAspectCrop(cropConfig, originalAspect)
      : cropConfig;

    this.setState(
      {
        croppedImage: image,
        crop,
      },
      () => this.updateImage(image, crop),
    );
  }

  onComplete (crop) {
    const { croppedImage } = this.state;
    this.updateImage(croppedImage, crop);
  }

  toggleUpdatingImage () {
    const { isUpdatingImage } = this.state;

    this.setState({
      isUpdatingImage: !isUpdatingImage,
    });
  }

  updateImage (image, crop) {
    const { inModal, onUpdate } = this.props;
    const { previewImageFile } = this.state;

    const width = image.naturalWidth;
    const height = image.naturalHeight;

    const sx = crop.x / 100 * width;
    const sy = crop.y / 100 * height;
    const sWidth = crop.width / 100 * width;
    const sHeight = crop.height / 100 * height;

    const canvas = document.createElement('canvas');
    canvas.width = sWidth;
    canvas.height = sHeight;

    const ctx = canvas.getContext('2d');
    ctx.drawImage(image, sx, sy, sWidth, sHeight, 0, 0, sWidth, sHeight);

    canvas.toBlob(
      (blob) => {
        const uploadedImageFile = new File([blob], previewImageFile.name, {
          type: 'image/jpeg',
        });

        this.setState({ uploadedImageFile });
        if (inModal && onUpdate) onUpdate(uploadedImageFile);
      },
      'image/jpeg',
      1,
    );
  }

  async saveImage () {
    const { onSave } = this.props;
    const { uploadedImageFile } = this.state;

    try {
      this.setState({ error: null });
      await onSave(uploadedImageFile);
      this.discardImage();
    } catch (e) {
      const error = e.message;
      return this.setState({ error });
    }
  }

  discardImage () {
    const { inModal, onUpdate } = this.props;

    this.setState({
      isUpdatingImage: false,
      previewImageFile: null,
      croppedImage: null,
      uploadedImageFile: null,
      error: null,
    });

    if (inModal && onUpdate) onUpdate(null);
  }

  render () {
    const {
      image, label, width = 600, height = 400, inModal,
    } = this.props;
    const {
      isUpdatingImage, previewImageFile, crop, error,
    } = this.state;

    const imageStyle = {
      maxWidth: '100%',
      marginTop: 10,
    };

    const dropzoneStyle = {
      maxWidth: '100%',
      width,
      height,
      borderRadius: 2,
      margin: '10px auto 0 auto',
      backgroundColor: 'rgb(221, 221, 221)',
    };

    const dropzoneDivStyle = {
      position: 'relative',
      textAlign: 'center',
      top: '50%',
      transform: 'translateY(-50%)',
      padding: 20,
    };

    const dropzonePStyle = {
      fontSize: 16,
      color: 'rgb(47, 46, 46)',
      fontWeight: 600,
      lineHeight: '36px',
    };

    const ImageDropzone = (
      <Dropzone
        style={ dropzoneStyle }
        multiple={ false }
        onDrop={ this.onDrop.bind(this) }
      >
        <div style={ dropzoneDivStyle }>
          <p
            style={ {
              ...dropzonePStyle,
              textAlign: 'center',
              lineHeight: '18px',
              marginBottom: 10,
            } }
          >
            { t('Please drag your image here or click here to select one.') }
          </p>
          <Button underline>{ t('Select') }</Button>
        </div>
      </Dropzone>
    );

    const SaveButton = (
      <Button underline onClick={ this.saveImage.bind(this) }>
        { t('Save') }
      </Button>
    );

    const cropMessage = inModal
      ? t('Crop your image as you like.')
      : t('Please crop and save your new image.');

    const ImageCrop = (
      <div style={ { maxWidth: '100%' } }>
        <div style={ { margin: '10px 0' } }>
          <p style={ { ...dropzonePStyle, display: 'inline-block' } }>
            { cropMessage }
          </p>
          <div style={ { display: 'inline-block', float: 'right' } }>
            { !inModal ? SaveButton : null }
            <Button underline gray onClick={ this.discardImage.bind(this) }>
              { t('Cancel') }
            </Button>
          </div>
        </div>
        <div style={ { maxWidth: width, margin: '0 auto' } }>
          <ReactCrop
            src={ previewImageFile ? previewImageFile.preview : '' }
            crop={ crop }
            onChange={ this.onChange.bind(this) }
            onImageLoaded={ this.onImageLoaded.bind(this) }
            onComplete={ this.onComplete.bind(this) }
          />
        </div>
      </div>
    );

    const UpdateImageAction = (
      <Button
        underline
        gray={ isUpdatingImage }
        onClick={
          isUpdatingImage
            ? this.discardImage.bind(this)
            : this.toggleUpdatingImage.bind(this)
        }
      >
        { isUpdatingImage ? t('Cancel') : t('Update') }
      </Button>
    );

    const errorStyle = {
      position: 'absolute',
      right: 20,
      top: 60,
      fontSize: 12,
      lineHeight: '12px',
      color: 'rgb(255, 108, 108)',
    };

    const divStyle = {
      margin: '10px auto 20px auto',
      width,
    };

    return (
      <div style={ divStyle }>
        <div>
          <p
            style={ {
              display: 'inline-block',
              fontSize: 12,
              textTransform: 'capitalize',
              lineHeight: '36px',
              fontWeight: 600,
            } }
          >
            { label }
          </p>
          { image && !previewImageFile ? UpdateImageAction : null }
        </div>
        <p style={ errorStyle }>{ error }</p>

        { image && !isUpdatingImage ? (
          <img alt='' style={ imageStyle } src={ image } />
        ) : null }
        { previewImageFile
          ? ImageCrop
          : !image || isUpdatingImage ? ImageDropzone : null }
      </div>
    );
  }
}
