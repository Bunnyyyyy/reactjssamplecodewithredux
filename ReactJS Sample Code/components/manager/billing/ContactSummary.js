import React from 'react';
import { connect } from 'react-redux';
import styled from 'styled-components';

import i18n from '../../../../i18n';

const t = a => i18n.t([`translations:${ a }`]);

const Wrapper = styled.div`
  margin-left: 30px;
`;

const SummaryItem = styled.div`
  display: flex;
  width: 232px;
  height: 22px;
  padding: 0 10px;
  position: relative;
  align-items: center;
  font-size: 12px;
  color: #7d909a;
  > p {
    font-size: 12px;
    color: #7d909a;
    min-width: 120px;
  }
  > span {
    // position: absolute;
    // right: 85px;
  }

  background-color: white;
  &:nth-child(2n) {
    background-color: ${ props => props.theme.colors.bgGray };
  }

  ${ props => props.customCss || '' };
`;

const mapStateToProps = state => ({
  organization: state.manager.organization,
});

const ContactSummary = ({ organization }) => {
  const purchasedLimit = organization.billing_contact_count
    ? organization.billing_contact_count - 100
    : null;
  return (
    <Wrapper>
      <SummaryItem>
        <p>{ t('Free') }</p>
        <span>100</span>
      </SummaryItem>
      <SummaryItem>
        <p>{ t('Trial') }</p>
        <span>400</span>
      </SummaryItem>
      { purchasedLimit ? (
        <SummaryItem>
          <p>{ t('Purchased') }</p>
          <span>{ purchasedLimit.toLocaleString() }</span>
        </SummaryItem>
      ) : null }
      { organization.custom_contact_limit ? (
        <SummaryItem>
          <p>{ t('Custom') }</p>
          <span>{ organization.custom_contact_limit.toLocaleString() }</span>
        </SummaryItem>
      ) : null }
    </Wrapper>
  );
};

export default connect(mapStateToProps)(ContactSummary);
