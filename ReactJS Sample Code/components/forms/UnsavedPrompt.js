import React, { Component } from 'react';
import { Prompt } from 'react-router';

import { t as translate } from '../../../i18n';

const t = a => translate(a, 'components/forms/UnsavedPrompt');

export default class UnsavedPrompt extends Component {
  constructor () {
    super();

    const onBeforeUnload = (e) => {
      e.preventDefault();
      e.returnValue = '';
    };
    this.onBeforeUnloadHandler = onBeforeUnload.bind(this);
  }

  componentDidMount () {
    window.addEventListener('beforeunload', this.onBeforeUnloadHandler);
  }

  componentWillUnmount () {
    window.removeEventListener('beforeunload', this.onBeforeUnloadHandler);
  }

  render () {
    const { formikProps = {} } = this.props;
    return (
      <Prompt
        when={ formikProps.dirty }
        message={ t('You have unsaved changes. If you navigate away from this page, your changes will be lost.') }
      />
    );
  }
}
