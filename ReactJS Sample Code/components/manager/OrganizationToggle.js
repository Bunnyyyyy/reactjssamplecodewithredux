import React, { Component } from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router';
import { Link } from 'react-router-dom';
import styled from 'styled-components';
import CircularImage from '../common/CircularImage';

import logoPlaceholder from '../../assets/common/placeholder_logo.svg';

const OrganizationDiv = styled.div`
  background-color: white;
  width: 180px;
  ${ props => (props.visible ? '' : 'display: none;') };
  &:active, &:focus{
    outline: none;
  }
`;

const OrganizationListContainer = styled.div`
`;

const OrganizationInfoWrapper = styled.div`
  position: relative;
  display: flex;
  width: 180px;
  padding: 10px 15px 10px 15px;
  :hover {
    background-color: ${ props => props.theme.colors.bgGray };
    cursor: pointer;
  }
`;

const OrganizationNameWrapper = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
  flex-grow: 1;
  position: relative;
  opacity: 1;
  order: 1;
  margin: 0 0 0 10px;
`;

const OrganizationName = styled.div`
  position: relative;

  & h5 {
    font-size: 12px;
  }
`;

const mapStateToProps = state => ({
  organizations: state.manager.organizations,
  organization: state.manager.organization,
});

class OrganizationToggle extends Component {
  onSelectOrganization () {
    const { onSelect } = this.props;
    if (onSelect !== undefined) onSelect();
    this.toggle.blur();
  }

  toggleUpdatingPlan () {
    let { isUpdatingPlan } = this.state;
    isUpdatingPlan = !isUpdatingPlan;

    const stateUpdate = { isUpdatingPlan };
    if (!isUpdatingPlan) stateUpdate.isAddingCard = false;

    this.setState(stateUpdate);
  }

  render () {
    const {
      organizations, show, refHandler, onBlur,
    } = this.props;
    if (!organizations.length) return <div />;

    const OrganizationList = organizations.map(o => (
      <Link
        key={ `organization-toggle-link-${ o.id }` }
        to={ `/${ o.id }` }
        onClick={ this.onSelectOrganization.bind(this) }
      >
        <OrganizationInfoWrapper>
          <CircularImage
            noPadding
            size={ 30 }
            src={ o.logo_image_url || logoPlaceholder }
          />
          <OrganizationNameWrapper>
            <OrganizationName>
              <h5>{ o.name }</h5>
            </OrganizationName>
          </OrganizationNameWrapper>
        </OrganizationInfoWrapper>
      </Link>
    ));

    return (
      <OrganizationDiv
        innerRef={ (toggle) => {
          this.toggle = toggle;
          refHandler(toggle);
        } }
        tabIndex='-1'
        onBlur={ onBlur }
        visible={ show }
      >
        <OrganizationListContainer>
          { OrganizationList }
        </OrganizationListContainer>
      </OrganizationDiv>
    );
  }
}

export default withRouter(connect(mapStateToProps)(OrganizationToggle));
