import styled from 'styled-components';

const Button = styled.button`
  height: 42px;
  line-height: 42px;
  font-size: 15px;
  font-weight: 700;
  border-radius: 42px;
  background-color: white;
  color: ${ props => (props.gray
    ? props.theme.colors.mainGray
    : props.red ? props.theme.colors.mainRed : props.theme.colors.mainBlue) };
  padding: 0 23px;
  margin: 10px 10px 10px 0;
  transition: all .25s ease;

  & i {
    margin-right: 10px;
  }

  &:last-child {
    margin-right: 0;
  }

  &:hover, &:active {
    box-shadow: 0 0 6px 0 rgba(0, 0, 0, 0.2);
    background-color: ${ props => (props.gray
    ? props.theme.colors.mainGray
    : props.red ? props.theme.colors.mainRed : props.theme.colors.mainBlue) };
    color: white;
  }

  &:disabled {
    cursor: default;
    box-shadow: none;
    background-color: ${ props => props.theme.colors.lightGray };
    color: white;

    &:hover, &:active {
      box-shadow: none;
      background-color: ${ props => props.theme.colors.lightGray };
      color: white;
    }
  }

  ${ props => (props.small
    ? `
      min-width: 90px;
      height: 30px;
      line-height: 30px;
      font-size: 12px;
  `
    : '') }

  ${ props => (props.light
    ? `
    background-color: ${
    props.gray
      ? props.theme.colors.lightGray
      : props.red ? props.theme.colors.mainRed : props.theme.colors.mainBlue
    };
    color: white;
    ${ props.gray ? 'box-shadow: none;' : '' }

    &:hover, &:active {
      box-shadow: 0 0 6px 0 rgba(0, 0, 0, 0.2);
      color: ${
    props.gray
      ? props.theme.colors.mainGray
      : props.red ? props.theme.colors.mainRed : 'white'
    };
      background-color: ${ props.theme.colors.cerulean };
    }
  `
    : '') }

  ${ props => (props.fullHeight || props.clipboard
    ? `
    background-color: ${ props.theme.colors.mainBlue };
    color: white;
    margin: 0;

    &:hover, &:active {
      background-color: ${ props.theme.colors.darkBlue };
    }
  `
    : '') }

  ${ props => (props.fullWidth
    ? `
    display: block;
    width: 100%;
    margin: 20px 0;
  `
    : '') }

  ${ props => (props.fullHeight
    ? `
    height: 100%;
    line-height: 100%;
    border-radius: 0;
    padding: 0 40px;

    @media (max-width: 900px) {
      height: 100%;
    }
  `
    : '') }

  ${ props => (props.clipboard
    ? `
    box-shadow: 0 0 6px 0 rgba(0, 0, 0, 0.28);
    height: 44px;
    line-height: 44px;
    padding: 0 16px;
    border-radius: 0 4px 4px 0;
  `
    : '') }

  ${ props => (props.underline
    ? `
    position: relative;
    height: 30px;
    font-size: 20px;
    line-height: 25px;
    background-color: transparent;
    color: ${ props.theme.colors.darkGray };
    border-radius: 0;
    box-shadow: none;
    padding: 0;
    border-bottom: 5px solid ${
    props.gray ? props.theme.colors.mainGray : props.theme.colors.mainBlue
    };

    &:hover, &:active {
      background-color: transparent;
      color: ${ props.theme.colors.darkGray };
      box-shadow: none;
    }
  `
    : '') }

  ${ props => (props.plain
    ? `
    height: 30px;
    font-size: 20px;
    line-height: 30px;
    background: transparent;
    padding: 0;
    box-shadow: none;
    color: ${ props.theme.colors.darkGray };

    &:hover, &:active {
      background: transparent;
      box-shadow: none;
      color: ${ props.theme.colors.darkGray };
    }

    &:disabled {
      background: transparent;
      color: ${ props.theme.colors.darkGray };
      opacity: .5;

      &:hover, &:active {
        box-shadow: none;
        background: transparent;
        color: ${ props.theme.colors.darkGray };
      }
    }
  `
    : '') }

  ${ props => (props.fullHeight && props.fullWidth
    ? `
    margin: 0px;
  `
    : '') }

  ${ props => props.customCss || '' }
`;

export default Button;
