import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { withRouter } from 'react-router';
import { Redirect } from 'react-router-dom';
import styled from 'styled-components';
import moment from 'moment';
import OtpInput from 'react-otp-input';

import * as UiActionCreators from '../../redux/ui';
import * as MeActionCreators from '../../redux/me';
import * as ManagerActionCreators from '../../redux/manager';

import Header from '../../components/header/Header';
import Button from '../../components/common/Button';
import { t as translate } from '../../../i18n';

import theme from '../../utils/theme';
import { boxShadow } from '../../utils/styles';

const t = a => translate(a, 'manager/verfiy');

const Container = styled.div`
  min-height: 100vh;
  background-color: white;
  display: flex;
  justify-content: center;
  align-items: center;
`;

const Wrapper = styled.div`
  position: relative;
`;

const Circle = styled.div`
  background-color: ${ props => props.backgroundColor };
  border-radius: 100%;
  display: inline-block;
  height: ${ props => props.size }px;
  opacity: 1;
  position: absolute;
  width: ${ props => props.size }px;
  ${ ({ bottom }) => bottom && `bottom: ${ bottom }px;` }
  ${ ({ left }) => left && `left: ${ left }px;` }
  ${ ({ right }) => right && `right: ${ right }px;` }
  ${ ({ top }) => top && `top: ${ top }px;` }

  @media (max-width: 900px) {
    display: none;
  }
`;

const Content = styled.div`
  position: relative;
  background-color: white;
  min-height: 435px;
  width: 430px;
  padding: 30px;
  border-radius: 10px;
  ${ boxShadow('0 3px 30px 0 rgba(0, 0, 0, .1)') }
`;

const Title = styled.h3`
  margin-bottom: 20px;
  text-align: center;
`;

const Message = styled.p`
  margin: 0 10px 30px 10px;
  text-align: center;
`;

const UserEmail = styled.span`
  font-weight: 700;
  color: ${ props => props.theme.colors.mainBlue };
`;

const ConfirmButton = styled(Button)`
  display: block;
  width: 100%;
  margin-right: 15px;
  background-color: ${ props => props.theme.colors.mainBlue } !important;
  color: white !important;

  &:disabled {
    background-color: ${ props => props.theme.colors.lightGray } !important;
  }
`;

const CodeActions = styled.div`
  min-height: 60px;
  margin: 30px;
  text-align: center;
`;

const CodeStatus = styled.p`
  ${ props => (props.expired ? `color: ${ props.theme.colors.mainRed } !important;` : '') };
`;

const ResendButton = Button.extend`
  text-transform: none;
  font-weight: 400;
  font-size: 15px;
  line-height: 21px;
  height: 21px;
  color: ${ props => props.theme.colors.mainBlue };

  &:active,
  &:hover {
    color: ${ props => props.theme.colors.darkBlue };
  }

  @media (max-width: 900px) {
    font-size: 13px;
    line-height: 19px;
    height: 19px;
  }
`;

const otpContainerStyle = {
  justifyContent: 'center',
};

const otpInputStyle = {
  textAlign: 'center',
  height: '3rem',
  width: '45px',
  fontSize: '2rem',
  margin: '10px',
  backgroundColor: 'white',
  border: 'none',
  borderBottom: '2px solid #7d909a',
};

const mapStateToProps = state => ({
  user: state.me.user,
});

const ActionCreators = Object.assign(
  {},
  UiActionCreators,
  MeActionCreators,
  ManagerActionCreators,
);
const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators(ActionCreators, dispatch),
});

const CODE_LENGTH = 6;

class VerifyView extends Component {
  constructor (props) {
    super();

    this.state = {
      remainingTime: this.getRemainingTime(props.user),
      code: '',
      error: null,
    };
  }

  componentDidMount () {
    this.timer = setInterval(this.setRemainingTime.bind(this), 1000);
  }

  componentWillUnmount () {
    clearInterval(this.timer);
  }

  onChangeInput (code) {
    this.setState({
      code,
      error: null,
    });
  }

  async onRequestCode () {
    const { actions } = this.props;

    try {
      await actions.requestVerificationCode();
      await actions.getMe();
    } catch (e) {
      console.error(e);
    }
  }

  async onConfirm () {
    const { actions, history } = this.props;
    const { code } = this.state;

    if (code.length !== CODE_LENGTH) return;
    try {
      await actions.verify({ code });

      await actions.getMe();
      await actions.getMyManagedOrganizations();

      actions.showNotification({
        type: 'success',
        text: t('Account verified'),
      });

      return history.push('/');
    } catch (e) {
      this.setState({ error: t('Your code is invalid.') });
    }
  }


  setRemainingTime () {
    const { user } = this.props;
    const remainingTime = this.getRemainingTime(user);
    this.setState({ remainingTime });
  }

  getRemainingTime (newUser) {
    // eslint-disable-next-line
    const user = newUser || this.props.user;

    const expiringSeconds = user.verification_code_expire_at
      ? moment.utc(user.verification_code_expire_at).diff(new Date(), 'second')
      : null;

    // return null if remaining time is negative as that means it is expired already
    if (expiringSeconds > 0) {
      let timeMinutes = Math.floor(expiringSeconds / 60);
      let timeSeconds = expiringSeconds % 60;

      if (timeMinutes === 0) {
        timeMinutes = '00';
      } else if (timeMinutes < 10) {
        timeMinutes = `0${ timeMinutes }`;
      }
      if (timeSeconds === 0) {
        timeSeconds = '00';
      } else if (timeSeconds < 10) {
        timeSeconds = `0${ timeSeconds }`;
      }

      return `${ timeMinutes }:${ timeSeconds }`;
    }
    return null;
  }


  render () {
    const { user } = this.props;
    if (!user.id || user.verified) return <Redirect to='/' />;

    const {
      remainingTime, code, error,
    } = this.state;

    const validCode = code.length === CODE_LENGTH;
    const RemainingTimeDisplay = remainingTime ? (
      <CodeStatus>
        { `${ t('Current code expiring in') }: ${ remainingTime }` }
      </CodeStatus>
    ) : (
      <CodeStatus expired>{ t('Your code expired already.') }</CodeStatus>
    );

    return (
      <Container>
        <Header fixed />
        <Wrapper>
          <Circle
            backgroundColor={ theme.colors.bgGray }
            left={ -81 }
            top={ -66 }
            size={ 158 }
          />
          <Circle
            backgroundColor={ theme.colors.lightGray }
            bottom={ -107 }
            right={ -148 }
            size={ 355 }
          />
          <Content>
            <Title>{ t('Verify your account') }</Title>
            <div>
              <Message>
                { `${ t('Enter the 6 digit verification code we sent to your email at') } ` }
                <UserEmail>{ user.email }</UserEmail>
                { '.' }
              </Message>
              <OtpInput
                onChange={ this.onChangeInput.bind(this) }
                numInputs={ CODE_LENGTH }
                containerStyle={ otpContainerStyle }
                inputStyle={ otpInputStyle }
              />

              <CodeActions>
                { error ? (
                  <CodeStatus expired>{ error }</CodeStatus>
                ) : (
                  RemainingTimeDisplay
                ) }
                <ResendButton plain onClick={ this.onRequestCode.bind(this) }>
                  { t('Request new code') }
                </ResendButton>
              </CodeActions>
              <ConfirmButton
                onClick={ this.onConfirm.bind(this) }
                disabled={ !validCode }
              >
                { t('Confirm') }
              </ConfirmButton>
            </div>
          </Content>
        </Wrapper>
      </Container>
    );
  }
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(VerifyView));
