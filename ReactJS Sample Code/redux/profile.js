import { handleApiError, getCustomStyles } from '../utils';
import { getRequest } from './index';

const SET_PROFILE = 'SET_PROFILE';
const SET_PROFILE_COVER_IMAGES = 'SET_PROFILE_COVER_IMAGES';
const SET_PROFILE_ABOUT_PARAGRAPH = 'SET_PROFILE_ABOUT_PARAGRAPH';
const SET_PROFILE_DESCRIPTION_IMAGES = 'SET_PROFILE_DESCRIPTION_IMAGES';
const SET_PROFILE_UPDATES = 'SET_PROFILE_UPDATES';
const SET_PROFILE_DATA_FIELDS = 'SET_PROFILE_DATA_FIELDS';
const RESET_PROFILE = 'RESET_PROFILE';
const SET_SPONSORSHIP_PACKAGES = 'SET_SPONSORSHIP_PACKAGES';
const SET_SPONSORSHIP_PDF = 'SET_SPONSORSHIP_PDF';
const SET_PLEDGES = 'SET_PLEDGES';

export default function reducer (
  state = {
    profile: {},
    coverImages: [],
    aboutParagraph: {},
    descriptionImages: [],
    updates: [],
    dataFields: [],
    pledges: [],
    packages: [],
    pdf: null,
  },
  action,
) {
  switch (action.type) {
    case SET_PROFILE:
      return {
        ...state,
        profile: {
          ...action.payload,
          customStyles: getCustomStyles(action.payload.primary_color),
        },
      };

    case SET_PROFILE_COVER_IMAGES:
      return { ...state, coverImages: action.payload };

    case SET_PROFILE_ABOUT_PARAGRAPH:
      return { ...state, aboutParagraph: action.payload };

    case SET_PROFILE_DESCRIPTION_IMAGES:
      return { ...state, descriptionImages: action.payload };

    case SET_PROFILE_UPDATES:
      return { ...state, updates: action.payload };

    case SET_PROFILE_DATA_FIELDS:
      return { ...state, dataFields: action.payload };

    case SET_SPONSORSHIP_PACKAGES:
      return { ...state, packages: action.payload };

    case SET_SPONSORSHIP_PDF:
      return { ...state, pdf: action.payload };

    case SET_PLEDGES:
      return {
        ...state,
        pledges: action.pledges,
      };

    case RESET_PROFILE:
      return {
        profile: {},
        coverImages: [],
        aboutParagraph: {},
        descriptionImages: [],
        updates: [],
        dataFields: [],
        pledges: [],
        packages: [],
        pdf: null,
        error: null,
      };

    default:
      return state;
  }
}

export function setProfile (profile) {
  return {
    type: SET_PROFILE,
    payload: profile,
  };
}

export function setProfileCoverImages (images) {
  return {
    type: SET_PROFILE_COVER_IMAGES,
    payload: images,
  };
}

export function setProfileAboutParagraph (paragraph) {
  return {
    type: SET_PROFILE_ABOUT_PARAGRAPH,
    payload: paragraph,
  };
}

export function setProfileDescriptionImages (images) {
  return {
    type: SET_PROFILE_DESCRIPTION_IMAGES,
    payload: images,
  };
}

export function setProfileUpdates (updates) {
  return {
    type: SET_PROFILE_UPDATES,
    payload: updates,
  };
}

export function setProfileDataFields (dataFields) {
  return {
    type: SET_PROFILE_DATA_FIELDS,
    payload: dataFields,
  };
}

export function setSponsorshipPackages (packages) {
  return {
    type: SET_SPONSORSHIP_PACKAGES,
    payload: packages,
  };
}

export function setSponsorshipPDF (pdf) {
  return {
    type: SET_SPONSORSHIP_PDF,
    payload: pdf,
  };
}

export function setPledges (pledges) {
  return {
    type: SET_PLEDGES,
    pledges,
  };
}

export function resetProfile () {
  return {
    type: RESET_PROFILE,
  };
}

export function getProfile (pId) {
  return dispatch => new Promise(async (resolve) => {
    try {
      const PROFILE_TYPES = ['event', 'campaign', 'organization', 'catalog'];

      const request = getRequest(dispatch);

      const url = `/profiles/${ pId }`;
      const response = await request.get(url);

      const profile = response.data;

      if (PROFILE_TYPES.indexOf(profile.type) > -1) {
        dispatch(setProfile(profile));
      }

      return resolve(profile);
    } catch (e) {
      handleApiError(e);
      return resolve();
    }
  });
}

export function getProfileCoverImages (pId) {
  return dispatch => new Promise(async (resolve) => {
    try {
      const request = getRequest(dispatch);

      const url = `/profiles/${ pId }/cover_images`;
      const response = await request.get(url);

      const images = response.data;
      dispatch(setProfileCoverImages(images));

      return resolve();
    } catch (e) {
      handleApiError(e);
      return resolve();
    }
  });
}

export function getProfileAboutParagraph (pId) {
  return dispatch => new Promise(async (resolve) => {
    try {
      const request = getRequest(dispatch);

      const url = `/profiles/${ pId }/about_paragraph`;
      const response = await request.get(url);

      const paragraph = response.data;
      dispatch(setProfileAboutParagraph(paragraph));

      return resolve();
    } catch (e) {
      handleApiError(e);
      return resolve();
    }
  });
}

export function getProfileDescriptionImages (pId) {
  return dispatch => new Promise(async (resolve) => {
    try {
      const request = getRequest(dispatch);

      const url = `/profiles/${ pId }/description_images`;
      const response = await request.get(url);

      const images = response.data;
      dispatch(setProfileDescriptionImages(images));

      return resolve();
    } catch (e) {
      handleApiError(e);
      return resolve();
    }
  });
}

export function getProfileUpdates (pId) {
  return dispatch => new Promise(async (resolve) => {
    try {
      const request = getRequest(dispatch);

      const url = `/profiles/${ pId }/updates`;
      const response = await request.get(url);

      const updates = response.data;
      dispatch(setProfileUpdates(updates));

      return resolve();
    } catch (e) {
      handleApiError(e);
      return resolve();
    }
  });
}

export function updateProfileSubscription (pId, suId, data) {
  return dispatch => new Promise(async (resolve, reject) => {
    try {
      const request = getRequest(dispatch);

      const url = `/profiles/${ pId }/subscriptions/${ suId }`;
      await request.put(url, data);

      return resolve();
    } catch (e) {
      const error = handleApiError(e);
      return reject(error);
    }
  });
}

export function cancelProfileSubscription (pId, suId) {
  return dispatch => new Promise(async (resolve, reject) => {
    try {
      const request = getRequest(dispatch);

      const url = `/profiles/${ pId }/subscriptions/${ suId }`;
      await request.delete(url);

      return resolve();
    } catch (e) {
      const error = handleApiError(e);
      return reject(error);
    }
  });
}

export function getProfileDataFields (pId, params) {
  return dispatch => new Promise(async (resolve) => {
    try {
      const request = getRequest(dispatch);

      const url = `/profiles/${ pId }/data_fields`;
      const response = await request.get(url, { params });

      const dataFields = response.data;
      dispatch(setProfileDataFields(dataFields));

      return resolve();
    } catch (e) {
      handleApiError(e);
      return resolve();
    }
  });
}

export function getPledges (pId) {
  return dispatch => new Promise(async (resolve) => {
    try {
      const request = getRequest(dispatch);

      const url = `/profiles/${ pId }/donation_pledges`;
      const response = await request.get(url);

      const pledges = response.data;
      dispatch(setPledges(pledges));
      return resolve(pledges);
    } catch (e) {
      handleApiError(e);
      return resolve();
    }
  });
}

export function createProfileUpdate (pId, data) {
  return dispatch => new Promise(async (resolve, reject) => {
    try {
      const request = getRequest(dispatch);

      const url = `/profiles/${ pId }/updates`;
      await request.post(url, data);

      return resolve();
    } catch (e) {
      const error = handleApiError(e);
      return reject(error);
    }
  });
}

export function updateProfileUpdate (pId, uId, data) {
  return dispatch => new Promise(async (resolve, reject) => {
    try {
      const request = getRequest(dispatch);

      const url = `/profiles/${ pId }/updates/${ uId }`;
      await request.put(url, data);

      return resolve();
    } catch (e) {
      const error = handleApiError(e);
      return reject(error);
    }
  });
}

export function removeProfileUpdate (pId, uId) {
  return dispatch => new Promise(async (resolve, reject) => {
    try {
      const request = getRequest(dispatch);

      const url = `/profiles/${ pId }/updates/${ uId }`;
      await request.delete(url);

      return resolve();
    } catch (e) {
      const error = handleApiError(e);
      return reject(error);
    }
  });
}

export function getSponsorshipPackages (eId) {
  return dispatch => new Promise(async (resolve, reject) => {
    try {
      const request = getRequest(dispatch);

      const url = `/events/${ eId }/sponsor_packages`;
      const response = await request.get(url);

      const packages = response.data;
      dispatch(setSponsorshipPackages(packages));
      return resolve();
    } catch (e) {
      handleApiError(e);
      return reject();
    }
  });
}

export function getSponsorshipPDF (eId) {
  return dispatch => new Promise(async (resolve, reject) => {
    try {
      const request = getRequest(dispatch);

      const url = `/events/${ eId }/sponsor_packages/pdf`;
      const response = await request.get(url);

      const pdf = response.data;
      dispatch(setSponsorshipPDF(pdf));
      return resolve();
    } catch (e) {
      const error = handleApiError(e);
      return error.status === 404 ? resolve() : reject();
    }
  });
}

export function registerForEvent (eId, data) {
  return dispatch => new Promise(async (resolve, reject) => {
    try {
      const request = getRequest(dispatch);

      const url = `/events/${ eId }/register`;
      await request.post(url, data);

      return resolve();
    } catch (e) {
      const error = handleApiError(e);
      return reject(error);
    }
  });
}

export function resendGuestEmail (eId, data) {
  return dispatch => new Promise(async (resolve, reject) => {
    try {
      const request = getRequest(dispatch);

      const url = `/events/${ eId }/resend_guest_email`;
      await request.post(url, data);

      return resolve();
    } catch (e) {
      const error = handleApiError(e);
      return reject(error);
    }
  });
}
