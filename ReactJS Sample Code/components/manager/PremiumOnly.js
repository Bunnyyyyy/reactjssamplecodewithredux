import React from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import styled from 'styled-components';

const mapStateToProps = state => ({
  organization: state.manager.organization,
  event: state.manager.event
});

const Div = styled.div`
  text-align: center;
`;

const EmojiP = styled.p`
  font-size: 72px;
  line-height: 1.2;
  margin: 40px 0 0 0 !important;
`;

const TextP = styled.p`
  font-size: 24px;
  font-weight: 600;
  line-height: 1.2;
`;

const SpanP = styled.span`
  font-size: 16px;
  line-height: 36px;
  color: ${ props => props.theme.colors.mainBlue };
`;

const PremiumOnly = ({ organization, event }) => (
  <Div>
    <EmojiP>🔐</EmojiP>
    <TextP>This feature is for Premium events only</TextP>
    <Link to={ `/${ organization.id }/events/${ event.id }` }>
      <SpanP>Change your event pricing plan</SpanP>
    </Link>
  </Div>
);

export default connect(mapStateToProps)(PremiumOnly);
