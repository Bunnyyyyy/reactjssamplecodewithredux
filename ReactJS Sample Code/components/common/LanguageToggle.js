import React, { Component } from 'react';
import styled, { ThemeProvider } from 'styled-components';

import { boxShadow } from '../../utils/styles';
import i18n from './../../../i18n';

const Wrapper = styled.div`
  position: relative;
  display: flex;
  ${ props =>
    props.theme.loggedIn
      ? `
      flex-direction: column;
      font-size: ${ props.theme.mobile ? '20px' : '15px' };
  `
      : `
      height: ${ props.theme.manager ? '56px' : '78px' };
  ` };

  justify-content: center;
  align-items: center;

  ${ props => (props.theme.mobile ? 'margin-bottom: 20px;' : '') };
`;

const Title = styled.span`
  font-weight: 700;
  color: ${ props =>
    props.transparent ? 'white' : props.theme.colors.darkGray };

  ${ props =>
    props.theme.loggedIn
      ? `
      font-size: ${ props.theme.mobile ? '20px' : '12px' };
      line-height: 33px;
      margin-right: auto;
  `
      : `
      font-size: 12px;
      line-height: 42px;
  ` };
`;

const Icon = styled.i`
  width: 20px;
  text-align: center;
  margin-right: 5px;
`;

const TitleWrapper = styled.div`
  display: flex;
  cursor: pointer;

  &:hover,
  &:active {
    ${ Title }, i {
      color: ${ props =>
    props.transparent ? 'white' : props.theme.colors.mainBlue };
    }
  }

  ${ props =>
    props.theme.loggedIn
      ? `
      width: 100%;
      align-items: center;
    `
      : `
      flex: 1;
      padding: 20px;
      justify-content: flex-end;
  ` };
`;

const LanguageWrapper = styled.ul`
  background-color: white;
  display: ${ props => (props.toggled ? 'inline-block' : 'none') };
  padding: 2px 0;
  overflow: hidden;
  vertical-align: top;
  width: 100%;
  ${ props =>
    props.theme.loggedIn
      ? `
      padding-left: 25px;
    `
      : `
      position: absolute;
      top: 55px;
      border-radius: 5px 0 0 5px;
      transition: all .3s ease;
      ${ boxShadow('-3px 3px 3px 0 rgba(0, 0, 0, 0.1)') };
  ` };
`;

const LanguageTitle = styled.li`
  display: block;
  text-align: left;
  cursor: pointer;
  font-size: ${ props => (props.theme.mobile ? '20px' : '12px') };
  line-height: 21px;
  font-weight: 700;

  ${ props =>
    props.active
      ? `
    color: ${ props.theme.colors.mainBlue };
  `
      : '' } ${ props =>
  props.theme.loggedIn
    ? `
    line-height: 33px;

    &:hover, &:active {
      color: ${ props.theme.colors.darkBlue };
    }
  `
    : `
    height: 41px;
    padding: 10px 20px;

    &:hover, &:active {
      background-color: ${ props.theme.colors.bgGray };
    }
  ` };
`;

const Toggle = styled.div`
  height: ${ props => (props.theme.loggedIn ? '33px' : '42px') };
  width: 21px;
  cursor: pointer;

  & i {
    line-height: ${ props => (props.theme.loggedIn ? '33px' : '42px') };
    color: ${ props =>
    props.transparent ? 'white' : props.theme.colors.darkGray };
  }
`;

const languages = [
  {
    id: 'en',
    text: 'EN'
  },
  {
    id: 'zhTW',
    text: '繁中'
  },
  {
    id: 'zhCN',
    text: '简中'
  }
];

export default class LanguageToggle extends Component {
  state = {
    toggled: false
  };

  toggleCollapsed = () => this.setState({ toggled: !this.state.toggled });

  changeLanguage = id => {
    if (i18n.language !== id) {
      i18n.changeLanguage(id);
      window.location.reload();
    }
  };

  render () {
    const { loggedIn, transparent, mobile } = this.props;
    const { toggled } = this.state;

    const currentLanguage = i18n.language || 'en';
    const activeLanguage =
      languages.find(l => l.id === currentLanguage) || languages[0];
    const title = activeLanguage.text;

    const CollapseToggle = (
      <Toggle transparent={ transparent }>
        <i
          className={ `fa fa-caret-${ toggled ? 'down' : 'left' }` }
          aria-hidden='true'
        />
      </Toggle>
    );

    const ActiveLanguageToggle = (
      <LanguageTitle
        active
        key={ activeLanguage.id }
        onClick={ this.toggleCollapsed.bind(this) }
      >
        { activeLanguage.text }
      </LanguageTitle>
    );

    const Languages = languages
      .filter(language => language.id !== activeLanguage.id)
      .map(language => (
        <LanguageTitle
          key={ language.id }
          onClick={ () => this.changeLanguage(language.id) }
        >
          { language.text }
        </LanguageTitle>
      ));

    return (
      <ThemeProvider theme={ { loggedIn, mobile } }>
        <Wrapper loggedIn={ loggedIn }>
          <TitleWrapper
            transparent={ transparent }
            onClick={ this.toggleCollapsed.bind(this) }
          >
            { loggedIn && <Icon className={ `fas fa-globe` } aria-hidden='true' /> }
            <Title transparent={ transparent }>{ title }</Title>
            { CollapseToggle }
          </TitleWrapper>
          <LanguageWrapper toggled={ toggled }>
            { ActiveLanguageToggle }
            { Languages }
          </LanguageWrapper>
        </Wrapper>
      </ThemeProvider>
    );
  }
}
