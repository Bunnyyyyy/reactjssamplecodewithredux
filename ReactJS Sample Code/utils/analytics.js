import React from 'react';
import moment from 'moment';

import Card from './../components/manager/cards/Card';
import PieChart from './../components/manager/charts/PieChart';
import BarChart from './../components/manager/charts/BarChart';
import LineChart from './../components/manager/charts/LineChart';

const fromDate = new Date();
fromDate.setDate(fromDate.getDate() - 7);

export const PieChartForTickets = ({ guests, tickets, label }) => {
  const data = {};
  tickets.forEach(e => {
    data[e.id] = {
      label: e.name,
      value: 0
    };
  });
  guests.forEach(e => {
    if (e.event_ticket_id && data[e.event_ticket_id]) {
      data[e.event_ticket_id].value++;
    }
  });

  const dataArray = [];
  (Object.keys(data) || []).forEach(key => {
    dataArray.push(data[key]);
  });

  return (
    <Card title={ label }>
      <PieChart data={ dataArray } />
    </Card>
  );
};

export const BarChartForTags = ({ tags, entities, label }) => {
  const data = {};
  entities.forEach(e => {
    if (Array.isArray(e.tags)) {
      e.tags.forEach(tagId => {
        if (data[tagId]) {
          data[tagId]++;
        } else {
          data[tagId] = 1;
        }
      });
    }
  });

  const chartData = [];
  for (let tagId in data) {
    const tag = tags.find(t => t.id === Number(tagId));
    if (tag) {
      chartData.push({
        label: tag.tag.toUpperCase(),
        value: data[tagId]
      });
    }
  }
  chartData.sort((d1, d2) => (d1.value < d2.value ? 1 : -1));

  return (
    <Card title={ label }>
      <BarChart data={ chartData } />
    </Card>
  );
};

export const BarChartForTickets = ({ guests, tickets, label, type }) => {
  const ticketObj = {};

  (tickets || []).forEach(t => {
    ticketObj[t.id] = t.price;
  });
  const data = {};
  if (type === 'DAY') {
    (guests || []).forEach(e => {
      if (e.created_at && e.event_ticket_id) {
        const strDate = moment
          .utc(e.created_at)
          .local()
          .format('YYYY/MM/DD');
        if (data[strDate]) {
          data[strDate] += ticketObj[e.event_ticket_id];
        } else {
          data[strDate] = ticketObj[e.event_ticket_id];
        }
      }
    });
  } else {
    (guests || []).forEach(e => {
      const strWeek = moment(e.created_at).week();
      if (data[strWeek]) {
        data[strWeek] += ticketObj[e.ticket_name];
      } else {
        data[strWeek] = ticketObj[e.ticket_name];
      }
    });
  }

  const chartData = [];
  for (let label in data) {
    // HACK: to reduce length of x-axis label
    chartData.push({
      label: chartData.length
        ? label
          .split('/')
          .slice(1)
          .join('/')
        : label,
      value: data[label]
    });
  }

  return (
    <Card title={ label }>
      <BarChart data={ chartData } />
    </Card>
  );
};

export const LineChartForCount = ({ entities, dateProperty, title, type }) => {
  const dataObj = {};
  let summaryValue = 0;
  if (type === 'DAY') {
    (entities || []).forEach(e => {
      if (!e[dateProperty] || moment(e[dateProperty]).isBefore(fromDate)) {
        return;
      }

      summaryValue++;
      const strDate = moment(e[dateProperty]).format('YYYY/MM/DD');
      if (dataObj[strDate]) {
        dataObj[strDate]++;
      } else {
        dataObj[strDate] = 1;
      }
    });
  } else {
    (entities || []).forEach(e => {
      const strWeek = moment(e[dateProperty]).week();
      if (dataObj[strWeek]) {
        dataObj[strWeek]++;
      } else {
        dataObj[strWeek] = 1;
      }
    });
  }

  const data = [];
  for (let label in dataObj) {
    // HACK: to reduce length of x-axis label
    data.push({
      label: data.length
        ? label
          .split('/')
          .slice(1)
          .join('/')
        : label,
      value: dataObj[label]
    });
  }

  return (
    <Card>
      <LineChart
        data={ data }
        summaryValue={ summaryValue }
        summaryDirection='up'
        title={ title }
      />
    </Card>
  );
};

export const LineChartForGrowth = ({ entities, title, type }) => {
  const dataObj = {};

  let summaryValue = 0;
  if (type === 'DAY') {
    (entities || []).forEach(e => {
      if (e.created_at) {
        summaryValue++;
        const strDate = moment
          .utc(e.created_at)
          .local()
          .format('YYYY/MM/DD');
        if (dataObj[strDate]) {
          dataObj[strDate]++;
        } else {
          dataObj[strDate] = 1;
        }
      }
    });
  } else {
    (entities || []).forEach(e => {
      const strWeek = moment(e.created_at).week();
      if (dataObj[strWeek]) {
        dataObj[strWeek]++;
      } else {
        dataObj[strWeek] = 1;
      }
    });
  }

  const data = [];
  for (let label in dataObj) {
    // HACK: to reduce length of x-axis label
    data.push({
      label: data.length
        ? label
          .split('/')
          .slice(1)
          .join('/')
        : label,
      value: dataObj[label]
    });
  }

  return (
    <Card title={ title }>
      <LineChart
        data={ data }
        summaryValue={ summaryValue }
        summaryDirection='up'
      />
    </Card>
  );
};
