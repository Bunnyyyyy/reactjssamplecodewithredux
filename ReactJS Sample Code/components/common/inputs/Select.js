import React, { Component } from 'react';
import styled from 'styled-components';
import Select from 'react-select';

import Label from './Label';
import InvalidMessage from '../InvalidMessage';
import { nowrap } from '../../../utils/styles';

const Wrapper = styled.div`
  &:focused {
    label {
      opacity: 1;
    }
  }

  & .select {
    &__container {
      background: transparent;
      border: none;
      padding: 0;

      &:hover {
        .select__placeholder {
          color: ${ props => (props.white ? '' : 'rgba(0, 167, 255, 0.25)') };
        }

        .select__single-value,
        .select__indicator {
          color: ${ props => (props.white ? '' : props.theme.colors.mainBlue) } !important;
        }
      }
    }

    &__control {
      cursor: pointer;
      display: flex;
      justify-content: space-between;
      min-height: 32px;
      height: 32px;
      background: transparent;
      border: none;
      border-radius: 0;
      box-shadow: none;
      ${ props => (props.underlined
    ? 'border-bottom: 1px solid rgba(47, 46, 46, 0.25);'
    : '') } ${ props => (props.invalid
  ? `border-color: ${ props.theme.colors.mainRed } !important;`
  : '') } &--is-focused {
        border-color: rgb(0, 167, 255);

        .select__indicator {
          color: ${ props => (props.white ? '' : props.theme.colors.mainBlue) } !important;
        }
      }
    }

    &__value-container {
      position: relative;
      padding: 0;
      height: 32px;
      flex: 1;

      & input {
        outline: none;
        border-width: 0 !important;
        width: 1px;
        font-size: 15px !important;
        line-height: 1.4;
        font-weight: 400;
      }
    }

    &__input {
      margin: -2px;
    }

    &__placeholder,
    &__single-value {
      position: absolute;
      top: 50%;
      max-width: calc(100% - 8px);
      -webkit-transform: translateY(-50%);
      -ms-transform: translateY(-50%);
      transform: translateY(-50%);
      font-size: 15px;
      line-height: 1.4;
      font-weight: 400;
      margin: 0;
      ${ nowrap };
    }

    &__placeholder {
      color: ${ props => (props.white ? 'white' : 'rgba(47, 46, 46, 0.25)') };
    }

    &__dropdown-indicator {
      color: ${ props => (props.white ? 'white !important' : '') };
    }

    &__single-value {
      color: ${ props => props.theme.colors.darkGray };
    }

    &__indicators {
      display: flex;
      align-items: center;
    }

    &__indicator {
      display: flex;
      padding: 0 8px;
      color: ${ (props) => {
    if (props.white) {
      return 'white';
    } if (props.hasValue) {
      return props.theme.colors.darkGray;
    }

    return 'rgba(47, 46, 46, 0.25)';
  } } !important;

      & svg {
        display: inline-block;
        fill: currentColor;
        line-height: 1;
        stroke: currentColor;
        stroke-width: 0;
      }
    }

    &__indicator-separator {
      display: none;
    }

    &__menu {
      overflow: scroll;
      max-height: ${ props => (props.bottomModal ? '100px' : '200px') };
      box-shadow: 0 3px 6px rgba(0, 0, 0, 0.16);
      ${ (props) => {
    if (props.menuFixed) {
      return 'top: -50px;';
    } if (props.menuOnTop) {
      return `
              top: auto;
              bottom: calc(100% - 10px);
      `;
    }
    return `
              top: 22px;
      `;
  } };
    }

    &__menu-list {
      padding: 0;
      max-height: ${ props => (props.bottomModal ? '98px' : '198px') };
    }

    &__option {
      cursor: pointer;
      font-size: 13px;
      font-weight: 400;
      background-color: transparent;
      color: ${ props => props.theme.colors.darkGray };

      &--is-selected {
        background-color: rgba(0, 133, 204, 0.08);
      }

      &--is-focused {
        background-color: rgba(0, 133, 204, 0.15);
      }
    }

    &__menu-notice--no-options {
      margin: 5px 0;
      font-size: 13px;
      font-weight: 400;
    }
  }

  position: relative;
  display: inline-block;
  width: ${ props => (props.fullWidth ? '100%' : '200px') };
  margin-top: 20px;
  margin-bottom: 20px;

  ${ props => props.customCss };
`;

export default class SelectComponent extends Component {
  constructor () {
    super();

    this.state = {
      focused: false,
    };
  }

  setFocused (focused) {
    this.setState({ focused });
  }

  render () {
    const {
      label,
      field,
      required,
      onChange,
      value,
      options,
      bottomModal,
      customCss,
      errorMessage,
      fullWidth,
      underlined,
      clearable = false,
      searchable = true,
      white,
      menuOnTop,
      menuFixed,
      ...props
    } = this.props;
    const { focused } = this.state;

    // cheap hack to stay consistent with old convention
    // to store just the value, not the option, in state
    const selectedOption = options.find(o => o.value === value);

    return (
      <Wrapper
        customCss={ customCss }
        fullWidth={ fullWidth }
        bottomModal={ bottomModal }
        underlined={ underlined }
        hasValue={ !!selectedOption }
        invalid={ !!errorMessage }
        white={ white }
        menuOnTop={ menuOnTop }
        menuFixed={ menuFixed }
      >
        { label ? (
          <Label
            field={ field }
            label={ label }
            focused={ focused || selectedOption }
            invalid={ !!errorMessage }
            required={ required }
          />
        ) : null }
        <Select
          className='select__container'
          classNamePrefix='select'
          value={ selectedOption || null }
          options={ options }
          onChange={ (option) => {
            const changeValue = option && option.value !== undefined ? option.value : null;
            return onChange(changeValue);
          } }
          onFocus={ this.setFocused.bind(this, true) }
          onBlur={ this.setFocused.bind(this, false) }
          errorMessage={ errorMessage }
          isClearable={ clearable }
          isSearchable={ searchable }
          white={ white }
          { ...props }
        />
        { errorMessage ? <InvalidMessage>{ errorMessage }</InvalidMessage> : null }
      </Wrapper>
    );
  }
}
