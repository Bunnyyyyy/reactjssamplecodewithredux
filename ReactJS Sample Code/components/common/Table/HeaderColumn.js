import React, { Component } from 'react';
import styled from 'styled-components';

import { userSelect, nowrap } from './../../../utils/styles';

const FilterDropdownWrapper = styled.div`
  display: none;
  color: ${ props => props.theme.colors.darkGray };
  margin-left: -20px;
  padding: 0 20px;
  position: absolute;
  z-index: 2;
`;

const ArrowDown = styled.div`
  width: 0;
  height: 0;
  border-left: 4.5px solid transparent;
  border-right: 4.5px solid transparent;
  border-top: 6px solid
    ${ props =>
    props.active ? props.theme.colors.darkBlue : props.theme.colors.mainGray };
  margin-top: 3px;
`;

const ArrowUp = styled.div`
  width: 0;
  height: 0;
  border-left: 4.5px solid transparent;
  border-right: 4.5px solid transparent;
  border-bottom: 6px solid
    ${ props =>
    props.active ? props.theme.colors.darkBlue : props.theme.colors.mainGray };
`;

const Wrapper = styled.div`
  flex-grow: 1;
  flex-basis: 0;
  text-align: left;
  height: 48px;
  position: relative;
  cursor: ${ props => (props.unsortable ? 'default' : 'pointer') };
  letter-spacing: normal;
  text-transform: capitalize;

  ${ userSelect('none') } &:hover ${ FilterDropdownWrapper } {
    display: block;
  }

  &:hover p {
    color: ${ props =>
    props.unsortable
      ? props.theme.colors.mainGray
      : props.theme.colors.mainBlue };
  }
`;

// HACK: extremely shitty hack to make sure column width stays the same when text is long and with ellipsis
const ToggleWrapper = styled.div`
  position: absolute;
  top: 0;
  bottom: 0;
  left: 0;
  right: 0;
  display: flex;
  align-items: center;
  padding: ${ props => (props.fixed ? '0 5px 0 30px' : '0 5px 0 15px') };
`;

const ColumnTitle = styled.p`
  font-size: 12px;
  line-height: 24px;
  color: ${ props => (props.active ? props.theme.colors.darkBlue : '#9c9c9c') };
  ${ nowrap };
`;

const ArrowSpan = styled.span`
  display: inline-block;
  flex-shrink: 0;
  padding-left: 10px;
`;

export default class HeaderColumn extends Component {
  constructor () {
    super();

    this.state = {
      isFilterMenuOpen: false
    };
  }

  render () {
    const {
      fixed,
      title,
      style,
      renderFilter,
      onSort,
      sortBy,
      sortOrder,
      field,
      unsortable
    } = this.props;
    let activeSortOrder = null;
    if (sortBy === field) {
      activeSortOrder = sortOrder;
    }

    return (
      <Wrapper
        style={ style }
        unsortable={ unsortable }
        onClick={ !unsortable ? onSort.bind(this, field) : null }
      >
        <ToggleWrapper fixed={ fixed }>
          <ColumnTitle active={ !!activeSortOrder }>{ title }</ColumnTitle>
          { !unsortable && typeof renderFilter !== 'function' ? (
            <ArrowSpan>
              <ArrowUp active={ activeSortOrder === 'asc' } />
              <ArrowDown active={ activeSortOrder === 'desc' } />
            </ArrowSpan>
          ) : null }
        </ToggleWrapper>
        <FilterDropdownWrapper>
          { renderFilter && renderFilter() }
        </FilterDropdownWrapper>
      </Wrapper>
    );
  }
}
