import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { withRouter } from 'react-router';
import styled from 'styled-components';
import moment from 'moment';

import * as UiActionCreators from '../../../../../redux/ui';
import * as ManagerActionCreators from '../../../../../redux/manager';
import * as EventActionCreators from '../../../../../redux/event';
import * as EventPromoCodesActionCreators from '../../../../../redux/eventPromoCodes';
import * as ProfileActionCreators from '../../../../../redux/profile';

import ItemTable from '../../../../../components/common/ItemTableV2';

import { t as translate } from '../../../../../../i18n';

const t = a => translate(a, 'manager/events/ticketing/promoCodes/index');

const StatusBall = styled.div`
  width: 10px;
  height: 10px;
  border-radius: 50%;
  background-color: ${ props => (props.active ? props.theme.colors.mainGreen : props.theme.colors.lightGray) };
  margin-right: 5px;
`;

const AlignedDiv = styled.div`
  display: flex;
  align-items: center;
`;

const StatusRowColumn = state => (
  <AlignedDiv>
    <StatusBall active={ state === 'Active' } />
    { state }
  </AlignedDiv>
);

const mapStateToProps = state => ({
  events: state.manager.events,
  event: state.manager.event,
  organization: state.manager.organization,
  promoCodes: state.eventPromoCodes.promoCodes,
  customFields: state.profile.customFields,
});

const ActionCreators = Object.assign(
  {},
  UiActionCreators,
  ManagerActionCreators,
  EventActionCreators,
  EventPromoCodesActionCreators,
  ProfileActionCreators,
);
const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators(ActionCreators, dispatch),
});

const tableColumns = [
  {
    field: 'name',
    title: t('Promotion Name'),
    flex: '3',
    customCss: 'font-weight: 700',
  },
  {
    field: 'code',
    title: t('Code'),
    flex: '2',
  },
  {
    field: 'value',
    title: t('Discount'),
    flex: '1',
    format: (value, item) => (item.type === 'percentage'
      ? `${ value }%`
      : `HK$${ value ? value.toLocaleString() : 0 }`),
  },
  {
    field: 'quantity',
    title: t('Qty'),
    flex: '1',
    format: v => (v !== null ? v : t('Unlimited')),
  },
  {
    field: 'quantity_used',
    title: t('Qty Used'),
    flex: '1',
  },
  {
    field: 'status',
    title: t('Status'),
    flex: '2',
    format: (_, item) => {
      let statusInput = '';
      const now = moment();
      if (now < moment(item.start_at) || now > moment(item.end_at)) {
        statusInput = 'Inactive';
      } else {
        statusInput = 'Active';
      }

      return StatusRowColumn(statusInput);
    },
  },
];

class EventTicketingPromoCodesListView extends Component {
  async componentDidMount () {
    const { actions, match } = this.props;
    const { eId } = match.params;

    actions.setLoading(true);

    try {
      const promises = [
        actions.getEventPromoCodes(eId),
        actions.getProfileDataFields(eId, { type: 'guest' }),
      ];
      await Promise.all(promises);
    } catch (e) {
      console.error('ERROR: EventTicketingPromoCodesListView', e);
    } finally {
      actions.setLoading(false);
    }
  }

  componentWillUnmount () {
    const { actions } = this.props;
    actions.resetSaveActions();
  }

  render () {
    const { event = {}, organization, promoCodes } = this.props;

    const orderPromoCodes = promoCodes.map((p, i) => ({
      ...p,
      order: p.order || i,
      used: p.used || 0,
    }));

    const tableEditUrl = item => `/${ organization.id }/events/${ event.id }/ticketing/promo-codes/${ item.id }`;

    const tableNewItemUrl = `/${ organization.id }/events/${
      event.id
    }/ticketing/promo-codes/new`;

    return (
      <ItemTable
        columns={ tableColumns }
        rows={ orderPromoCodes }
        editUrl={ tableEditUrl }
        itemText={ t('promotion code') }
        newItemUrl={ tableNewItemUrl }
        searchField='name'
      />
    );
  }
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(EventTicketingPromoCodesListView));
