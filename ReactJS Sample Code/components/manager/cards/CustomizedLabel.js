import React from 'react';
import styled from 'styled-components';

import sampleQrcode from '../../../assets/manager/sample-qrcode.png';

const Label50 = styled.div`
  min-width: 360px;
  height: 200px;
  border-radius: 5px;
  background-color: white;
  box-shadow: 0 3px 30px 0 rgba(0, 0, 0, 0.1);
  margin: auto;
  margin-top: 40px;
  display: flex;
  flex-direction: column;
  position: relative;
  ${ props => props.customCss || '' };
`;

const Label50Flex = styled.div`
  display: flex;
  width: 100%;
  padding: 0 30px;
  align-items: center;
  margin: auto 0;
`;

const LabelContent = styled.div`
  flex-grow: 1;
`;

const Label29 = styled.div`
  display: flex;
  align-items: center;
  min-width: 360px;
  height: 106px;
  border-radius: 5px;
  background-color: #ffffff;
  box-shadow: 0 3px 30px 0 rgba(0, 0, 0, 0.1);
  margin: auto;
  margin-top: 40px;
  position: relative;
  padding: 20px;
  ${ props => props.customCss || '' };
`;

const LabelEventName = styled.div`
  text-align: center;
  height: 44px;
  border-radius: 5px 5px 0px 0px;
  background-color: black;
  font-size: 12px;
  color: white;
  line-height: 44px;
  width: 100%;
`;

const Label29EventName = styled.p`
  font-size: 12px;
  line-height: 15px;
  margin-bottom: 5px;
`;

const LabelQrcode = styled.div`
  width: 75px;
  height: 75px;
  margin-right: 20px;

  img {
    width: 100%;
  }
`;

const LabelDataFields = styled.div`
  font-size: 10px;

  > p {
    font-size: 10px;
    line-height: 15px;
  }
`;

const LabelGuestName = styled.h5`
  font-size: 20px;
  margin-bottom: 10px;

  ${ props => (props.small
    ? `
    font-size: 15px;
    margin-bottom: 5px;
  `
    : '') } ${ props => props.customCss || '' };
`;

const LabelTags = styled.div`
  margin-left: 20px;
  margin-bottom: 20px;
`;

const LabelTag = styled.span`
  border-radius: 5px;
  background-color: black;
  font-size: 10px;
  color: white;
  margin-right: 10px;
  padding: 4px 10px;
  margin-top: 5px;
  white-space: nowrap;
`;

const CustomizedLabel = ({ event = {}, fields = [], labelData = {} }) => {
  const {
    size = '90,29',
    guest_name = true,
    guest_qr_code,
    event_name,
    tags,
    data_field_ids,
  } = labelData;
  const dataFieldIds = data_field_ids || [];
  const QrCode = guest_qr_code ? (
    <LabelQrcode>
      <img alt='Sample QR code' src={ sampleQrcode } />
    </LabelQrcode>
  ) : null;
  const DataFieldValues = [];
  dataFieldIds.forEach((fId) => {
    const field = fields.find(f => f.id === fId);
    if (field) {
      DataFieldValues.push(<p>{ `${ field.label }: ...` }</p>);
    }
  });

  const DataFields = DataFieldValues.length ? (
    <LabelDataFields>{ DataFieldValues }</LabelDataFields>
  ) : null;

  const Tags = [
    <LabelTag>VIP</LabelTag>,
    <LabelTag>Music</LabelTag>,
    <LabelTag>Travel</LabelTag>,
  ];

  if (size === '90,50') {
    return (
      <Label50>
        { event_name && <LabelEventName>{ event.name }</LabelEventName> }
        <Label50Flex>
          { QrCode }
          <LabelContent>
            { guest_name ? (
              <LabelGuestName small={ event_name && !!DataFieldValues.length }>
                John Doe
              </LabelGuestName>
            ) : null }
            { DataFields }
          </LabelContent>
        </Label50Flex>
        { tags ? <LabelTags>{ Tags }</LabelTags> : null }
      </Label50>
    );
  }

  if (size === '90,29') {
    return (
      <Label29>
        { QrCode }
        <LabelContent>
          { event_name && <Label29EventName>{ event.name }</Label29EventName> }
          { guest_name ? (
            <LabelGuestName small={ tags }>John Doe</LabelGuestName>
          ) : null }
          { tags ? <div>{ Tags }</div> : null }
        </LabelContent>
      </Label29>
    );
  }

  return null;
};

export default CustomizedLabel;
