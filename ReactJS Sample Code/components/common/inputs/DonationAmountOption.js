import styled from 'styled-components';

export default styled.div`
  cursor: pointer;
  display: flex;
  justify-content: center;
  padding: 0 15px;
  width: ${ props => (props.custom ? 'calc(50% - 15px)' : 'calc(25% - 22.5px)') };
  height: 54px;
  border-radius: 5px;
  border: solid 1px ${ props => props.theme.colors.lightBlue };
  box-shadow: 0 0 6px 0 rgba(0, 0, 0, 0.09);
  margin: 0 30px 10px 0;
  text-align: center;

  & p,
  & span {
    font-size: 15px;
    line-height: 54px;
  }

  & span {
    flex-shrink: 0;
  }

  &:nth-child(4n) {
    margin-right: 0;
  }

  @media (max-width: 900px) {
    width: ${ props => (props.custom ? '100%' : 'calc(25% - 11.25px)') };
    margin-right: 15px;
  }

  ${ props => (props.selected
    ? `
      border-width: 2px;
      border-color: ${ props.theme.colors.mainBlue } !important;
      box-shadow: none;
    `
    : '') };

  ${ props => props.customCss || '' };
`;
