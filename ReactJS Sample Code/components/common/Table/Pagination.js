import React, { Component } from 'react';
import styled from 'styled-components';

import Button from './../Button';

import i18n from '.././../../../i18n';
const t = a => i18n.t([`components/common:${ a }`]);

const PaginationDiv = styled.div`
  display: flex;
  align-items: center;
`;

const PaginationButton = styled(Button)`
  margin: 0 0 0 10px;
  min-width: 30px;
  height: 30px;
  line-height: 28px;
  border-radius: 5px;
  font-size: 15px;
  font-weight: 400;

  ${ props =>
    props.current
      ? `
      border: solid 1px ${ props.theme.colors.mainBlue };
      color: ${ props.theme.colors.mainBlue };
      font-weight: 700;
    `
      : '' } &:disabled {
    opacity: 0.3;
  }

  > i {
    font-size: 20px;
    color: ${ props => props.theme.colors.mainGray };
    margin: 5px 0;
  }

  &:hover,
  &:active {
    opacity: 1;
    color: ${ props => props.theme.colors.mainBlue };

    > i {
      color: ${ props => props.theme.colors.mainBlue };
    }
  }
`;

const DotsButton = Button.extend`
  margin: 0 0 0 10px;
  height: 30px;

  > i {
    position: relative;
    top: -3px;
    font-size: 7.5px;
    line-height: 30px;
    margin-right: 2px;
    color: ${ props => props.theme.colors.lightGray };
  }

  &:hover,
  &:active {
    > i {
      color: ${ props => props.theme.colors.mainGray };
    }
  }
`;

const Pagination = ({ meta, perPage, onChangeFrom }) => {
  const currentPage = Math.ceil(meta.from / perPage);
  const pageCount = Math.ceil(meta.count / perPage);
  let addStarLast = false;
  let addStarFirst = false;

  if (currentPage + 2 < pageCount && pageCount > 3) {
    addStarLast = true;
  }
  if (currentPage - 2 > 1 && pageCount > 3) {
    addStarFirst = true;
  }
  const PageButtons = [];
  if (addStarFirst) {
    PageButtons.push(
      <PaginationButton
        plain
        current={ currentPage === 1 }
        onClick={ onChangeFrom.bind(this, perPage * (1 - 1) + 1) }
      >
        { 1 }
      </PaginationButton>
    );
    if (currentPage - 2 > 2 && pageCount > 3) {
      PageButtons.push(
        <DotsButton
          plain
          onClick={ onChangeFrom.bind(this, perPage * (currentPage - 4) + 1) }
        >
          <i class='fa fa-circle' aria-hidden='true' />
          <i class='fa fa-circle' aria-hidden='true' />
          <i class='fa fa-circle' aria-hidden='true' />
        </DotsButton>
      );
    }
  }
  for (
    let i = Math.max(1, currentPage - 2);
    i <= Math.min(pageCount, currentPage + 2);
    i++
  ) {
    PageButtons.push(
      <PaginationButton
        plain
        current={ currentPage === i }
        onClick={ onChangeFrom.bind(this, perPage * (i - 1) + 1) }
      >
        { i }
      </PaginationButton>
    );
  }
  if (addStarLast) {
    if (currentPage + 2 < pageCount - 1 && pageCount > 3) {
      PageButtons.push(
        <DotsButton
          plain
          onClick={ onChangeFrom.bind(this, perPage * (currentPage + 2) + 1) }
        >
          <i class='fa fa-circle' aria-hidden='true' />
          <i class='fa fa-circle' aria-hidden='true' />
          <i class='fa fa-circle' aria-hidden='true' />
        </DotsButton>
      );
    }
    PageButtons.push(
      <PaginationButton
        plain
        current={ currentPage === pageCount }
        onClick={ onChangeFrom.bind(this, perPage * (pageCount - 1) + 1) }
      >
        { pageCount }
      </PaginationButton>
    );
  }

  return (
    <PaginationDiv>
      <PaginationButton
        plain
        onClick={ onChangeFrom.bind(this, Math.max(1, meta.from - perPage)) }
        disabled={ !perPage || meta.from <= 1 }
      >
        <i className='fa fa-chevron-left' aria-hidden='true' />
      </PaginationButton>
      { PageButtons }
      <PaginationButton
        plain
        onClick={ onChangeFrom.bind(this, meta.from + perPage) }
        disabled={ !perPage || meta.from + perPage > meta.count }
      >
        <i className='fa fa-chevron-right' aria-hidden='true' />
      </PaginationButton>
    </PaginationDiv>
  );
};

export default Pagination;
