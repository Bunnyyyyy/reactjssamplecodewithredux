import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { withRouter } from 'react-router';
import { Route, Switch, Redirect } from 'react-router-dom';
import styled from 'styled-components';

import SideMenu from '../../components/manager/SideMenu';
import DashboardIndexView from './dashboard/Index';
import EventsIndexView from './events/Index';
import MembershipIndexView from './membership/Index';
import FundraisingIndexView from './fundraising/Index';
import EmailsIndexView from './emails/Index';
import FormsIndexView from './forms/Index';
import SettingsIndexView from './settings/Index';
import ContactIndexView from './contact/Index';
import IndexView from '../Index';

import * as UiActionCreators from '../../redux/ui';
import * as MeActionCreators from '../../redux/me';
import * as ManagerActionCreators from '../../redux/manager';
import * as OrganizationActionCreators from '../../redux/organization';

const ManagerContainerDiv = styled.div`
  position: relative;
  height: 100vh;
  min-width: 100vw;
  display: flex;
  padding-left: ${ props => (props.collapsed ? '60px' : '180px') };
  flex-direction: row;
  transition: all 0.5s ease-in-out;
`;

const ContentWrapper = styled.div`
  position: relative;
  height: 100vh;
  overflow: hidden;
  width: 100%;
  min-width: 900px;
`;

const mapStateToProps = state => ({
  user: state.me.user,
  organizations: state.manager.organizations,
  organization: state.manager.organization,
  collapseSideMenu: state.config.collapseSideMenu,
});

const ActionCreators = Object.assign(
  {},
  UiActionCreators,
  MeActionCreators,
  ManagerActionCreators,
  OrganizationActionCreators,
);
const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators(ActionCreators, dispatch),
});

class ManagerIndexView extends Component {
  componentDidMount () {
    const { match: { params: { oId } } } = this.props;
    return this.loadOrganization(oId);
  }

  componentDidUpdate (prevProps) {
    const { match: { params: { oId } } } = this.props;
    const prevOId = prevProps.match.params.oId;

    if (oId && oId !== prevOId) this.loadOrganization(oId);
  }

  async loadOrganization (oId) {
    const { actions, history, organizations } = this.props;

    try {
      actions.setLoading(true);
      await actions.getOrganization(oId);
      await actions.getOrganizationContactCounts(oId);
      actions.setLoading(false);

      const { organization } = this.props;
      // redirect to manager index if organization is not managed by user
      // TODO: fix this hack to work around null organization issue
      if (organization && (!organization.id || !organizations.find(o => o.id === organization.id))) {
        return history.replace('/');
      }

      const updateMeData = {
        last_managed_organization: organization.id,
      };

      await actions.updateMe(updateMeData);
    } catch (e) {
      console.error('ERROR: ManagerIndexView', e);
    }
  }

  render () {
    const {
      user,
      organization,
      collapseSideMenu,
    } = this.props;
    if (!organization) return null;
    if (!user.id) return <Redirect to='/login' />;
    if (!user.verified) return <Redirect to='/verify' />;

    return (
      <ManagerContainerDiv collapsed={ collapseSideMenu }>
        <SideMenu />
        <ContentWrapper>
          <Switch>
            <Route exact path={ ['/:oId', '/:oId/home/contacts', '/:oId/home/income'] } component={ DashboardIndexView } />
            <Route path='/:oId/events' component={ EventsIndexView } />
            <Route path='/:oId/membership' component={ MembershipIndexView } />
            <Route path='/:oId/fundraising' component={ FundraisingIndexView } />
            <Route path='/:oId/emails' component={ EmailsIndexView } />
            <Route path='/:oId/forms' component={ FormsIndexView } />
            <Route path='/:oId/settings' component={ SettingsIndexView } />
            <Route path='/:oId/contacts/:usId' component={ ContactIndexView } />
            <Route path='/:oId/*' component={ IndexView } />
          </Switch>
        </ContentWrapper>
      </ManagerContainerDiv>
    );
  }
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(ManagerIndexView));
