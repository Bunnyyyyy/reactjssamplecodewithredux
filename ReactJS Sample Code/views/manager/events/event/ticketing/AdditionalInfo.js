import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { withRouter } from 'react-router';
import styled from 'styled-components';

import * as UiActionCreators from '../../../../../redux/ui';
import * as ManagerActionCreators from '../../../../../redux/manager';
import * as EventActionCreators from '../../../../../redux/event';

import SaveActions from '../../../../../components/manager/SaveActions';
import EditInfoCard from '../../../../../components/manager/cards/EditInfoCard';

import { t as translate } from '../../../../../../i18n';

const t = a => translate(a, 'manager/events/ticketing/additionalInfo');

const ButtonActions = styled.div`
  display: flex;
  align-items: center;
  padding: 10px 0;
  border-top: 1px solid ${ props => props.theme.colors.lightGray };
`;

const mapStateToProps = state => ({
  events: state.manager.events,
  event: state.manager.event,
  organization: state.manager.organization,
});

const ActionCreators = Object.assign(
  {},
  UiActionCreators,
  ManagerActionCreators,
  EventActionCreators,
);
const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators(ActionCreators, dispatch),
});

class EventTicketingAdditionalInfoView extends Component {
  componentWillUnmount () {
    const { actions } = this.props;
    actions.resetSaveActions();
  }

  async onFinishSaving () {
    try {
      const { actions, event } = this.props;
      const eId = event.id;
      await Promise.all([actions.getOrganizationEvent(eId)]);
    } catch (e) {
      throw e;
    }
  }

  render () {
    const { actions, event = {} } = this.props;

    return (
      <EditInfoCard
        name='ticketing-appendix'
        title={ t('Additional Information') }
        fields={ [
          {
            type: 'rte',
            label: t('Terms & Conditions'),
            field: 'terms',
            placeholder: t('Add terms & conditions here'),
            default: event.terms,
          },
          {
            type: 'rte',
            label: 'Ticket Appendix',
            field: 'ticketing_appendix',
            placeholder: t('Add ticket appendix here'),
            default: event.ticketing_appendix,
          },
          {
            type: 'custom',
            field: 'save-info',
            readOnly: true,
            renderComponent: () => (
              <ButtonActions>
                <SaveActions
                  onFinish={ this.onFinishSaving.bind(this) }
                />
              </ButtonActions>
            ),
          },
        ] }
        action={ state => actions.updateEvent(event.id, state) }
        actionOrder={ 0 }
        validateForm={ !!event.id }
      />
    );
  }
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(EventTicketingAdditionalInfoView));
