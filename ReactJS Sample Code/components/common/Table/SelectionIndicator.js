import React from 'react';
import styled from 'styled-components';
import Button from './../Button';
import { get } from 'lodash';

import i18n from '.././../../../i18n';
const t = a => i18n.t([`components/common:${ a }`]);

const Wrapper = styled.p`
  font-size: 12px;
  line-height: 18px;
`;

const SelectAllButton = Button.extend`
  font-size: 12px;
  font-weight: 400;
  line-height: 18px;
  color: ${ props => props.theme.colors.mainBlue };
  text-transform: none;
  text-decoration: underline;
  margin: 0 5px;
`;

const SelectionIndicator = ({
  selectAll,
  selectedRowCount,
  singleItemText,
  pluralItemText,
  setSelectAll,
  search,
  filters,
  meta
}) => {
  const allCount = meta ? meta.count : null;
  const allItemText =
    allCount !== null ? (allCount > 1 ? pluralItemText : singleItemText) : '';

  const selectedItemText =
    selectedRowCount > 1 ? pluralItemText : singleItemText;
  const hasFilters = filters && Object.keys(filters).length;
  const filterText = hasFilters ? t(' matching the filters') : '';

  let SelectAllElements = [];
  if (!search && meta) {
    SelectAllElements = [
      <SelectAllButton plain onClick={ setSelectAll.bind(this, true) }>
        { `${ t('Select all ') }${ allCount } ${ allItemText }${ filterText }` }
      </SelectAllButton>,
      <span>or</span>
    ];
  }

  return (
    <Wrapper>
      { selectAll ? (
        <span>
          <b>{ `${ t('All ') }${ allCount } ${ allItemText }${ filterText }${ t(
            ' are selected.'
          ) }` }</b>
        </span>
      ) : (
        [
          <span>
            <b>
              { `${ selectedRowCount } ${ selectedItemText }${ t(
                ' on this page selected.'
              ) }` }
            </b>
          </span>,
          ...SelectAllElements
        ]
      ) }
      <SelectAllButton plain onClick={ setSelectAll.bind(this, false) }>
        { t('Clear selection') }
      </SelectAllButton>
    </Wrapper>
  );
};

export default SelectionIndicator;
