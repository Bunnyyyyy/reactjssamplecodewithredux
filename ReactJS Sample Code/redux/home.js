import { handleApiError } from '../utils';
import { getRequest } from './index';

const SET_HOME_GOALS = 'SET_HOME_GOALS';
const SET_HOME_EVENTS = 'SET_HOME_EVENTS';
const SET_HOME_ORGANIZATIONS = 'SET_HOME_ORGANIZATIONS';

const defaultState = {
  goals: [],
  events: [],
  organizations: [],
};

export default function reducer (state = defaultState, action) {
  switch (action.type) {
    case SET_HOME_GOALS:
      return { ...state, goals: action.payload };

    case SET_HOME_EVENTS:
      return { ...state, events: action.payload };

    case SET_HOME_ORGANIZATIONS:
      return { ...state, organizations: action.payload };

    default:
      return state;
  }
}

export function setHomeGoals (goals) {
  return {
    type: SET_HOME_GOALS,
    payload: goals,
  };
}

export function setHomeEvents (events) {
  return {
    type: SET_HOME_EVENTS,
    payload: events,
  };
}

export function setHomeOrganizations (organizations) {
  return {
    type: SET_HOME_ORGANIZATIONS,
    payload: organizations,
  };
}

export function getHomeGoals () {
  return dispatch => new Promise(async (resolve) => {
    try {
      const request = getRequest(dispatch);

      const url = '/goals';
      const response = await request.get(url);

      const goals = response.data;
      dispatch(setHomeGoals(goals));

      return resolve();
    } catch (e) {
      handleApiError(e);
      return resolve();
    }
  });
}

export function getHomeEvents () {
  return dispatch => new Promise(async (resolve) => {
    try {
      const request = getRequest(dispatch);

      const url = '/events?home=true&sort_by=display_at_home&sort_order=asc';
      const response = await request.get(url);

      const events = response.data;
      dispatch(setHomeEvents(events));

      return resolve();
    } catch (e) {
      handleApiError(e);
      return resolve();
    }
  });
}

export function getHomeOrganizations () {
  return dispatch => new Promise(async (resolve) => {
    try {
      const request = getRequest(dispatch);

      const url = '/organizations?home=true';
      const response = await request.get(url);

      const orgs = response.data;
      dispatch(setHomeOrganizations(orgs));

      return resolve();
    } catch (e) {
      handleApiError(e);
      return resolve();
    }
  });
}
