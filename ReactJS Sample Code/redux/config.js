const SET_TABLE_CONFIG = 'SET_TABLE_CONFIG';
const TOOGLE_SIDEBAR_MENU = 'TOOGLE_SIDEBAR_MENU';

const defaultState = {
  tableConfigs: {},
  collapseSideMenu: false
};

export default function reducer (state = defaultState, action) {
  switch (action.type) {
    case SET_TABLE_CONFIG:
      const tableConfigs = Object.assign({}, state.tableConfigs);
      const { tableId, columns } = action.payload;
      tableConfigs[tableId] = columns;
      return { ...state, tableConfigs };
    case TOOGLE_SIDEBAR_MENU:
      return { ...state, collapseSideMenu: !state.collapseSideMenu };
    default:
      return state;
  }
}

export function setTableConfig (tableId, columns) {
  return {
    type: SET_TABLE_CONFIG,
    payload: { tableId, columns }
  };
}

export function toogleSidebarMenu () {
  return {
    type: TOOGLE_SIDEBAR_MENU
  };
}
