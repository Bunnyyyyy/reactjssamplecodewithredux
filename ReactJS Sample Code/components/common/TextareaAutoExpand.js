import React from 'react';
import styled from 'styled-components';
import TextareaAutosize from 'react-autosize-textarea';

import Label from './inputs/Label';
import InvalidMessage from './InvalidMessage';

const Div = styled.div`
  position: relative;
  display: inline-block;
  margin-top: 20px;
  margin-bottom: 20px;
  margin-right: 20px;

  & textarea::-webkit-input-placeholder {
    color: #2f2e2e4d;
  }
  & textarea:-moz-placeholder {
    color: #2f2e2e4d;
  }
  & textarea::-moz-placeholder {
    color: #2f2e2e4d;
  }
  & textarea:-ms-input-placeholder {
    color: #2f2e2e4d;
  }
`;

const style = {
  marginTop: '10px',
  padding: '10px 0',
  width: '100%',
  fontSize: '24px',
  linHeight: '35px',
  resize: 'none',
  border: '0px none'
};

const TextareaAutoExpand = ({
  className,
  type,
  label,
  field,
  onChange,
  error,
  children,
  divStyle = {},
  required,
  errorMessage,
  ...props
}) => (
  <Div className={ className } style={ divStyle }>
    { label ? <Label field={ field } label={ label } required={ required } /> : null }
    <TextareaAutosize
      id={ field }
      data-field={ field }
      onChange={ e => onChange(field, e.target.value) }
      style={ style }
      { ...props }
    />
    { errorMessage ? <InvalidMessage>{ errorMessage }</InvalidMessage> : null }
    { children }
  </Div>
);

export default TextareaAutoExpand;
