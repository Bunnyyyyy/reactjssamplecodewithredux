import { handleApiError } from '../utils';
import { getRequest } from './index';
import { setMeta } from './manager';

const SET_EVENT_GUEST_COUNTS = 'SET_EVENT_GUEST_COUNTS';
const SET_EVENT_TICKET_SALES = 'SET_EVENT_TICKET_SALES';
const SET_EVENT_DATES = 'SET_EVENT_DATES';
const SET_EVENT_SESSIONS = 'SET_EVENT_SESSIONS';
const SET_EVENT_TICKETS = 'SET_EVENT_TICKETS';
const SET_EVENT_TICKET = 'SET_EVENT_TICKET';
const SET_EVENT_TICKET_PACKAGES = 'SET_EVENT_TICKET_PACKAGES';
const SET_EVENT_TICKET_PACKAGE = 'SET_EVENT_TICKET_PACKAGE';
const RESET_PROFILE = 'RESET_PROFILE';
const SET_TICKET_TYPE = 'SET_TICKET_TYPE';
const SET_EVENT_TICKET_TYPE_SESSIONS = 'SET_EVENT_TICKET_TYPE_SESSIONS';
const SET_EVENT_DATA_FIELDS = 'SET_EVENT_DATA_FIELDS';
const SET_EVENTS = 'SET_EVENTS';
const PUSH_EVENTS = 'PUSH_EVENTS';
const SET_EVENT_LABEL = 'SET_EVENT_LABEL';
const SET_PAYMENT_INFO = 'SET_PAYMENT_INFO';

const defaultState = {
  guestCounts: {},
  ticketSales: {},
  dates: [],
  sessions: [],
  tickets: [],
  ticket: {},
  ticketTypeSessions: [],
  ticketPackages: [],
  ticketPackage: {},
  selectedTicketType: {},
  dataFields: [],
  events: [],
  label: {},
  paymentInfo: {},
};

export default function reducer(state = defaultState, action) {
  switch (action.type) {
    case SET_EVENTS:
      return { ...state, events: action.payload };

    case PUSH_EVENTS:
      return { ...state, events: state.events.concat(action.payload) };

    case SET_EVENT_GUEST_COUNTS:
      return { ...state, guestCounts: action.payload };

    case SET_EVENT_TICKET_SALES:
      return { ...state, ticketSales: action.payload };

    case SET_EVENT_DATES:
      return { ...state, dates: action.payload };

    case SET_EVENT_SESSIONS:
      return { ...state, sessions: action.payload };

    case SET_EVENT_TICKETS:
      return { ...state, tickets: action.payload };

    case SET_EVENT_LABEL:
      return { ...state, label: action.payload };

    case SET_EVENT_TICKET:
      return { ...state, ticket: action.payload };

    case SET_EVENT_TICKET_TYPE_SESSIONS:
      return { ...state, ticketTypeSessions: action.payload };

    case SET_EVENT_DATA_FIELDS:
      return { ...state, dataFields: action.payload };

    case RESET_PROFILE:
      return defaultState;

    case SET_EVENT_TICKET_PACKAGES:
      return { ...state, ticketPackages: action.payload };

    case SET_EVENT_TICKET_PACKAGE:
      return { ...state, ticketPackage: action.payload };

    case SET_TICKET_TYPE:
      return { ...state, selectedTicketType: action.payload };

    case SET_PAYMENT_INFO:
      return { ...state, paymentInfo: action.payload };

    default:
      return state;
  }
}

export function setEventGuestCounts(counts) {
  return {
    type: SET_EVENT_GUEST_COUNTS,
    payload: counts,
  };
}

export function setEventTicketSales(amounts) {
  return {
    type: SET_EVENT_TICKET_SALES,
    payload: amounts,
  };
}

export function setEventDates(dates) {
  return {
    type: SET_EVENT_DATES,
    payload: dates,
  };
}

export function setEventSessions(sessions) {
  return {
    type: SET_EVENT_SESSIONS,
    payload: sessions,
  };
}

export function setEventTickets(tickets) {
  return {
    type: SET_EVENT_TICKETS,
    payload: tickets,
  };
}
export function setEventLabel(label) {
  return {
    type: SET_EVENT_LABEL,
    payload: label,
  };
}

export function setEventTicket(ticket) {
  return {
    type: SET_EVENT_TICKET,
    payload: ticket,
  };
}

export function setEventTicketPackages(ticketPackages) {
  return {
    type: SET_EVENT_TICKET_PACKAGES,
    payload: ticketPackages,
  };
}

export function setEventTicketPackage(ticketPackage) {
  return {
    type: SET_EVENT_TICKET_PACKAGE,
    payload: ticketPackage,
  };
}

export function setEventTicketTypeSessions(typeSessions) {
  return {
    type: SET_EVENT_TICKET_TYPE_SESSIONS,
    payload: typeSessions,
  };
}

export function setEventDataFields(fields) {
  return {
    type: SET_EVENT_DATA_FIELDS,
    payload: fields,
  };
}

export function setType(ticketsType) {
  return {
    type: SET_TICKET_TYPE,
    payload: ticketsType,
  };
}
export function setEvents(events) {
  return {
    type: SET_EVENTS,
    payload: events,
  };
}

export function setPaymentInfo(paymentInfo) {
  return {
    type: SET_PAYMENT_INFO,
    payload: paymentInfo,
  };
}

export function getEvents(params) {
  return dispatch => new Promise(async (resolve, reject) => {
    try {
      const request = getRequest(dispatch);

      const url = '/events';
      const response = await request.get(url, { params });

      const events = response.data;
      const meta = {
        count: Number(response.headers['juven-count']) || 0,
        from: Number(response.headers['juven-from']) || 0,
        to: Number(response.headers['juven-to']) || 0,
      };

      dispatch(setEvents(events));
      dispatch(setMeta(meta));

      return resolve(events);
    } catch (e) {
      const error = handleApiError(e);
      return reject(error);
    }
  });
}

export function removeEvent(eId) {
  return dispatch => new Promise(async (resolve, reject) => {
    try {
      const request = getRequest(dispatch);

      const url = `/events/${eId}`;
      await request.delete(url);

      return resolve();
    } catch (e) {
      const error = handleApiError(e);
      return reject(error);
    }
  });
}

export function updateEventBracket(eId, data) {
  return dispatch => new Promise(async (resolve, reject) => {
    try {
      const request = getRequest(dispatch);

      const url = `/events/${eId}/bracket`;
      await request.post(url, data);

      return resolve();
    } catch (e) {
      const error = handleApiError(e);
      return reject(error);
    }
  });
}

export function getEventGuestCounts(eId) {
  return dispatch => new Promise(async (resolve) => {
    try {
      const request = getRequest(dispatch);

      const url = `/events/${eId}/guests/counts`;
      const response = await request.get(url);

      const counts = response.data;
      dispatch(setEventGuestCounts(counts));

      return resolve(counts);
    } catch (e) {
      handleApiError(e);
      return resolve();
    }
  });
}

export function bulkAddEventGuests(eId, data) {
  return dispatch => new Promise(async (resolve, reject) => {
    try {
      const request = getRequest(dispatch);

      const url = `/events/${eId}/guests/bulk`;
      await request.post(url, data);

      return resolve();
    } catch (e) {
      const error = handleApiError(e);
      return reject(error);
    }
  });
}

export function getEventTicketSales(eId) {
  return dispatch => new Promise(async (resolve) => {
    try {
      const request = getRequest(dispatch);

      const url = `/events/${eId}/guests/sales`;
      const response = await request.get(url);

      const amounts = response.data;
      dispatch(setEventTicketSales(amounts));

      return resolve(amounts);
    } catch (e) {
      handleApiError(e);
      return resolve();
    }
  });
}

export function getEventDates(eId) {
  return dispatch => new Promise(async (resolve) => {
    try {
      const request = getRequest(dispatch);

      const url = `/events/${eId}/dates`;
      const response = await request.get(url);

      const dates = response.data;
      dispatch(setEventDates(dates));

      return resolve();
    } catch (e) {
      handleApiError(e);
      return resolve();
    }
  });
}

export function createEventDate(eId, data) {
  return dispatch => new Promise(async (resolve, reject) => {
    try {
      const request = getRequest(dispatch);

      const url = `/events/${eId}/dates`;
      const response = await request.post(url, data);

      const date = response.data;
      return resolve(date);
    } catch (e) {
      const error = handleApiError(e);
      return reject(error);
    }
  });
}

export function updateEventDate(eId, daId, data) {
  return dispatch => new Promise(async (resolve, reject) => {
    try {
      const request = getRequest(dispatch);

      const url = `/events/${eId}/dates/${daId}`;
      await request.put(url, data);

      return resolve();
    } catch (e) {
      const error = handleApiError(e);
      return reject(error);
    }
  });
}

export function removeEventDate(eId, daId) {
  return dispatch => new Promise(async (resolve, reject) => {
    try {
      const request = getRequest(dispatch);

      const url = `/events/${eId}/dates/${daId}`;
      await request.delete(url);

      return resolve();
    } catch (e) {
      const error = handleApiError(e);
      return reject(error);
    }
  });
}

export function addEventDateTimeslot(eId, daId, data) {
  return dispatch => new Promise(async (resolve, reject) => {
    try {
      const request = getRequest(dispatch);

      const url = `/events/${eId}/dates/${daId}/timeslots`;
      const response = await request.post(url, data);

      const timeslot = response.data;
      return resolve(timeslot);
    } catch (e) {
      const error = handleApiError(e);
      return reject(error);
    }
  });
}

export function updateEventDateTimeslot(eId, daId, tmsId, data) {
  return dispatch => new Promise(async (resolve, reject) => {
    try {
      const request = getRequest(dispatch);

      const url = `/events/${eId}/dates/${daId}/timeslots/${tmsId}`;
      await request.put(url, data);

      return resolve();
    } catch (e) {
      const error = handleApiError(e);
      return reject(error);
    }
  });
}

export function removeEventDateTimeslot(eId, daId, tmsId) {
  return dispatch => new Promise(async (resolve) => {
    try {
      const request = getRequest(dispatch);

      const url = `/events/${eId}/dates/${daId}/timeslots/${tmsId}`;
      await request.delete(url);

      return resolve();
    } catch (e) {
      handleApiError(e);
      return resolve();
    }
  });
}

export function getEventSessions(eId) {
  return dispatch => new Promise(async (resolve) => {
    try {
      const request = getRequest(dispatch);

      const url = `/events/${eId}/dates/sessions`;
      const response = await request.get(url);

      const sessions = response.data;
      dispatch(setEventSessions(sessions));

      return resolve();
    } catch (e) {
      handleApiError(e);
      return resolve();
    }
  });
}

export function getEventTickets(eId) {
  return dispatch => new Promise(async (resolve) => {
    try {
      const request = getRequest(dispatch);

      const url = `/events/${eId}/tickets`;
      const response = await request.get(url);

      const tickets = response.data;
      dispatch(setEventTickets(tickets));

      return resolve(tickets);
    } catch (e) {
      handleApiError(e);
      return resolve();
    }
  });
}

export function getEventTicket(eId, tkId) {
  return dispatch => new Promise(async (resolve) => {
    try {
      const request = getRequest(dispatch);

      const url = `/events/${eId}/tickets/${tkId}`;
      const response = await request.get(url);

      const ticket = response.data;
      dispatch(setEventTicket(ticket));

      return resolve(ticket);
    } catch (e) {
      handleApiError(e);
      return resolve();
    }
  });
}

export function removeEventTicketsType(eId, tkId) {
  return dispatch => new Promise(async (resolve, reject) => {
    try {
      const request = getRequest(dispatch);

      const url = `/events/${eId}/tickets/${tkId}`;
      await request.delete(url);

      return resolve();
    } catch (e) {
      const error = handleApiError(e);
      return reject(error);
    }
  });
}

export function getEventTicketPackages(eId) {
  return dispatch => new Promise(async (resolve) => {
    try {
      const request = getRequest(dispatch);

      const url = `/events/${eId}/tickets/packages`;
      const response = await request.get(url);

      const ticketPackages = response.data;
      dispatch(setEventTicketPackages(ticketPackages));

      return resolve(ticketPackages);
    } catch (e) {
      handleApiError(e);
      return resolve();
    }
  });
}

export function getEventTicketPackage(eId, pkId) {
  return dispatch => new Promise(async (resolve) => {
    try {
      const request = getRequest(dispatch);

      const url = `/events/${eId}/tickets/packages/${pkId}`;
      const response = await request.get(url);

      const ticketPackage = response.data;
      dispatch(setEventTicketPackage(ticketPackage));

      return resolve(ticketPackage);
    } catch (e) {
      handleApiError(e);
      return resolve();
    }
  });
}

export function getEventTicketTypeSessions(eId, params) {
  return dispatch => new Promise(async (resolve) => {
    try {
      const request = getRequest(dispatch);

      const url = `/events/${eId}/tickets/sessions`;
      const response = await request.get(url, { params });

      const typeSessions = response.data;
      dispatch(setEventTicketTypeSessions(typeSessions));

      return resolve(typeSessions);
    } catch (e) {
      handleApiError(e);
      return resolve();
    }
  });
}

export function getPaymentInfo(eId, token) {
  return dispatch => new Promise(async (resolve, reject) => {
    try {
      const request = getRequest(dispatch);

      const url = `/events/${eId}/tickets/payment?token=${token}`;
      const response = await request.get(url);

      const paymentData = response.data;
      dispatch(setPaymentInfo(paymentData));
      return resolve();
    } catch (e) {
      const error = handleApiError(e);
      return reject(error);
    }
  });
}

export function purchaseReservedTickets(eId, data) {
  return dispatch => new Promise(async (resolve, reject) => {
    try {
      const request = getRequest(dispatch);

      const url = `/events/${eId}/tickets/payment`;
      const response = await request.post(url, data);

      const charge = response.data;
      return resolve(charge);
    } catch (e) {
      const error = handleApiError(e);
      return reject(error);
    }
  });
}

export function purchaseEventTickets(eId, data) {
  return dispatch => new Promise(async (resolve, reject) => {
    try {
      const request = getRequest(dispatch);

      const url = `/events/${eId}/tickets/purchase`;
      const response = await request.post(url, data);

      const charge = response.data;
      return resolve(charge);
    } catch (e) {
      const error = handleApiError(e);
      return reject(error);
    }
  });
}

export function reserveEventTickets(eId, data) {
  return dispatch => new Promise(async (resolve, reject) => {
    try {
      const request = getRequest(dispatch);

      const url = `/events/${eId}/tickets/reserve`;
      const response = await request.post(url, data);

      return resolve(response.data);
    } catch (e) {
      const error = handleApiError(e);
      return reject(error);
    }
  });
}

export function createEventTicket(eId, data) {
  return dispatch => new Promise(async (resolve, reject) => {
    try {
      const request = getRequest(dispatch);

      const url = `/events/${eId}/tickets`;
      const response = await request.post(url, data);

      const ticketType = response.data;
      return resolve(ticketType);
    } catch (e) {
      const error = handleApiError(e);
      return reject(error);
    }
  });
}

export function updateEventTicket(eId, tId, data) {
  return dispatch => new Promise(async (resolve, reject) => {
    try {
      const request = getRequest(dispatch);

      const url = `/events/${eId}/tickets/${tId}`;
      await request.put(url, data);

      return resolve();
    } catch (e) {
      const error = handleApiError(e);
      return reject(error);
    }
  });
}

export function removeEventTicket(eId, tId) {
  return dispatch => new Promise(async (resolve, reject) => {
    try {
      const request = getRequest(dispatch);

      const url = `/events/${eId}/tickets/${tId}`;
      await request.delete(url);

      return resolve();
    } catch (e) {
      const error = handleApiError(e);
      return reject(error);
    }
  });
}

export function createEventTicketPackage(eId, data) {
  return dispatch => new Promise(async (resolve, reject) => {
    try {
      const request = getRequest(dispatch);

      const url = `/events/${eId}/tickets/packages`;
      await request.post(url, data);

      return resolve();
    } catch (e) {
      const error = handleApiError(e);
      return reject(error);
    }
  });
}

export function updateEventTicketPackage(eId, pId, data) {
  return dispatch => new Promise(async (resolve, reject) => {
    try {
      const request = getRequest(dispatch);

      const url = `/events/${eId}/tickets/packages/${pId}`;
      await request.put(url, data);

      return resolve();
    } catch (e) {
      const error = handleApiError(e);
      return reject(error);
    }
  });
}

export function removeEventTicketPackage(eId, pId) {
  return dispatch => new Promise(async (resolve, reject) => {
    try {
      const request = getRequest(dispatch);

      const url = `/events/${eId}/tickets/packages/${pId}`;
      await request.delete(url);

      return resolve();
    } catch (e) {
      const error = handleApiError(e);
      return reject(error);
    }
  });
}

export function sendEventTicketEmails(eId, data) {
  return dispatch => new Promise(async (resolve, reject) => {
    try {
      const request = getRequest(dispatch);

      const url = `/events/${eId}/emails/tickets/send`;
      await request.post(url, data);

      return resolve();
    } catch (e) {
      const error = handleApiError(e);
      return reject(error);
    }
  });
}

export function getEventDataFields(eId) {
  return dispatch => new Promise(async (resolve) => {
    try {
      const request = getRequest(dispatch);

      const url = `/events/${eId}/data_fields`;
      const response = await request.get(url);

      const fields = response.data;
      dispatch(setEventDataFields(fields));

      return resolve(fields);
    } catch (e) {
      handleApiError(e);
      return resolve();
    }
  });
}

export function createEventDataField(eId, data) {
  return dispatch => new Promise(async (resolve, reject) => {
    try {
      const request = getRequest(dispatch);

      const url = `/events/${eId}/data_fields`;
      await request.post(url, data);

      return resolve();
    } catch (e) {
      const error = handleApiError(e);
      return reject(error);
    }
  });
}

export function updateEventDataField(eId, fiId, data) {
  return dispatch => new Promise(async (resolve, reject) => {
    try {
      const request = getRequest(dispatch);

      const url = `/events/${eId}/data_fields/${fiId}`;
      await request.put(url, data);

      return resolve();
    } catch (e) {
      const error = handleApiError(e);
      return reject(error);
    }
  });
}

export function removeEventDataField(eId, fiId) {
  return dispatch => new Promise(async (resolve, reject) => {
    try {
      const request = getRequest(dispatch);

      const url = `/events/${eId}/data_fields/${fiId}`;
      await request.delete(url);

      return resolve();
    } catch (e) {
      const error = handleApiError(e);
      return reject(error);
    }
  });
}

export function setTicketType(tickets, tkId) {
  return dispatch => new Promise(async (resolve) => {
    try {
      const selectedType = tickets.find(item => item.id === Number(tkId));
      dispatch(setType(selectedType));
      return resolve();
    } catch (e) {
      handleApiError(e);
      return resolve();
    }
  });
}

export function getEventLabel(eId) {
  return dispatch => new Promise(async (resolve) => {
    try {
      const request = getRequest(dispatch);

      const url = `/events/${eId}/label`;
      const response = await request.get(url);

      const label = response.data || {};
      dispatch(setEventLabel(label));

      return resolve(label);
    } catch (e) {
      handleApiError(e);
      return resolve();
    }
  });
}

export function updateEventLabel(eId, data) {
  return dispatch => new Promise(async (resolve, reject) => {
    try {
      const request = getRequest(dispatch);

      const url = `/events/${eId}/label`;
      await request.put(url, data);

      return resolve();
    } catch (e) {
      const error = handleApiError(e);
      return reject(error);
    }
  });
}
