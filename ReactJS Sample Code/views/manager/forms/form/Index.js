import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { withRouter } from 'react-router';
import { Route, Switch } from 'react-router-dom';
import { Helmet } from 'react-helmet';

import Header from '../../../../components/header/Header';
import FormNavBar from '../../../../components/manager/navigation/FormNavBar';

import FormEditView from './Edit';
import FormOverviewView from './Overview';
import FormsRespondentsView from './Respondents';

import * as OrganizationActionCreators from '../../../../redux/organization';
import { t as translate } from '../../../../../i18n';

const t = a => translate(a, 'manager/forms/index');

const mapStateToProps = state => ({
  organization: state.manager.organization,
  forms: state.organization.forms,
  form: state.organization.form,
});
const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators(OrganizationActionCreators, dispatch),
});

class FormIndexView extends Component {
  async componentDidMount () {
    try {
      const { actions, match, history } = this.props;
      const { oId, foId } = match.params;

      const promises = [
        actions.getOrganizationForms(oId),
        actions.getOrganizationForm(oId, foId),
      ];
      await Promise.all(promises);

      const { organization, forms, form } = this.props;
      if (!form || !form.id || !forms.find(f => f.id === form.id)) {
        return history.push(`/${ organization.id }/forms`);
      }
    } catch (e) {
      console.error('ERROR: FormIndexView', e);
    }
  }

  render () {
    const { location, organization, form } = this.props;
    const { pathname } = location;

    if (!form) return null;
    const navPages = [
      {
        title: t('Forms'),
        url: `/${ organization.id }/forms`,
      },
      {
        title: form.title,
        url: `/${ organization.id }/forms/${ form.id }`,
      },
    ];
    if (pathname.indexOf(`/${ form.id }/edit`) > 0) {
      navPages.push({
        title: t('Edit'),
      });
    } else if (pathname.indexOf(`/${ form.id }/respondents`) > 0) {
      navPages.push({
        title: t('Respondents'),
      });
    } else {
      delete navPages[1].url;
    }

    const pageTitle = navPages
      .map(p => p.title)
      .reverse()
      .concat('Juven Manager')
      .join(' - ');

    return (
      <div>
        <Helmet>
          <title>{ pageTitle }</title>
          <meta property='og:title' content={ pageTitle } />
        </Helmet>
        <Header navPages={ navPages } />
        <FormNavBar organization={ organization } form={ form } />
        <Switch>
          <Route exact path='/:oId/forms/:foId' component={ FormOverviewView } />
          <Route exact path='/:oId/forms/:foId/edit' component={ FormEditView } />
          <Route exact path='/:oId/forms/:foId/respondents' component={ FormsRespondentsView } />
        </Switch>
      </div>
    );
  }
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(FormIndexView));
