import React, { Component } from 'react';
import styled from 'styled-components';
import { Editor } from 'slate-react';
import Html from 'slate-html-serializer';

import {
  BOLD,
  ITALIC,
  UNDERLINED,
  A,
  P,
  H5,
  QUOTE,
  OL,
  UL,
  LI,
} from './constants';

import Bold from './tags/Bold';
import Italic from './tags/Italic';
import LinkTag from './tags/Link';
import OrderedList from './tags/OrderedList';
import ListItem from './tags/ListItem';
import BulletedList from './tags/BulletedList';
import BlockQuote from './tags/BlockQuote';

import rules from './rules';

import Typographical from './toolbar/Typographical';
import LinkTool from './toolbar/Link';
import Lists from './toolbar/Lists';
import FontStyle from './toolbar/FontStyle';
import Label from '../Label';

const html = new Html({ rules });

const Wrapper = styled.div`
  width: 100%;
  margin-top: 20px;
  margin-bottom: 20px;
  margin-right: 20px;
  ${ props => props.customCss || '' }
`;

const EditorWrapper = styled.div`
  border-radius: 5px;
  background-color: ${ props => props.theme.colors.white };
  border: 1px solid ${ props => props.theme.colors.deadGray };
  ${ props => props.customCss || '' }
`;

const ToolbarWrapper = styled.div`
  height: 45px;
  border-bottom: 1px solid ${ props => props.theme.colors.deadGray };
`;

const EditorComponent = styled.div`
  height: 145px;
  padding: 20px 30px;

  .editor {
    line-height: normal;
    overflow-y: scroll;
    height: 110px;
    color: ${ props => props.theme.colors.darkGray };
    ${ props => props.insideCss || '' }
  }
  ${ props => props.customCss || '' }
`;

function wrapLink (editor, href) {
  editor.wrapInline({
    type: A,
    data: { href },
  });
  editor.moveToEnd();
}

function unwrapLink (editor) {
  editor.unwrapInline(A);
}

class RichTextEditor extends Component {
  constructor ({ value }) {
    super();

    this.state = {
      value: html.deserialize(value || ''),
    };
  }

  componentWillReceiveProps (props) {
    const newValue = props.value;
    const { value } = this.state;

    if (newValue !== html.serialize(value || '')) {
      this.setState({ value: html.deserialize(newValue || '') });
    }
  }

  ref = (editor) => {
    this.editor = editor;
  };

  onChange = ({ value }) => {
    this.setState({ value });
    const { action, field } = this.props;
    const newValue = value === '' ? null : html.serialize(value);

    if (action) action(field, newValue);
  }

  hasMark = (type) => {
    const { value } = this.state;
    return value.activeMarks.some(mark => mark.type === type);
  }

  hasLinks = () => {
    const { value } = this.state;
    return value.inlines.some(inline => inline.type === A);
  }

  hasBlock = (type) => {
    const { value } = this.state;
    return value.blocks.some(node => node.type === type);
  }

  hasBlockList = (type) => {
    let isActive = this.hasBlock(type);

    if ([OL, UL].includes(type)) {
      const { value: { document, blocks } } = this.state;

      if (blocks.size > 0) {
        const parent = document.getParent(blocks.first().key);
        isActive = this.hasBlock(LI) && parent && parent.type === type;
      }
    }

    return isActive;
  }

  toggleMark = (type) => {
    this.editor.toggleMark(type);
  };

  toggleLink = () => {
    const { editor } = this;
    const { value } = editor;
    const hasLinks = this.hasLinks();

    if (hasLinks) {
      editor.command(unwrapLink);
    } else if (value.selection.isExpanded) {
      // eslint-disable-next-line
      const href = window.prompt('Enter the URL of the link:');
      if (href === null) {
        return null;
      }
      editor.command(wrapLink, href);
    } else {
      // eslint-disable-next-line
      const href = window.prompt('Enter the URL of the link:');
      if (href === null) {
        return null;
      }
      // eslint-disable-next-line
      const text = window.prompt('Enter the text for the link:');
      if (text === null) {
        return null;
      }
      editor
        .insertText(text)
        .moveFocusBackward(text.length)
        .command(wrapLink, href);
    }
  };

  toggleBlock = (type) => {
    const { editor } = this;
    const { value } = editor;
    const { document } = value;

    // Handle everything but list buttons.
    if (type !== UL && type !== OL) {
      const isActive = this.hasBlock(type);
      const isList = this.hasBlock(LI);

      if (isList) {
        editor
          .setBlocks(isActive ? P : type)
          .unwrapBlock(UL)
          .unwrapBlock(OL);
      } else {
        editor.setBlocks(isActive ? P : type);
      }
    } else {
      // Handle the extra wrapping required for list buttons.
      const isList = this.hasBlock(LI);
      const isType = value.blocks.some(block => (
        !!document.getClosest(block.key, parent => parent.type === type)
      ));

      if (isList && isType) {
        editor
          .setBlocks(P)
          .unwrapBlock(UL)
          .unwrapBlock(OL);
      } else if (isList) {
        editor
          .unwrapBlock(
            type === UL ? OL : UL,
          ).wrapBlock(type);
      } else {
        editor.setBlocks(LI).wrapBlock(type);
      }
    }
  }

  renderMark = (props, editor, next) => {
    const { mark, children, attributes } = props;
    switch (mark.type) {
      case BOLD:
        return <Bold { ...props } />;
      case ITALIC:
        return <Italic { ...props } />;
      case UNDERLINED:
        return <u { ...attributes }>{ children }</u>;
      default: {
        return next();
      }
    }
  }

  renderNode = (props, editor, next) => {
    switch (props.node.type) {
      case A:
        return <LinkTag { ...props } />;
      case H5:
        return <h5>{ props.children }</h5>;
      case QUOTE:
        return <BlockQuote { ...props } />;
      case OL:
        return <OrderedList { ...props } />;
      case LI:
        return <ListItem { ...props } />;
      case UL:
        return <BulletedList { ...props } />;
      default: {
        return next();
      }
    }
  }

  render () {
    const {
      label,
      required,
      errorMessage,
      field,
      placeholder,
      wrapperCss,
      editorWrapperCss,
      editorComponentCss,
      insideCss,
    } = this.props;
    const { value } = this.state;
    return (
      <Wrapper customCss={ wrapperCss }>
        { label ? (
          <Label
            field={ field }
            label={ label }
            required={ required }
            invalid={ !!errorMessage }
            customCss='margin-bottom: 20px;'
          />
        ) : null }
        <EditorWrapper customCss={ editorWrapperCss }>
          <ToolbarWrapper>
            <FontStyle onChange={ this.toggleBlock } selected={ this.hasBlock } />
            <Typographical onClick={ this.toggleMark } selected={ this.hasMark } />
            <LinkTool onClick={ this.toggleLink } selected={ this.hasLinks } />
            <Lists onClick={ this.toggleBlock } selected={ this.hasBlockList } />
          </ToolbarWrapper>
          <EditorComponent customCss={ editorComponentCss } insideCss={ insideCss }>
            <Editor
              spellCheck
              className='editor'
              placeholder={ placeholder }
              ref={ this.ref }
              value={ value || '' }
              onChange={ this.onChange }
              renderMark={ this.renderMark }
              renderNode={ this.renderNode }
            />
          </EditorComponent>
        </EditorWrapper>
      </Wrapper>
    );
  }
}

export default RichTextEditor;
