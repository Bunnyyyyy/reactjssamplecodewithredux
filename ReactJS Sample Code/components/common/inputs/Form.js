import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import styled from 'styled-components';
import moment from 'moment';
import ReactTooltip from 'react-tooltip';

import Input from '../Input';
import ColorPicker from '../ColorPicker';
import Select from './Select';
import MultiSelect from '../MultiSelect';
import MultiInput from './MultiInput';
import Checkbox from '../Checkbox';
import Options from './Options';
import DatePicker from '../DatePicker';
import TimePicker from '../TimePicker';
import PhoneInput from './PhoneInput';
import JuvenIdInput from '../JuvenIdInput';
import ImageUploadV2 from '../ImageUploadV2';
import FileUpload from '../FileUpload';
import RichTextEditor from './RichTextEditor';
import Button from '../Button';
import * as MeActionCreators from '../../../redux/me';

import i18n from '../../../../i18n';

const t = a => i18n.t([`common:${ a }`]);

export const Title = styled.h5`
  flex: 100% 0 0;
  letter-spacing: 0.5px;
  margin-bottom: 15px;
  ${ props => props.customCss || '' };
`;

export const Separator = styled.div`
  border-top: 1px solid ${ props => props.theme.colors.lightGray };
  width: calc(100% + 40px);
  height: 20px;
  margin: 20px -40px 0 -30px;
`;

export const FormStyled = styled.div`
  flex-grow: 1;
  width: 100%;
  display: flex;
  flex-wrap: wrap;
  margin-right: ${ props => (props.customCss && props.customCss.marginRight ? props.customCss.marginRight : '-20px') };

  ${ props => props.customCss || '' };
`;

export const CustomDiv = styled.div`
  ${ props => props.customCss || '' };
`;

export const SectionDiv = styled.div`
  display: flex;
  flex-wrap: wrap;
  ${ props => props.customCss || '' };
`;

export const ColorPickerDiv = styled.div`
  width: 100%;
  margin: 15px 0;
  ${ props => props.customCss || '' };
`;

export const AddButton = styled(Button)`
  text-align: left;
  color: ${ props => props.theme.colors.darkBlue };
  margin: ${ props => (props.empty ? '0' : '10px 0 0 0') };
  font-size: ${ props => (props.empty ? '20px' : '15px') };

  & span {
    display: inline-block;
    text-align: center;
    height: 25px;
    width: 25px;
    border-radius: 12.5px;
    line-height: 25px;
    background-color: ${ props => props.theme.colors.darkBlue };
    margin-right: 10px;

    & i {
      font-size: 15px;
      line-height: 25px;
      color: white;
      margin-right: 0;
    }
  }

  &:hover,
  &:active {
    color: ${ props => props.theme.colors.mainBlue };

    & span {
      background-color: ${ props => props.theme.colors.mainBlue };
    }
  }
`;

export const RemoveButton = styled(Button)`
  flex-shrink: 0;
  height: 25px;
  width: 25px;
  border-radius: 12.5px;
  line-height: 25px;
  opacity: 0.25;
  margin: 0 10px 0 -10px;

  &:hover,
  &:active {
    opacity: 1;
    color: ${ props => props.theme.colors.mainRed };
  }

  & i {
    margin-right: 0;
  }
`;

const InfoIcon = styled.i`
  margin-left: 10px;
  margin-top: 4px;
  width: 16px;
  height: 16px;
  color: ${ props => props.theme.colors.mainGray };
`;

const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators(MeActionCreators, dispatch),
});

class Form extends Component {
  constructor (props) {
    super();

    const { fields, onInit } = props;

    this.state = {
      _updatedFields: {},
      _iceredIdAvailable: null,
    };

    let allFields = fields;

    // recursively add all nested section fields
    const addSectionFields = (f) => {
      if (f.type !== 'section' || !f.fields || !f.fields.length) return;

      allFields = allFields.concat(f.fields.filter(f2 => f2.type !== 'section'));
      f.fields.filter(f2 => f2.type === 'section').forEach(addSectionFields);
    };
    fields.filter(f => f.type === 'section').forEach(addSectionFields);

    allFields.forEach((f) => {
      const readOnly = f.readOnly && typeof f.readOnly !== 'function';

      if (!f.field || readOnly || f.type === 'image') return;
      if (f.field && f.default !== undefined) {
        this.state[f.field] = typeof f.default === 'function' ? f.default() : f.default;

        if (
          (f.type === 'multiSelect' || f.type === 'multiInput')
          // eslint-disable-next-line
          && !this.state[f.field]
        ) {
          this.state[f.field] = [];
        }
      } else {
        this.state[f.field] = f.type === 'checkbox' ? false : null;
      }
    });

    if (onInit) onInit(this.state);
  }

  componentDidMount () {
    this.autofillFields();
  }

  // hack to enable its parent to make the Form reset
  componentWillReceiveProps (nextProps) {
    const { reset } = this.props;
    if (nextProps.reset === true && reset !== true) {
      this.resetDefaultState(nextProps);
    }
  }

  onUpdatedState () {
    const { fields, onUpdate } = this.props;
    const updateState = Object.assign({}, this.state);
    delete updateState._updatedFields;

    // remove invisible fields
    fields
      .filter(
        f => typeof f.condition === 'function' && !f.condition(updateState),
      )
      .forEach((f) => {
        updateState[f.field] = null;
        this.setState({ [f.field]: null });
      });

    onUpdate(updateState);
    this.autofillFields();
  }

  onCustomChangeState (stateUpdate) {
    this.setState(stateUpdate, this.onUpdatedState);
  }

  getFieldComponent ({
    value, item, key, pos, errorMessage, ...fieldProps
  }) {
    const {
      name, reset, bottomModal, additionalData = {},
    } = this.props;
    const { _iceredIdAvailable } = this.state;
    const {
      type,
      fields = [],
      field,
      defaultDate,
      placeholder,
      renderComponent,
      required,
      getErrorMessage,
      text,
      condition,
      hidden,
      customCss = '',
      readOnly,
      ...props
    } = fieldProps.field;

    const flex = fieldProps.field.flex && typeof fieldProps.field.flex === 'function'
      ? fieldProps.field.flex(this.state)
      : fieldProps.field.flex;
    const flexCss = `
      flex: ${ (typeof flex === 'object' ? flex.flex : flex) || '0 0 100%' };
      @media (max-width: 900px) {
        flex: 0 0 100% !important;
      }
    `;

    const cssString = typeof customCss === 'function' ? customCss(this.state) : customCss;
    const fieldCss = cssString + flexCss;

    props.readOnly = typeof readOnly === 'function' ? readOnly(this.state, item) : readOnly;

    if (typeof condition === 'function' && !condition(this.state)) {
      return null;
    }

    if (type === 'section') {
      return (
        <SectionDiv key={ `section-${ key }` } customCss={ fieldCss }>
          { fields.map(this.getValidatedFieldComponent.bind(this)) }
        </SectionDiv>
      );
    } if (type === 'title') {
      return (
        <Title key={ `title-${ key }` } customCss={ fieldCss }>
          { text }
        </Title>
      );
    } if (type === 'separator') {
      return <Separator key={ `separator-${ key }` } />;
    } if (type === 'select') {
      return (
        <Select
          key={ `select-${ key }` }
          plain={ props.plain !== undefined || true }
          fullWidth
          underlined
          bottomModal={ bottomModal }
          field={ field }
          value={ value }
          placeholder={ placeholder }
          required={ required }
          onChange={ v => this.updateField(field, v) }
          customCss={ `
            min-width: auto;
            padding-right: 20px;
            margin-right: 0;
            ${ fieldCss || '' };
          ` }
          errorMessage={ errorMessage }
          { ...props }
        />
      );
    } if (type === 'multiSelect') {
      return (
        <MultiSelect
          key={ `multi-select-${ key }` }
          underline
          fullWidth
          field={ field }
          value={ value || [] }
          placeholder={ placeholder }
          onChange={ v => this.updateField(field, v) }
          customCss={ `
            min-width: auto;
            padding-right: 20px;
            margin-right: 0;
            ${ flexCss } ${ props.customCss || '' };
          ` }
          errorMessage={ errorMessage }
          { ...props }
        />
      );
    } if (type === 'multiInput') {
      return (
        <MultiInput
          key={ `multi-input-${ key }` }
          underline
          fullWidth
          field={ field }
          value={ value || [] }
          placeholder={ placeholder }
          onChange={ v => this.updateField(field, v) }
          customCss={ `
            min-width: auto;
            padding-right: 20px;
            margin-right: 0;
            ${ flexCss } ${ props.customCss || '' };
          ` }
          errorMessage={ errorMessage }
          { ...props }
        />
      );
    } if (type === 'options') {
      return (
        <Options
          field={ field }
          key={ `options-${ key }` }
          value={ value }
          required={ required }
          onChange={ v => this.updateField(field, v) }
          customCss={ `
            padding-right: 20px;
            margin-right: 0;
            ${ fieldCss || '' };
          ` }
          errorMessage={ errorMessage }
          { ...props }
        />
      );
    } if (type === 'checkbox') {
      // need to specify id for Checkbox due to internal workings of Checkbox
      return (
        <Checkbox
          key={ `checkbox-${ key }` }
          id={ name ? `${ name }-${ field }` : null }
          field={ field }
          required={ required }
          checked={ value }
          onChange={ v => this.updateField(field, v) }
          customCss={ fieldCss }
          label={ fieldProps.field.label }
          errorMessage={ errorMessage }
          { ...props }
        >
          { fieldProps.field.tooltip
            ? [
              <InfoIcon
                className='fas fa-info-circle'
                data-tip={ fieldProps.field.tooltip }
                data-place='right'
                data-multiline
              />,
              <ReactTooltip />,
            ]
            : null }
        </Checkbox>
      );
    } if (type === 'date') {
      return (
        <DatePicker
          key={ `date-${ key }` }
          fullWidth
          field={ field }
          defaultDate={ defaultDate ? defaultDate(this.state) : null }
          date={ value }
          required={ required }
          placeholder={ placeholder }
          onChange={ (v, timezone) => this.changeDate(field, v, timezone)
          }
          customCss={ `
            padding-right: 20px;
            margin-right: 0;
            ${ fieldCss || '' };
          ` }
          errorMessage={ errorMessage }
          { ...props }
        />
      );
    } if (type === 'time') {
      return (
        <TimePicker
          fullWidth
          key={ `time-${ key }` }
          field={ field }
          defaultDate={ defaultDate ? defaultDate(this.state) : null }
          date={ value }
          required={ required }
          onChange={ (dateObj, defaultDate2, timezone) => {
            this.changeTime(field, dateObj, defaultDate2, timezone);
          } }
          customCss={ `
            padding-right: 20px;
            margin-right: 0;
            ${ fieldCss || '' };
          ` }
          errorMessage={ errorMessage }
          { ...props }
        />
      );
    } if (type === 'juvenId') {
      return (
        <JuvenIdInput
          key={ `juvenId-${ key }` }
          fullWidth
          field={ field }
          value={ value }
          error={ _iceredIdAvailable === false ? true : null }
          success={ _iceredIdAvailable ? true : null }
          onChange={ this.changeJuvenId.bind(this) }
          customCss={ `
            padding-right: 20px;
            margin-right: 0;
            ${ fieldCss || '' };
          ` }
          { ...props }
        />
      );
    } if (type === 'input') {
      return (
        <Input
          key={ `input-${ key }` }
          fullWidth
          field={ field }
          value={ value }
          required={ required }
          placeholder={ placeholder }
          onChange={ this.updateField.bind(this) }
          customCss={ `
            padding-right: 20px;
            margin-right: 0;
            ${ fieldCss || '' };
          ` }
          errorMessage={ errorMessage }
          { ...props }
        />
      );
    } if (type === 'phone') {
      return (
        <PhoneInput
          key={ `phone-${ key }` }
          fullWidth
          field={ field }
          value={ value }
          required={ required }
          placeholder={ placeholder }
          onChange={ (_, v) => this.updateField(field, v) }
          customCss={ `
            width: 100%;
            padding-right: 20px;
            margin-right: 0;
            ${ fieldCss || '' };
          ` }
          errorMessage={ errorMessage }
          { ...props }
        />
      );
    } if (type === 'image') {
      return (
        <ImageUploadV2
          key={ `image-${ key }` }
          reset={ reset }
          image={ fieldProps.field.default }
          field={ field }
          value={ value }
          required={ required }
          onChange={ this.changeFile.bind(this) }
          customCss={ `
            padding-right: 20px;
            ${ fieldCss || '' };
          ` }
          errorMessage={ errorMessage }
          { ...props }
        />
      );
    } if (type === 'file') {
      return (
        <FileUpload
          key={ `file-${ key }` }
          reset={ reset }
          file={ fieldProps.field.default }
          field={ field }
          value={ value }
          required={ required }
          onUpdate={ file => this.changeFile(field, file) }
          customCss={ `
            padding-right: 20px;
            ${ fieldCss || '' };
          ` }
          errorMessage={ errorMessage }
          { ...props }
        />
      );
    } if (type === 'custom') {
      // HACK: use additionalData to allow passing in external data
      const formState = Object.assign({}, additionalData, this.state);
      return (
        <CustomDiv key={ `custom-${ key }` } customCss={ fieldCss }>
          { renderComponent(
            value,
            formState,
            this.onCustomChangeState.bind(this),
          ) }
        </CustomDiv>
      );
    } if (type === 'rte') {
      return (
        <RichTextEditor
          key={ `rte-${ key }` }
          action={ (_, v) => this.updateField(field, v) }
          field={ field }
          value={ value }
          required={ required }
          placeholder={ placeholder }
          errorMessage={ errorMessage }
          customCss={ fieldCss }
          { ...props }
        />
      );
    } if (type === 'color') {
      return (
        <ColorPickerDiv key={ `color-${ key }` } customCss={ flexCss }>
          <ColorPicker
            field={ field }
            value={ value }
            onChange={ v => this.updateField(field, v) }
            customCss={ customCss }
            { ...props }
          />
        </ColorPickerDiv>
      );
    }
  }

  getValidatedFieldComponent (f, i) {
    const { item, validate } = this.props;
    const {
      type,
      field,
      getValue,
      required,
      getErrorMessage,
      hidden,
      condition,
    } = f;

    if (hidden) return null;
    if (typeof condition === 'function') {
      if (!condition(this.state)) return null;
    }

    let defaultErrorMessage = t('Please fill in this field');
    if (type === 'select') {
      defaultErrorMessage = t('Please select an option');
    } else if (type === 'options') {
      defaultErrorMessage = t('Please select at least one option');
    }

    const readOnly = typeof f.readOnly === 'function'
      ? f.readOnly(this.state, item)
      : f.readOnly;

    let errorMessage = null;
    if (validate) {
      if (
        required
        // eslint-disable-next-line
        && (this.state[field] === null || this.state[field] === undefined)
      ) {
        errorMessage = defaultErrorMessage;
      } else {
        const customErrorMessage = getErrorMessage
          // eslint-disable-next-line
          && getErrorMessage(this.state, this.state[field], f.label);

        if (customErrorMessage) errorMessage = customErrorMessage;
      }
    }

    let value;
    if (typeof getValue === 'function') {
      value = getValue(this.state);
    } else if (readOnly) {
      value = f.default;
    } else {
      // eslint-disable-next-line
      value = this.state[field];
    }

    return this.getFieldComponent({
      field: f,
      item,
      value,
      errorMessage,
      key: `${ f.field }-${ i }`,
    });
  }

  // eslint-disable-next-line
  isEmpty = field => !this.state[field];

  resetDefaultState (props) {
    const { fields = [] } = props || this.props;

    const newState = {
      _updatedFields: {},
    };

    let allFields = fields;
    // recursively add all nested section fields
    const addSectionFields = (f) => {
      if (f.type !== 'section' || !f.fields || !f.fields.length) return;

      allFields = allFields.concat(f.fields.filter(f2 => f2.type !== 'section'));
      f.fields.filter(f2 => f2.type === 'section').forEach(addSectionFields);
    };
    fields.filter(f => f.type === 'section').forEach(addSectionFields);

    allFields.forEach((f) => {
      const readOnly = f.readOnly && typeof f.readOnly !== 'function';

      if (!f.field || readOnly || f.type === 'image') return;
      if (f.field && f.default !== undefined) {
        newState[f.field] = typeof f.default === 'function' ? f.default() : f.default;

        if (
          (f.type === 'multiSelect' || f.type === 'multiInput')
          && !newState[f.field]
        ) {
          newState[f.field] = [];
        }
      } else {
        newState[f.field] = f.type === 'multiSelect' || f.type === 'multiInput'
          ? []
          : f.type === 'checkbox' ? false : null;
      }
    });

    this.setState(newState, this.autofillFields);
  }

  updateField (field, value) {
    const { _updatedFields } = this.state;
    _updatedFields[field] = true;
    const stateUpdate = {
      [field]: value,
      _updatedFields,
    };

    this.setState(stateUpdate, this.onUpdatedState);
  }

  autofillFields () {
    const { fields } = this.props;
    const { _updatedFields } = this.state;

    fields.forEach((f) => {
      const { type, field, autofill } = f;

      if (autofill && !_updatedFields[field]) {
        // eslint-disable-next-line
        const value = autofill(this.state);
        this.setState({
          [field]: value,
        });

        if (type === 'juvenId') {
          this.checkIceredId(value);
        }
      }
    });
  }

  checkIceredId (id) {
    const { actions, onUpdate } = this.props;

    clearTimeout(this.iceredIdTimer);
    this.iceredIdTimer = setTimeout(() => {
      actions
        .checkIceredId(id)
        .then((isAvailable) => {
          this.setState({ _iceredIdAvailable: isAvailable }, () => {
            const updateState = Object.assign({}, this.state);
            delete updateState._updatedFields;

            onUpdate(updateState);
          });
        })
        .catch(console.error);
    }, 300);
  }

  changeJuvenId (field, value) {
    this.updateField(field, value);
    this.checkIceredId(value);
  }

  changeDate (field, date, timezone) {
    // eslint-disable-next-line
    const oldValue = this.state[field];
    let newValue;
    if (date) {
      let dateObj;
      if (oldValue) {
        dateObj = timezone
          ? moment.tz(oldValue, timezone)
          : moment(oldValue).local();

        dateObj
          .year(date.year())
          .month(date.month())
          .date(date.date());
      } else {
        dateObj = date
          .hour(0)
          .minute(0)
          .second(0)
          .millisecond(0);
      }

      newValue = dateObj.toDate();
    } else {
      newValue = null;
    }
    this.updateField(field, newValue);
  }

  changeTime (field, date, defaultDate, timezone) {
    // eslint-disable-next-line
    const oldValue = this.state[field];

    let dateObj;
    if (oldValue) {
      dateObj = timezone
        ? moment.tz(oldValue, timezone)
        : moment(oldValue).local();
    } else if (defaultDate) {
      dateObj = timezone
        ? moment.tz(defaultDate, timezone)
        : moment(defaultDate).local();
    } else {
      dateObj = timezone ? moment.tz(timezone) : moment().local();

      dateObj.second(0).millisecond(0);
    }
    dateObj.hour(date.hour()).minute(date.minute());

    this.updateField(field, dateObj.toDate());
  }

  changeFile (field, file) {
    this.updateField(field, file);
  }


  render () {
    // need to specify name for Checkbox due to internal workings of Checkbox
    const { fields, customCss } = this.props;
    const Fields = fields.map(this.getValidatedFieldComponent.bind(this));

    return <FormStyled customCss={ customCss }>{ Fields }</FormStyled>;
  }
}

export default connect(null, mapDispatchToProps)(Form);
