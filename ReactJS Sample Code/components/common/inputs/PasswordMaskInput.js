import React, { Component } from 'react';
import styled from 'styled-components';

import Input from '../Input';

const Icon = styled.i`
  position: absolute;
  cursor: pointer;
  right: 0;
  bottom: 0 !important;
  line-height: 32px !important;
`;

class PasswordMaskInput extends Component {
  state = {
    isPasswordVisible: false,
  };

  toggleVisibility () {
    const { isPasswordVisible } = this.state;
    this.setState({ isPasswordVisible: !isPasswordVisible });
  }

  render () {
    const { children } = this.props;
    const { isPasswordVisible } = this.state;
    const icon = isPasswordVisible ? 'far fa-eye' : 'far fa-eye-slash';
    const rest = {
      ...this.props,
      type: isPasswordVisible ? 'text' : 'password',
    };

    return (
      <Input { ...rest }>
        <Icon className={ icon } onClick={ this.toggleVisibility.bind(this) } />
        { children }
      </Input>
    );
  }
}

export default PasswordMaskInput;
