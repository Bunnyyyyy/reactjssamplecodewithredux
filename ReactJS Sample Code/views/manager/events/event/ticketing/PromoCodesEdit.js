import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { withRouter } from 'react-router';
import styled from 'styled-components';
import moment from 'moment-timezone';
import * as UiActionCreators from '../../../../../redux/ui';
import * as ManagerActionCreators from '../../../../../redux/manager';
import * as EventActionCreators from '../../../../../redux/event';
import * as EventPromoCodesActionCreators from '../../../../../redux/eventPromoCodes';

import Button from '../../../../../components/common/Button';
import ConfirmModal from '../../../../../components/common/ConfirmModal';
import EditInfoCard from '../../../../../components/manager/cards/EditInfoCard';
import BiOption from '../../../../../components/common/inputs/BiOption';
import SaveActions from '../../../../../components/manager/SaveActions';

import { t as translate } from '../../../../../../i18n';

const t = a => translate(a, 'manager/events/ticketing/promoCodes/edit');

const TrashButton = Button.extend`
  color: ${ props => props.theme.colors.mainGray };
  margin-left: 20px;

  &:hover,
  &:active {
    color: ${ props => props.theme.colors.mainRed };
  }
`;

const IconTitle = styled.i`
  color: ${ props => props.theme.colors.mainGray };
  font-size: 20px;
  font-weight: bolder;
  margin-right: 20px;
  cursor: pointer;
`;

const FlexAlignItemCenter = styled.div`
  display: flex;
  align-items: center;
`;

const QUANTITY_OPTIONS = [
  { label: 'Unlimited', value: true },
  { label: 'Fixed', value: false },
];

const DISCOUNT_TYPE_OPTIONS = [
  { label: '$', value: 'amount' },
  { label: '%', value: 'percentage' },
];

const START_AT_OPTIONS = [
  { label: t('Immediately'), value: true },
  { label: t('Customize Date'), value: false },
];

const END_AT_OPTIONS = [
  { label: t('Starting of First Session'), value: true },
  { label: t('Customize Date'), value: false },
];

const mapStateToProps = state => ({
  event: state.manager.event,
  organization: state.manager.organization,
  promoCode: state.eventPromoCodes.promoCode,
  loading: state.ui.loading,
});

const ActionCreators = Object.assign(
  {},
  UiActionCreators,
  ManagerActionCreators,
  EventActionCreators,
  EventPromoCodesActionCreators,
);
const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators(ActionCreators, dispatch),
});

class EventTicketingPromoCodesEditView extends Component {
  constructor () {
    super();

    this.state = {
      validateForm: false,
      promoCodeAlreadyExist: false,
    };
  }

  async componentDidMount () {
    const { actions, match } = this.props;
    const { eId, pcId } = match.params;

    actions.setLoading(true);

    try {
      const promises = [
        actions.getEventPromoCodes(),
      ];
      if (pcId) {
        promises.push(actions.getEventPromoCode(eId, pcId));
      } else {
        actions.setEventPromoCode({});
      }

      await Promise.all(promises);
    } catch (e) {
      console.error('ERROR: EventTicketingPromoCodesEditView', e);
    } finally {
      actions.setLoading(false);
    }
  }

  componentWillUnmount () {
    const { actions } = this.props;
    actions.resetSaveActions();
  }

  async onInvalidSave () {
    this.setState({ validateForm: true });
  }

  async onCancel () {
    try {
      const { history, organization, event } = this.props;
      history.push(
        `/${ organization.id }/events/${ event.id }/ticketing/promo-codes`,
      );
    } catch (e) {
      throw e;
    }
  }

  async onDelete () {
    const {
      promoCode, organization, event, actions, history,
    } = this.props;
    const eId = event.id;
    const pcId = promoCode.id;

    if (eId && pcId) {
      await Promise.all([actions.removePromoCode(eId, pcId)]);
    }

    history.push(
      `/${ organization.id }/events/${ event.id }/ticketing/promo-codes`,
    );
  }

  toggleDeleting () {
    const { isDeleting } = this.state;
    this.setState({
      isDeleting: !isDeleting,
    });
  }

  async savePromoCode (state) {
    const {
      promoCode, organization, event, actions, history,
    } = this.props;
    const eId = event.id;
    const pcId = promoCode.id;

    if (!eId) return;

    try {
      const data = {
        name: state.name,
        type: state.type,
        value: state.value,
        quantity: state.unlimited_quantity ? null : state.quantity,
        // TODO: refactor when backend promo code logic can handle multiple codes
        // code: state.code.length ? state.code[state.code.length - 1] : null,
        code: state.code,
        start_at: state.start_immediately ? null : state.start_at,
        end_at: state.end_automatically ? null : state.end_at,
      };

      if (pcId) {
        await actions.updatePromoCode(eId, pcId, data);
        await actions.getEventPromoCode(eId, pcId);
      } else {
        await actions.createPromoCode(eId, data);
        history.push(
          `/${ organization.id }/events/${ event.id }/ticketing/promo-codes`,
        );
      }
    } catch (e) {
      this.setState({
        promoCodeAlreadyExist: e.message === 'Promo code have already existed',
      });
      throw e;
    }
  }

  render () {
    const {
      history,
      organization,
      event = {},
      promoCode = {},
      loading,
    } = this.props;
    const { validateForm, isDeleting, promoCodeAlreadyExist } = this.state;

    const TitleWrapper = () => (
      <div>
        <IconTitle
          className='fas fa-chevron-left'
          onClick={ () => {
            history.push(
              `/${ organization.id }/events/${ event.id }/ticketing/promo-codes`,
            );
          } }
        />
        { promoCode.id
          ? t('EDIT YOUR PROMOTION CODE')
          : t('CREATE PROMOTION CODE') }
      </div>
    );

    const comboSelectCss = `
      padding-right: 0;
      .select__container {
        border-right: 1px solid rgba(47, 46, 46, .25);
      }
    `;

    const comboInputCss = `
      input {
        padding-left: 10px;
      }
    `;

    const promoCodesFields = [
      {
        type: 'input',
        field: 'name',
        label: t('PROMOTION NAME'),
        default: promoCode.name,
        required: true,
        flex: '0 0 50%',
      },
      {
        type: 'input',
        field: 'code',
        label: t('Promotion Code'),
        flex: '0 0 100%',
        default: promoCode.code,
        errorMessage: promoCodeAlreadyExist ? t('This promocode already Exist') : null,
      },
      {
        type: 'select',
        required: true,
        field: 'unlimited_quantity',
        label: t('Quantity'),
        flex: state => `0 0 ${ state.unlimited_quantity ? '50%' : '10%' }`,
        default: promoCode.id ? promoCode.quantity === null : true,
        options: QUANTITY_OPTIONS,
        searchable: false,
        customCss: state => (!state.unlimited_quantity ? comboSelectCss : ''),
      },
      {
        type: 'input',
        number: true,
        field: 'quantity',
        label: ' ',
        default: promoCode.quantity,
        condition: state => !state.unlimited_quantity,
        flex: '0 0 40%',
        customCss: comboInputCss,
        getErrorMessage: (formState, value) => {
          if (formState.unlimited_quantity || value) {
            return null;
          }
          return t('Please enter a valid Quantity');
        },
      },
      {
        type: 'select',
        required: true,
        field: 'type',
        label: t('Discount'),
        flex: '0 0 10%',
        default: promoCode.type,
        options: DISCOUNT_TYPE_OPTIONS,
        searchable: false,
        customCss: comboSelectCss,
      },
      {
        type: 'input',
        number: true,
        required: true,
        field: 'value',
        label: ' ',
        default: promoCode.value,
        flex: '0 0 40%',
        getErrorMessage: (formState, value) => {
          if (value) {
            return null;
          }
          return t('Please enter a valid Amount');
        },
        customCss: comboInputCss,
      },
      {
        type: 'options',
        field: 'start_immediately',
        label: t('Promotion Starts At'),
        default: !promoCode.start_at,
        required: true,
        options: START_AT_OPTIONS.map(c => ({
          value: c.value,
          component: () => (
            <BiOption>
              <p>{ c.label }</p>
            </BiOption>
          ),
        })),
        flex: state => (state.start_immediately ? '0 0 100%' : '0 0 50%'),
        customCss: state => (state.start_immediately ? 'padding-right: calc(50% + 20px);' : ''),
      },
      {
        type: 'date',
        field: 'start_at',
        label: t('Start Date & Time'),
        default: promoCode.start_at,
        timezone: event.timezone || null,
        condition: state => !state.start_immediately,
        flex: '0 0 35%',
        getErrorMessage: (formState, value) => {
          if (formState.start_immediately || value) {
            return null;
          }
          return t('Please fill in a Start Date & Time');
        },
        customCss: `
          & .DateInput_input {
            height: 42px;
            line-height: 40px;
          }
        `,
      },
      {
        field: 'start_at',
        type: 'time',
        label: ' ',
        default: promoCode.start_at,
        timezone: event.timezone || null,
        condition: state => !state.start_immediately,
        flex: '0 0 15%',
      },
      {
        type: 'options',
        field: 'end_automatically',
        label: t('Promotion Ends At'),
        default: !promoCode.end_at,
        required: true,
        options: END_AT_OPTIONS.map(c => ({
          value: c.value,
          component: () => (
            <BiOption>
              <p>{ c.label }</p>
            </BiOption>
          ),
        })),
        flex: state => (state.end_automatically ? '0 0 100%' : '0 0 50%'),
        customCss: state => (state.end_automatically ? 'padding-right: calc(50% + 20px);' : ''),
      },
      {
        type: 'date',
        field: 'end_at',
        label: t('End Date & Time'),
        default: promoCode.end_at,
        timezone: event.timezone || null,
        condition: state => !state.end_automatically,
        flex: '0 0 35%',
        getErrorMessage: (formState, value) => {
          const startDate = moment(formState.start_at).clone();
          const endDate = moment(value).clone();

          if (!formState.end_automatically && !value) {
            return t('Please fill in an End Date & Time');
          }
          if (formState.start_immediately || formState.end_automatically || startDate.isSameOrBefore(endDate)) {
            return null;
          }
          return t('End Date must be after Start Date');
        },
        customCss: `
          & .DateInput_input {
            height: 42px;
            line-height: 40px;
          }
        `,
      },
      {
        type: 'time',
        field: 'end_at',
        label: ' ',
        default: promoCode.end_at,
        timezone: event.timezone || null,
        condition: state => !state.end_automatically,
        flex: '0 0 15%',
      },
      {
        type: 'custom',
        flex: '0 0 100%',
        renderComponent: () => (
          <FlexAlignItemCenter>
            <SaveActions
              saveNewItem={ !promoCode.id }
              onInvalid={ this.onInvalidSave.bind(this) }
              onCancel={ this.onCancel.bind(this) }
            />
            { promoCode.id && (
              <TrashButton plain onClick={ this.toggleDeleting.bind(this) }>
                <i className='fa fa-trash' aria-hidden='true' />
              </TrashButton>
            ) }
          </FlexAlignItemCenter>
        ),
      },
    ];

    return (
      <div>
        { !loading ? (
          <EditInfoCard
            name='promo-codes'
            title={ <TitleWrapper /> }
            fields={ promoCodesFields }
            action={ this.savePromoCode.bind(this) }
            actionOrder={ 0 }
            formCss='align-items: flex-end;'
            validateForm={ !!promoCode.id || validateForm }
          />
        ) : null }
        { isDeleting ? (
          <ConfirmModal
            warning
            message={ t('Are you sure you want to delete this Promo Code?') }
            onConfirm={ this.onDelete.bind(this) }
            onCancel={ this.toggleDeleting.bind(this) }
          />
        ) : null }
      </div>
    );
  }
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(EventTicketingPromoCodesEditView));
