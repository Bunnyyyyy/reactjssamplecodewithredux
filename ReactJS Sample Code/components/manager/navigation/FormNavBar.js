import React, { Component } from 'react';
import styled from 'styled-components';

import NavBar from '../../common/NavBar';
import i18n from '.././../../../i18n';
const t = a => i18n.t([`components/manager:${ a }`]);

const NavBarContainer = styled.h4`
  width: 100%;
  display: flex;
  flex-grow: 2;
`;

const FormNavBar = ({ organization, form }) => {
  const links = [
    {
      title: t('Overall'),
      route: `/${ organization.id }/forms/${ form.id }`,
      exact: true
    },
    {
      title: t('Edit'),
      route: `/${ organization.id }/forms/${ form.id }/edit`
    },
    {
      title: t('Respondents'),
      route: `/${ organization.id }/forms/${ form.id }/respondents`
    }
  ];

  return (
    <NavBarContainer>
      <NavBar links={ links } />
    </NavBarContainer>
  );
};

export default FormNavBar;
