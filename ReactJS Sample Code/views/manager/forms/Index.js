import React from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router';
import { Route, Switch } from 'react-router-dom';
import { Helmet } from 'react-helmet';

import Header from '../../../components/header/Header';

import FormsListView from './List';
import FormEditView from './form/Edit';
import FormIndexView from './form/Index';

import { t as translate } from '../../../../i18n';

const t = a => translate(a, 'manager/forms/index');

const mapStateToProps = state => ({
  organization: state.manager.organization,
  form: state.organization.form,
});

const FormsIndexView = ({ location, organization }) => {
  const { pathname } = location;

  let navPages = null;
  let pageTitle = null;
  if (pathname.indexOf(`${ organization.id }/forms/new`) > 0) {
    navPages = [
      {
        title: t('Forms'),
        url: `/${ organization.id }/forms`,
      },
      {
        title: t('New'),
      },
    ];
  } else if (pathname.indexOf(`${ organization.id }/forms/`) === -1) {
    navPages = [
      {
        title: t('Forms'),
      },
    ];
  }

  if (navPages) {
    pageTitle = navPages
      .map(p => p.title)
      .reverse()
      .concat('Juven Manager')
      .join(' - ');
  }

  return (
    <div>
      { pageTitle ? (
        <Helmet>
          <title>{ pageTitle }</title>
          <meta property='og:title' content={ pageTitle } />
        </Helmet>
      ) : null }
      { navPages ? <Header navPages={ navPages } /> : null }
      <Switch>
        <Route exact path='/:oId/forms' component={ FormsListView } />
        <Route exact path='/:oId/forms/new' component={ FormEditView } />
        <Route path='/:oId/forms/:foId' component={ FormIndexView } />
      </Switch>
    </div>
  );
};

export default withRouter(connect(mapStateToProps)(FormsIndexView));
