import React from 'react';
import { connect } from 'react-redux';
import styled from 'styled-components';
import { DragSource } from 'react-dnd';

import Input from '../../common/Input';
import Checkbox from '../../common/Checkbox';
import RichTextEditor from '../../common/inputs/RichTextEditor';
import ImageUploadV2 from '../../common/ImageUploadV2';
import Select from '../../common/inputs/Select';
import MultiInput from '../../common/inputs/MultiInput';
import MultiSelect from '../../common/MultiSelect';

import logoPlaceholder from '../../../assets/common/placeholder_logo.svg';

import i18n from '../../../../i18n';

const t = a => i18n.t([`components/manager:${ a }`]);

// React DnD logic
const ELEMENTS_BUILDER_ELEMENT = 'ELEMENTS_BUILDER_ELEMENT';
const source = {
  beginDrag (props) {
    return {
      type: 'existing',
      element: props.element,
      oldOrder: props.order,
    };
  },

  endDrag () {
    return {};
  },
};
// eslint-disable-next-line
function collect(connect) {
  return {
    dragPreview: connect.dragPreview(),
    dragSource: connect.dragSource(),
  };
}

const ActionsWrapper = styled.div`
  display: block;
  text-align: right;
`;

const ElementWrapper = styled.div`
  outline: none;
  position: relative;
  background-color: white;
  padding: 5px;
  margin-bottom: 20px;

  ${ props => (!props.section ? 'margin-left: 40px;' : '') }
`;

const ElementHeader = styled.div`
  display: flex;
  justify-content: space-between;
  padding: 0 10px;
`;

const ElementLeftDots = styled.div`
  position: absolute;
  top: 0;
  left: -30px;
  height: 100%;
  border-left: 5px dotted ${ props => props.theme.colors.mainBlue };
  opacity: 0.5;
`;

const GeneralElementWrapper = styled.div`
  background-color: white;
  padding: 10px;
`;

const InputElementWrapper = styled.div`
  position: relative;
  background-color: white;
  padding: 10px 10px 40px 10px;

  & h5 {
    flex-shrink: 0;
    margin-right: 20px;
    line-height: 32px;
  }
`;

const SelectElementWrapper = styled.div`
  position: relative;
  background-color: white;
  padding: 10px 10px 40px 10px;

  & h5 {
    flex-shrink: 0;
    margin-right: 20px;
    line-height: 25px;
  }
`;

const CheckboxElementWrapper = styled.div`
  background-color: white;
  padding: 10px;

  & h5 {
    flex-shrink: 0;
    margin-right: 20px;
    line-height: 32px;
  }
`;

const CheckboxConfigsWrapper = styled.div`
  display: flex;
  margin: 0 20px 20px 0;
`;

const InputConfigWrapper = styled.div`
  display: flex;
  align-items: center;
`;

const Action = styled.div`
  display: inline-block;
  cursor: pointer;
  width: 40px;
  height: 40px;
  border-radius: 20px;
  padding: 0;
  text-align: center;
  font-size: 20px;
  line-height: 40px;
  background-color: white;
  color: ${ props => props.theme.colors.mainGray };

  &:active {
    background-color: white;
    color: ${ props => props.theme.colors.darkGray };
  }

  & i {
    margin: 0;
  }
`;

const ImageElementWrapper = styled.div`
  display: flex;
  background-color: white;
  padding: 10px;
`;

const LogoElementWrapper = styled.div`
  display: flex;
  background-color: white;
  padding: 10px;

  & img {
    display: block;
    margin: 0 auto;
    max-height: 100px;
  }
`;

const StandardElementWrapper = styled.div`
padding-left: 10px;
`;

const SectionElementWrapper = styled.div`
  position: relative;
  display: flex;
  flex-direction: column;
  background-color: white;
  padding: 10px;

  & h5 {
    flex-shrink: 0;
    font-size: 20px;
    line-height: 25px;
    margin-bottom: 10px;
  }

  & > div {
    flex-grow: 1;
  }
`;

const ElementTypeTitle = styled.h5`
  flex-shrink: 0;
  font-size: ${ props => (props.section ? '20px' : '15px') };
  line-height: 40px;
  color: ${ props => props.theme.colors.darkBlue };
`;

const SectionInstruction = styled.p`
  font-weight: 700;
  margin: 10px 0 20px 40px;
`;

const mapStateToProps = state => ({
  organization: state.manager.organization,
});

const Element = ({
  dragPreview,
  dragSource,
  elements = [],
  element = {},
  isEmail,
  onUpdateField,
  onRemoveElement,
  organization,
}) => {
  const {
    id,
    newId,
    order,
    data_type,
    label,
    url,
    options,
    multiple_options,
    image_url,
    required,
    conditional,
    condition_field_id,
    condition_field_values,
  } = element;
  let { display_type, display_text } = element;

  if (isEmail) {
    display_type = element.type;
    display_text = element.text;
  }

  const eId = id || `new-${ newId }`;
  const InputConfigs = (
    <CheckboxConfigsWrapper>
      <Checkbox
        id={ `element-${ eId }-required` }
        customCss='margin-bottom: 0;'
        field={ `element-${ eId }-required` }
        checked={ required }
        onChange={ value => onUpdateField(order, 'required', value) }
        label='Required'
      />
      <Checkbox
        id={ `element-${ eId }-conditional` }
        customCss='margin-bottom: 0;'
        field={ `element-${ eId }-conditional` }
        checked={ conditional }
        onChange={ value => onUpdateField(order, 'conditional', value) }
        label='Conditional'
      />
    </CheckboxConfigsWrapper>
  );

  const conditionalField = condition_field_id
    ? elements.find(e => e.id === condition_field_id)
    : {};
  let ConditionFieldValue = null;
  if (Array.isArray(conditionalField.options)) {
    ConditionFieldValue = (
      <MultiSelect
        field={ `element-${ eId }-condition-element-values` }
        label={ t('Matching Values') }
        placeholder={ t('Enter conditional field matching values') }
        required={ conditional }
        value={ condition_field_values || [] }
        options={
          conditionalField && Array.isArray(conditionalField.options)
            ? conditionalField.options.map(o => ({
              label: o,
              value: o,
            }))
            : []
        }
        customCss='
        margin-top: 20px;
    width: 100 %;
    '
        onChange={
      value => onUpdateField(order, 'condition_field_values', value)
    }
      />
    );
  } else if (conditionalField.data_type) {
    ConditionFieldValue = (
      <MultiInput
        field={ `element-${ eId }-condition-element-values` }
        label={ t('Matching Values') }
        placeholder={ t('Enter conditional field matching values') }
        required={ conditional }
        value={ condition_field_values || [] }
        customCss='
      margin-top: 20px;
  width: 100 %;
  '
        onChange={
    value => onUpdateField(order, 'condition_field_values', value)
  }
      />
    );
  }

  const ConditionalConfig = (
    <InputConfigWrapper>
      <Select
        fullWidth
        field={ `element-${ eId }-condition-element-id` }
        label='Conditional Field'
        value={ condition_field_id }
        required={ conditional }
        onChange={ value => onUpdateField(order, 'condition_field_id', value) }
        customCss={ `
          min-width: auto;
          margin-top: 20px;
          padding-right: ${ !ConditionFieldValue ? 'calc(50% + 20px)' : '20px' };
          margin-right: 0;
        ` }
        options={ elements
          .filter(e => e.eId !== eId && !!e.data_type)
          .map(e => ({
            label: e.label,
            value: e.id,
          })) }
      />
      { ConditionFieldValue }
    </InputConfigWrapper>
  );

  const TitleElement = (
    <GeneralElementWrapper empty={ !display_text || !display_text.length }>
      <Input
        placeholder={ t('Enter text...') }
        value={ display_text }
        customCss='margin-bottom: 0; width: 100%;'
        style={ {
          height: 30,
          fontSize: 25,
          lineHeight: '30px',
          borderBottom: 'none',
        } }
        onChange={ (field, value) => onUpdateField(order, isEmail ? 'text' : 'display_text', value)
      }
      />
    </GeneralElementWrapper>
  );
  const LinkButtonElement = (
    <GeneralElementWrapper empty={ !display_text || !url || display_text.length || !url.length }>
      <Input
        label={ t('Button Text') }
        value={ display_text }
        customCss='margin-bottom: 0; width: 100%;'
        onChange={ (_, value) => onUpdateField(order, isEmail ? 'text' : 'display_text', value)
      }
      />
      <Input
        label={ t('URL Link') }
        value={ url }
        customCss='margin-bottom: 0; width: 100%;'
        onChange={ (_, value) => onUpdateField(order, 'url', value)
      }
      />
    </GeneralElementWrapper>
  );
  const TextElement = (
    <GeneralElementWrapper>
      <RichTextEditor
        placeholder='Enter text...'
        value={ display_text }
        action={ (_, value) => onUpdateField(order, isEmail ? 'text' : 'display_text', value)
      }
      />
    </GeneralElementWrapper>
  );
  const StandardDefaultElement = (
    <StandardElementWrapper>
      { InputConfigs }
      { conditional ? ConditionalConfig : null }
    </StandardElementWrapper>
  );

  const GreetingElement = (
    <GeneralElementWrapper>
      <h4>{ t('Dear ___,') }</h4>
    </GeneralElementWrapper>
  );

  const StandardElement = (
    <InputElementWrapper>
      <Input
        placeholder={ t('Enter field title...') }
        label={ t('Title') }
        value={ label }
        customCss='
      margin-top: 5px;
          margin-bottom: 20px;
          width: 100%;
        '
        onChange={ (field, value) => onUpdateField(order, 'label', value) }
      />
      { InputConfigs }
      { conditional ? ConditionalConfig : null }
    </InputElementWrapper>
  );

  const SelectElement = (
    <SelectElementWrapper>
      <InputConfigWrapper>
        <Input
          placeholder={ t('Enter field title...') }
          label={ t('Title') }
          required
          value={ label }
          customCss='
        margin-top: 5px;
            margin-bottom: 20px;
            width: 100%;
          '
          onChange={ (field, value) => onUpdateField(order, 'label', value) }
        />
        <MultiInput
          label='Options'
          placeholder={ t('Enter options') }
          required
          value={ options }
          customCss='
        margin-top: 10px;
            width: 100%;
          '
          onChange={ value => onUpdateField(order, 'options', value) }
        />
      </InputConfigWrapper>
      { InputConfigs }
      { conditional ? ConditionalConfig : null }
    </SelectElementWrapper>
  );

  const CheckboxElement = (
    <CheckboxElementWrapper>
      <Input
        placeholder={ t('Enter field title...') }
        label={ t('Title') }
        value={ label }
        customCss='
      margin-top: 5px;
          margin-bottom: 20px;
          width: 100%;
        '
        onChange={ (field, value) => onUpdateField(order, 'label', value) }
      />
      { InputConfigs }
      { conditional ? ConditionalConfig : null }
    </CheckboxElementWrapper>
  );

  const ImageElement = (
    <ImageElementWrapper>
      <ImageUploadV2
        image={ image_url }
        onChange={ (field, value) => onUpdateField(order, 'file', value) }
        customCss='width: 100%'
        enableResize
      />
    </ImageElementWrapper>
  );

  const LogoImageElement = (
    <LogoElementWrapper>
      <img
        alt='Organization logo'
        src={
        organization.logo_image_url || logoPlaceholder
      }
      />
    </LogoElementWrapper>
  );

  const SectionElement = (
    <SectionElementWrapper>
      <div>
        <Input
          placeholder={ t('Enter field title...') }
          label='Title'
          required
          value={ display_text }
          customCss='
        margin-top: 5px;
            margin-bottom: 20px;
            width: 100%;
          '
          onChange={ (field, value) => onUpdateField(order, 'display_text', value)
      }
        />
      </div>
    </SectionElementWrapper>
  );

  const ContactsElement = (
    <GeneralElementWrapper>
      <p>
        <span>{ `${ t('Includes the fields') }: ` }</span>
        <b>{ `${ t('First Name') }, ${ t('Last Name') }, ${ t('Email') } & ${ t('Phone') }` }</b>
      </p>
    </GeneralElementWrapper>
  );

  let elementTitle;
  let Content;
  if (element.standard_field_name && element.standard_field_name === 'gender') {
    elementTitle = 'Gender';
    Content = StandardDefaultElement;
  } else if (element.standard_field_name && element.standard_field_name === 'date_of_birth') {
    elementTitle = 'Date of birth';
    Content = StandardDefaultElement;
  } else if (element.standard_field_name && element.standard_field_name === 'country') {
    elementTitle = 'Country';
    Content = StandardDefaultElement;
  } else if (element.standard_field_name && element.standard_field_name === 'address') {
    elementTitle = 'Address';
    Content = StandardDefaultElement;
  } else if (element.standard_field_name && element.standard_field_name === 'education') {
    elementTitle = 'Level of education';
    Content = StandardDefaultElement;
  } else if (element.standard_field_name && element.standard_field_name === 'occupation') {
    elementTitle = 'Occupation';
    Content = StandardDefaultElement;
  } else if (element.standard_field_name && element.standard_field_name === 'company_address') {
    elementTitle = 'Office address';
    Content = StandardDefaultElement;
  } else if (element.standard_field_name && element.standard_field_name === 'company_name') {
    elementTitle = 'Company name';
    Content = StandardDefaultElement;
  } else if (element.standard_field_name && element.standard_field_name === 'industry') {
    elementTitle = 'Industry';
    Content = StandardDefaultElement;
  } else if (display_type === 'title') {
    elementTitle = t('Title');
    Content = TitleElement;
  } else if (display_type === 'link_button') {
    elementTitle = t('Link Button');
    Content = LinkButtonElement;
  } else if (display_type === 'greeting') {
    elementTitle = t('Greeting');
    Content = GreetingElement;
  } else if (display_type === 'text') {
    elementTitle = t('Paragraph');
    Content = TextElement;
  } else if (display_type === 'section') {
    elementTitle = t('Section');
    Content = SectionElement;
  } else if (display_type === 'contacts') {
    elementTitle = t('Contact Information (Compulsory)');
    Content = ContactsElement;
  } else if (display_type === 'image') {
    elementTitle = t('Image');
    Content = ImageElement;
  } else if (display_type === 'logo') {
    elementTitle = t('Organization Logo');
    Content = LogoImageElement;
  } else if (options) {
    elementTitle = multiple_options
      ? t('Multiple Choice Options')
      : t('Single Choice Options');
    Content = SelectElement;
  } else if (
    data_type === 'string'
  || data_type === 'number'
  || data_type === 'text'
  || data_type === 'file'
  ) {
    if (data_type === 'file') {
      elementTitle = t('File Upload');
    } else if (data_type === 'text') {
      elementTitle = t('Long Text Input');
    } else if (data_type === 'string') {
      elementTitle = t('Short Text Input');
    } else if (data_type === 'number') {
      elementTitle = t('Number Input');
    }
    Content = StandardElement;
  } else if (data_type === 'boolean') {
    elementTitle = t('Yes / No Field');
    Content = CheckboxElement;
  }

  return dragPreview(
    <div>
      <ElementWrapper
        tabIndex='-1'
      // HACK
        section={ display_type === 'section' }
      >
        { display_type !== 'section' ? <ElementLeftDots /> : null }
        <ElementHeader>
          <ElementTypeTitle
            section={ display_type === 'section' }
          >
            { elementTitle }
          </ElementTypeTitle>
          <ActionsWrapper>
            { dragSource(
              <div style={ { display: 'inline-block' } }>
                <Action>
                  <i className='fas fa-arrows-alt' aria-hidden='true' />
                </Action>
              </div>,
            ) }
            { display_type !== 'contacts' ? (
              <Action onClick={ onRemoveElement.bind(this, order) }>
                <i className='fas fa-trash' aria-hidden='true' />
              </Action>
            ) : null }
          </ActionsWrapper>
        </ElementHeader>
        { Content }
      </ElementWrapper>
      { display_type === 'section' ? (
        <SectionInstruction>
          { `${ t('You may add elements to the section') } "${ display_text }" ${ t('by dragging them here.') }` }
        </SectionInstruction>
      ) : null }
    </div>,
  );
};

export default connect(mapStateToProps)(
  DragSource(ELEMENTS_BUILDER_ELEMENT, source, collect)(Element),
);
