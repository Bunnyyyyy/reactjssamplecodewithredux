import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import styled from 'styled-components';

import Row from './Row';
import Input from '../Input';
import Button from '../Button';
import { userSelect } from '../../../utils/styles';

import i18n from '../../../../i18n';

const t = a => i18n.t([`components/common:${ a }`]);

const TableHeader = styled.div`
  display: flex;
  align-items: center;
  padding: 0 110px 0 56px;
  margin: 30px 0 20px 0;
`;

const TopWrapper = styled.div`
  display: flex;
  height: 42px;
  align-items: center;
  background-color: white;
  box-shadow: 0 3px 6px 0 rgba(0, 0, 0, 0.16);
`;

const SearchIcon = styled.i`
  margin-left: 33px;
  color: ${ props => props.theme.colors.lightGray };
`;

const searchInputCss = `
  flex-grow: 1;
  margin: 5px 15px;

  & input {
    font-size: 15px;
    border-bottom: none;
  }
`;

const SearchCancelButton = styled(Button)`
  height: 42px;
  width: 42px;
  line-height: 42px;
`;

const HeaderColumn = styled.div`
  flex-grow: 1;
  flex-basis: 0;
  text-align: left;
  padding: 0 10px;
  font-size: 12px;
  font-weight: 700;
  line-height: 14px;
  color: ${ props => props.theme.colors.mainGray };

  ${ userSelect('none') } & i {
    position: relative;
    top: 2px;
    font-size: 18px;
    margin-left: 10px;
    color: rgb(208, 208, 208);
  }
`;

const NewItemButton = styled(Button)`
  min-width: 100px;
  height: 42px;
  line-height: 42px;
  font-size: 12px;
  padding: 0 20px;
`;

const EmptyText = styled.h4`
  text-align: center;
  margin-top: 30px;
`;

const mapStateToProps = state => ({
  loading: state.ui.loading,
});

class ItemTable extends Component {
  constructor () {
    super();

    this.state = {
      searchValue: null,
    };
  }

  changeSearchValue (searchValue) {
    this.setState({ searchValue });
  }

  render () {
    const {
      columns,
      rows,
      editUrl,
      searchField,
      itemText = 'Item',
      newItemUrl,
      loading,
    } = this.props;
    const { searchValue } = this.state;

    let TopSection = null;
    if (searchField || newItemUrl) {
      TopSection = (
        <TopWrapper>
          { searchField
            ? [
              <SearchIcon className='fa fa-search' aria-hidden='true' />,
              <Input
                placeholder='Search'
                customCss={ searchInputCss }
                value={ searchValue }
                onChange={ (_, value) => this.changeSearchValue(value) }
              />,
              <SearchCancelButton
                plain
                onClick={ this.changeSearchValue.bind(this, null) }
              >
                  ✕
              </SearchCancelButton>,
            ]
            : null }
          { newItemUrl ? (
            <Link to={ newItemUrl }>
              <NewItemButton fullHeight>
                +
                { ' ' }
                { t('New') }
                { ' ' }
                { itemText }
              </NewItemButton>
            </Link>
          ) : null }
        </TopWrapper>
      );
    }

    const filteredRows = searchField && searchValue
      ? rows.filter((r) => {
        const regex = new RegExp(searchValue, 'i', 'g');
        return regex.test(r[searchField]);
      })
      : rows;

    const Rows = filteredRows.map((r, i) => {
      const rowKey = r.id ? `table-row-${ r.id }` : `table-row-pos-${ i }`;
      // const targetKey = r.id
      //   ? `table-row-target-${ r.id }`
      //   : `table-row-pos-target-${ i }`;

      const RowContent = [
        <Row
          key={ rowKey }
          rowKey={ rowKey }
          columns={ columns }
          item={ r }
          order={ i + 1 }
          editUrl={ editUrl }
        />,
      ];

      return RowContent;
    });

    let Header = null;
    let Content = null;

    if (filteredRows.length) {
      const HeaderColumns = columns.map((c, i) => {
        const { title, flex } = c;
        const style = { flex };

        return (
          <HeaderColumn key={ `header-column-${ i }` } style={ style }>
            { title }
          </HeaderColumn>
        );
      });
      Header = <TableHeader>{ HeaderColumns }</TableHeader>;

      Content = <div>{ Rows }</div>;
    } else if (!loading) {
      const textContent = searchValue
        ? `${ t('No') } ${ itemText } ${ t('found') }.`
        : `${ t('There is no') } ${ itemText } ${ t('yet') }.`;
      Content = <EmptyText>{ textContent }</EmptyText>;
    }

    return (
      <div>
        { TopSection }
        { Header }
        { Content }
      </div>
    );
  }
}

export default connect(mapStateToProps)(ItemTable);
