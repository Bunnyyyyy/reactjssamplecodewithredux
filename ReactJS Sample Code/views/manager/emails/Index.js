import React from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router';
import { Route, Switch } from 'react-router-dom';
import { Helmet } from 'react-helmet';

import Header from '../../../components/header/Header';

import EmailsListView from './List';
import EmailsEditView from './Edit';

import { t as translate } from '../../../../i18n';

const t = a => translate(a, 'manager/emails/index');

const mapStateToProps = state => ({
  organization: state.manager.organization,
  template: state.emailTemplates.template,
});

const EmailsIndexView = ({ location, organization, template }) => {
  const { pathname } = location;

  const navPages = [
    {
      title: t('Emails'),
      url: `/${ organization.id }/emails`,
    },
  ];
  if (pathname.indexOf(`${ organization.id }/emails/new`) > 0) {
    navPages.push({
      title: t('New'),
    });
  } else if (pathname.indexOf(`${ organization.id }/emails/`) > 0) {
    navPages.push({
      title: template.title || '',
    });
  } else {
    delete navPages[0].url;
  }

  const pageTitle = navPages
    .map(p => p.title)
    .reverse()
    .concat('Juven Manager')
    .join(' - ');

  return (
    <div>
      <Helmet>
        <title>{ pageTitle }</title>
        <meta property='og:title' content={ pageTitle } />
      </Helmet>
      <Header navPages={ navPages } />
      <Switch>
        <Route exact path='/:oId/emails' component={ EmailsListView } />
        <Route exact path='/:oId/emails/new' component={ EmailsEditView } />
        <Route exact path='/:oId/emails/:emId' component={ EmailsEditView } />
      </Switch>
    </div>
  );
};

export default withRouter(connect(mapStateToProps)(EmailsIndexView));
