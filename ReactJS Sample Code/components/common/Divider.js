import styled from 'styled-components';

export default styled.hr`
  border: 0;
  color: ${ props => props.theme.colors.lightGray };
  height: 1.5em;
  line-height: 1em;
  margin-bottom: 30px;
  outline: 0;
  position: relative;
  text-align: center;

  &:after {
    background-color: ${ props => props.theme.colors.white };
    color: ${ props => props.theme.colors.mainGray };
    content: '${ props => props.text }';
    display: inline-block;
    line-height: 1.5em;
    padding: 0 20px;
    position: relative;
  }

  &:before {
    background-color: ${ props => props.theme.colors.lightBlue };
    content: '';
    height: 1px;
    left: 0;
    position: absolute;
    top: 50%;
    width: 100%;
  }

  @media (max-width: 900px) {
    margin-bottom: 20px;
  }
`;
