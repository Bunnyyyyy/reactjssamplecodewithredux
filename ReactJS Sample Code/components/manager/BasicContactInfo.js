import React from 'react';
import { Link } from 'react-router-dom';
import styled from 'styled-components';

import UserAvatar from '../common/UserAvatar';

const Container = styled.div`
  cursor: ${ props => (!props.disabled ? 'pointer' : 'default') };
  display: flex;
  flex-direction: row;
  align-items: center;

  ${ props => (!props.disabled
    ? `
      p:hover, p:active {
        color: ${ props.theme.colors.darkBlue };
      }
    `
    : '') };
`;

const NameText = styled.p`
  font-weight: 700;
  margin-left: 10px;
  white-space: nowrap;
  width: 85px;
`;

const Wrapper = styled.div`
  display: flex;
  align-items: center;
  flex: 1;
  justify-content: space-between;
`;

const GuestCount = styled.div`
  margin-left: 10px;
  color: ${ props => props.theme.colors.mainBlue };
  :hover > div {
    visibility: visible;
  }
`;

const ToolTip = styled.div`
  visibility: hidden;
  background-color: white;
  color: ${ props => props.theme.colors.darkGray };
  border-radius: 6px;
  padding: 5px 10px;
  box-shadow: 0 6px 6px 0 rgba(0, 0, 0, 0.1);
  position: absolute;
  z-index: 1;
  left: 215px;
  top: 20px;

  p {
    font-size: 10px;
    line-height: 15px;
    padding: 5px;
  }
`;

export default function BasicContactInfo ({ contact, type, organization }) {
  if (!contact) return null;
  const link = contact.user_id
    ? `/${ organization.id }/contacts/${
      contact.user_id
    }?activity_type=${ type }&activity_id=${ contact.id }`
    : null;

  const guestCount = contact.childs && contact.childs.length > 0 ? (
    <GuestCount>
        +
      { contact.childs.length }
      { ' ' }
guests
      { ' ' }
      <ToolTip>{ contact.childs.map(c => <p>{ c.name }</p>) }</ToolTip>
      { ' ' }
    </GuestCount>
  ) : null;

  const NameLink = link
    ? (
      <Link to={ link }>
        <Container>
          <UserAvatar size={ 36 } user={ contact } />
          <NameText>
            { `${ contact.first_name }${
              contact.last_name ? ` ${ contact.last_name }` : ''
            }`.trim() }
          </NameText>
        </Container>
      </Link>
    ) : (
      <Container>
        <UserAvatar size={ 36 } user={ contact } />
        <NameText>
          { `${ contact.first_name }${
            contact.last_name ? ` ${ contact.last_name }` : ''
          }`.trim() }
        </NameText>
      </Container>
    );

  return (
    <Wrapper>
      { NameLink }
      { guestCount }
    </Wrapper>
  );
}
