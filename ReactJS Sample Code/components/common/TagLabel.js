import React from 'react';
import styled from 'styled-components';

const Tag = styled.div`
  display: inline-block;
  height: 20px;
  font-size: 12px;
  line-height: 17px;
  color: white;
  background-color: ${ props =>
    props.theme.colors[props.bgColor || 'mainGreen'] };
  font-weight: 700;
  text-transform: uppercase;
  padding: 2px 5px;
  margin: 0 10px 0 10px;
`;

const TagLabel = ({ bgColor, label, ...props }) => (
  <Tag bgColor={ bgColor }>{ label }</Tag>
);

export default TagLabel;
