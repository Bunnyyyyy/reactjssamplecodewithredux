import Color from 'color';
import moment from 'moment-timezone';
import { parseNumber } from 'libphonenumber-js';
import axios from 'axios';
import crypto from 'crypto';

import * as RequestActionCreators from '../redux/request';

const {
  STRIPE_API_KEY,
  CLOUDFRONT_DOMAIN,
  THUMBOR_SECURITY_KEY,
} = process.env;
const STRIPE_FEE_FIXED = Number(process.env.STRIPE_FEE_FIXED);
const STRIPE_FEE_RATE = Number(process.env.STRIPE_FEE_RATE);

export function createServerRequest (store, req) {
  const { createRequest } = RequestActionCreators;
  const cookie = req ? req.headers.cookie : null;
  store.dispatch(createRequest(cookie));
}

export function getCustomStyles (primary_color = 'rgb(0, 167, 255)') {
  const customStyles = {
    solid: {},
    outline: {},
    text: {},
    input: {},
  };
  customStyles.color = primary_color;
  customStyles.solid.backgroundColor = primary_color;
  customStyles.outline.color = primary_color;
  customStyles.outline.borderColor = primary_color;
  customStyles.text.color = primary_color;
  customStyles.input = {
    inactive: {
      color: primary_color,
    },
    active: {
      color: primary_color,
      borderColor: primary_color,
    },
  };
  return customStyles;
}

export function handleApiError (e) {
  const { response } = e;
  const error = {};

  if (response) {
    error.status = response.status;
    error.message = response.data || response.statusText;
    console.error(`${ response.status } - ${ error.message }`);
  }

  return error;
}

export function validateEmail (email) {
  // eslint-disable-next-line
  const regex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  return regex.test(email);
}

export function validatePhone (phone) {
  return (
    phone
    && typeof phone === 'string'
    && parseNumber(phone, '', { extended: true }).valid
  );
}

const VALID_GENDER_VALUES = [
  'Male',
  'Female',
  'Other',
  'Prefer not to disclose',
];
export function validateGender (gender) {
  return VALID_GENDER_VALUES.indexOf(gender) > -1;
}

const VALID_EDUCATION_VALUES = [
  'Primary or below',
  'Secondary',
  'College or associate degree',
  'Bachelor',
  'Master',
  'Doctorate',
  'Other',
  'Prefer not to disclose',
];
export function validateEducation (education) {
  return VALID_EDUCATION_VALUES.indexOf(education) > -1;
}

export function validateURL (str) {
  const urlRegex = '^(?!mailto:)(?:(?:http|https|ftp)://)(?:\\S+(?::\\S*)?@)?(?:(?:(?:[1-9]\\d?|1\\d\\d|2[01]\\d|22[0-3])(?:\\.(?:1?\\d{1,2}|2[0-4]\\d|25[0-5])){2}(?:\\.(?:[0-9]\\d?|1\\d\\d|2[0-4]\\d|25[0-4]))|(?:(?:[a-z\\u00a1-\\uffff0-9]+-?)*[a-z\\u00a1-\\uffff0-9]+)(?:\\.(?:[a-z\\u00a1-\\uffff0-9]+-?)*[a-z\\u00a1-\\uffff0-9]+)*(?:\\.(?:[a-z\\u00a1-\\uffff]{2,})))|localhost)(?::\\d{2,5})?(?:(/|\\?|#)[^\\s]*)?$';
  const url = new RegExp(urlRegex, 'i');
  return str.length < 2083 && url.test(str);
}

export function generateUrl (
  baseUrl,
  limit,
  offset,
  sortBy,
  sortOrder,
  queryFilter,
) {
  const queryParams = [];
  if (limit) queryParams.push(`limit=${ limit }`);
  if (offset) queryParams.push(`offset=${ offset }`);
  if (sortBy) queryParams.push(`sort_by=${ sortBy }`);
  if (sortOrder) queryParams.push(`sort_order=${ sortOrder }`);

  if (queryFilter && Object.keys(queryFilter).length) {
    const queryObject = {};

    // filter out empty filters
    (Object.keys(queryFilter) || []).forEach((field) => {
      const value = queryFilter[field];
      if (value != null && value !== undefined && value !== '') {
        queryObject[field] = value;
      }
    });

    if (Object.keys(queryObject).length) {
      queryParams.push(`q=${ JSON.stringify(queryObject) }`);
    }
  }

  return queryParams.length ? `${ baseUrl }?${ queryParams.join('&') }` : baseUrl;
}

export async function getStripeToken (card) {
  const STRIPE_URL = 'https://api.stripe.com/v1/tokens';
  const cardDetails = {
    number: '4242424242424242',
    cvc: '220'
  }
  const data = Object.entries(cardDetails)
    .map(([key, value]) => `card[${ key }]=${ value }`)
    .reduce((previous, current) => `${ previous }&${ current }`, '');

  const headers = {
    Accept: 'application/json',
    Authorization: `Bearer ${ STRIPE_API_KEY }`,
    'Content-Type': 'application/x-www-form-urlencoded',
  };

  const result = await axios({
    method: 'POST',
    url: STRIPE_URL,
    data,
    headers,
  });

  return result.data;
}

export function calculateStripeFee (netAmount, applicationFeePercent = 0) {
  const applicationFee = Math.round(netAmount * applicationFeePercent) / 100;
  const totalAmount = (netAmount + applicationFee + STRIPE_FEE_FIXED)
    / (1 - STRIPE_FEE_RATE / 100);

  return totalAmount - netAmount - applicationFee;
}

export function formatAmount (amount) {
  return amount.toLocaleString('en-US', {
    minimumFractionDigits: 2,
    maximumFractionDigits: 2,
  });
}

export function stateHasSelectedRows (state) {
  const { selectedRows } = state;
  let selectedRowCount = 0;
  for (const i in selectedRows) {
    if (selectedRows[i]) selectedRowCount++;
  }
  return selectedRowCount;
}

export function formatDateTime (dateTime, timezone) {
  if (moment(dateTime).isValid()) {
    if (timezone) {
      return moment
        .utc(dateTime)
        .tz(timezone)
        .format('D MMM YYYY (ddd), h:mm A');
    }
    return moment
      .utc(dateTime)
      .local()
      .format('D MMM YYYY (ddd), h:mm A');
  }
  return '';
}

export function formatDate (date) {
  if (moment(date).isValid()) {
    return moment
      .utc(date)
      .local()
      .format('D MMM YYYY (ddd)');
  }
  return '';
}

export function formatTime (time) {
  if (moment(time).isValid()) {
    return moment
      .utc(time)
      .local()
      .format('h:mm A');
  }
  return '';
}

export function formatLocalDate (date) {
  if (moment(date).isValid()) {
    return moment(date).format('D MMM YYYY (ddd)');
  }
  return '';
}

export function formatLocalTime (time, currentFormat = null) {
  if (moment(time, currentFormat).isValid()) {
    return moment(time, currentFormat).format('h:mm A');
  }
  return '';
}

export function getDiscountedPrice (ticketPackage, totalPrice) {
  if (ticketPackage) {
    if (ticketPackage.discount_type === 'fixed_amount') {
      return Math.max(0, totalPrice - ticketPackage.discount_value);
    } if (ticketPackage.discount_type === 'percentage') {
      const discountAmount = totalPrice * Number(ticketPackage.discount_value) * 0.01;
      return Math.max(0, totalPrice - discountAmount);
    }
  } else {
    return totalPrice;
  }
}

export function generateRamdomColor () {
  return Color.hsl([Math.floor(Math.random() * 360), 100, 50])
    .rgb()
    .string();
}

export function validateFormFields (data, fields = []) {
  let isValid = true;
  fields
    .filter((f) => {
      const { readOnly, condition } = f;
      if (typeof readOnly === 'function') {
        if (readOnly(data)) return false;
      } else if (readOnly) {
        return false;
      }

      if (typeof condition === 'function') {
        if (!condition(data)) return false;
      }

      return true;
    })
    .forEach((f) => {
      const {
        field, label, required, getErrorMessage,
      } = f;

      if (
        (getErrorMessage && getErrorMessage(data, data[field], label))
        || (required && (data[field] === null || data[field] === undefined))
      ) {
        isValid = false;
      }

      const childIsValid = validateFormFields(data, f.fields);
      if (!childIsValid) isValid = false;
    });

  return isValid;
}

export function getUrlQueryValue (field) {
  const value = decodeURIComponent(
    window.location.search.replace(
      new RegExp(
        // eslint-disable-next-line
        `^(?:.*[&\\?]${encodeURIComponent(field).replace(/[\.\+\*]/g, '\\$&')}(?:\\=([^&]*))?)?.*$`,
        'i',
      ),
      '$1',
    ),
  );
  return value.length ? value : null;
}

export function getImageUrl (s3Url, resize = '1000x') {
  const { DISABLE_IMAGE_RESIZING } = process.env;
  // thumbor URL format: *CLOUDFRONT_DOMAIN*/hmac/trim/AxB:CxD/fit-in/-Ex-F/HALIGN/VALIGN/smart/filters:FILTERNAME(ARGUMENT):FILTERNAME(ARGUMENT)/*image-uri*
  // default resize is width = 1000 px (height is auto)
  if (!s3Url) return '';
  if (
    // can deactivate image resizing from .env in case of problem
    DISABLE_IMAGE_RESIZING === 'true'
    || s3Url.indexOf(CLOUDFRONT_DOMAIN) === 0
  ) {
    return s3Url;
  }
  if (resize.indexOf('FORCE_UPSCALE') === -1) {
    // apply no_upscale option by default
    resize += '/filters:no_upscale()';
  } else {
    resize = resize.replace('FORCE_UPSCALE', '');
  }
  const hmac = crypto.createHmac('sha1', THUMBOR_SECURITY_KEY.toString());
  hmac.update(`${ resize }/${ encodeURIComponent(s3Url) }`);
  const authenticationCode = hmac
    .digest('base64')
    .replace(/\+/g, '-')
    .replace(/\//g, '_');
  return `${ CLOUDFRONT_DOMAIN }/${ authenticationCode }/${ resize }/${ encodeURIComponent(s3Url) }`;
}

export function changeToUrlable (url) {
  try {
    return url
      .replace(/ /g, '-')
      .replace(/[\u1000-\uFFFF]+/g, '')
      .replace(/[^a-zA-Z0-9.]/g, '');
  } catch (e) {
    return '';
  }
}
