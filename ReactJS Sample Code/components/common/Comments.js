import React, { Component } from 'react';
import styled from 'styled-components';
import Input from './Input';
import Button from './Button';
import Comment from './Comment';

import i18n from '../../../i18n';

const t = a => i18n.t([`components/common:${ a }`]);

const CommentInputWrap = styled.div`
  margin-top: 36px;
  display: flex;
  width: 100%;
  border-top: 1px solid ${ props => props.theme.colors.lightGray };
`;
const InputWrap = styled.div`
  flex: 1;
`;
const ButtonWrap = styled.div`
  height: 48px;
  align-self: center;
  text-align: right;
`;

const EmptyItemText = styled.p`
  margin-top: 20px;
  margin-bottom: 20px;
  text-align: center;
`;

class Comments extends Component {
  constructor () {
    super();

    this.state = {
      error: null,
      comment: '',
    };
  }

  changeInput = (field, value) => this.setState({ [field]: value });

  addComment = () => {
    const { addComment } = this.props;
    const { comment } = this.state;

    if (!addComment) return;
    if (!comment || !comment.length) {
      this.setState({ error: 'Comment is required' });
      return;
    }
    addComment(comment);
    this.setState({ error: null, comment: '' });
  };

  render () {
    const { commentsData = [] } = this.props;
    const { comment, error } = this.state;
    return (
      <div>
        { commentsData.length ? (
          commentsData.map((cd, i) => <Comment key={ i } { ...cd } />)
        ) : (
          <EmptyItemText>{ t('No comments yet.') }</EmptyItemText>
        ) }
        <CommentInputWrap>
          <InputWrap>
            <Input
              fullWidth
              placeholder='Leave your comment...'
              field='comment'
              value={ comment }
              error={ error }
              onChange={ this.changeInput }
              customCss={ {
                border: 0,
                margin: 0,
                paddingLeft: '20px',
                fontSize: '12px',
              } }
              inputCss={ 'border: 0; height: 48px; font-size: 15px; &::placeholder {color: #7d909a; opacity: 1; font-weight: 100}' }
              borderBottom={ 0 }
            />
          </InputWrap>
          <ButtonWrap>
            <Button
              fullHeight
              onClick={ this.addComment }
              customCss={ `
                background: white;
                color: rgb(225, 232, 237);
                box-shadow: none;
                font-size: 28px;
                padding: 0 20px;
                &:hover,
                &:active {
                  background: white;
                  color: rgb(200, 200, 200);
                  box-shadow: none;
                }
              ` }
            >
              <i className='far fa-paper-plane' />
            </Button>
          </ButtonWrap>
        </CommentInputWrap>
      </div>
    );
  }
}

export default Comments;
