import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { withRouter } from 'react-router';
import { Route, Switch } from 'react-router-dom';
import { Helmet } from 'react-helmet';
import styled, { ThemeProvider, injectGlobal } from 'styled-components';
import queryString from 'query-string';
import ReactGA from 'react-ga';

import theme from './utils/theme';

import IndexView from './views/Index';
import VerifyView from './views/manager/Verify';
import NewOrganizationView from './views/manager/NewOrganization';
import AuthIndexView from './views/auth/Index';
import KitchenSinkView from './views/KitchenSink';
import ManagerView from './views/manager/Index';
import ToastNotification from './components/common/ToastNotification';
import Loading from './components/common/Loading';

import * as UiActionCreators from './redux/ui';
import * as MeActionCreators from './redux/me';
import * as ManagerActionCreators from './redux/manager';
import * as PricingActionCreators from './redux/pricing';

const { GOOGLE_ANALYTICS_KEY } = process.env;

// eslint-disable-next-line
injectGlobal`
  * {
    box-sizing: border-box !important;
    -ms-overflow-style: none;
  }

  ::-webkit-scrollbar {
    display: none;
  }

  *:not(i) {
    font-family: 'Muli', sans-serif !important;
  }

  -webkit-font-smoothing: antialiased;
  -moz-osx-font-smoothing: auto;

  .clearfix:after {
    visibility: hidden;
    display: block;
    font-size: 0;
    content: " ";
    clear: both;
    height: 0;
  }

  h1,
  h2,
  h3,
  h4,
  h5,
  p {
    white-space: pre-line;
    color: rgb(47, 46, 46);
  }

  h1, h2, h3, h4, h5, h6 {
    font-weight: 700;
  }

  h1 {
    font-size: 45px;
    line-height: 55px;
  }

  h2 {
    font-size: 35px;
    line-height: 40px;
    letter-spacing: .5px;
  }

  h3 {
    font-size: 25px;
    line-height: 30px;
  }

  h4 {
    font-size: 20px;
    line-height: 24px;
  }

  h5 {
    font-size: 15px;
    line-height: 21px;
  }

  h6 {
    font-size: 12px;
    line-height: 18px;
  }

  p, li, a {
    font-size: 15px;
    line-height: 21px;
    margin: 0;
  }

  a {
    outline: none;
    text-decoration: none;
    color: rgb(47, 46, 46);
    cursor: pointer;

    &:hover {
      color: rgb(0, 133, 204);
    }
  }

  p {
    a {
      color: rgb(0, 133, 204);
    }

    strong {
      font-weight: 700;
    }

    em {
      font-style: italic;
    }
  }

  body {
    background: transparent !important;
  }

  button,
  body a.button {
    outline: none;
    cursor: pointer;
    height: 40px;
    padding: 0 12px;
    border: none;
    border-radius: 2px;
    background-color: rgb(0, 167, 255);
    text-align: center;
    color: white;
    font-size: 16px;
    font-weight: 500;
    // line-height: 40px;

    &:disabled {
      background-color: rgb(227, 227, 227);
      color: rgb(208, 208, 208);
    }

    &._full {
      height: 56px;
      border-radius: 4px;
      font-size: 13px;
      line-height: 56px;
      letter-spacing: 1.5px;
    }

    &._blue {
      background-color: rgb(40, 189, 245);
    }

    &._outline {
      background-color: transparent;
      color: rgb(0, 167, 255);
      border: 1px solid rgb(0, 167, 255);
    }

    &._white {
      background-color: white;
      color: rgb(208, 208, 208);
      border: 1px solid rgb(208, 208, 208);

      &:disabled {
        background-color: white;
        color: rgb(227, 227, 227);
        border-color: rgb(227, 227, 227);
      }
    }
  }

  body a span {
    display: block;
  }

  body a.button {
    display: inline-block;
    white-space: nowrap;
  }

  ::placeholder,
  ::-webkit-input-placeholder,
  ::-moz-placeholder,
  :-ms-input-placeholder,
  :-moz-placeholder {
    color: rgb(167, 167, 167);
  }

  b {
    font-weight: 600;
  }

  i:not(.fas):not(.fa):not(.fab):not(.far) {
    text-decoration: underline;
  }

  .container {
    position: relative;
    max-width: 1080px;
    margin-left: auto;
    margin-right: auto;
  }

  .content {
    min-height: calc(100vh - 60px);
    padding-bottom: 40px;
    margin-top: 60px;
  }

  .content.manager {
    min-height: calc(100vh - 105px);
    margin-top: 105px;
    padding-top: 20px;
    background-color: ${ theme.colors.lightGray };
  }

  .content.manager .manager-menu {
    display: inline-block;
    vertical-align: text-top;
    width: 190px;
    margin-right: 40px;
  }

  .content.manager .manager-content {
    display: inline-block;
    vertical-align: text-top;
    overflow: hidden;
    width: calc(100% - 230px);

    border-radius: 2px;
    box-shadow: 0 2px 4px 0 rgba(0, 0, 0, 0.02);
  }

  .content.manager .container {
    max-width: 100%;
    padding: 0 20px;
  }

  // to make body not scrollable when react-modal is open
  .ReactModal__Body--open {
    overflow: hidden;
  }

  // Override css of timepicker
  .rc-time-picker-panel-inner {
    border-radius: 10px;
  }
  .rc-time-picker-panel-input {
    font-size:15px;
  }
  .rc-time-picker-panel-input-wrap {
    padding: 6px 20px;
  }
  .rc-time-picker-panel-clear-btn {
    top: 10px;
    width: 15px;
    height: 15px;
    background-color: #7d909a;
    border-radius: 50%;
    display: flex;
  }
  .rc-time-picker-panel-clear-btn:after {
    font-size: 15px;
    color: #fff;
    width: 15px;
    margin-left: 1.5px;
    margin-top: -1.5px;
  }
  .rc-time-picker-panel-select {
    height: 280px;
    max-height: none;
  }
  .rc-time-picker-panel-select li {
    height: 40px;
    line-height: 40px;
  }
  li.rc-time-picker-panel-select-option-selected {
    background-color: ${ theme.colors.bgGray };
  }
`;

const LoadingDiv = styled.div`
  position: absolute;
  width: 100%;
  height: 100%;
  left: 0;
  top: 0;
  min-height: 100vh;

  i {
    position: absolute;
    left: 50%;
    top: 50%;
    width: 72px;
    height: 72px;
    margin: -36px 0 0 -36px;
    font-size: 72px;
    color: ${ theme.colors.mainBlue };
  }
`;

const mapStateToProps = state => ({
  user: state.me.user,
  organizations: state.manager.organizations,
  error: state.manager.error,
  loading: state.ui.loading,
  requestsLoading: state.ui.requestsLoading,
});

const ActionCreators = Object.assign(
  {},
  UiActionCreators,
  MeActionCreators,
  ManagerActionCreators,
  PricingActionCreators,
);
const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators(ActionCreators, dispatch),
});

class App extends Component {
  async componentDidMount () {
    try {
      const { actions, location, history } = this.props;
      const { token } = queryString.parse(location.search);

      ReactGA.initialize(GOOGLE_ANALYTICS_KEY);
      ReactGA.pageview(window.location.pathname + window.location.search + window.location.hash);

      if (token) {
        const baseUrl = location.pathname.split('?token')[0];
        // log in with token
        try {
          const response = await actions.login({ token });
          if (response) {
            const cookie = response.headers['set-cookie'][0];
            document.cookie = cookie;
          } else {
            // bounce if token is incorrect
            actions.setMe({});
            actions.logout(true);
          }
        } catch (e) {
          // bounce if token is incorrect
          actions.setMe({});
          actions.logout(true);
        }

        return history.replace(baseUrl);
      }

      return Promise.all([actions.getMe(), actions.getMyManagedOrganizations()]);
    } catch (e) {
      console.error(e);
    }
  }

  componentWillReceiveProps (nextProps) {
    const { location } = this.props;
    const currentPage = location.pathname + location.search + location.hash;
    const nextPage = nextProps.location.pathname + nextProps.location.search + nextProps.location.hash;

    if (currentPage !== nextPage) {
      ReactGA.pageview(nextPage); // NOTE: page title is wrong here at the moment
    }
  }

  render () {
    const {
      user, organizations, loading, requestsLoading,
    } = this.props;
    const hasLoadingRequests = !loading && !!Object.keys(requestsLoading).length;

    if (!user || !organizations) {
      return (
        <LoadingDiv>
          <i className='fas fa-spinner fa-pulse fa-3x fa-fw' />
        </LoadingDiv>
      );
    }

    const headDescription = 'On Juven, you can use our automated and intelligent tools to manage events, memberships and donations.';
    const headImage = 'https://s3.amazonaws.com/juven-prd/og_juven_logo.png';

    return (
      <ThemeProvider theme={ theme }>
        <div>
          <Helmet>
            <title>Juven Manager</title>
            <meta
              name='viewport'
              content='width=device-width, shrink-to-fit=no, initial-scale=1'
            />
            <meta name='description' content={ headDescription } />
            <meta name='twitter:card' content='summary_large_image' />
            <meta
              name='twitter:image'
              content={ headImage }
            />
            <meta property='og:type' content='website' />
            <meta property='og:title' content='Juven Manager' />
            <meta property='og:description' content={ headDescription } />
            <meta property='og:url' content='https://manager.juven.co' />
            <meta
              property='og:image'
              content={ headImage }
            />
            <meta property='og:site_name' content='Juven' />
            <meta property='og:locale' content='en_US' />
            <meta property='og:image:width' content='1000' />
            <meta property='og:image:height' content='500' />
          </Helmet>
          <ToastNotification />
          <Switch>
            <Route exact path={ ['/login', '/signup', '/forgot-password'] } component={ AuthIndexView } />
            <Route exact path='/juven-design-kitchen-sink' component={ KitchenSinkView } />
            <Route path='/verify' component={ VerifyView } />
            <Route path='/new' component={ NewOrganizationView } />
            <Route path='/:oId' component={ ManagerView } />
            <Route path='*' component={ IndexView } />
          </Switch>
          <Loading fullScreen isLoading={ hasLoadingRequests } />
        </div>
      </ThemeProvider>
    );
  }
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(App));
