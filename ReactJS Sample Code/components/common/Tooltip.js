import styled from 'styled-components';

const Tooltip = styled.div`
  visibility: hidden;
  background-color: white;
  font-size: 10px;
  line-height: 15px;
  font-weight: 400;
  text-transform: none;
  color: ${ props => props.theme.colors.darkGray };
  border-radius: 6px;
  padding: 5px 10px;
  box-shadow: 0 6px 6px 0 rgba(0, 0, 0, 0.1);
  /* Position the tooltip */
  position: absolute;
  z-index: 1000;
  left: 15px;
  top: 40px;
  max-width: 185px;
  white-space: nowrap;

  ${ props => props.customCss || '' };
`;

export default Tooltip;
