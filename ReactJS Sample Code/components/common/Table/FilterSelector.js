import React, { Component } from 'react';
import styled from 'styled-components';
import Button from '../Button';
import Input from '../Input';
import MultiSelect from '../MultiSelect';

import { boxShadow } from '../../../utils/styles';

import i18n from '../../../../i18n';

const t = a => i18n.t([`components/common:${ a }`]);

const Wrapper = styled.div`
  position: relative;
  height: 48px;
  width: 48px;
`;

const ToggleButton = Button.extend`
  height: 48px;
  width: 48px;
  padding: 12px;
  margin: 0;

  & i {
    margin-right: 0;
    font-size: 24px;
    line-height: 24px;
    color: ${ props => (props.active ? props.theme.colors.darkBlue : props.theme.colors.mainGray) };
  }

  &:hover, &:active {
    & i {
      color: ${ props => props.theme.colors.mainBlue };
    }
  }
`;

const Dropdown = styled.div`
  position: absolute;
  z-index: 300;
  right: -10px;
  top: 53px;
  width: 400px;
  min-height: 100px;
  border-radius: 6px;
  padding: 30px;
  background-color: white;

  ${ boxShadow('0 6px 6px 0 rgba(0, 0, 0, 0.1)') };
`;

const FieldWrapper = styled.div`
  display: flex;
  align-items: center;
  padding-top: 15px;
`;

const FieldLabel = styled.p`
  flex-basis: 90px;
  flex-shrink: 0;
  font-size: 12px;
  line-height: 36px;
  padding-top: 4px;
  color: #9c9c9c;
`;

const ActionsWrapper = styled.div`
  text-align: right;
  padding-top: 30px;
`;

const ActionButton = Button.extend`
  width: 100px;
  height: 30px;
  line-height: 30px;
  border-radius: 15px;
  font-size: 12px;
  text-transform: none;
  padding: 0;
  margin: 0 20px 0 0;

  &:last-child {
    margin-right: 0;
  }
`;

const inputCss = `
  width: 250px;
  height: 40px;
  margin-top: 0;
  margin-bottom: 0;
  margin-right: 0;

  input {
    height: 40px;
  }
`;

const multiSelectCss = `
  width: 250px;
  margin-top: 0;
  margin-bottom: 0;

  .control__control {
    padding: 4px 0 !important;
    border-bottom: 1px solid rgba(47, 46, 46, .25) !important;
  }
`;

export default class FilterSelector extends Component {
  constructor (props) {
    super();

    this.state = {
      showingDropdown: false,
      updated: false,
      filterData: props.filters || {},
    };

    this.hideDropdownHandler = this.hideDropdown.bind(this);
  }

  componentDidMount () {
    document.addEventListener('mousedown', this.hideDropdownHandler, false);
  }

  componentWillReceiveProps (nextProps) {
    const { filters, expandFilters } = this.props;
    if (nextProps.filters !== filters) {
      this.setState({
        filterData: nextProps.filters,
        updated: false,
      });
    }

    // HACK: use this prop to stimulate expanding of dropdown inside FilterSelector
    if (nextProps.expandFilters && !expandFilters) {
      this.setState({
        showingDropdown: true,
      });
    }
  }

  componentWillUnmount () {
    document.removeEventListener('mousedown', this.hideDropdownHandler, false);
  }

  hideDropdown (e) {
    if (this.dropdownWrapper && this.dropdownWrapper.contains(e.target)) return;
    this.setState({ showingDropdown: false });
  }

  toggleDropdown () {
    const { showingDropdown } = this.state;
    this.setState({
      showingDropdown: !showingDropdown,
    });
  }

  updateFilterData (field, value) {
    const { filterData } = this.state;
    const newFilterData = Object.assign({}, filterData);
    const hasValue = Array.isArray(value) ? value.length : !!value;

    if (hasValue) {
      newFilterData[field] = value;
    } else {
      delete newFilterData[field];
    }

    this.setState({
      filterData: newFilterData,
      updated: true,
    });
  }

  applyFilters () {
    const { onSetFilters } = this.props;
    const { filterData } = this.state;
    if (!onSetFilters) return;

    this.toggleDropdown();
    onSetFilters(filterData);
  }

  resetFilters () {
    const { onSetFilters } = this.props;
    if (!onSetFilters) return;

    this.setState({ filterData: {} }, () => {
      this.toggleDropdown();
      onSetFilters({});
    });
  }

  render () {
    const { fields = [] } = this.props;
    const { showingDropdown, updated, filterData } = this.state;

    const hasAppliedFilters = Object.keys(filterData).length;

    const FieldComponents = fields.map((f, i) => {
      const { field, label = '', options = [] } = f;
      const value = filterData[field];

      let InputComponent = null;
      if (f.type === 'input') {
        InputComponent = (
          <Input
            key={ `table-filter-${ i }` }
            fullWidth
            field={ field }
            value={ value }
            onChange={ this.updateFilterData.bind(this) }
            customCss={ inputCss }
          />
        );
      } else if (f.type === 'multiSelect') {
        InputComponent = (
          <MultiSelect
            key={ `table-filter-${ i }` }
            underline
            fullWidth
            field={ field }
            value={ value || [] }
            options={ options }
            onChange={ v => this.updateFilterData(field, v) }
            customCss={ multiSelectCss }
          />
        );
      }

      if (!InputComponent) return null;

      return (
        <FieldWrapper>
          <FieldLabel>{ label }</FieldLabel>
          { InputComponent }
        </FieldWrapper>
      );
    });

    const FilterDropdown = (
      <Dropdown innerRef={ wrapper => (this.dropdownWrapper = wrapper) }>
        <div>{ FieldComponents }</div>
        <ActionsWrapper>
          <ActionButton gray onClick={ this.resetFilters.bind(this) }>
            { t('Reset') }
          </ActionButton>
          <ActionButton
            light
            onClick={
              updated
                ? this.applyFilters.bind(this)
                : this.toggleDropdown.bind(this)
            }
          >
            { t('Apply') }
          </ActionButton>
        </ActionsWrapper>
      </Dropdown>
    );

    return (
      <Wrapper>
        <ToggleButton
          plain
          active={ hasAppliedFilters }
          onClick={ this.toggleDropdown.bind(this) }
        >
          <i className='fas fa-filter' />
        </ToggleButton>
        { showingDropdown ? FilterDropdown : null }
      </Wrapper>
    );
  }
}
