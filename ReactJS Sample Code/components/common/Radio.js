import React from 'react';
import styled from 'styled-components';

const Span = styled.span`
  display: inline-block;
  vertical-align: text-top;
  position: relative;
  width: 18px;
  height: 18px;
  margin-right: 10px;
`;

const RadioStyled = styled.input`
  display: none;
  user-select: none;
  margin-right: 10px;
  vertical-align: middle;

  &:checked + label {
    background-color: ${ props => props.theme.colors.mainBlue };
    border-color: ${ props => props.theme.colors.mainBlue };
    opacity: 1;

    &:after {
      opacity: 1;
      border-color: white;
    }

    &:hover {
      opacity: 0.85;
    }
  }
`;

const Label = styled.label`
  position: absolute;
  left: 0;
  top: 0;
  width: 18px;
  height: 18px;
  cursor: pointer;
  border-radius: 9px;
  border: 1px solid ${ props => props.theme.colors.lightGray };

  &:after {
    opacity: 0;
    content: '';
    position: absolute;
    width: 6px;
    height: 6px;
    border-radius: 3px;
    left: 5px;
    top: 5px;
    background: white;
  }

  &:hover,
  &:active {
    background-color: ${ props => props.theme.colors.mainBlue };
    border-color: ${ props => props.theme.colors.lightGray };
    opacity: 0.15;
  }

  &:hover:after {
    opacity: 0.7;
  }

  &:active:after {
    opacity: 0.85;
  }
`;

const Radio = ({
  field, value, style, spanStyle, ...props
}) => (
  <Span style={ spanStyle }>
    <RadioStyled
      type='radio'
      id={ `${ field }-${ value }` }
      data-field={ field }
      { ...props }
      value={ value }
    />
    <Label htmlFor={ `${ field }-${ value }` } style={ style } />
  </Span>
);

export default Radio;
