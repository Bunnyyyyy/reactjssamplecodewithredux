import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { withRouter } from 'react-router';
import styled from 'styled-components';

import * as UiActionCreators from '../../../../../redux/ui';
import * as ManagerActionCreators from '../../../../../redux/manager';
import * as EventActionCreators from '../../../../../redux/event';

import Button from '../../../../../components/common/Button';
import ConfirmModal from '../../../../../components/common/ConfirmModal';
import EditInfoCard from '../../../../../components/manager/cards/EditInfoCard';
import Label from '../../../../../components/common/inputs/Label';
import SaveActions from '../../../../../components/manager/SaveActions';

import i18n from '../../../../../../i18n';

const t = a => i18n.t([`translations:${ a }`]);

const TrashButton = Button.extend`
  color: ${ props => props.theme.colors.mainGray };
  margin-left: 20px;

  &:hover,
  &:active {
    color: ${ props => props.theme.colors.mainRed };
  }
`;

const IconTitle = styled.i`
  color: ${ props => props.theme.colors.mainGray };
  font-size: 20px;
  font-weight: bolder;
  margin-right: 20px;
  cursor: pointer;
`;

const FlexAlignItemCenter = styled.div`
  display: flex;
  align-items: center;
`;

const ProvidesP = styled.p`
  height: 32px;
  line-height: 32px;
  margin: 0 0 20px 0;
`;

const DiscountWhenP = styled.p`
  height: 32px;
  line-height: 32px;
  margin: 20px 0;
`;

const comboSelectCss = `
  padding-right: 0;
  .select__container {
    border-right: 1px solid rgb(47, 46, 46, .25);
  }

  .select__indicator {
    @media (max-width: 1200px) {
      padding: 0 2px;
    }
  }
`;

const comboInputCss = `
  input {
    padding-left: 10px;
  }
`;

const DISCOUNT_TYPE_OPTIONS = [
  { label: '$', value: 'fixed_amount' },
  { label: '%', value: 'percentage' },
];

const TICKET_QUANTITY_OPTIONS = [
  { label: '=', value: 'exact' },
  { label: '>=', value: 'at_least' },
];

const mapStateToProps = state => ({
  event: state.manager.event,
  organization: state.manager.organization,
  ticketPackage: state.event.ticketPackage,
  tickets: state.event.tickets,
  loading: state.ui.loading,
});

const ActionCreators = Object.assign(
  {},
  UiActionCreators,
  ManagerActionCreators,
  EventActionCreators,
);
const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators(ActionCreators, dispatch),
});

class EventTicketingPackagesEditView extends Component {
  constructor () {
    super();

    this.state = {
      validateForm: false,
    };
  }

  async componentDidMount () {
    const { actions, match } = this.props;
    const { eId, pkId } = match.params;

    actions.setLoading(true);

    try {
      const promises = [
        actions.getEventTickets(eId),
        actions.getEventTicketPackages(eId),
      ];
      if (pkId) {
        promises.push(actions.getEventTicketPackage(eId, pkId));
      } else {
        actions.setEventTicketPackage({});
      }

      await Promise.all(promises);
    } catch (e) {
      console.error('ERROR: EventTicketingPackagesEditView', e);
    } finally {
      actions.setLoading(false);
    }
  }

  componentWillUnmount () {
    const { actions } = this.props;
    actions.resetSaveActions();
  }

  async onInvalidSave () {
    this.setState({ validateForm: true });
  }

  async onCancel () {
    const { history, organization, event } = this.props;
    history.push(
      `/${ organization.id }/events/${ event.id }/ticketing/packages`,
    );
  }

  async onDelete () {
    const {
      ticketPackage, organization, event, actions, history,
    } = this.props;
    const eId = event.id;
    const pkId = ticketPackage.id;

    if (eId && pkId) {
      await Promise.all([actions.removeEventTicketPackage(eId, pkId)]);
    }

    history.push(`/${ organization.id }/events/${ event.id }/ticketing/packages`);
  }

  async savePackage (state) {
    const {
      ticketPackage, organization, event, actions, history,
    } = this.props;
    const eId = event.id;
    const pkId = ticketPackage.id;

    try {
      if (pkId) {
        await actions.updateEventTicketPackage(eId, pkId, state);
        await actions.getEventTicketPackage(eId, pkId);
      } else {
        const newPackage = await actions.createEventTicketPackage(eId, state);
        history.push(
          `/${ organization.id }/events/${ event.id }/ticketing/packages/${
            newPackage.id
          }`,
        );
      }
    } catch (e) {
      throw e;
    }
  }

  toggleDeleting () {
    const { isDeleting } = this.state;
    this.setState({
      isDeleting: !isDeleting,
    });
  }

  render () {
    const {
      history,
      organization,
      event,
      tickets,
      ticketPackage = {},
      loading,
    } = this.props;
    const { validateForm, isDeleting } = this.state;

    const TitleWrapper = () => (
      <div>
        <IconTitle
          className='fas fa-chevron-left'
          onClick={ () => {
            history.push(`/${ organization.id }/events/${ event.id }/ticketing/packages`);
          } }
        />
        { ticketPackage.id ? t('EDIT YOUR PACKAGE') : t('CREATE PACKAGE') }
      </div>
    );

    const eventTicketPackagesFields = [
      {
        type: 'input',
        field: 'name',
        label: t('Package Name'),
        default: ticketPackage.name,
        required: true,
      },
      {
        type: 'custom',
        field: 'title-3',
        readOnly: true,
        flex: '0 0 15%',
        label: t('Condition'),
        renderComponent: () => (
          <div>
            <Label focused label={ t('Condition') } />
            <ProvidesP>{ t('Provides') }</ProvidesP>
          </div>
        ),
      },
      {
        type: 'select',
        required: true,
        field: 'discount_type',
        flex: '0 0 8%',
        default: ticketPackage.discount_type,
        options: DISCOUNT_TYPE_OPTIONS,
        customCss: comboSelectCss,
      },
      {
        type: 'input',
        number: true,
        field: 'discount_value',
        label: ' ',
        default: ticketPackage.discount_value,
        flex: '0 0 15%',
        customCss: comboInputCss,
        getErrorMessage: (formState, value) => {
          if (formState.unlimited_quantity || value) {
            return null;
          }
          return t('Please enter a valid Quantity');
        },
      },
      {
        type: 'custom',
        field: 'title-3',
        readOnly: true,
        flex: '0 0 39%',
        renderComponent: () => (
          <DiscountWhenP>
            { t('discount when total tickets quantity') }
          </DiscountWhenP>
        ),
      },
      {
        type: 'select',
        required: true,
        field: 'quantity_match_type',
        flex: '0 0 8%',
        default: ticketPackage.quantity_match_type,
        options: TICKET_QUANTITY_OPTIONS,
        customCss: comboSelectCss,
      },
      {
        type: 'input',
        number: true,
        field: 'quantity',
        label: ' ',
        default: ticketPackage.quantity,
        flex: '0 0 15%',
        customCss: comboInputCss,
        getErrorMessage: (formState, value) => {
          if (formState.unlimited_quantity || value) {
            return null;
          }
          return t('Please enter a valid Quantity');
        },
      },
      {
        type: 'multiSelect',
        field: 'tickets',
        label: t('Ticket Types included in this package'),
        default: ticketPackage.tickets,
        options: tickets.map(ti => ({
          label: ti.name,
          value: ti.id,
        })),
        required: true,
      },
      {
        type: 'boolean',
        field: 'distinct_ticket_types',
        label: t(
          'Apply package only when each ticket has a different ticket type',
        ),
        default: ticketPackage.distinct_ticket_types,
      },
      {
        type: 'custom',
        flex: '0 0 100%',
        renderComponent: () => (
          <FlexAlignItemCenter>
            <SaveActions
              saveNewItem={ !ticketPackage.id }
              onInvalid={ this.onInvalidSave.bind(this) }
              onCancel={ this.onCancel.bind(this) }
            />
            { ticketPackage.id && (
              <TrashButton plain onClick={ this.toggleDeleting.bind(this) }>
                <i className='fa fa-trash' aria-hidden='true' />
              </TrashButton>
            ) }
          </FlexAlignItemCenter>
        ),
      },
    ];

    return (
      <div>
        { !loading ? (
          <EditInfoCard
            name='packages'
            title={ <TitleWrapper /> }
            fields={ eventTicketPackagesFields }
            action={ this.savePackage.bind(this) }
            actionOrder={ 0 }
            formCss='align-items: flex-end;'
            validateForm={ !!ticketPackage.id || validateForm }
          />
        ) : null }
        { isDeleting ? (
          <ConfirmModal
            warning
            message={ t('Are you sure you want to delete this Package') }
            onConfirm={ this.onDelete.bind(this) }
            onCancel={ this.toggleDeleting.bind(this) }
          />
        ) : null }
      </div>
    );
  }
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(EventTicketingPackagesEditView));
