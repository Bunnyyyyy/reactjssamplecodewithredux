import React from 'react';
import styled from 'styled-components';

import theme from '../../../../../utils/theme';

const Wrapper = styled.a`
  display: inline-block;
  font-size: inherit;
  color: ${ theme.colors.mainBlue };
  line-height: inherit;
`;

function Link ({ children, attributes, node }) {
  return (
    <Wrapper { ...attributes } href={ node.data.get('href') } target='_blank'>
      { children }
    </Wrapper>
  );
}

export default Link;
