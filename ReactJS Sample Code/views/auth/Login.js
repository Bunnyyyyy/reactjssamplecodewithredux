import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { withRouter } from 'react-router';
import { Link } from 'react-router-dom';
import styled from 'styled-components';
import queryString from 'query-string';
import { Formik } from 'formik';
import * as Yup from 'yup';

import * as MeActionCreators from '../../redux/me';
import * as ManagerActionCreators from '../../redux/manager';

import Input from '../../components/common/Input';
import PasswordMaskInput from '../../components/common/inputs/PasswordMaskInput';
import Button from '../../components/common/Button';
import Divider from '../../components/common/Divider';
import FacebookLoginButton from '../../components/common/FacebookLoginButton';
import GoogleLoginButton from '../../components/common/GoogleLoginButton';

import { t as translate } from '../../../i18n';

const t = a => translate(a, 'manager/index');

const Title = styled.h3`
  text-align: center;
  margin-bottom: 30px;
`;

const StyledInput = styled(Input)`
  margin-bottom: 30px;
`;

const ForgotButton = styled.button`
  background-color: transparent;
  color: ${ props => props.theme.colors.darkGray};
  font-size: 12px;
  font-weight: 700;
  line-height: 14px;
  height: 24px;
  opacity: 0.4;
  padding: 5px 0;
  position: absolute;
  right: 0;
  top: -5px;

  &:hover,
  &:active {
    background-color: transparent;
  }

  &:hover {
    opacity: 1;
  }
`;

const ToggleWrapper = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  margin: 30px 0;
  
  p {
    margin-right: 10px;
  }

  a {
    color: ${ props => props.theme.colors.mainBlue};
  }
`;

const mapStateToProps = state => ({
  user: state.me.user,
});

const ActionCreators = Object.assign(
  {},
  MeActionCreators,
  ManagerActionCreators,
);
const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators(ActionCreators, dispatch),
});

const INVALID_CREDENTIALS_MESSAGE = t('Your login credentials are incorrect.');

class AuthLoginView extends Component {
  constructor() {
    super();

    this.state = {
      loginError: null,
    };
  }

  componentDidMount() {
    if (this.input) {
      this.input.focus();
    }
  }

  onLoggedIn() {
    const { location, history } = this.props;
    const { redirect } = queryString.parse(location.search);

    const redirectUrl = redirect ? decodeURIComponent(redirect) : '/';
    return history.push(redirectUrl);
  }

  async login(state) {
    const { actions } = this.props;

    try {
      this.setState({ loginError: null });
      await actions.login(state);
      await actions.getMyManagedOrganizations();
      await actions.getMe();

      this.onLoggedIn();
    } catch (e) {
      const loginError = e.status === 401
        ? INVALID_CREDENTIALS_MESSAGE
        : e.message;
      return this.setState({ loginError });
    }
  }

  render() {
    const { loginError } = this.state;

    const LoginFormSchema = Yup.object().shape({
      login: Yup.string()
        .nullable()
        .required(t('Please fill in your Juven ID or email address')),
      password: Yup.string()
        .nullable()
        .required(t('Please fill in your password')),
    });

    return (
      <Formik
        initialValues={{}}
        validationSchema={LoginFormSchema}
        onSubmit={this.login.bind(this)}
      >
        {(props) => {
          const {
            values, errors, setFieldValue, handleSubmit, submitCount, isValid
          } = props;
          const validateOnChange = submitCount > 0;
          console.log(isValid);
          return (
            <div>
              <Title>{t('Log in to Juven Manager')}</Title>
              <div>
                <FacebookLoginButton onFinish={this.onLoggedIn.bind(this)} />
                <GoogleLoginButton onFinish={this.onLoggedIn.bind(this)} />
                <Divider text='OR' />
              </div>
              <StyledInput
                key='login'
                field='login'
                fullWidth
                label={`${t('Email')} / ${t('Juven ID')}`}
                inputRef={input => (this.input = input)}
                name='login'
                value={values.login}
                errorMessage={errors.login}
                onChange={(_, value) => setFieldValue('login', value, validateOnChange)}
              />
              <PasswordMaskInput
                type='password'
                key='password'
                field='password'
                fullWidth
                label={t('Password')}
                name='password'
                value={values.password}
                errorMessage={loginError || errors.password}
                onChange={(_, value) => setFieldValue('password', value, validateOnChange)}
              >
                <Link to='/forgot-password'>
                  <ForgotButton type='button'>
                    {t('Forgot password?')}
                  </ForgotButton>
                </Link>
              </PasswordMaskInput>
              <Button fullWidth onClick={handleSubmit}>
                {t('Log in')}
              </Button>
              <ToggleWrapper>
                <p>{t('Don\'t have an account?')}</p>
                <Link to='/signup'>
                  {t('Create one now')}
                </Link>
              </ToggleWrapper>
            </div>
          );
        }}
      </Formik>
    );
  }
}

export default withRouter(
  connect(mapStateToProps, mapDispatchToProps)(AuthLoginView),
);
