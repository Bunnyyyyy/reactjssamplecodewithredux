import React from 'react';

import NavBar from '../../common/NavBar';
import i18n from '../../../../i18n';

const t = a => i18n.t([`components/manager:${ a }`]);

const HomeNavBar = ({ organization }) => {
  const navLinks = [
    {
      title: t('Overview'),
      route: `/${ organization.id }`,
      exact: true,
    },
    {
      title: t('Income'),
      route: `/${ organization.id }/home/income`,
    },
    {
      title: t('Contacts'),
      route: `/${ organization.id }/home/contacts`,
    },
  ];

  return (
    <NavBar links={ navLinks } />
  );
};

export default HomeNavBar;
