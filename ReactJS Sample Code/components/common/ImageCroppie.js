import React, { Component } from 'react';
import styled from 'styled-components';

import Button from './Button';
import imageIcon from '../../assets/manager/icon-image.svg';

const ImageCroppieDiv = styled.div`
  position: relative;
  margin-bottom: 0 !important;

  & .croppie-container {
    height: auto;
    overflow-y: scroll;
    padding-left: 30px;
    padding-right: 30px;
  }

  & .cr-boundary {
    width: 100% !important;
    height: calc(100vh - 343px) !important;
    margin-top: 78px;
  }

  & .cr-slider-wrap {
    background-color: white;
    z-index: 200;
    position: absolute;
    width: 100%;
    top: 30px;
    padding: 15px 0;
    padding-top: 40px;
    margin: 0;
  }

  & .small-image {
    position: absolute;
    z-index: 201;
    right: 66%;
    top: 75px;
  }

  & .big-image {
    position: absolute;
    z-index: 201;
    left: 72%;
    top: 69px;
  }
`;

const ButtonsWrapper = styled.div`
  padding-top: 20px;
  padding-bottom: 20px;
  display: flex;
  justify-content: flex-end;
  padding-right: 30px;
`;

class ImageCroppie extends Component {
  constructor () {
    super();

    this.state = {
      croppie: null,
    };
  }

  componentDidMount () {
    const {
      viewportWidth,
      viewportHeight,
      viewportType,
      enableResize,
      previewImageFile,
    } = this.props;

    const opts = {
      viewport: {
        width: viewportWidth * 1.5,
        height: viewportHeight * 1.5,
        type: viewportType,
      },
      boundary: {
        width: viewportWidth,
        height: viewportHeight,
      },
      enableResize,
      mouseWheelZoom: false,
    };

    // eslint-disable-next-line
    const c = new Croppie(this.refs.croppie, opts);
    c.bind({
      url: previewImageFile.preview,
    });
    this.setState({
      croppie: c,
    });
  }

  onCrop () {
    const { croppie } = this.state;
    const { onCroppieSave, field } = this.props;
    croppie.result({ type: 'blob', size: 'original' }).then((blob) => {
      onCroppieSave(field, blob);
    });
  }

  render () {
    const { onClose } = this.props;

    return (
      <ImageCroppieDiv>
        <div ref='croppie' />
        <ButtonsWrapper>
          <Button small gray onClick={ onClose }>
            Cancel
          </Button>
          <Button small light onClick={ this.onCrop.bind(this) }>
            Save
          </Button>
        </ButtonsWrapper>
        <img
          alt=''
          src={ imageIcon }
          className='small-image'
          width='16px'
          height='12px'
        />
        <img
          alt=''
          src={ imageIcon }
          className='big-image'
          width='23px'
          height='21px'
        />
      </ImageCroppieDiv>
    );
  }
}

export default ImageCroppie;
