import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import styled from 'styled-components';
import i18n from '../../../i18n';

import Input from './Input';
import Button from './Button';
import Modal from './ModalV2';
import FacebookLoginButton from './FacebookLoginButton';
import GoogleLoginButton from './GoogleLoginButton';

import * as UiActionCreators from '../../redux/ui';
import * as MeActionCreators from '../../redux/me';
import * as ManagerActionCreators from '../../redux/manager';

import { validateEmail } from '../../utils';
import { track, events } from '../../utils/mixpanel';

const t = a => i18n.t([`components/common:${ a }`]);

const ActionCreators = Object.assign(
  {},
  UiActionCreators,
  MeActionCreators,
  ManagerActionCreators,
);

const Separator = styled.p`
  line-height: 0.1em;
  margin: 30px 0 30px !important;
  border-bottom: 2px solid ${ props => props.theme.colors.lightGray };
  text-align: center;
  text-transform: uppercase;
  font-size: 10px;
  font-weight: 600;
  letter-spacing: 1.5px;
  color: ${ props => props.theme.colors.deadGray };

  & span {
    background-color: white;
    padding: 0 5px;
  }
`;

const LoginModalDiv = styled.div`
  padding-bottom: 30px;

  & form {
    text-align: center;
  }

  h5,
  p {
    text-align: center;
    margin-bottom: 10px;
  }

  & .success {
    text-align: center;
    margin-bottom: 0 !important;
    width: 100%;

    .email {
      font-size: 24px;
      line-height: 48px;
      font-weight: 600;
      color: ${ props => props.theme.colors.mainGreen };
    }
  }

  & .toggle {
    display: block;
    margin: 0 auto;
    background: none;
    font-size: 12px;
    height: 30px;
    line-height: 30px;
    font-weight: 500;
    color: ${ props => props.theme.colors.mainBlue };
  }

  @media (max-width: 900px) {
    & .modal-section {
      width: 100%;
    }
  }
`;

const ForgotButton = styled.button`
  position: absolute;
  top: -3px;
  right: 40px;
  height: 32px;
  padding: 0;
  background-color: transparent;
  font-size: 14px;
  color: white;
  letter-spacing: 0.6px;
  opacity: 0.5;

  &:hover,
  &:active {
    background-color: transparent;
  }

  &:hover {
    opacity: 1;
  }
`;

const RedirectButton = styled.button`
  font-size: 16px;
  color: white;
  background: none;
  font-weight: bold;
`;
const Underline = styled.span`
  text-decoration: underline;
`;

const InputGroup = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: space-evenly;

  > * {
    margin-right: 0;
    width: 50%;

    &:nth-child(1) {
      padding-right: 0.5rem;
    }

    &:nth-child(2) {
      padding-left: 0.5rem;
    }
  }
`;

const mapStateToProps = state => ({
  profile: state.profile.profile,
  loading: state.ui.loading,
});
const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators(ActionCreators, dispatch),
});

class LoginModal extends Component {
  constructor (props) {
    super();

    const { signup } = props;
    this.state = {
      login: '',
      password: '',
      email: '',
      error: null,
      isSigningUp: !!signup,
      isSignedUp: false,
      forgotPassword: false,
      sentResetPassword: false,
    };
  }

  getSignupFormError () {
    const {
      first_name, last_name, email, password,
    } = this.state;

    let error = null;
    if (!first_name || !first_name.length) {
      error = t('Please fill in your first name');
    } else if (!last_name || !last_name.length) {
      error = t('Please fill in your last name');
    } else if (!email || !email.length || !validateEmail(email)) {
      error = t('Please fill in a valid email address');
    } else if (!password || password.length < 8) {
      error = t('Please fill in a password with at least 8 characters');
    }

    return error;
  }

  getLoginFormError () {
    const { login, password } = this.state;

    let error = null;
    if (!login || !login.length) {
      error = t('Please fill in your email / Juven ID');
    } else if (!password || !password.length) {
      error = t('Please fill in your password');
    }

    return error;
  }

  async login (e) {
    e.preventDefault();

    const { actions, onLoggedIn } = this.props;
    const { login, password } = this.state;

    const error = this.getLoginFormError();
    if (error) return this.setState({ error });

    try {
      const loginData = {
        login,
        password,
      };
      await actions.login(loginData);
      await actions.getMyManagedOrganizations();
      await actions.getMe();
      await onLoggedIn();
    } catch (err) {
      const errorMessage = err.message;
      return this.setState({ error: errorMessage });
    }
  }

  async signup (e) {
    e.preventDefault();

    const { actions } = this.props;
    const {
      first_name, last_name, email, password,
    } = this.state;

    const error = this.getSignupFormError();
    if (error) return this.setState({ error });

    try {
      const signupData = {
        first_name, last_name, email, password,
      };
      await actions.signup(signupData);
      await actions.getMyManagedOrganizations();
      await actions.getMe();

      track(events.SIGN_UP);
      this.setState({
        isSigningUp: false,
        isSignedUp: true,
      });
    } catch (err) {
      const errorMessage = err.message;
      return this.setState({ error: errorMessage });
    }
  }

  async forgotPassword (e) {
    e.preventDefault();

    const { actions } = this.props;
    const { email } = this.state;

    if (!email || !email.length) {
      return this.setState({
        error: t('Please fill in your Email Address'),
      });
    }

    const forgotData = { email };

    try {
      await actions.forgotPassword(forgotData);
      this.setState({
        forgotPassword: false,
        sentResetPassword: true,
      });
    } catch (err) {
      const errorMessage = err.message;
      return this.setState({ error: errorMessage });
    }
  }

  toggleLoggingIn () {
    const { isLoggingIn } = this.state;

    this.setState({
      isLoggingIn: !isLoggingIn,
      error: null,
    });
  }

  toggleSigningUp () {
    const { isSigningUp } = this.state;

    this.setState({
      isSigningUp: !isSigningUp,
      error: null,
    });
  }

  toggleForgotPassword () {
    const { forgotPassword } = this.state;

    this.setState({
      forgotPassword: !forgotPassword,
      error: null,
    });
  }

  changeInput (field, value) {
    this.setState({
      [field]: value,
      error: null,
    });
  }

  render () {
    const {
      isOpen, onLoggedIn, onCancel, onCheckout, isPaying, newStyle,
    } = this.props;
    const {
      login,
      first_name,
      last_name,
      email,
      password,
      error,
      isLoggingIn,
      isSigningUp,
      isSignedUp,
      forgotPassword,
      sentResetPassword,
    } = this.state;

    const invalidLoginForm = !!this.getLoginFormError();
    const invalidSignupForm = !!this.getSignupFormError();

    let LoginContent = null;
    if (!isPaying || isLoggingIn) {
      LoginContent = [
        <Input
          fullWidth
          field='login'
          label={ t('Juven ID / Email') }
          value={ login }
          onChange={ this.changeInput.bind(this) }
        />,
        <Input
          fullWidth
          field='password'
          type='password'
          label={ t('Password') }
          value={ password }
          onChange={ this.changeInput.bind(this) }
          error={ error }
        >
          <ForgotButton
            type='button'
            onClick={ this.toggleForgotPassword.bind(this) }
          >
            { t('Forgot?') }
          </ForgotButton>
        </Input>,
        <Button
          fullWidth
          disabled={ invalidLoginForm }
          onClick={ this.login.bind(this) }
        >
          { t('Log in') }
        </Button>,
        <button
          type='button'
          className='toggle'
          onClick={ this.toggleSigningUp.bind(this) }
        >
          { t('Create new account with email') }
        </button>,
      ];

      if (isPaying) {
        LoginContent.push(
          <button type='button' className='toggle' onClick={ onCheckout }>
            { t('Pay as guest') }
          </button>,
        );
      }
    } else {
      LoginContent = [
        <Button fullWidth type='button' onClick={ onCheckout }>
          { t('Pay as guest') }
        </Button>,
        <RedirectButton type='button' onClick={ this.toggleLoggingIn.bind(this) }>
          { t('Already have an account?') }
          { ' ' }
          <Underline>{ t('Sign in') }</Underline>
        </RedirectButton>,
      ];
    }

    const LoginForm = (
      <form onSubmit={ this.login.bind(this) }>
        <div className='modal-section login'>
          <FacebookLoginButton
            onFinish={ onLoggedIn }
            newStyle={ newStyle }
            customCss='margin-top: 0;'
          />
          <GoogleLoginButton
            onFinish={ onLoggedIn }
            newStyle={ newStyle }
          />
          <Separator>
            <span>{ t('or') }</span>
          </Separator>
          { LoginContent }
        </div>
      </form>
    );

    const SignupForm = (
      <div>
        <form onSubmit={ this.signup.bind(this) }>
          <FacebookLoginButton
            textContent={ t('Sign up with Facebook') }
            style={ { marginBottom: 10 } }
            onFinish={ onLoggedIn }
            newStyle={ newStyle }
          />
          <GoogleLoginButton
            textContent={ t('Sign up with Google') }
            onFinish={ onLoggedIn }
            newStyle={ newStyle }
          />
          <Separator>
            <span>or</span>
          </Separator>
          <InputGroup>
            <Input
              fullWidth
              field='first_name'
              label={ t('First Name') }
              value={ first_name }
              onChange={ this.changeInput.bind(this) }
            />
            <Input
              fullWidth
              field='last_name'
              label={ t('Last Name') }
              value={ last_name }
              onChange={ this.changeInput.bind(this) }
            />
          </InputGroup>
          <Input
            fullWidth
            field='email'
            label={ t('Email Address') }
            value={ email }
            onChange={ this.changeInput.bind(this) }
          />
          <Input
            fullWidth
            field='password'
            type='password'
            label='Password'
            value={ password }
            onChange={ this.changeInput.bind(this) }
            error={ error }
          />
          <Button
            fullWidth
            onClick={ this.signup.bind(this) }
            gray={ invalidSignupForm }
          >
            { t('Sign up') }
          </Button>
          <button
            type='button'
            className='toggle'
            onClick={ this.toggleSigningUp.bind(this) }
          >
            { t('Have an account already?') }
          </button>
        </form>
      </div>
    );

    const SignupSuccess = (
      <div>
        <p>{ t('Welcome onboard Juven!') }</p>
        <p>
          { t(
            'Please be reminded that you would have to verify your account before you can view your profile.',
          ) }
        </p>
      </div>
    );

    const ForgotPasswordForm = (
      <div>
        <p>
          { t(
            'Please fill in your email address so we can send you a link to reset your password.',
          ) }
        </p>
        <form onSubmit={ this.forgotPassword.bind(this) }>
          <Input
            fullWidth
            field='email'
            label={ t('Email Address') }
            value={ email }
            onChange={ this.changeInput.bind(this) }
            error={ error }
          />
          <Button fullWidth onClick={ this.forgotPassword.bind(this) }>
            { t('Confirm') }
          </Button>
          <button
            type='button'
            className='toggle'
            onClick={ this.toggleForgotPassword.bind(this) }
          >
            { t('Back to Login') }
          </button>
        </form>
      </div>
    );

    const SentReset = (
      <div>
        <p>{ t('An email with the reset link has been sent to') }</p>
        <h5>{ email }</h5>
        <p>
          { t('Please follow the instructions there to reset your password.') }
        </p>
      </div>
    );

    let modalTitle = t('Log in to Juven now');
    let ModalContent = LoginForm;
    if (isSigningUp) {
      ModalContent = SignupForm;
      modalTitle = t('Sign up, it\'s free!');
    } else if (isSignedUp) {
      ModalContent = SignupSuccess;
      modalTitle = t('Thank you for signing up!');
    } else if (forgotPassword) {
      ModalContent = ForgotPasswordForm;
      modalTitle = t('Forgot your password?');
    } else if (sentResetPassword) {
      ModalContent = SentReset;
      modalTitle = t('Reset your password');
    }

    return (
      <Modal small isOpen={ isOpen } onClose={ onCancel } title={ modalTitle }>
        <LoginModalDiv>{ ModalContent }</LoginModalDiv>
      </Modal>
    );
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(LoginModal);
