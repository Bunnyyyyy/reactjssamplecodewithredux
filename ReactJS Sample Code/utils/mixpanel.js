import mixpanel from 'mixpanel-browser';

const { MIXPANEL_API_KEY } = process.env;

export const track = config => {
  if (process.env.NODE_ENV === 'production' && process.browser) {
    mixpanel.init(MIXPANEL_API_KEY);
    return mixpanel.track(config);
  } else {
    return () => null;
  }
};

export const events = {
  PAGE_VISIT: 'Page Visit',
  SIGN_UP: 'Sign Up'
};
