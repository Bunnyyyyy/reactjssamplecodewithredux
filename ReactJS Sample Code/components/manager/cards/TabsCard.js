import React, { Component } from 'react';
import styled from 'styled-components';
import Card from './Card';

const Wrapper = styled.div`
  display: flex;
`;

const ItemIcon = styled.img`
  display: inline;
  height: 24px;
  width: 24px;
  margin: 0 15px;
  cursor: pointer;
  &:last-child {
    margin: 0 0 0 15px;
  }
`;

const TabHeader = styled.div`
  display: inline-block;
  margin-right: 30px;
  cursor: pointer;
  color: ${ props => (props.active ? props.theme.colors.mainBlue : 'inherit') };
  &:hover {
    color: ${ props => props.theme.colors.mainBlue };
    border-bottom: 5px solid;
  }
  ${ props => (props.active ? 'border-bottom: 5px solid;' : null) };
`;

const Label = styled.h5`
  color: ${ props => (props.active ? props.theme.colors.mainBlue : 'inherit') };
  line-height: 60px;
  height: 60px;
`;

class TabsCard extends Component {
  state = {
    active: 0,
  };

  render () {
    const {
      title, tabs, customCss, useTabIcons,
    } = this.props;
    const { active } = this.state;

    const cardProps = { customCss };

    if (useTabIcons) {
      cardProps.title = title;
      cardProps.headerComponent = (
        <Wrapper>
          { tabs.map(({ iconUrl, iconBlueUrl }, index) => {
            const isActive = active === index;
            const src = isActive ? iconBlueUrl : iconUrl;
            return (
              <ItemIcon
                onClick={ () => this.setState({ active: index }) }
                src={ src }
                onMouseOver={ e => (e.currentTarget.src = iconBlueUrl) }
                onFocus={ e => (e.currentTarget.src = iconBlueUrl) }
                onMouseOut={ e => (e.currentTarget.src = src) }
                onBlur={ e => (e.currentTarget.src = src) }
              />
            );
          }) }
        </Wrapper>
      );
    } else {
      const Title = () => (
        <div>
          { tabs.map(({ label }, index) => (
            <TabHeader
              active={ active === index }
              onClick={ () => this.setState({ active: index }) }
            >
              <Label active={ active === index }>{ label }</Label>
            </TabHeader>
          )) }
        </div>
      );

      cardProps.title = <Title />;
      cardProps.headerCss = `
        justify-content: normal;
        min-height: 0;
        padding: 0 30px;
      `;
    }

    return <Card { ...cardProps }>{ tabs[active].render }</Card>;
  }
}

export default TabsCard;
