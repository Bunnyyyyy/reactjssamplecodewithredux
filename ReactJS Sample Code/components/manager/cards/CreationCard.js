import React from 'react';
import { Link } from 'react-router-dom';
import styled from 'styled-components';

import Button from '../../common/Button';
import Card from './Card';
import { transform } from '../../../utils/styles';

const TitleWrapper = styled.span`
  display: flex;
  align-items: center;
`;

const TitleIcon = styled.img`
  vertical-align: middle;
  margin-right: 10px;
`;

const CardImage = styled.img`
  width: 100px;
`;

const ContentWrapper = styled.div`
  text-align: center;
  padding: 20px 15px;

  h5 {
    margin-bottom: 5px;
  }

  p {
    margin-bottom: 70px;
  }
`;

const LinkButton = styled(Button)`
  position: absolute;
  bottom: 30px;
  left: 50%;
  width: 180px;
  height: 30px;
  line-height: 28px;
  font-size: 13px;
  padding: 0;
  border: solid 1px ${ props => props.theme.colors.mainBlue };
  box-shadow: none;

  ${ transform('translateX(-50%)') };
`;

const cardCss = `
  flex-basis: calc(33% - 20px);
  margin-right: 30px;

  &:nth-child(3n) {
    margin-right: 0;
  }
`;

const CreationCard = ({
  headerTitle,
  title,
  icon,
  imgSrc,
  details,
  buttonText,
  detailsUrl,
}) => (
  <Card
    title={ (
      <TitleWrapper>
        { icon ? <TitleIcon src={ icon } /> : '' }
        { headerTitle }
      </TitleWrapper>
      ) }
    customCss={ cardCss }
  >
    <ContentWrapper>
      <CardImage src={ imgSrc } />
      <h5>{ title }</h5>
      <p>{ details }</p>
      <Link to={ detailsUrl }>
        <LinkButton>{ buttonText }</LinkButton>
      </Link>
    </ContentWrapper>
  </Card>
);

export default CreationCard;
