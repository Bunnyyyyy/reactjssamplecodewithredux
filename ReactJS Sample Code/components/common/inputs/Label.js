import React from 'react';
import styled from 'styled-components';

const StyledLabel = styled.label`
  display: block;
  text-align: ${ props => (props.labelRight ? 'right' : 'left') };
  min-height: 14px;
  font-size: 12px;
  line-height: 14px;
  font-weight: normal;
  color: ${ props => (props.theme.light ? 'white' : props.theme.colors.darkGray) };
  margin-bottom: 5px;
  letter-spacing: 0.5px;

  span {
    color: ${ props => props.theme.colors.pinkRed };
  }

  ${ props => (props.invalid
    ? `
    color: ${
    props.theme.light
      ? props.theme.colors.mainYellow
      : props.theme.colors.pinkRed
    };
  `
    : '') } ${ props => props.customCss || '' };
`;

const SubLabel = styled.label`
  font-size: 12px;
  color: #9c9c9c;
  line-height: normal;
`;

const Label = ({
  field, label, required, subLabel, ...props
}) => (
  <StyledLabel htmlFor={ field } { ...props }>
    { label }
    { required ? <span>*</span> : '' }
    { subLabel ? <SubLabel>{ subLabel }</SubLabel> : '' }
  </StyledLabel>
);

export default Label;
