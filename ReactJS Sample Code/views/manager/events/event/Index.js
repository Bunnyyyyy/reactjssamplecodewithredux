import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { withRouter } from 'react-router';
import { Route, Switch } from 'react-router-dom';
import { Helmet } from 'react-helmet';

import Header from '../../../../components/header/Header';
import EventNavBar from '../../../../components/manager/navigation/EventNavBar';

import EventOverviewView from './Overview';
import EventInfoView from './Info';
import EventTicketingIndexView from './ticketing/Index';
import EventGuestsView from './Guests';
import EventImportGuestsView from './ImportGuests';
import EventHelpersView from './Helpers';

import * as ManagerActionCreators from '../../../../redux/manager';
import { t as translate } from '../../../../../i18n';

const t = a => translate(a, 'manager/settings');

const mapStateToProps = state => ({
  organization: state.manager.organization,
  events: state.manager.events,
  event: state.manager.event,
});
const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators(ManagerActionCreators, dispatch),
});

class EventIndexView extends Component {
  async componentDidMount () {
    try {
      const { actions, match, history } = this.props;
      const { oId, eId } = match.params;

      const promises = [
        actions.getOrganizationEvents(oId),
        actions.getOrganizationEvent(eId),
      ];
      await Promise.all(promises);

      const { organization, events, event } = this.props;
      if (!event || !event.id || !events.find(e => e.id === event.id)) {
        return history.push(`/${ organization.id }/events`);
      }
    } catch (e) {
      return Promise.reject(e);
    }
  }

  render () {
    const { location, organization, event } = this.props;
    const { pathname } = location;

    if (!event) return null;

    let isHidingNavBar = false;
    let navPages = [
      {
        title: t('Events'),
        url: `/${ organization.id }/events`,
      },
      {
        title: event.name,
        url: `/${ organization.id }/events/${ event.id }`,
      },
    ];
    if (pathname.indexOf(`${ event.id }/info`) > 0) {
      isHidingNavBar = true;
      navPages.push({
        title: t('Information'),
      });
    } else if (pathname.indexOf(`${ event.id }/ticketing`) > 0) {
      navPages.push({
        title: t('Ticketing'),
      });
    } else if (pathname.indexOf(`${ event.id }/guests/import`) > 0) {
      navPages = navPages.concat([
        {
          title: t('Guests'),
          url: `/${ organization.id }/events/${ event.id }/guests`,
        },
        {
          title: t('Import'),
        },
      ]);
    } else if (pathname.indexOf(`${ event.id }/guests`) > 0) {
      navPages.push({
        title: t('Guests'),
      });
    } else if (pathname.indexOf(`${ event.id }/helpers`) > 0) {
      navPages.push({
        title: t('Helpers'),
      });
    } else {
      delete navPages[1].url;
    }

    const pageTitle = navPages
      .map(p => p.title)
      .reverse()
      .concat('Juven Manager')
      .join(' - ');

    return (
      <div>
        <Helmet>
          <title>{ pageTitle }</title>
          <meta property='og:title' content={ pageTitle } />
        </Helmet>
        <Header navPages={ navPages } />
        { !isHidingNavBar ? <EventNavBar organization={ organization } event={ event } /> : null }
        <Switch>
          <Route exact path='/:oId/events/:eId' component={ EventOverviewView } />
          <Route exact path='/:oId/events/:eId/info' component={ EventInfoView } />
          <Route path='/:oId/events/:eId/ticketing' component={ EventTicketingIndexView } />
          <Route exact path='/:oId/events/:eId/guests' component={ EventGuestsView } />
          <Route exact path='/:oId/events/:eId/guests/import' component={ EventImportGuestsView } />
          <Route exact path='/:oId/events/:eId/helpers' component={ EventHelpersView } />
        </Switch>
      </div>
    );
  }
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(EventIndexView));
