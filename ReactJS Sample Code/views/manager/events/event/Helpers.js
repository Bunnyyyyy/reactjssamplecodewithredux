import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { withRouter } from 'react-router';

import { stateHasSelectedRows } from '../../../../utils';
import * as UiActionCreators from '../../../../redux/ui';
import * as MeActionCreators from '../../../../redux/me';
import * as ManagerActionCreators from '../../../../redux/manager';

import Table from '../../../../components/common/Table';
import SettingModal from '../../../../components/manager/SettingModalV2';
import ManagerContainer from '../../../../components/layouts/ManagerContainer';

import trashIcon from '../../../../assets/manager/bulk-actions/icon-trash.svg';
import addContactIcon from '../../../../assets/manager/bulk-actions/icon-contact-add.svg';
import eventWhiteIcon from '../../../../assets/header/icon_event_white.svg';

import { t as translate } from '../../../../../i18n';

const t = a => translate(a, 'manager/events/helpers');

const mapStateToProps = state => ({
  user: state.me.user,
  organizations: state.manager.organizations,
  organization: state.manager.organization,
  events: state.manager.events,
  event: state.manager.event,
  helpers: state.manager.eventHelpers,
  features: state.manager.features,
  meta: state.manager.meta,
});
const ActionCreators = Object.assign(
  {},
  UiActionCreators,
  MeActionCreators,
  ManagerActionCreators,
);
const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators(ActionCreators, dispatch),
});

class EventHelpersView extends Component {
  constructor () {
    super();

    // default table settings
    this.state = {
      sortBy: 'name',
      sortOrder: 'asc',
      isAddingHelper: false,
      editingHelper: {},
    };
  }

  async componentDidMount () {
    const { actions, match } = this.props;
    const { eId } = match.params;

    actions.setLoading(true);

    try {
      const promises = [
        actions.getEventHelpers(eId),
        actions.getFeatures(),
      ];
      await Promise.all(promises);
    } catch (e) {
      console.error('ERROR: EventHelpersView', e);
    } finally {
      actions.setLoading(false);
    }
  }

  setEditingHelper (editingHelper) {
    this.setState({ editingHelper });
  }

  sortHelpers (newSortBy) {
    const { sortBy } = this.state;
    let { sortOrder } = this.state;

    sortOrder = sortBy === newSortBy ? (sortOrder === 'asc' ? 'desc' : 'asc') : 'asc';
    this.setState({
      sortBy: newSortBy,
      sortOrder,
    }, this.reloadHelpers);
  }

  async reloadHelpers () {
    const { actions, event } = this.props;
    const { sortBy, sortOrder } = this.state;

    try {
      await actions.getEventHelpers(event.id, sortBy, sortOrder);
    } catch (e) {
      console.error(e);
    }
  }

  toggleAddingHelper () {
    const { isAddingHelper } = this.state;
    this.setState({ isAddingHelper: !isAddingHelper });
  }

  async saveHelper (data) {
    const { actions, event } = this.props;
    const { email, ...permissions } = data;

    const features = [];
    for (const id in permissions) {
      if (permissions[id]) features.push(id);
    }

    let errorMessage;
    if (!email.length) {
      errorMessage = t('Please fill in the Email');
    } else if (!features.length) {
      errorMessage = t('Please choose at least one feature Permission');
    }

    if (errorMessage) {
      throw new Error({ message: errorMessage });
    }

    const saveData = {
      email,
      features,
    };

    try {
      await actions.addEventHelper(event.id, saveData);
      await this.reloadHelpers();
      this.toggleAddingHelper();
    } catch (e) {
      throw e;
    }
  }

  async updateHelper (permissions) {
    const { actions, event } = this.props;
    const { editingHelper } = this.state;

    const features = [];
    for (const id in permissions) {
      if (permissions[id]) features.push(id);
    }

    let errorMessage;
    if (!features.length) {
      errorMessage = t('Please choose at least one feature Permission');
    }

    if (errorMessage) {
      throw new Error({ message: errorMessage });
    }

    const saveData = {
      features,
    };

    try {
      await actions.updateEventHelper(event.id, editingHelper.id, saveData);
      await this.reloadHelpers();
      this.setEditingHelper({});
    } catch (e) {
      throw e;
    }
  }

  async removeHelpers (helpers) {
    const { actions, event } = this.props;
    const promises = helpers.map(h => actions.removeEventHelper(event.id, h.id));

    try {
      await Promise.all(promises);
      this.reloadHelpers();
    } catch (e) {
      throw e;
    }
  }

  render () {
    const {
      event, helpers, features,
    } = this.props;
    const {
      sortBy, sortOrder, isAddingHelper, editingHelper,
    } = this.state;

    // add up to only 85% due to checkbox and edit button
    const tableColumns = [
      {
        title: t('Name'),
        field: 'name',
        compulsory: true,
        unscrollable: true,
      },
      {
        title: t('Permissions'),
        compulsory: true,
        format: (a, helper) => helper.features
          .map((hf) => {
            const feature = features.find(f => hf === f.id);
            return feature ? feature.name : null;
          })
          .filter(hf => !!hf)
          .join(', '),
      },
    ];

    const tableActions = [
      {
        title: t('Add'),
        function: this.toggleAddingHelper.bind(this),
      },
    ];

    const tableBulkActions = [
      {
        name: t('Remove Helpers'),
        icon: trashIcon,
        function: this.removeHelpers.bind(this),
        condition: state => stateHasSelectedRows(state),
        confirmMessage: `${ t(
          'Are you sure you want to remove the selected Helpers from',
        ) } ${ event.name }?`,
        successMessage: `${ t(
          'You have successfully removed the selected Helpers from',
        ) } ${ event.name }.`,
        errorTitle: `${ t(
          'Oops! there is an error when removing the selected Helpers from',
        ) } ${ event.name }.`,
      },
    ];

    const rowActions = [
      {
        label: t('Edit'),
        icon: eventWhiteIcon,
        onClick: row => this.setEditingHelper(row),
      },
    ];

    const tableAddButtonActions = [
      {
        title: t('Add Helper'),
        icon: addContactIcon,
        function: this.toggleAddingHelper.bind(this),
      },
    ];

    const addHelperModalFeatureFields = features.map(f => ({
      type: 'checkbox',
      field: f.id,
      label: f.name,
    }));

    const addHelperModalFields = [
      {
        type: 'input',
        field: 'email',
        label: t('Helper\'s Email'),
        flex: '0 0 100%',
      },
      {
        type: 'custom',
        field: 'message-1',
        readOnly: true,
        renderComponent: () => (
          <p>
            { t('Please select the permissions for the helper.') }
          </p>
        ),
      },
      ...addHelperModalFeatureFields,
    ];

    const editingHelperModalFeatureFields = features.map(f => ({
      type: 'checkbox',
      field: f.id,
      label: f.name,
      default: editingHelper.features
        ? editingHelper.features.indexOf(f.id) !== -1
        : false,
    }));

    const editingHelperModalFields = [
      {
        type: 'custom',
        field: 'message-1',
        readOnly: true,
        renderComponent: () => (
          <p>
            { t('Please select the permissions for the helper.') }
          </p>
        ),
      },
      ...editingHelperModalFeatureFields,
    ];

    return (
      <ManagerContainer>
        <Table
          id='helpers'
          noPagination
          columns={ tableColumns }
          rows={ helpers }
          sortBy={ sortBy }
          sortOrder={ sortOrder }
          onSort={ this.sortHelpers.bind(this) }
          actions={ tableActions }
          rowActions={ rowActions }
          bulkActions={ tableBulkActions }
          singleItemText={ t('helper') }
          pluralItemText={ t('helpers') }
          addButtonActions={ tableAddButtonActions }
          emptyText={ t('There are no helpers yet.') }
        />
        { isAddingHelper ? (
          <SettingModal
            medium
            isOpen
            title={ t('Add Helper') }
            fields={ addHelperModalFields }
            action={ this.saveHelper.bind(this) }
            onCancel={ this.toggleAddingHelper.bind(this) }
          />
        ) : null }
        { editingHelper.id ? (
          <SettingModal
            medium
            isOpen
            title={ `${ t('Editing Helper') } - ${ editingHelper.name }` }
            fields={ editingHelperModalFields }
            action={ this.updateHelper.bind(this) }
            onCancel={ this.setEditingHelper.bind(this, {}) }
          />
        ) : null }
      </ManagerContainer>
    );
  }
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(EventHelpersView));
