import React from 'react';
import { Link } from 'react-router-dom';
import styled from 'styled-components';

import { boxShadow } from '../../../utils/styles';

const Container = styled.div`
  position: relative;
  background-color: white;
  margin: 0 auto 30px auto;
  box-shadow: ${ boxShadow('0 3px 6px 0 rgba(0, 0, 0, 0.16)') };
  border-radius: 5px;
  max-width: ${ props => (props.edit ? '900' : '1280') }px;
  ${ props => props.customCss || '' }
`;

const HeaderWrapper = styled.div`
  display: flex;
  align-items: center;
  min-height: 60px;
  padding: 15px 30px;
  border-bottom: 1px solid ${ props => props.theme.colors.lightGray };
  justify-content: space-between;

  ${ props => props.customCss || '' };
`;

const LeftWrapper = styled.div`
  display: flex;
  align-items: center;
`;

const Body = styled.div`
  > div {
    padding-left: 30px;
    padding-right: 30px;
  }

  ${ props => props.customCss || '' };
`;

const Title = styled.h5`
  line-height: 25px;
`;

const BackIcon = styled.div`
  width: 20px;
  height: 20px;
  text-align: center;
  margin-right: 15px;
`;

const Card = ({
  title,
  headerComponent,
  backRoute,
  children,
  edit = false,
  customCss = '',
  bodyCss,
  headerCss,
}) => {
  const Header = title || headerComponent || backRoute ? (
    <HeaderWrapper customCss={ headerCss }>
      <LeftWrapper>
        { backRoute ? (
          <Link to={ backRoute }>
            <BackIcon>
              <i className='fas fa-chevron-left' />
            </BackIcon>
          </Link>
        ) : null }
        <Title>{ title || '' }</Title>
      </LeftWrapper>
      { headerComponent || null }
    </HeaderWrapper>
  ) : null;

  return (
    <Container customCss={ customCss } edit={ edit }>
      { Header }
      <Body customCss={ bodyCss }>{ children }</Body>
    </Container>
  );
};

export default Card;
