import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import styled from 'styled-components';

import * as UiActionCreators from './../../redux/ui';

import Button from './../common/Button';

import i18n from './../../../i18n';
const t = a => i18n.t([`translations:${ a }`]);

const Wrapper = styled.div`
  position: relative;
  overflow: hidden;
  transition: 0.75s;
  flex-shrink: 0;

  &:hover {
    overflow: visible;
  }

  ${ props => (props.hasPopup ? 'overflow: visible;' : '') };
`;

const ActionButton = Button.extend`
  margin-top: 7px;
  margin-bottom: 7px;
`;

const SaveButton = ActionButton.extend`
  ${ props =>
    props.invalid
      ? `
      background-color: ${ props.theme.colors.mainRed } !important;

      &:hover, &:active {
        background-color: ${ props.theme.colors.mainRed } !important;
        color: white;
      }
    `
      : props.disabled
        ? `
        &:disabled, &:hover, &:active {
          opacity: 0.4;
          background-color: ${ props.theme.colors.darkBlue } !important;
        }
      `
        : '' };
`;

const mapStateToProps = state => ({
  formHandleSubmits: state.ui.formHandleSubmits,
  formHandleResets: state.ui.formHandleResets,
  formErrors: state.ui.formErrors,
  formDirties: state.ui.formDirties
});

const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators(UiActionCreators, dispatch)
});

class SaveActions extends Component {
  clickReset () {
    const { formHandleResets } = this.props;
    for (let id in formHandleResets) {
      formHandleResets[id]();
    }
  }

  clickSave () {
    const { formHandleSubmits } = this.props;

    for (let id in formHandleSubmits) {
      formHandleSubmits[id]();
    }
  }

  render () {
    const {
      formErrors,
      formDirties,
      saveText = t('Save'),
      cancelText = t('Cancel')
    } = this.props;

    let hasDirty = false;
    for (let id in formDirties) {
      if (formDirties[id]) hasDirty = true;
    }

    let hasInvalidForm = false;
    for (let id in formErrors) {
      if (Object.keys(formErrors[id]).length) hasInvalidForm = true;
    }

    return (
      <Wrapper>
        <ActionButton
          gray
          small
          disabled={ !hasDirty }
          onClick={ this.clickReset.bind(this) }
        >
          { cancelText }
        </ActionButton>
        <SaveButton
          small
          light
          disabled={ !hasDirty || hasInvalidForm }
          invalid={ hasInvalidForm }
          onClick={ this.clickSave.bind(this) }
        >
          { saveText }
        </SaveButton>
      </Wrapper>
    );
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(SaveActions);
