export function userSelect (css) {
  if (!css || !css.length) return '';

  return `
    -webkit-user-select: ${ css };
    -moz-user-select: ${ css };
    -ms-user-select: ${ css };
    user-select: ${ css };
  `;
}

export function transform (css) {
  if (!css || !css.length) return '';

  return `
    -webkit-transform: ${ css };
    -moz-transform: ${ css };
    -ms-transform: ${ css };
    -o-transform: ${ css };
    transform: ${ css };
  `;
}

export function boxShadow (css) {
  if (!css || !css.length) return '';

  return `
    -webkit-box-shadow: ${ css };
    -moz-box-shadow: ${ css };
    box-shadow: ${ css };
  `;
}

export function inputPlaceholder (css) {
  if (!css || !css.length) return '';

  return `
    ::-webkit-input-placeholder {
      ${ css }
    }
    ::-moz-placeholder {
      ${ css }
    }
    :-ms-input-placeholder {
      ${ css }
    }
    :-moz-placeholder {
      ${ css }
    }
  `;
}

export const nowrap = `
  text-overflow: ellipsis;
  overflow: hidden;
  white-space: nowrap;
`;

export function transition (css) {
  if (!css || !css.length) return '';

  return `
    -webkit-transition: ${ css };
    -moz-transition: ${ css };
    -ms-transition: ${ css };
    -o-transition: ${ css };
    transition: ${ css };
  `;
}
