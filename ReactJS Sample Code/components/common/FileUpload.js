import React, { Component } from 'react';
import Dropzone from 'react-dropzone';
import styled from 'styled-components';
import { get } from 'lodash';

import Button from './Button';
import Label from './inputs/Label';
import InvalidMessage from './InvalidMessage';

import i18n from '../../../i18n';

require('formdata-polyfill');

const t = a => i18n.t([`components/common:${ a }`]);

const Wrapper = styled.div`
  width: 100%;
  height: 100%;
  ${ props => props.customCss || '' };
`;

const DropzoneWrapper = styled.div`
  display: flex;
  cursor: pointer;
  width: 100%;
  height: 100%;
  padding: 15px;
  background-color: ${ props => props.theme.colors.bgGray };
  border-radius: 5px;
`;

const DottedContainer = styled.div`
  border: 2px dotted ${ props => props.theme.colors.lightBlue };
  display: flex;
  flex: 1;
  justify-content: center;
  flex-direction: column;
  align-items: center;

  p {
    color: ${ props => props.theme.colors.mainGray };
    margin-bottom: 10px;
  }
`;

const CheckIcon = styled.i`
  font-size: 30px;
  margin-bottom: 20px;
  color: #32a844;
`;

const UploadIcon = styled.i`
  font-size: 30px;
  margin-bottom: 20px;
  color: ${ props => props.theme.colors.mainGray };
`;

const RemoveButton = Button.extend`
  height: 21px;
  font-size: 15px;
  line-height: 21px;
  font-weight: 400;
  text-transform: none;
  text-decoration: underline;
  color: ${ props => props.theme.colors.mainGray };
  margin: 0 0 0 10px;

  &:hover,
  &:active {
    color: ${ props => props.theme.colors.mainBlue };
  }
`;

export default class FileUpload extends Component {
  constructor () {
    super();

    this.state = {
      isUpdatingFile: false,
      uploadedFile: null,
    };
  }

  // hack to enable its parent to make the ImageUpload component reset
  componentWillReceiveProps (nextProps) {
    const { reset } = this.props;
    if (nextProps.reset === true && reset !== true) {
      this.discardFile();
    }
  }

  onDrop (acceptedFiles) {
    const { onUpdate } = this.props;

    const uploadedFile = acceptedFiles[0];
    this.setState({ uploadedFile });

    if (onUpdate) onUpdate(uploadedFile);
  }

  discardFile (e) {
    e.stopPropagation();
    const { onUpdate } = this.props;
    this.setState({
      isUpdatingFile: false,
      uploadedFile: null,
    });

    if (onUpdate) onUpdate(null);
  }

  toggleUpdatingFile () {
    const { isUpdatingFile } = this.state;
    this.setState({
      isUpdatingFile: !isUpdatingFile,
    });
  }

  render () {
    const {
      file,
      field,
      label,
      required,
      accept,
      customCss,
      errorCss,
      errorMessage,
    } = this.props;
    const { isUpdatingFile, uploadedFile } = this.state;

    const dropzoneStyle = {
      width: '100%',
      height: '100%',
    };

    const FileDropzone = (
      <Dropzone
        style={ dropzoneStyle }
        multiple={ false }
        onDrop={ this.onDrop.bind(this) }
        accept={ accept }
      >
        <DropzoneWrapper>
          <DottedContainer>
            <UploadIcon className='fas fa-cloud-upload-alt' />
            <p>{ t('Drop the file here or Click to browse') }</p>
          </DottedContainer>
        </DropzoneWrapper>
      </Dropzone>
    );

    let fileSize = '';
    if (uploadedFile) {
      fileSize = uploadedFile.size > 1000000
        ? `${ Math.floor(uploadedFile.size / 10000) / 100 } MB`
        : `${ Math.floor(uploadedFile.size / 10) / 100 } KB`;
    }

    let fileName = '';
    if (file && !isUpdatingFile) {
      const fileUrlArr = file.split('/');
      const fileNameArr = fileUrlArr[fileUrlArr.length - 1].split('_');
      fileName = fileNameArr[fileNameArr.length - 1];
    }

    const FileInfo = (
      <Dropzone
        style={ dropzoneStyle }
        multiple={ false }
        onDrop={ this.onDrop.bind(this) }
        accept={ accept }
      >
        <DropzoneWrapper>
          <DottedContainer>
            <CheckIcon className='fas fa-check' />
            <p>
              <span>
                <b>
                  { file && !isUpdatingFile
                    ? fileName
                    : get(uploadedFile, 'name') }
                  { uploadedFile ? ` (${ fileSize })` : t(' (Original)') }
                </b>
              </span>
              <RemoveButton plain onClick={ this.discardFile.bind(this) }>
                { t('Remove file') }
              </RemoveButton>
            </p>
            <p>{ t('Drop the file here or Click to browse') }</p>
          </DottedContainer>
        </DropzoneWrapper>
      </Dropzone>
    );

    return (
      <Wrapper customCss={ customCss }>
        { label ? (
          <Label field={ field } label={ label } required={ required } />
        ) : null }
        { (file && !isUpdatingFile) || uploadedFile ? FileInfo : FileDropzone }
        { errorMessage ? (
          <InvalidMessage customCss={ errorCss }>{ errorMessage }</InvalidMessage>
        ) : null }
      </Wrapper>
    );
  }
}
