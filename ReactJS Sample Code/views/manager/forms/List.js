import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { withRouter } from 'react-router';
import { Link } from 'react-router-dom';
import styled from 'styled-components';
import moment from 'moment';

import * as OrganizationActionCreators from '../../../redux/organization';
import * as UiActionCreators from '../../../redux/ui';

import ManagerContainer from '../../../components/layouts/ManagerContainer';
import Button from '../../../components/common/Button';
import formsActionImage from '../../../assets/common/actions/forms.svg';

import { t as translate } from '../../../../i18n';

const t = a => translate(a, 'manager/forms/index');

const TilesWrapper = styled.div`
  display: flex;
  flex-wrap: wrap;
`;

const Tile = styled.div`
  position: relative;
  flex: 0 0 30%;
  height: 200px;
  margin: 0 3.3333% 25px 0;
  padding: 40px 20px 10px 20px;
  background-color: white;
  box-shadow: 0 0 6px 0 rgba(0, 0, 0, 0.16);

  &:nth-child(3n) {
    margin-right: 0;
  }
`;

const TileUpdateTime = styled.div`
  position: absolute;
  left: 20px;
  bottom: 15px;

  & p {
    font-size: 12px;
    line-height: 18px;

    &:first-child {
      font-weight: 700;
    }
  }
`;

const TileActions = styled.div`
  display: flex;
  position: absolute;
  right: 0;
  bottom: 10px;
  padding: 0 15px;
  justify-content: space-between;
  justify-self: flex-end;
`;

const IconButton = styled(Button)`
  font-size: 15px;
  line-height: 21px;
  font-weight: 700;
  letter-spacing: 0.5px;
  color: ${ props => props.theme.colors.mainGray };
  text-transform: none;
  margin: 0;

  & i {
    margin-right: 10px;
  }

  &:hover,
  &:active {
    color: ${ props => props.theme.colors.darkGray };
  }
`;

const AddFormTileContent = styled.div`
  position: absolute;
  left: 50%;
  top: 50%;
  padding-bottom: 20px;
  transform: translate(-50%, -50%);
  text-align: center;

  & img {
    width: 90px;
  }

  & h4 {
    color: ${ props => props.theme.colors.mainBlue };

    & i {
      margin-right: 10px;
    }
  }

  &:hover,
  &:active {
    h4 {
      color: ${ props => props.theme.colors.darkBlue };
    }
  }
`;

const mapStateToProps = state => ({
  organization: state.manager.organization,
  forms: state.organization.forms,
});

const ActionCreators = Object.assign(
  {},
  OrganizationActionCreators,
  UiActionCreators,
);
const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators(ActionCreators, dispatch),
});

class FormsListView extends Component {
  async componentDidMount () {
    const { actions, match } = this.props;
    const { oId } = match.params;

    actions.setLoading(true);

    try {
      await actions.getOrganizationForms(oId);
    } catch (e) {
      console.error('ERROR: FormsListView', e);
    } finally {
      actions.setLoading(false);
    }
  }

  render () {
    const { organization, forms } = this.props;

    const Forms = forms.map(e => (
      <Tile key={ `form-tile-${ e.id }` }>
        <h4>{ e.title }</h4>
        <TileUpdateTime>
          <p>{ t('Last Updated At') }</p>
          <p>
            { moment
              .utc(e.updated_at)
              .local()
              .format('DD MMM YYYY HH:mm') }
          </p>
        </TileUpdateTime>
        <TileActions>
          <Link to={ `/${ organization.id }/forms/${ e.id }` }>
            <IconButton plain>
              <i className='fas fa-chart-bar' aria-hidden='true' />
              { t('Manage') }
            </IconButton>
          </Link>
        </TileActions>
      </Tile>
    ));

    return (
      <ManagerContainer noSubheader>
        <TilesWrapper>
          { Forms }
          <Tile>
            <Link to={ `/${ organization.id }/forms/new` }>
              <AddFormTileContent>
                <img alt='' src={ formsActionImage } />
                <h4>{ t('Create New Form') }</h4>
              </AddFormTileContent>
            </Link>
          </Tile>
        </TilesWrapper>
      </ManagerContainer>
    );
  }
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(FormsListView));
