import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { Transition } from 'react-transition-group';
import styled from 'styled-components';

import * as ActionCreators from './../../redux/manager';

const NotificationDiv = styled.div`
  position: absolute;
  cursor: pointer;
  transition: opacity 300ms ease;
  display: none;
  z-index: 899;
  left: 50%;
  top: ${ props => (props.modal || props.preview ? '10px' : '105px') };
  transform: translateX(-50%);
  width: 510px;
  max-width: calc(100% - 20px);
  padding: 20px 0px;
  border-radius: 4px;
  box-shadow: 2px 6px 10px 0 rgba(0, 0, 0, 0.03);
  background-color: ${ props => props.theme.colors.darkBlue };
  color: white;
  flex-direction: row;
  justify-content: center;
  align-items: center;

  & div {
    display: inline-block;
    vertical-align: text-top;
    color: white;
  }

  & h5,
  & i {
    font-size: 20px;
    line-height: 28px;
    color: white;
  }

  & h5 {
    font-weight: 600;
    margin-bottom: 5px;
    color: white;
  }

  & i {
    margin-right: 10px;
    color: white;
  }

  & p {
    font-size: 16px;
    line-height: 22px;
    color: white;
  }
`;

const CancelP = styled.p`
  display: inline-block;
  position: absolute;
  width: 16px;
  top: 5px;
  right: 5px;
`;

const IconWrapper = styled.div`
  width: 30%;
  text-align: center;
`;

const mapStateToProps = state => ({
  notification: state.manager.notification
});

const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators(ActionCreators, dispatch)
});

class Notification extends Component {
  constructor (props) {
    super();

    this.state = {
      notification: null,
      isExiting: false
    };
  }

  componentWillReceiveProps (nextProps) {
    if (this.props && this.props.notification === nextProps.notification) {
      return;
    }
    this.setNotificationState(nextProps.notification);
  }

  setNotificationState (notification) {
    clearTimeout(this.timer1);
    clearTimeout(this.timer2);

    const stateUpdate = {
      isExiting: !notification
    };
    if (notification) stateUpdate.notification = notification;
    this.setState(stateUpdate);

    if (notification) {
      this.timer1 = setTimeout(() => {
        this.setState({
          isExiting: true
        });
      }, 4700);

      this.timer2 = setTimeout(() => {
        this.setState({
          notification: null
        });
      }, 5000);
    } else {
      this.timer2 = setTimeout(() => {
        this.setState({
          notification: null
        });
      }, 300);
    }
  }

  closeNotification () {
    const { actions } = this.props;
    actions.setNotification(null);
  }

  render () {
    const { modal, preview, style = {} } = this.props;
    const { notification, isExiting } = this.state;

    let type = notification ? notification.type : null;
    let iconClassName = 'fa-hourglass-half';

    if (type === 'success') {
      iconClassName = 'fa-trophy';
    } else if (type === 'error') {
      iconClassName = 'fa-exclamation-triangle';
    } else if (type === 'warning') {
      iconClassName = 'fa-info-circle';
    }

    const transitionSuccessStyles = {
      entering: { display: 'flex', opacity: 0 },
      entered: { display: 'flex', opacity: 1 },
      exiting: { display: 'flex', opacity: 1 },
      exited: { display: 'none', opacity: 0 }
    };

    return (
      <Transition in={ !!notification && !isExiting } timeout={ 0 }>
        { state => (
          <NotificationDiv
            modal={ modal }
            preview={ preview }
            type={ type }
            style={ {
              ...style,
              ...transitionSuccessStyles[state]
            } }
            onClick={ this.closeNotification.bind(this) }
          >
            <CancelP>✕</CancelP>
            <IconWrapper>
              <i className={ `fas ${ iconClassName }` } style={ { fontSize: 54 } } />
            </IconWrapper>
            <div style={ { width: 'calc(100% - 30px)' } }>
              <h5>{ notification ? notification.title : '' }</h5>
              <p>{ notification ? notification.message : '' }</p>
            </div>
          </NotificationDiv>
        ) }
      </Transition>
    );
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Notification);
