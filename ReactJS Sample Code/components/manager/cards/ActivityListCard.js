import React from 'react';
import { Link } from 'react-router-dom';
import styled from 'styled-components';

import Card from './Card';
import Button from '../../common/Button';

import showMoreIcon from '../../../assets/manager/icon_showmore.svg';

import i18n from '../../../../i18n';

const t = a => i18n.t([`components/manager:${ a }`]);

const MoreInfoIcon = styled.img`
  width: 20px;
  cursor: pointer;
`;

const CardBodyContainer = styled.div`
  padding: 0 20px 10px 20px;
`;

const DetailsButton = styled(Button)`
  display: block;
  height: 20px;
  font-size: 12px;
  line-height: 17px;
  border-bottom-width: 3px;
  margin: 5px auto 15px auto;

  &:last-child {
    margin-right: auto;
  }
`;

const RowWrapper = styled.div`
  margin: 15px 0;
  display: flex;
  align-items: flex-start;
`;

const RowContentWrapper = styled.div`
  flex-grow: 1;
  display: flex;
  flex-direction: column;
  justify-content: center;
  min-height: 40px;
  padding: 0 10px;
`;

const RowContent = styled.div`
  font-size: 15px;
  line-height: 21px;
  color: ${ props => props.theme.colors.darkGray };
  margin: 0;
`;

const RowImage = styled.div`
  user-select: none;
  -moz-user-select: none;
  -webkit-user-select: none;
  -ms-user-select: none;
  flex-shrink: 0;
  width: 40px;
  height: 40px;
  border-radius: 50%;
  border: 1px solid ${ props => props.theme.colors.lightGray };
  overflow: hidden;
  text-align: center;
  line-height: 38px;
  color: ${ props => props.theme.colors.mainBlue };
  font-size: 15px;
  font-weight: 700;

  & img {
    max-width: 100%;
    max-height: 100%;
  }
`;

const RowTimestamp = styled.div`
  font-size: 12px;
  line-height: 14px;
  color: ${ props => props.theme.colors.mainGray };
  margin-top: 5px;
`;

const EmptyItemText = styled.p`
  margin-top: 20px;
  margin-bottom: 20px;
  text-align: center;
`;

const ActivityListCard = ({
  title,
  onMoreInfo,
  emptyText,
  rows = [],
  // rowImage takes either an <img /> component or a string
  rowImage = () => null,
  rowContent = () => null,
  rowTimestamp = () => null,
  detailsUrl,
  customCss,
}) => {
  const Rows = rows.map(r => (
    <RowWrapper>
      <RowImage>{ rowImage(r) }</RowImage>
      <RowContentWrapper>
        <RowContent>{ rowContent(r) }</RowContent>
        <RowTimestamp>{ rowTimestamp(r) }</RowTimestamp>
      </RowContentWrapper>
    </RowWrapper>
  ));

  return (
    <Card
      title={ title }
      headerComponent={
        onMoreInfo ? (
          <MoreInfoIcon src={ showMoreIcon } onClick={ onMoreInfo } />
        ) : null
      }
      customCss={ customCss }
    >
      <CardBodyContainer>
        { Rows }
        { !rows.length ? <EmptyItemText>{ emptyText }</EmptyItemText> : null }
        { detailsUrl && rows.length ? (
          <Link to={ detailsUrl }>
            <DetailsButton underline>{ t('See Details') }</DetailsButton>
          </Link>
        ) : null }
      </CardBodyContainer>
    </Card>
  );
};

export default ActivityListCard;
