import i18n from '../../i18n';

const t = a => i18n.t([`translations:${ a }`]);

export default {
  hkd: {
    name: t('Hong Kong Dollar'),
    symbol: 'HK$',
    minPaymentAmount: 20,
    maxPaymentAmount: 1000000,
  },
  usd: {
    name: t('United States Dollar'),
    symbol: 'US$',
    minPaymentAmount: 3,
    maxPaymentAmount: 1000000,
  },
  sgd: {
    name: t('Singapore Dollar'),
    symbol: 'S$',
    minPaymentAmount: 4,
    maxPaymentAmount: 1000000,
  },
  cny: {
    name: t('Chinese Yuan'),
    symbol: '¥',
    minPaymentAmount: 20,
    maxPaymentAmount: 1000000,
  },
};
