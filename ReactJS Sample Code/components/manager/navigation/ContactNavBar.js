import React from 'react';
import styled from 'styled-components';

import NavBar from '../../common/NavBar';

const NavBarContainer = styled.h4`
  width: 100%;
  display: flex;
  flex-grow: 2;
`;

const ContactNavBar = ({ organization, contact, activityType, activityId }) => {
  const routeQuery =
    activityType && activityId
      ? `?activity_type=${ activityType }&activity_id=${ activityId }`
      : '';

  const navLinks = [
    {
      title: 'Overview',
      route: `/${ organization.id }/contacts/${ contact.id }${ routeQuery }`,
      exact: true
    },
    {
      title: 'Information',
      route: `/${ organization.id }/contacts/${
        contact.id
      }/information${ routeQuery }`
    }
  ];

  return (
    <NavBarContainer>
      <NavBar links={ navLinks } />
    </NavBarContainer>
  );
};

export default ContactNavBar;
