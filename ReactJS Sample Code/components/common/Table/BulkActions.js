import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import styled from 'styled-components';

import * as ActionCreators from '../../../redux/manager';

import Button from '../Button';
import ConfirmModal from '../ConfirmModal';
import Tooltip from '../Tooltip';

import i18n from '../../../../i18n';

const t = a => i18n.t([`components/common:${ a }`]);

const BulkActionsDiv = styled.div`
  display: flex;
  position: relative;
  align-items: center;
  height: 100%;
  padding: 0 6px 0 15px;
  border-right: 1px solid ${ props => props.theme.colors.lightGray };
  flex: ${ props => (!props.selectedRowCount
    ? '1 0 calc(50% - 115px);'
    : '1 0 calc(100% - 65px);') };
  ${ props => props.customCss || '' };
`;

const BulkActionButton = styled(Button)`
  position: relative;
  flex-shrink: 0;
  font-size: 12px;
  height: 48px;
  padding: 12px;
  margin: 0 6px 0 0;

  &:disabled {
    opacity: 1 !important;

    > img {
      background: transparent;
      color: rgb(47, 46, 46);
      opacity: 0.3 !important;
    }
  }

  &:hover,
  &:active {
    opacity: 1;

    > img {
      opacity: 0.7 !important;
    }
  }

  &:hover ${ Tooltip } {
    visibility: visible;
  }
`;

const mapDispatchToProps = dispatch => ({
  reduxActions: bindActionCreators(ActionCreators, dispatch),
});

class BulkActions extends Component {
  constructor () {
    super();

    this.state = {
      onConfirm: null,
      confirmMessage: null,
    };
  }

  setConfirm (onConfirm = null, confirmMessage = null) {
    this.setState({ onConfirm, confirmMessage });
  }

  closeConfirm () {
    this.setConfirm();
  }

  render () {
    const {
      reduxActions,
      tableState,
      actions,
      rows,
      filters = {},
      onSuccess,
      customCss,
      RowSelectedText,
    } = this.props;
    const { selectedRows, selectAll } = tableState;
    const { onConfirm, confirmMessage } = this.state;

    let selectedRowCount = 0;
    for (const i in selectedRows) {
      if (selectedRows[i]) selectedRowCount++;
    }
    const actionParams = {};
    if (selectAll) {
      if (filters && Object.keys(filters).length) {
        actionParams.filters = filters;
      } else {
        actionParams.apply_to_all = true;
      }
    } else {
      const actionRows = rows.filter((r, i) => selectedRows[i]);
      actionParams.filters = {
        id: actionRows.map(r => r.id),
      };
    }

    const BulkActionButtons = actions.map((a, i) => {
      const onClick = async () => {
        const actionFunction = async () => {
          try {
            await a.function(actionParams);
            this.closeConfirm();
            onSuccess();

            if (a.successMessage) {
              reduxActions.setNotification({
                type: 'success',
                title: t('Good Job!'),
                message: a.successMessage,
              });
            }
          } catch (e) {
            if (a.errorTitle) {
              reduxActions.setNotification({
                type: 'error',
                title: a.errorTitle,
                message: e.message,
              });
            }
          }
        };

        if (a.confirmMessage) {
          this.setConfirm(actionFunction, a.confirmMessage);
        } else {
          actionFunction();
        }
      };
      const ShowHide = a.condition ? a.condition(tableState) : true;
      if (a.render) {
        return a.render(actionParams);
      }

      return (
        <BulkActionButton
          plain
          key={ `bulk-action-${ i }` }
          onClick={ onClick }
          disabled={ !ShowHide }
        >
          <img alt={ a.name } src={ a.icon } />
          { a.name && <Tooltip>{ a.name }</Tooltip> }
        </BulkActionButton>
      );
    });

    return (
      <BulkActionsDiv customCss={ customCss } selectedRowCount={ selectedRowCount }>
        { BulkActionButtons }
        { onConfirm ? (
          <ConfirmModal
            message={ confirmMessage }
            onConfirm={ onConfirm }
            onCancel={ this.closeConfirm.bind(this) }
          />
        ) : null }
        { RowSelectedText }
      </BulkActionsDiv>
    );
  }
}

export default connect(null, mapDispatchToProps)(BulkActions);
