import React from 'react';
import styled from 'styled-components';

const Wrapper = styled.blockquote`
  border-left: 2px solid #ddd;
  margin-left: 0;
  margin-right: 0;
  padding-left: 10px;
  color: #aaa;
  font-style: italic;
`;

function BlockQuote ({ children, attributes }) {
  return (
    <Wrapper { ...attributes }>
      { children }
    </Wrapper>
  );
}

export default BlockQuote;
