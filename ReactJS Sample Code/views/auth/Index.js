import React from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router';
import { Route, Switch, Redirect } from 'react-router-dom';
import styled from 'styled-components';

import AuthLoginView from './Login';
import AuthSignupView from './Signup';
import AuthForgotPasswordView from './ForgotPassword';
import SideView from '../../components/manager/SideView';
import Loading from '../../components/common/Loading';

import loginImage from '../../assets/manager/sign-in_img.png';
import theme from '../../utils/theme';

import { t as translate } from '../../../i18n';

const t = a => translate(a, 'manager/home');

const Wrapper = styled.div`
  display: flex;
  margin: 0;
  min-height: 100vh;

  @media (max-width: 900px) {
    min-height: auto;
    padding-top: 50px;
  }
`;

const MainViewWrapper = styled.div`
  align-items: center;
  display: flex;
  justify-content: center;
  padding: 60px;
  position: relative;
  width: 66.667%;

  @media (max-width: 900px) {
    width: 100%;
    padding: 15px;
  }
`;

const MainViewContent = styled.div`
  background-color: ${ props => props.theme.colors.white };
  border-radius: 5px;
  box-shadow: 0 3px 30px 0 rgba(0, 0, 0, 0.1);
  padding: 30px;
  width: 430px;
  margin: auto;

  @media (max-width: 900px) {
    width: 100%;
    padding: 15px;
  }
`;

const Circle = styled.div`
  background-color: ${ props => props.backgroundColor };
  border-radius: 100%;
  display: inline-block;
  height: ${ props => props.size }px;
  opacity: 1;
  position: absolute;
  width: ${ props => props.size }px;
  z-index: -1;
  ${ ({ bottom }) => bottom && `bottom: ${ bottom }%;` }
  ${ ({ left }) => left && `left: ${ left }%;` }
  ${ ({ right }) => right && `right: ${ right }%;` }
  ${ ({ top }) => top && `top: ${ top }%;` }

  @media (max-width: 900px) {
    display: none;
  }
`;

const mapStateToProps = state => ({
  user: state.me.user,
  loading: state.ui.loading,
});

const AuthIndexView = ({ location: { pathname }, user, loading }) => {
  if (user.id) return <Redirect to='/' />;

  const sideViewProps = pathname.indexOf('/signup') === 0
    ? {
      description: t(
        'On Juven, you can use our automated and intelligent tools to manage events, memberships and donations.',
      ),
      imgSrc: loginImage,
      title: t('Create events in seconds'),
    }
    : {
      description: t(
        'On Juven, you can use our automated and intelligent tools to manage events, memberships and donations.',
      ),
      imgSrc: loginImage,
      title: t('Welcome back!'),
    };

  return (
    <Wrapper>
      <SideView { ...sideViewProps } />
      <MainViewWrapper>
        <Circle
          backgroundColor={ theme.colors.bgGray }
          left={ 17 }
          size={ 158 }
          top={ 10 }
        />
        <Circle
          backgroundColor={ theme.colors.lightGray }
          bottom={ 10 }
          right={ 10 }
          size={ 355 }
        />
        <MainViewContent>
          <Switch>
            <Route exact path='/login' component={ AuthLoginView } />
            <Route exact path='/signup' component={ AuthSignupView } />
            <Route exact path='/forgot-password' component={ AuthForgotPasswordView } />
          </Switch>
        </MainViewContent>
      </MainViewWrapper>
      <Loading fullScreen isLoading={ loading } />
    </Wrapper>
  );
};

export default withRouter(connect(mapStateToProps)(AuthIndexView));
