import React from 'react';
import styled from 'styled-components';

const ToggleWrapper = styled.div`
  height: 50px;
  display: inline-block;
  text-align: center;
  padding: 10px 0;
  margin: 0px 15px;
`;

const ToggleSwitch = styled.input`
  cursor: pointer;
`;

const BottomText = styled.p`
  font-weight: bold;
  font-size: 12px;
  line-height: 14px;
`;

const Label = styled.label`
  position: relative;
  display: inline-block;
  width: 35px;
  height: 15px;
  & input {
    display: none;
  }
`;

const Slider = styled.span`
  position: absolute;
  cursor: pointer;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  background-color: ${ props =>
    props.on ? props.theme.colors.mainBlue : props.theme.colors.mainRed };
  -webkit-transition: all 0.4s ease;
  transition: all 0.4s ease;
  border-radius: 20px;
  &:before {
    position: absolute;
    content: '';
    height: 8px;
    width: 8px;
    ${ props => (props.on ? 'right: 4px;' : 'left: 4px;') };
    bottom: 4px;
    background-color: white;
    -webkit-transition: all 0.4s ease;
    transition: all 0.4s ease;
    border-radius: 50%;
  }
`;

export default class Toggle extends React.Component {
  constructor (props) {
    super(props);
    this.state = {
      on: this.props.on
    };
  }

  handleChange = () => {
    const on = !this.state.on;
    this.setState({ on });
    this.props.updateEvent(on);
  };

  render () {
    return (
      <ToggleWrapper>
        <Label>
          <ToggleSwitch
            type='checkbox'
            checked={ this.state.on }
            onChange={ this.handleChange }
          />
          <Slider on={ this.state.on } />
        </Label>
        <BottomText>
          { this.state.on ? this.props.onValue : this.props.offValue }
        </BottomText>
      </ToggleWrapper>
    );
  }
}
