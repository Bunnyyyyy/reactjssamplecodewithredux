import React from 'react';
import { Link } from 'react-router-dom';
import { withRouter } from 'react-router';
import styled from 'styled-components';

import { userSelect } from '../../utils/styles';

const SubpageMenuDiv = styled.div`
  position: relative;
  background: white;
  z-index: 101;
  width: 240px;
  height: calc(100vh - 100px);
  padding: 15px 30px;
  box-shadow: 0 3px 6px 0 rgba(0, 0, 0, 0.16);
`;

const SubpageLinkDiv = styled.div`
  ${ userSelect('none') } cursor: ${ props => (props.active ? 'default' : 'pointer') };
  height: 51px;
  line-height: 51px;
  font-size: 15px;
  font-weight: 400;
  color: ${ props => props.theme.colors.darkGray };

  &:hover,
  &:active {
    color: ${ props => props.theme.colors.mainBlue };
  }

  ${ props => (props.active
    ? `font-weight: 700;
      color: ${ props.theme.colors.mainBlue };
      `
    : '') };
`;

const SubpageMenu = ({ location: { pathname }, pages }) => {
  const SubpageLinks = pages.map((p, i) => {
    const active = p.exact
      ? p.route === pathname
      : pathname.indexOf(p.route) === 0;

    return (
      <Link to={ p.route } key={ `subpage-link-${ i }` }>
        <SubpageLinkDiv active={ active }>{ p.title }</SubpageLinkDiv>
      </Link>
    );
  });

  return <SubpageMenuDiv>{ SubpageLinks }</SubpageMenuDiv>;
};

export default withRouter(SubpageMenu);
