import { handleApiError } from '../utils';
import { getRequest } from './index';

const SET_PRICING_BRACKETS = 'SET_PRICING_BRACKETS';

const defaultState = {
  brackets: [],
};

export default function reducer (state = defaultState, action) {
  switch (action.type) {
    case SET_PRICING_BRACKETS:
      return { ...state, brackets: action.payload };

    default:
      return state;
  }
}

function setPricingBrackets (brackets) {
  return {
    type: SET_PRICING_BRACKETS,
    payload: brackets,
  };
}

export function getPricingBrackets (isCharity = null) {
  return dispatch => new Promise(async (resolve) => {
    try {
      const request = getRequest(dispatch);

      let url = '/pricing/brackets';
      if (isCharity !== null) url += `?charity=${ isCharity }`;

      const response = await request.get(url);
      const brackets = response.data;

      dispatch(setPricingBrackets(brackets));
      return resolve(brackets);
    } catch (e) {
      handleApiError(e);
      return resolve();
    }
  });
}
