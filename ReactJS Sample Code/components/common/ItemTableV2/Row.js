import React from 'react';
import { Link } from 'react-router-dom';
import styled from 'styled-components';

import Button from '../Button';
import { userSelect, nowrap } from '../../../utils/styles';

const RowWrapper = styled.div`
  display: flex;
  justify-content: space-evenly;
  background-color: #fff;
  height: 62px;
  align-items: center;
  margin-top: 10px;
  padding: 0 30px;
  box-shadow: 0 3px 6px 0 rgba(0, 0, 0, 0.16);
`;

const RowColumn = styled.div`
  flex-grow: 1;
  flex-basis: 0;
  padding: 0 10px;
  font-size: 15px;
  font-weight: 400;
  line-height: 21px;
  ${ nowrap } & p {
    font-size: 15px;
    font-weight: 700;
    line-height: 21px;
  }

  ${ props => props.customCss || '' };
`;

const RowNumber = styled.div`
  text-align: center;
  width: 21px;
  height: 21px;
  line-height: 17px;
  border-radius: 50%;
  font-size: 12px;
  font-weight: 700;
  color: ${ props => props.theme.colors.mainBlue };
  border: solid 2px ${ props => props.theme.colors.mainBlue };
  margin-right: 5px;
  ${ userSelect('none') };
`;

const RowActions = styled.div`
  width: 80px;
  text-align: right;

  a {
    display: inline-block;
  }
`;

const ActionIconButton = styled(Button)`
  width: 30px;
  height: 30px;
  text-align: center;
  margin: 0 0 0 10px;

  i {
    margin-right: 0;
    color: ${ props => props.theme.colors.mainGray };

    &:hover,
    &:active {
      color: ${ props => props.theme.colors.darkGray };
    }
  }
`;

const Row = ({
  rowKey, columns, item, order, editUrl,
}) => {
  const RowColumns = columns.map((c, i) => {
    const {
      field, format, flex, customCss,
    } = c;
    const value = format ? format(item[field], item) : item[field];
    const style = { flex };

    return (
      <RowColumn key={ `${ rowKey }-column-${ i }` } style={ style } customCss={ customCss }>
        { value }
      </RowColumn>
    );
  });

  let EditButton = null;
  if (editUrl && typeof editUrl === 'function') {
    EditButton = (
      <Link to={ editUrl(item) }>
        <ActionIconButton plain>
          <i className='fa fa-ellipsis-v' aria-hidden='true' />
        </ActionIconButton>
      </Link>
    );
  }

  return (
    <RowWrapper>
      <RowNumber>{ order }</RowNumber>
      { RowColumns }
      <RowActions>
        <span style={ { display: 'inline-block' } }>
          <ActionIconButton plain>
            <i className='fa fa-arrows-alt' aria-hidden='true' />
          </ActionIconButton>
        </span>
        { EditButton }
      </RowActions>
    </RowWrapper>
  );
};

export default Row;
