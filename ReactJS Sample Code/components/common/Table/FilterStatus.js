import React from 'react';
import styled from 'styled-components';

import Button from '../Button';

import i18n from '../../../../i18n';

const t = a => i18n.t([`components/common:${ a }`]);

const Wrapper = styled.div`
  display: flex;
  align-items: center;
  height: 48px;
  background-color: white;
  padding: 11px 20px 11px 30px;
  border-top: 1px solid ${ props => props.theme.colors.lightGray };
`;

const FiltersWrapper = styled.div`
  display: flex;
  align-items: center;
  flex-grow: 1;
`;

const FilterWrapper = styled.div`
  display: flex;
  align-items: center;
  margin-left: 20px;
`;

const FilterBy = styled.p`
  font-size: 12px;
  line-height: 26px;
  color: #9c9c9c;
`;

const FilterLabel = styled.p`
  font-size: 12px;
  line-height: 26px;
  margin-right: 10px;
`;

const FilterValue = styled.div`
  height: 26px;
  background-color: ${ props => props.theme.colors.lightGray };
  margin-right: 5px;
  border-radius: 4px;
  padding: 0 10px;

  &:last-child {
    margin-right: 0;
  }

  p {
    font-size: 12px;
    line-height: 26px;
    color: ${ props => props.theme.colors.mainGray };
  }
`;

const EditFilterButton = styled(Button)`
  margin-left: 20px;
  color: ${ props => props.theme.colors.darkBlue };
  font-size: 12px;

  &:hover, &:active {
    color: ${ props => props.theme.colors.mainBlue };
  }
`;

const FilterStatus = ({ filters = {}, fields = [], onExpandFilter = () => {} }) => {
  if (!Object.keys(filters).length) return null;

  const FilterDivs = [];
  for (const key in filters) {
    const value = filters[key];
    const field = fields.find(f => f.field === key);
    if (!field) continue;

    let textValues = [];
    if (field.type === 'input') {
      textValues = [value];
    } else if (field.type === 'multiSelect') {
      textValues = value
        .map((v) => {
          const matchingOption = field.options.find(o => o.value === v);
          return matchingOption ? matchingOption.label : null;
        })
        .filter(tv => tv !== null);
    }
    if (textValues && textValues.length) {
      const FilterDiv = (
        <FilterWrapper>
          <FilterLabel>
            { field.label }
            :
          </FilterLabel>
          { textValues.map(tv => (
            <FilterValue>
              <p>{ tv }</p>
            </FilterValue>
          )) }
        </FilterWrapper>
      );

      FilterDivs.push(FilterDiv);
    }
  }
  if (!FilterDivs.length) return null;

  return (
    <Wrapper>
      <FilterBy>
        { t('Filtered by') }
        { ' ' }
:
      </FilterBy>
      <FiltersWrapper>
        { FilterDivs }
        <EditFilterButton plain onClick={ onExpandFilter.bind(this) }>
          { t('Edit filters') }
        </EditFilterButton>
      </FiltersWrapper>
    </Wrapper>
  );
};

export default FilterStatus;
