// @flow
import React from 'react';
import styled from 'styled-components';
import { Line, defaults } from 'react-chartjs-2';
import { Row, Col } from 'react-grid-system';

defaults.global.responsive = true;
defaults.global.elements.line.tension = 0.1;
defaults.global.defaultFontFamily = '"Muli",sans-serif';

type Props = {
  labels: string[],
  yAxesDisplayFormat: Function,
  datasets: {
    data: number[],
    color: string,
    label: string
  }[],
  chartHeight?: number,
  controlsElem?: React.Node
};

const MultiLineChartContainer = styled.div`
  display: flex;
`;
const LeftContainer = styled.div`
  flex: 1;
  display: flex;
  flex-direction: column;
  padding: 0 20px;
`;
const RightContainer = styled.div`
  padding-left: 60px;
  padding-bottom: 60px;
  flex: 4;
  border-left: 1px solid ${ props => props.theme.colors.mainGray };
  height: ${ props => props.height }px;
`;
const ColorIcon = styled.span`
  width: 16px;
  height: 16px;
  background-color: ${ props => props.color };
`;
const Title = styled.h2`
  color: ${ props => props.theme.colors.darkGray };
  font-size: 100%;
  text-align: left;
  margin-bottom: 20px;
`;
const TopPanelContainer = styled.div`
  min-height: 60px;
  padding: 20px 0;
`;
const LegendsContainer = styled.div`
  display: flex;
  min-height: 60%;
  justify-content: space-between;
  flex-direction: column;
`;
const LegendLabelContainer = styled.span`
  display: flex;
  flex: 1;
  flex-direction: column;
  justify-content: space-between;
  margin-left: 10px;
`;
const LegendItem = styled(Row)`
  margin-top: 10px;
  margin-bottom: 10px;
`;
const LegendLabel = styled.span`
  font-weight: bold;
`;
const LegendValue = styled.span`
  font-weight: thin;
  margin-top: 4px;
`;

const DEFAULT_CHART_HEIGHT = 320;

const MultiLineChart = (props: Props) => {
  const options = {
    scales: {
      xAxes: [
        {
          gridLines: {
            display: false,
            drawBorder: false
          }
        }
      ],
      yAxes: props.yAxesDisplayFormat
        ? [
          {
            ticks: {
              fontColor: 'rgb(47, 46, 46)',
              fontStyle: 'bold',
              callback: function (value, index, values) {
                return `${ props.yAxesDisplayFormat(value) }      `;
              }
            },
            gridLines: {
              drawBorder: false
            }
          }
        ]
        : []
    },
    maintainAspectRatio: false
  };

  const data = {
    labels: props.labels,
    datasets: (props.datasets || []).map(x => ({
      label: x.label,
      fill: false,
      backgroundColor: x.color,
      borderColor: x.color,
      borderCapStyle: 'butt',
      borderDash: [],
      borderDashOffset: 0.0,
      borderJoinStyle: 'miter',
      pointBorderColor: x.color,
      pointBackgroundColor: '#fff',
      pointBorderWidth: 1,
      pointHoverRadius: 5,
      pointHoverBackgroundColor: x.color,
      pointHoverBorderColor: 'rgba(220,220,220,1)',
      pointHoverBorderWidth: 2,
      pointRadius: 1,
      pointHitRadius: 10,
      data: x.data
    }))
  };
  return (
    <MultiLineChartContainer>
      <LeftContainer>
        <Title>{ props.title }</Title>
        <LegendsContainer>
          <TopPanelContainer>{ props.controlsElem }</TopPanelContainer>
          { (props.datasets || []).map(x => (
            <LegendItem key={ x }>
              <ColorIcon color={ x.color } />
              <LegendLabelContainer>
                <LegendLabel>
                  { x.label != 'TOTAL REVENUE'
                    ? `${ x.label } (${ Math.floor(
                      x.data[x.data.length - 1] / 1000
                    ) })`
                    : x.label }
                </LegendLabel>
                <LegendValue>
                  ${ x.data.reduce((a, b) => a + b, 0).toLocaleString() }
                </LegendValue>
              </LegendLabelContainer>
            </LegendItem>
          )) }
        </LegendsContainer>
      </LeftContainer>
      <RightContainer height={ props.chartHeight || DEFAULT_CHART_HEIGHT }>
        <TopPanelContainer />
        <Line data={ data } options={ options } legend={ { display: false } } />
      </RightContainer>
    </MultiLineChartContainer>
  );
};

export default MultiLineChart;
