import React, { Component } from 'react';
import Dropzone from 'react-dropzone';
import ImageLoader from 'react-image-file';
import styled from 'styled-components';

import Button from './Button';
import Modal from './ModalV2';
import Label from './inputs/Label';
import ImageCroppie from './ImageCroppie';
import InvalidMessage from './InvalidMessage';

import uploadIcon from '../../assets/common/icon-upload.svg';
import editIcon from '../../assets/common/icon-edit.svg';
import deleteIcon from '../../assets/common/icon-delete.svg';

require('formdata-polyfill');

const ImageUploadWrapper = styled.div`
  position: relative;
  width: 100%;
  margin-top: 20px;
  margin-bottom: 20px;
  ${ props => props.customCss || ''};
`;

const ImageUploadAspect = styled.div`
  position: relative;
  width: 100%;
  padding-bottom: ${ props => props.ratio || 100}%;
`;

const ImageUploadDiv = styled.div`
  position: absolute;
  top: 0;
  bottom: 0;
  left: 0;
  right: 0;
  ${ props => props.customCss || ''};
`;

const DefaultImage = styled.div`
  background-image: url("${ props => props.src}");
  width: 100%;
  height: 100%;
  background-position: center;
  background-size: contain;
  background-repeat: no-repeat;
`;

const DropzoneWrapper = styled.div`
  position: relative;
  width: 100%;
  height: 100%;
  padding: 5px;
  border-radius: ${ props => (props.circle ? '50%' : '0')};
  // overflow: hidden;
  box-shadow: 0 0 6px 0 rgba(0, 0, 0, 0.16);
`;

const ImageWrapper = styled.div`
  position: relative;
  width: 100%;
  height: 100%;
`;

const ImageLoaderWrapper = styled.div`
  height: 100%;

  & > div {
    height: 100% !important;
  }
`;

const ImageActionButton = styled(Button)`
  position: absolute;
  z-index: 200;
  width: 38px;
  height: 38px;
  right: -2px;
  background-color: white;
  box-shadow: 0 0 6px 0 rgba(0, 0, 0, 0.16);
  border-radius: 19px;
  padding: 0;
  margin: 0;

  & img {
    width: 14px;
    margin-top: 5px;
  }

  &:hover,
  &:active {
    background-color: white;
  }

  ${ props => (props.bottom ? 'bottom: -2px;' : 'top: -2px;')};
`;

const Hints = styled.div`
  margin-top: 10px;
  text-align: left;

  & p {
    font-size: 12px;
    line-height: 14px;
    margin-bottom: 5px;
  }
`;

const ModalDiv = styled(Modal)`
  overflow: hidden;

  & > div:nth-child(1) {
    background-color: white;
    padding-top: 15px;
    padding-bottom: 15px;
    margin-bottom: 0;
  }

  & > div:nth-child(1) > button {
    font-size: 26px;
    font-weight: normal;
    top: 6px;
  }

  & > div:nth-child(1) > h4 {
    font-size: 15px;
    text-align: left;
  }

  & > div:nth-child(2) {
    padding-top: 0;
    padding-left: 0;
    padding-right: 0;
    overflow: hidden;
  }
`;

export default class ImageUploadV2 extends Component {
  constructor() {
    super();

    this.state = {
      isCroppieModalOpen: false,
      previewImageFile: null,
      uploadedImageFile: null,
      error: null,
    };
  }

  // hack to enable its parent to make the ImageUpload component reset
  componentWillReceiveProps(nextProps) {
    const { reset } = this.props;
    if (nextProps.reset === true && reset !== true) {
      this.discardImage();
    }
  }

  onDrop(acceptedFiles) {

    const { setImage } = this.props;
    const previewImageFile = acceptedFiles[0];
    this.setState({
      isCroppieModalOpen: true,
      previewImageFile,
    });
    setImage(previewImageFile);
  }

  onCroppieSave(field, blob) {
    const { previewImageFile } = this.state;
    const uploadedImageFile = new File(
      [blob],
      previewImageFile.name,
      {
        type: 'image/png',
      },
    );

    this.setState({
      uploadedImageFile,
      isCroppieModalOpen: false,
    });

    const { onChange } = this.props;
    onChange(field, uploadedImageFile);
  }

  onCroppieClose() {
    this.setState({ isCroppieModalOpen: false });
  }

  editImage() {
    this.setState({
      isCroppieModalOpen: true,
    });
  }

  discardImage() {
    this.setState({
      previewImageFile: null,
      uploadedImageFile: null,
      error: null,
    });
  }

  render() {
    const {
      divStyle = {},
      customCss,
      image,
      label,
      hints = [],
      modaltitle = 'Crop image',
      field,
      width = 150,
      height = 150,
      viewportType = 'square',
      enableResize,
      required,
      errorMessage,
    } = this.props;
    const {
      isCroppieModalOpen,
      previewImageFile,
      uploadedImageFile,
      error,
    } = this.state;

    const dropzoneStyle = {
      width: '100%',
      height: '100%',
      cursor: 'pointer',
    };

    const actionImageStyle = {
      marginTop: '10px',
      width: '14px',
    };

    const ImageDropzone = (
      <DropzoneWrapper circle={viewportType === 'circle'}>
        <Dropzone
          style={dropzoneStyle}
          multiple={false}
          onDrop={this.onDrop.bind(this)}
        >
          <img
            alt='Image dropzone'
            src={uploadIcon}
            style={{
              textAlign: 'center',
              width: '42px',
              position: 'absolute',
              top: '50%',
              left: '50%',
              transform: 'translate(-50%, -50%)',
            }}
          />
        </Dropzone>
      </DropzoneWrapper>
    );

    const EditImageButton = uploadedImageFile ? (
      <ImageActionButton onClick={this.editImage.bind(this)}>
        <img alt='Edit image' src={editIcon} style={actionImageStyle} />
      </ImageActionButton>
    ) : (
        <Dropzone
          style={{
            cursor: 'pointer',
            position: 'absolute',
            zIndex: 200,
            width: 38,
            height: 38,
            right: -2,
            top: -2,
            textAlign: 'center',
            backgroundColor: '#ffffff',
            boxShadow: '0 0 6px 0 rgba(0, 0, 0, 0.16)',
            borderRadius: '19px',
          }}
          onDrop={this.onDrop.bind(this)}
        >
          <img alt='Edit image' src={editIcon} style={actionImageStyle} />
        </Dropzone>
      );

    const DiscardImageButton = uploadedImageFile ? (
      <ImageActionButton bottom onClick={this.discardImage.bind(this)}>
        <img alt='Remove image' src={deleteIcon} style={actionImageStyle} />
      </ImageActionButton>
    ) : null;

    const ImageCrop = (
      <ImageWrapper>
        {EditImageButton}
        {DiscardImageButton}
        <DropzoneWrapper circle={viewportType === 'circle'}>
          {uploadedImageFile ? (
            <ImageLoaderWrapper>
              <ImageLoader file={uploadedImageFile} height={height - 10} />
            </ImageLoaderWrapper>
          ) : (
              <DefaultImage src={image} />
            )}
        </DropzoneWrapper>
      </ImageWrapper>
    );

    const errorStyle = {
      position: 'absolute',
      right: 20,
      top: 60,
      fontSize: 12,
      lineHeight: '12px',
      color: 'rgb(255, 108, 108)',
    };

    return (
      <ImageUploadWrapper style={divStyle} customCss={customCss}>
        {label ? (
          <Label
            field={field}
            label={label}
            required={required}
            customCss='opacity: 1;'
          />
        ) : null}
        <ImageUploadAspect ratio={height / width * 100}>
          <ImageUploadDiv>
            <p style={errorStyle}>{error}</p>
            {previewImageFile || image ? ImageCrop : ImageDropzone}
            <Hints>{hints.map(hint => <p>{hint}</p>)}</Hints>
            <ModalDiv
              large
              isOpen={isCroppieModalOpen}
              onClose={this.onCroppieClose.bind(this)}
              title={modaltitle}
              customCss={`
                width: 80vw;
                height: 80vh;
              `}
            >
              <ImageCroppie
                field={field}
                viewportWidth={width}
                viewportHeight={height}
                viewportType={viewportType}
                enableResize={enableResize}
                previewImageFile={previewImageFile}
                onCroppieSave={this.onCroppieSave.bind(this)}
                onClose={this.onCroppieClose.bind(this)}
              />
            </ModalDiv>
          </ImageUploadDiv>
        </ImageUploadAspect>
        {errorMessage ? <InvalidMessage>{errorMessage}</InvalidMessage> : null}
      </ImageUploadWrapper>
    );
  }
}
