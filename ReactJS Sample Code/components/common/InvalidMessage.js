import styled from 'styled-components';

const InvalidMessage = styled.p`
  position: absolute;
  font-size: 12px;
  color: ${ props => (props.theme.light
    ? props.theme.colors.mainYellow
    : props.theme.colors.pinkRed) } !important;
  ${ props => props.customCss || '' };
`;

export default InvalidMessage;
