import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as UiActionCreators from './../../../redux/ui';

const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators(UiActionCreators, dispatch)
});

class FormikUpdater extends Component {
  componentDidUpdate (prevProps) {
    if (
      this.props.formikProps &&
      this.props.formikProps !== prevProps.formikProps
    ) {
      const { actions, id, formikProps } = this.props;
      const { handleSubmit, handleReset, errors, dirty } = formikProps;

      // if form is not dirty, pass in null
      actions.setFormHandleSubmit(id, dirty ? handleSubmit : null);
      actions.setFormHandleReset(id, dirty ? handleReset : null);
      actions.setFormErrors(id, dirty ? errors : null);
      actions.setFormDirty(id, dirty || null);
    }
  }

  componentWillUnmount () {
    const { actions, id } = this.props;
    actions.setFormHandleSubmit(id, null);
    actions.setFormHandleReset(id, null);
    actions.setFormErrors(id, null);
    actions.setFormDirty(id, null);
  }

  render () {
    return <div />;
  }
}

export default connect(null, mapDispatchToProps)(FormikUpdater);
