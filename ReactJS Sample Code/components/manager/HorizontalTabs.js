import React from 'react';
import styled from 'styled-components';

const Container = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: space-around;
  width: 100%;
  height: 50px;
  background-color: white;
`;

const TabContainer = styled.div`
  height: 100%;
  display: flex;
  align-items: center;
  justify-content: center;
  padding-left: 5px;
  padding-right: 5px;
  border-bottom: solid ${ props => (props.selected ? '4' : '0') }px
    ${ props => props.theme.colors.mainBlue };
`;

export default function HorizontalTabs ({ tabs, value, onChange }) {
  const TabsWithUI = tabs.map(tab => {
    const selected = value && tab.value === value;
    return (
      <TabContainer
        selected={ selected }
        onClick={ () => {
          if (!selected) {
            onChange(tab.value);
          }
        } }
      >
        <h5>{ tab.label }</h5>
      </TabContainer>
    );
  });

  return <Container>{ TabsWithUI }</Container>;
}
