import React from 'react';
import { darken, lighten } from 'polished';
import styled from 'styled-components';

export const Tabs = styled.div`
  display: flex;
  flex-direction: row;
`;

const TabWrapper = styled.div`
  flex: 1;
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
  background-color: ${ props =>
    props.selected ? 'white' : props.theme.colors.lightGray };
  padding: 5px 12px;
  cursor: pointer;
  min-height: 60px;
`;

const TabHeader = styled.h3`
  color: ${ props =>
    props.selected ? props.theme.colors.darkGray : props.theme.colors.mainGray };
`;

export const Tab = ({ children, selected, onClick }) => (
  <TabWrapper { ...{ selected, onClick } }>
    <TabHeader capitalize align='center' selected={ selected }>
      { children }
    </TabHeader>
  </TabWrapper>
);
