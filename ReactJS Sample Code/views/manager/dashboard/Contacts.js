import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { withRouter } from 'react-router';

import { formatDateTime, stateHasSelectedRows } from '../../../utils';
import * as UiActionCreators from '../../../redux/ui';
import * as ManagerActionCreators from '../../../redux/manager';
import * as OrganizationActionCreators from '../../../redux/organization';
import * as ContactsActionCreators from '../../../redux/contacts';
import * as EmailTemplatesActionCreators from '../../../redux/emailTemplates';

import ManagerContainer from '../../../components/layouts/ManagerContainer';
import Table from '../../../components/common/Table';
import BasicContactInfo from '../../../components/manager/BasicContactInfo';
import Tag from '../../../components/manager/tags/Tag';
import EditTag from '../../../components/manager/tags/EditTag';
import ActivityBubble from '../../../components/common/ActivityBubble';

import { t as translate } from '../../../../i18n';

const t = a => translate(a, 'manager/home/contacts');

const mapStateToProps = state => ({
  organization: state.manager.organization,
  contacts: state.contacts.contacts,
  tags: state.organization.tags,
  customFields: state.organization.customFields,
  meta: state.manager.meta,
});

const ActionCreators = Object.assign(
  {},
  UiActionCreators,
  ManagerActionCreators,
  OrganizationActionCreators,
  ContactsActionCreators,
  EmailTemplatesActionCreators,
);
const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators(ActionCreators, dispatch),
});

class DashboardContactsView extends Component {
  constructor () {
    super();

    // default table settings
    this.state = {
      limit: 10,
      offset: 0,
      sortBy: 'created_at',
      sortOrder: 'desc',
      sortType: null,
    };
  }

  async componentDidMount () {
    const { actions, match } = this.props;
    const { oId } = match.params;

    actions.setLoading(true);

    try {
      const promises = [
        actions.getContacts(oId, { limit: 10, offset: 0 }),
        actions.getOrganizationTags(oId),
        actions.getOrganizationCustomFields(oId),
        actions.getEmailTemplates(oId),
      ];

      await Promise.all(promises);
    } catch (e) {
      console.error('ERROR: DashboardContactsView', e);
    } finally {
      actions.setLoading(false);
    }
  }

  getMembersFrom (newFrom) {
    this.setState({ offset: newFrom - 1 }, this.reloadContacts);
  }

  sortMembers (newSortBy, sortType) {
    const { sortBy, sortOrder } = this.state;

    const newSortOrder = sortBy === newSortBy ? (sortOrder === 'asc' ? 'desc' : 'asc') : 'asc';
    this.setState({
      sortBy: newSortBy,
      sortOrder: newSortOrder,
      sortType,
    }, this.reloadContacts);
  }

  changeMembersPerPage (newPerPage) {
    this.setState(
      {
        offset: 0,
        limit: newPerPage,
      },
      this.reloadContacts,
    );
  }

  async reloadContacts () {
    const { actions, organization } = this.props;
    const {
      limit, offset, sortBy, sortOrder, sortType,
    } = this.state;

    try {
      await actions.getContacts(organization.id, {
        limit,
        offset,
        sort_by: sortBy,
        sort_order: sortOrder,
        sort_type: sortType,
      });
    } catch (e) {
      console.error(e);
    }
  }

  render () {
    const {
      organization,
      contacts,
      tags,
      customFields,
      meta,
    } = this.props;
    const { limit, sortBy, sortOrder } = this.state;
    let tableColumns = [
      {
        title: t('Name'),
        field: 'name',
        compulsory: true,
        unscrollable: true,
        flex: '1.5 0 0',
        format: (d, r) => (
          <BasicContactInfo
            type='contact'
            organization={ organization }
            contact={ r }
          />
        ),
        tooltipFormat: d => d,
      },
      {
        title: t('Ref. ID'),
        field: 'id',
        format: v => `#U${ v }`,
      },
      {
        title: t('Tags'),
        field: 'tags',
        css: 'overflow: visible;line-height: normal;',
        bodyCss: 'overflow: visible;',
        format: (d, r) => {
          const contactTags = tags.filter(
            tag => r.tags && r.tags.indexOf(tag.id) > -1,
          );

          return contactTags.length
            ? contactTags.map(tag => <Tag value={ tag.tag } color={ tag.color } />)
            : '-';
        },
      },
      {
        title: t('Activities'),
        field: 'sources',
        flex: '3 0 0',
        format: sources => (sources
          ? sources.map(s => (
            <ActivityBubble
              contactType={ s.contact_type }
              contactId={ s.contact_id }
              sourceType={ s.source_type }
              sourceId={ s.source_id }
            />
          ))
          : '-'),
      },
      {
        title: t('Last Activity At'),
        field: 'updated_at',
        format: v => formatDateTime(v),
      },
      {
        title: t('First Activity At'),
        field: 'created_at',
        format: v => formatDateTime(v),
      },
    ];

    tableColumns = tableColumns.concat(
      customFields.map(f => ({
        title: f.name,
        field: f.id,
        customField: true,
        format: (d, r) => {
          let value = r.custom_fields ? r.custom_fields[f.id] : null;
          if (!value) return '-';
          if (f.data_type === 'boolean') value = value === 1 ? 'Yes' : 'No';
          return value;
        },
      })),
    );

    const tableBulkActions = [
      {
        render: (params) => {
          const actionParams = Object.assign(params, { contact_type: 'user' });

          return (
            <EditTag
              tags={ tags }
              reloadData={ this.reloadContacts.bind(this) }
              actionParams={ actionParams }
            />
          );
        },
        condition: state => stateHasSelectedRows(state),
      },
    ];

    return (
      <ManagerContainer>
        <Table
          id='contacts'
          title={ t('Contact list') }
          subtitle={ `${ t('Total contact number') }: ${ meta.count }` }
          columns={ tableColumns }
          rows={ contacts }
          meta={ meta }
          perPage={ limit }
          sortBy={ sortBy }
          sortOrder={ sortOrder }
          onSort={ this.sortMembers.bind(this) }
          onChangeFrom={ this.getMembersFrom.bind(this) }
          onChangePerPage={ this.changeMembersPerPage.bind(this) }
          bulkActions={ tableBulkActions }
          emptyText={ t('There are no contacts yet') }
          // HACK for adding tag dropdown visibility - this is a shit hack but no better way for now
          bulkActionsCss='
            overflow-x: visible;
            overflow-y: visible;
          '
        />
      </ManagerContainer>
    );
  }
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(DashboardContactsView));
