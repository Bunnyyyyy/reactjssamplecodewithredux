import React from 'react';
import styled from 'styled-components';
import Button from '../Button';

import visaCardIcon from '../../../assets/common/cards/visa.svg';
import amexCardIcon from '../../../assets/common/cards/amex.svg';
import mastercardCardIcon from '../../../assets/common/cards/mastercard.svg';
import discoverCardIcon from '../../../assets/common/cards/discover.svg';
import jcbCardIcon from '../../../assets/common/cards/jcb.svg';
import dinersCardIcon from '../../../assets/common/cards/diners.svg';
import unknownCardIcon from '../../../assets/common/cards/unknown.svg';

const Wrapper = styled.div`
  cursor: pointer;
  height: 55px;
  padding: 0 30px;
  border-radius: 4px;
  max-width: 690px;
  border: solid 1px #e1e8ed;
  display: flex;
  justify-content: space-between;
  align-items: center;
  margin: 20px auto 0 auto;
  ${ props => (props.selected ? 'border: solid 2px #00a7ff;' : '') } opacity: ${ props => (props.selected ? 1 : 1) };

  &:hover {
    opacity: 0.85;
  }

  & p {
    font-size: ${ props => (props.selected ? '15px' : '15px') };
    line-height: ${ props => (props.selected ? '40px' : '40px') };
  }

  & img {
    vertical-align: middle;
    height: 16px;
    margin: 0 20px 2px 0;
  }

  @media (max-width: 380px) {
    & p {
      font-size: 13px;
    }
  }

  @media (max-width: 320px) {
    & p {
      font-size: 11px;
    }
  }

  @media (max-width: 280px) {
    & p {
      font-size: 10px;
    }
  }
`;

const BrandNumber = styled.div`
  flex-grow: 1;
  display: flex;
  align-items: center;
`;

const Number = styled.div`
  display: flex;
  align-items: center;
`;

const NumberDot = styled.div`
  height: 8px;
  width: 8px;
  border-radius: 50%;
  background-color: black;
  margin-right: 5px;
  ${ props => (props.space ? 'margin-right: 20px;' : '') };
`;

const RemoveButton = Button.extend`
  margin-left: 30px;
  color: ${ props => props.theme.colors.mainGray };
  width: 40px;
  text-align: center;
`;

const CARD_IMAGE_URLS = {
  Visa: visaCardIcon,
  'American Express': amexCardIcon,
  MasterCard: mastercardCardIcon,
  Discover: discoverCardIcon,
  JCB: jcbCardIcon,
  'Diners Club': dinersCardIcon,
  Unknown: unknownCardIcon,
};

const CardOption = ({
  card = {}, selected = false, onRemove, ...props
}) => {
  const {
    brand = 'Unknown', last4 = '', exp_month = '', exp_year = '',
  } = card;

  const expiryMonthText = exp_month < 10 ? `0${ exp_month }` : exp_month.toString();
  const expiryYearText = exp_year.toString().slice(2);

  return (
    <Wrapper selected={ selected } { ...props }>
      <BrandNumber>
        <img alt='' src={ CARD_IMAGE_URLS[brand] } />
        { brand === 'American Express' ? (
          <Number>
            <NumberDot />
            <NumberDot />
            <NumberDot />
            <NumberDot space />
            <NumberDot />
            <NumberDot />
            <NumberDot />
            <NumberDot />
            <NumberDot />
            <NumberDot space />
            <NumberDot />
            <span>{ last4 }</span>
          </Number>
        ) : (
          <Number>
            <NumberDot />
            <NumberDot />
            <NumberDot />
            <NumberDot space />
            <NumberDot />
            <NumberDot />
            <NumberDot />
            <NumberDot space />
            <NumberDot />
            <NumberDot />
            <NumberDot />
            <NumberDot space />
            <span>{ last4 }</span>
          </Number>
        ) }
      </BrandNumber>
      <p>
        { expiryMonthText }
        { ' ' }
/
        { expiryYearText }
      </p>
      <RemoveButton
        plain
        disabled={ !onRemove }
        onClick={ onRemove ? onRemove.bind(this) : null }
      >
        { onRemove ? (
          <i className='fa fa-trash icon-class' aria-hidden='true' />
        ) : null }
      </RemoveButton>
    </Wrapper>
  );
};

export default CardOption;
