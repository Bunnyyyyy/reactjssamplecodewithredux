import Modal from 'react-modal';
import styled from 'styled-components';

import Button from './Button';

const customStyles = {
  content: {
    position: 'absolute',
    top: '120px',
    left: '40px',
    right: '40px',
    maxHeight: 'calc(100vh - 180px)',
    border: '1px solid #ccc',
    background: '#fff',
    overflow: 'auto',
    WebkitOverflowScrolling: 'touch',
    outline: 'none',
    borderRadius: 'none'
  },
  overlay: {
    backgroundColor: 'rgba(0, 0, 0, 0.5)',
    zIndex: 999
  }
};

const StyledModal = styled(Modal)`
  width: 60vw;
  height: auto;
  max-height: 80vh;
  margin: auto;
  background-color: rgba(0, 0, 0, 0.5);
  z-index: 999;

  @media (max-width: 900px) {
    width: 100vw;
    left: 0 !important;
  }
`;

const ModalClose = styled(Button)`
  position: fixed;
  top: 90px;
  right: 15vw;
  background: none;
  font-size: 50px;
`;

export default function ReactModal ({ onClose, isOpen, children }) {
  return (
    <StyledModal
      isOpen={ isOpen }
      onRequestClose={ onClose }
      ariaHideApp={ false }
      style={ customStyles }
    >
      <ModalClose onClick={ onClose }>
        <span aria-hidden='true'> ×</span>
      </ModalClose>
      { children }
    </StyledModal>
  );
}
