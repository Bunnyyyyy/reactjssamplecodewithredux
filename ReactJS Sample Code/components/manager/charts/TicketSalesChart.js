import React, { Component } from 'react';
import styled from 'styled-components';
import moment from 'moment-timezone';

import Select from '../../common/inputs/Select';
import LineChart from './LineChart';

import currencies from '../../../utils/currencies';
import { nowrap } from '../../../utils/styles';
import Card from '../cards/Card';

import moneyIcon from '../../../assets/manager/icon_money.svg';

const ChartWrapper = styled.div`
  padding: 0 !important;
`;

const ChartHeader = styled.div`
  display: flex;
  align-items: center;
  height: 48px;
  background-color: white;
  border-bottom: 1px solid ${ props => props.theme.colors.lightGray };
  padding: 0 30px;
`;

const HeaderCell = styled.div`
  flex: 2 0 0;
  height: 100%;
  display: flex;
  align-items: center;
  text-transform: capitalize;
  color: ${ props => props.theme.colors.mainGray };
  padding-right: 10px;
`;

const HeaderTitle = styled.div`
  flex-shrink: 1;
  font-size: 12px;
  line-height: 18px;
  height: 18px;
  ${ nowrap };
`;

const InputWrapper = styled.div`
  flex-shrink: 0;
  padding-left: 20px;
  line-height: 18px;
  font-size: 12px;
  color: ${ props => (props.active ? props.theme.colors.mainBlue : '#2f2e2e') };
  cursor: pointer;

  &:hover {
    color: ${ props => props.theme.colors.mainBlue };
  }
`;

const LineChartWrapper = styled.div`
  margin: 30px;
`;

const EmptyCardWrapper = styled.div`
  text-align: center;
  padding: 60px 0;

  p {
    padding-top: 15px;
  }
`;

const CardImage = styled.img`
  width: 140px;
  height: 80px;
`;

const VerticalDivider = styled.div`
  width: 1px;
  height: 18px;
  background-color: ${ props => props.theme.colors.lightGray };
  margin: 0 25px;
`;

const Label = styled.p`
  font-size: 12px;
  padding-bottom: 5px;
`;

const LabelWrapper = ({ label, description }) => (
  <div>
    <Label>{ label }</Label>
    <h4>{ description }</h4>
  </div>
);

const BodyWrapper = styled.div`
  display: flex;
  padding: 20px 30px;
  border-bottom: 1px solid ${ props => props.theme.colors.lightGray };
`;

const EmptyCard = ({ src, label }) => (
  <EmptyCardWrapper>
    <CardImage src={ src } />
    <p>{ label }</p>
  </EmptyCardWrapper>
);

const cardCss = `
  flex-basis: calc(50% - 15px);
  box-shadow: none;
  border-radius: 0;

  &:nth-child(2n) {
    margin-right: 0;
  }
  margin: 0 0 0 0;
`;

const leftLineCss = `
  padding-left: 30px;
  border-left: 1px solid #e1e8ed;
`;

const bodyCss = `
  > div {
    padding-left: 0;
    padding-right: 0;
  }
`;

export default class TicketSalesChart extends Component {
  constructor (props) {
    super();

    this.state = {
      groupBy: 'purchase_week',
      startAt: null,
      endAt: null,
      startAtOptions: [],
      endAtOptions: [],
    };

    this.updateDateOptions(props, this.state);
  }

  updateDateOptions (props, state, isUpdatingGroupByMode = false) {
    const { event, salesData = [] } = props || this.props;
    const { groupBy, startAt, endAt } = state || this.state;

    let data = [];
    if (groupBy === 'purchase_week' || groupBy === 'purchase_day') {
      data = salesData.purchase_dates;
    } else {
      data = salesData.dates;
    }

    if (!data || !data.length) return;

    const startAtOptions = [];
    const endAtOptions = [];

    const startingDay = moment
      .utc(data[0].date)
      .tz(event.timezone)
      .hour(0)
      .minute(0)
      .second(0)
      .millisecond(0);

    const endingDay = moment
      .utc(data[data.length - 1].date)
      .tz(event.timezone)
      .hour(23)
      .minute(59)
      .second(59)
      .millisecond(999);

    let maxEndAt;
    if (startAt) {
      if (groupBy === 'day' || groupBy === 'purchase_day') {
        maxEndAt = moment(startAt)
          .add(6, 'days')
          .hour(23)
          .minute(59)
          .second(59)
          .millisecond(999);
      }
    }

    let currentDay = moment(startingDay).clone();
    while (currentDay.isSameOrBefore(endingDay)) {
      let dayCount;
      if (groupBy === 'week' || groupBy === 'purchase_week') {
        dayCount = 7;
      } else {
        dayCount = 1;
      }

      startAtOptions.push({
        label: currentDay.format(
          startAtOptions.length ? 'MM/DD' : 'YYYY/MM/DD',
        ),
        value: currentDay.valueOf(),
      });

      if (
        (!startAt || currentDay.isSameOrAfter(startAt))
        && (!maxEndAt || currentDay.isBefore(maxEndAt))
      ) {
        let endAtLabel;
        if (groupBy === 'week' || groupBy === 'purchase_week') {
          endAtLabel = currentDay
            .clone()
            .add(6, 'day')
            .format('MM/DD');
        } else {
          endAtLabel = currentDay.format('MM/DD');
        }

        endAtOptions.push({
          label: endAtLabel,
          value: currentDay
            .clone()
            .hour(23)
            .minute(59)
            .second(59)
            .millisecond(999)
            .valueOf(),
        });
      }

      currentDay = currentDay.add(dayCount, 'day');
    }

    const startAtInRange = !!startAtOptions.find(o => o.value === startAt);
    const endAtInRange = !!endAtOptions.find(o => o.value === endAt);

    if (state) {
      state.startAt = startAtInRange ? startAt : startingDay.valueOf();
      state.endAt = endAtInRange
        ? endAt
        : endAtOptions.length
          ? endAtOptions[endAtOptions.length - 1].value
          : null;
      state.startAtOptions = startAtOptions;
      state.endAtOptions = endAtOptions;
    } else {
      this.setState({
        startAt:
          startAtInRange && !isUpdatingGroupByMode
            ? startAt
            : startingDay.valueOf(),
        endAt: endAtInRange
          ? endAt
          : endAtOptions.length
            ? endAtOptions[endAtOptions.length - 1].value
            : null,
        startAtOptions,
        endAtOptions,
      });
    }
  }

  changeGroupBy (newGroupBy) {
    const { groupBy } = this.state;
    const isUpdatingGroupByMode = newGroupBy.indexOf('purchase_') !== groupBy.indexOf('purchase_');

    const stateUpdate = {
      groupBy: newGroupBy,
      endAt: null,
    };

    if (isUpdatingGroupByMode) {
      stateUpdate.startAt = null;
    }

    this.setState(
      stateUpdate,
      this.updateDateOptions.bind(this, null, null, isUpdatingGroupByMode),
    );
  }

  changeStartAt (startAt) {
    this.setState({ startAt }, this.updateDateOptions);
  }

  changeEndAt (endAt) {
    this.setState({ endAt }, this.updateDateOptions);
  }

  render () {
    const { event, salesData = {}, isFreeEvent } = this.props;
    const {
      groupBy,
      startAt,
      endAt,
      startAtOptions,
      endAtOptions,
    } = this.state;
    const currency = currencies[event.currency || 'hkd'];

    const isEmpty = (!salesData.purchase_dates || !salesData.purchase_dates.length)
      && (!salesData.dates || !salesData.dates.length);

    const lineChartData = [];
    const amountValues = [];
    const countValues = [];

    let currentDay = moment.tz(startAt, event.timezone).clone();
    // while
    while (currentDay.isBefore(endAt)) {
      let amount = 0;
      let guests = 0;

      let dayCount;
      if (groupBy === 'week' || groupBy === 'purchase_week') {
        dayCount = 7;
      } else {
        dayCount = 1;
      }

      let data;
      if (groupBy === 'purchase_week' || groupBy === 'purchase_day') {
        data = salesData.purchase_dates;
      } else {
        data = salesData.dates;
      }

      data
        .filter(d => (
          moment
            .utc(d.date)
            .tz(event.timezone)
          // inclusive of currentDay and currentDay + dayCount
            .isBetween(
              currentDay,
              currentDay.clone().add(dayCount, 'day'),
              null,
              '[)',
            )
        ))
        .forEach((d) => {
          amount += d.amount;
          guests += d.guests;
        });

      const currentLabel = currentDay.format(
        lineChartData.length ? 'MM/DD' : 'YYYY/MM/DD',
      );

      amountValues.push(amount);
      countValues.push(guests);

      if (isFreeEvent) {
        lineChartData.push({ label: currentLabel, value: guests });
      } else {
        lineChartData.push({ label: currentLabel, value: amount });
      }

      currentDay = currentDay.add(dayCount, 'day');
    }

    const options = [
      { value: 'purchase_week', label: 'Week' },
      { value: 'purchase_day', label: 'Day' },
    ];

    const selectCss = `
      padding-left: 20px;
      margin-right: 40px;
      width: 128px;

      & .select {
        &__single-value {
          font-size: 12px;
        }
        &__indicator {
          & svg {
            width: 18px;
          }
        }
      }
    `;

    const headerItems = [
      {
        label: 'Time interval:',
        render: options.map(({ value, label }, index) => (
          <InputWrapper
            key={ index }
            active={ value === groupBy }
            onClick={ () => this.changeGroupBy(value) }
          >
            { label }
          </InputWrapper>
        )),
        divider: true,
      },
      {
        label: 'From:',
        render: (
          <Select
            searchable={ false }
            options={ startAtOptions }
            value={ startAt }
            onChange={ value => this.changeStartAt(value) }
            customCss={ selectCss }
          />
        ),
      },
      {
        label: 'To:',
        render: (
          <Select
            searchable={ false }
            options={ endAtOptions }
            value={ endAt }
            onChange={ value => this.changeEndAt(value) }
            customCss={ selectCss }
          />
        ),
      },
    ];

    return (
      <ChartWrapper>
        <ChartHeader>
          <HeaderCell>
            { headerItems.map(({ label, divider, render }) => [
              <HeaderTitle>{ label }</HeaderTitle>,
              render,
              divider ? <VerticalDivider /> : null,
            ]) }
          </HeaderCell>
        </ChartHeader>
        <BodyWrapper>
          { /* <Card customCss={ [cardCss, borderCss] } bodyCss={ bodyCss }> */ }
          <Card customCss={ cardCss } bodyCss={ bodyCss }>
            <LabelWrapper
              label='Total sales'
              description={ `${ currency.symbol }${ amountValues
                .reduce((pv, v) => pv + v, 0)
                .toLocaleString() }` }
            />
          </Card>
          <Card customCss={ cardCss + leftLineCss } bodyCss={ bodyCss }>
            <LabelWrapper
              label='Total no. of guests'
              description={ countValues.reduce((pv, v) => pv + v, 0) }
            />
          </Card>
        </BodyWrapper>
        { /* <Select
          fullWidth
          searchable={ false }
          label='Group By'
          options={ [
            { value: 'week', label: 'Week (Ticket Timeslot Date)' },
            { value: 'day', label: 'Day  (Ticket Timeslot Date)' },
            { value: 'purchase_week', label: 'Week (Purchase Date)' },
            { value: 'purchase_day', label: 'Day (Purchase Date)' }
          ] }
          value={ groupBy }
          onChange={ value => this.changeGroupBy(value) }
        />
        <div style={ { display: 'flex' } }>
          <Select
            fullWidth
            searchable={ false }
            label={ groupBy !== 'date' ? 'From' : 'Date' }
            options={ startAtOptions }
            value={ startAt }
            onChange={ value => this.changeStartAt(value) }
            customCss='margin-right: 20px; flex: 0 0 calc(50% - 10px);'
          />
          { groupBy !== 'date' ? (
            <Select
              fullWidth
              searchable={ false }
              label='To'
              options={ endAtOptions }
              value={ endAt }
              onChange={ value => this.changeEndAt(value) }
              customCss='flex: 0 0 calc(50% - 10px);'
            />
          ) : null }
        </div> */ }
        { isEmpty ? (
          <EmptyCard
            label='No sales yet'
            src={ moneyIcon }
          />
        ) : (
          <LineChartWrapper>
            <LineChart
              data={ lineChartData }
              tooltipFormat={ (item) => {
                if (isFreeEvent) {
                  return `${ item.label } - ${ item.value }`;
                }
                return (
                  `${ item.label } - ${
                    currency.symbol
                  }${ item.value.toLocaleString() }`
                );
              } }
            />
          </LineChartWrapper>
        ) }
      </ChartWrapper>
    );
  }
}
