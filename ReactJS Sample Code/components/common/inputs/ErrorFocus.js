import React, { Component } from 'react';

class ErrorFocus extends Component {
  componentDidUpdate (prevProps) {
    const { isSubmitting, isValidating, errors } = prevProps;
    const keys = Object.keys(errors);
    if (keys.length > 0 && isSubmitting && !isValidating) {
      const selector = `[id="${ keys[0] }"]`;
      const errorElement = document.querySelector(selector);
      errorElement.focus();
    }
  }

  render () {
    return null;
  }
}

export default ErrorFocus;
