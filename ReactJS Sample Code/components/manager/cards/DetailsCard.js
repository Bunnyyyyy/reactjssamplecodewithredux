import React from 'react';
import { Link } from 'react-router-dom';
import styled from 'styled-components';

import Card from './Card';
import Button from '../../common/Button';

const HeaderActionButton = styled(Button)`
  font-size: 12px;
  line-height: 18px;
  margin: 14px 0;
  color: ${ props => props.theme.colors.darkBlue };
`;

const BodyWrapper = styled.div`
  padding: 20px 0;
`;

const FixedWidthTitle = styled.h5`
  padding-right: 20px;

  ${ props => (props.newLine ? 'min-width: 170px;' : 'width: 170px;') } ${ props => props.customCss || '' };
`;

const Row = styled.div`
  display: flex;
  align-items: flex-start;
  flex-wrap: wrap;
  margin-bottom: 10px;
`;

const RowContent = styled.p`
  ${ props => (props.newLine ? 'min-width: 100%;' : '') };
`;

const ImageContainer = styled.div`
  margin-top: 20px;

  & img {
    width: 100%;
  }
`;

const DetailsButton = styled(Button)`
  display: block;
  height: 20px;
  font-size: 12px;
  line-height: 17px;
  border-bottom-width: 3px;
  margin: 5px auto 15px auto;

  &:last-child {
    margin-right: auto;
  }
`;

export default function ({
  title,
  data = [],
  detailsUrl,
  detailsOnNewLine,
  actions = [],
}) {
  const DetailFields = data.map(
    ({
      // eslint-disable-next-line
      title, titleCss, details, isHtml, isImageUrl, render,
    }) => {
      // please sanitize the details before you pass it in!
      let DetailContent = (
        <RowContent newLine={ detailsOnNewLine }>{ details || '-' }</RowContent>
      );

      if (isImageUrl) {
        DetailContent = (
          <ImageContainer>
            <img alt='' src={ details } />
          </ImageContainer>
        );
      } else if (isHtml) {
        DetailContent = <div dangerouslySetInnerHTML={ { __html: details } } />;
      } else if (render) {
        DetailContent = render();
      }

      return (
        <Row>
          <FixedWidthTitle newLine={ detailsOnNewLine } customCss={ titleCss }>
            { title }
          </FixedWidthTitle>
          { DetailContent }
        </Row>
      );
    },
  );

  const HeaderActions = actions.map(a => (
    <HeaderActionButton plain onClick={ a.function.bind(this) }>
      { a.label }
    </HeaderActionButton>
  ));

  return (
    <Card title={ title } headerComponent={ HeaderActions }>
      <BodyWrapper>
        { DetailFields }
        { detailsUrl && data.length ? (
          <Link to={ detailsUrl }>
            <DetailsButton underline>See Details</DetailsButton>
          </Link>
        ) : null }
      </BodyWrapper>
    </Card>
  );
}
