import React, { Component } from 'react';
import styled from 'styled-components';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { withRouter } from 'react-router';
import moment from 'moment';

import * as UiActionCreators from '../../../redux/ui';
import * as ManagerActionCreators from '../../../redux/manager';
import * as OrganizationActionCreators from '../../../redux/organization';
import * as DonationsActionCreators from '../../../redux/donations';
import * as ProfileActionCreators from '../../../redux/profile';

import ManagerContainer from '../../../components/layouts/ManagerContainer';
import NumbersCard from '../../../components/manager/cards/NumbersCard';
import ActivityListCard from '../../../components/manager/cards/ActivityListCard';
import WelcomeCard from '../../../components/manager/cards/WelcomeCard';
import CreationCard from '../../../components/manager/cards/CreationCard';

import eventsIcon from '../../../assets/manager/menu/icon_events.svg';
import formsIcon from '../../../assets/manager/menu/icon_forms.svg';
import emailsIcon from '../../../assets/manager/menu/icon_emails.svg';
import eventsImage from '../../../assets/common/actions/events.svg';
import formsImage from '../../../assets/common/actions/forms.svg';
import emailsImage from '../../../assets/common/actions/emails.svg';

import { t as translate } from '../../../../i18n';

const t = a => translate(a, 'manager/home/index');

const CreationCardWrapper = styled.div`
  width: 100%;
  max-width: 1280px;
  display: flex;
  margin: 0 auto;
`;

const mapStateToProps = state => ({
  user: state.me.user,
  organizations: state.manager.organizations,
  organization: state.manager.organization,
  stripeAccount: state.manager.stripeAccount,
  members: state.organization.members,
  memberCount: state.organization.memberCount,
  donations: state.donations.donations,
  donationsTotal: state.donations.donationsTotal,
});
const ActionCreators = Object.assign(
  {},
  UiActionCreators,
  ManagerActionCreators,
  OrganizationActionCreators,
  DonationsActionCreators,
  ProfileActionCreators,
);
const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators(ActionCreators, dispatch),
});

class DashboardHomeView extends Component {
  constructor () {
    super();

    this.state = {
      guestsTotal: [],
      ticketingGuests: [],
    };
  }

  async componentDidMount () {
    const { actions, match } = this.props;
    const { oId } = match.params;

    actions.setLoading(true);

    try {
      const promises = [
        actions.getOrganizationStripeAccount(oId),
        actions.getDonations(oId, 6, 0, 'created_at', 'desc'),
        actions.getDonationsTotal(oId),
        actions.getOrganizationMembers(oId, {
          limit: 6,
          offset: 0,
          sort_by: 'created_at',
          sort_order: 'desc',
        }),
        actions.getOrganizationMemberCount(oId, { active: true }),
      ];
      await Promise.all(promises);

      let guestsTotal = [];
      let ticketingGuests = [];

      guestsTotal = await actions.getOrganizationEventGuestsTotal(oId);
      ticketingGuests = await actions.getOrganizationEventTicketingGuests(oId);

      this.setState({
        guestsTotal,
        ticketingGuests,
      });
    } catch (e) {
      console.error('ERROR: DashboardHomeView', e);
    } finally {
      actions.setLoading(false);
    }
  }

  render () {
    const {
      organization,
      members,
      memberCount,
      donations,
      donationsTotal,
    } = this.props;
    const {
      guestsTotal = [],
      ticketingGuests = [],
    } = this.state;

    const summaryItems = [
      {
        title: t('Active Members'),
        number: memberCount !== null ? memberCount : '',
      },
      {
        title: t('Event Guests'),
        number: guestsTotal ? guestsTotal.count : 0,
      },
      {
        title: t('Donation Income'),
        number: donationsTotal.amount,
        format: v => (v ? `$${ v.toLocaleString() }` : 0),
      },
    ];

    const creationCards = [
      {
        headerTitle: t('Events'),
        title: t('Create event'),
        details: t(
          'Juven supports events of all types, including complex scheduling and ticketing.',
        ),
        icon: eventsIcon,
        imgSrc: eventsImage,
        buttonText: t('Create event'),
        detailsUrl: `/${ organization.id }/events/new`,
      },
      {
        headerTitle: t('Forms and Surveys'),
        title: t('Create survey or form'),
        details: t(
          'Design a form to collect feedback or survey your community.',
        ),
        icon: formsIcon,
        imgSrc: formsImage,
        buttonText: t('Create form'),
        detailsUrl: `/${ organization.id }/forms/new`,
      },
      {
        headerTitle: t('Emails'),
        title: t('Build your own email'),
        details: t(
          'Use simple drag-and-drop tools to design an email marketing template.',
        ),
        icon: emailsIcon,
        imgSrc: emailsImage,
        buttonText: t('Create email template'),
        detailsUrl: `/${ organization.id }/emails/new`,
      },
    ];

    return (
      <ManagerContainer>
        <WelcomeCard />
        <CreationCardWrapper>
          { creationCards.map((props, i) => <CreationCard key={ i } { ...props } />) }
        </CreationCardWrapper>
        <NumbersCard title={ t('Overall Statistics') } items={ summaryItems } />
        <ActivityListCard
          title={ t('Recent Membership Sales') }
          detailsUrl={ `/${ organization.id }/membership` }
          rows={ members.filter(m => !m.manual) }
          emptyText={ t('There are no recent membership sales.') }
          rowImage={ r => (r.profile_image_url ? (
            <img alt='' src={ r.profile_image_url } />
          ) : (
            `${ r.first_name[0] }${ r.last_name ? r.last_name[0] : '' }`
          ))
          }
          rowContent={ r => (
            <p>
              <strong>{ r.name }</strong>
              { ' ' }
              { t('Has Purchased a') }
              { ' ' }
              <strong>{ r.plan_name }</strong>
              { ` ${ t('Membership') }.` }
            </p>
          ) }
          rowTimestamp={ r => moment
            .utc(r.created_at)
            .local()
            .format('YYYY MMM DD HH:mm')
          }
        />
        <ActivityListCard
          title={ t('Recent Ticket Sales') }
          detailsUrl={ `/${ organization.id }/home/income` }
          rows={ ticketingGuests.filter(g => !!g.purchased_at) }
          emptyText={ t('There are no recent ticket sales.') }
          rowImage={ r => (r.profile_image_url ? (
            <img alt='' src={ r.profile_image_url } />
          ) : (
            `${ r.first_name[0] }${ r.last_name ? r.last_name[0] : '' }`
          ))
          }
          rowContent={ r => (
            <p>
              <strong>{ r.name }</strong>
              { ' ' }
              { t('Has Purchased') }
              { ' ' }
              <strong>{ r.ticket_name }</strong>
              { ' ' }
              { t('Ticket to') }
              { ' ' }
              { r.event_name }
.
            </p>
          ) }
          rowTimestamp={ r => moment
            .utc(r.purchased_at)
            .local()
            .format('YYYY MMM DD HH:mm')
          }
        />
        <ActivityListCard
          title={ t('Recent Donations') }
          detailsUrl={ `/${ organization.id }/fundraising` }
          rows={ donations }
          emptyText={ t('There are no recent donations.') }
          rowImage={ r => (r.image_url ? (
            <img alt='' src={ r.image_url } />
          ) : (
            `${ r.first_name[0] }${ r.last_name ? r.last_name[0] : '' }`
          ))
          }
          rowContent={ r => (
            <p>
              <strong>{ r.name }</strong>
              { ' ' }
              { t('Has Donated') }
              { ' ' }
              <strong>
$
                { (r.amount || 0).toLocaleString() }
              </strong>
              { ' ' }
              { t('to') }
              { ' ' }
              { r.source_name || organization.name }
.
            </p>
          ) }
          rowTimestamp={ r => moment
            .utc(r.created_at)
            .local()
            .format('YYYY MMM DD HH:mm')
          }
        />
      </ManagerContainer>
    );
  }
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(DashboardHomeView));
