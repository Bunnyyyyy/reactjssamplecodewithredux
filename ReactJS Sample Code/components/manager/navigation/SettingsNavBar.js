import React from 'react';
import NavBar from '../../common/NavBar';

import i18n from '../../../../i18n';

const t = a => i18n.t([`components/manager:${ a }`]);

const SettingsNavBar = ({ organization, inline }) => {
  const navLinks = [
    {
      title: t('Organization'),
      route: `/${ organization.id }/settings`,
      exact: true,
    },
    {
      title: t('Info Pages'),
      route: `/${ organization.id }/settings/info-pages`,
    },
    {
      title: t('Billing'),
      route: `/${ organization.id }/settings/billing`,
    },
    {
      title: t('Receive Payments'),
      route: `/${ organization.id }/settings/payments`,
    },
    {
      title: t('Admins'),
      route: `/${ organization.id }/settings/admins`,
    },
  ];

  return <NavBar inline={ inline } links={ navLinks } />;
};

export default SettingsNavBar;
