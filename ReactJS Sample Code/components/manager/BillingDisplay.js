import React, { Component } from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router';
import styled from 'styled-components';
import moment from 'moment';

import Tooltip from '../common/Tooltip';
import Button from '../common/Button';

import { userSelect } from '../../utils/styles';

import i18n from '../../../i18n';

const t = a => i18n.t([`components/manager:${ a }`]);

const Wrapper = styled.div`
  display: flex;
  align-items: center;
  flex-shrink: 0;
  height: 56px;
  padding: 0 20px;
  border-left: 1px solid ${ props => props.theme.colors.lightGray };
`;

const ContactCount = styled.div`
  display: flex;
  align-items: center;
  line-height: 30px;
  margin-right: 20px;

  & i {
    margin-right: 10px;
  }
`;

const TrialPopup = styled(Tooltip)`
  background-color: ${ props => (props.custom ? props.theme.colors.mainBlue : props.theme.colors.mainGreen) };
  color: white;

  ${ userSelect('none') };
`;

const ContactCountNumber = styled.div`
  position: relative;
  text-align: center;
  ${ userSelect('none') } h6 {
    font-size: 12px;
    font-weight: 300;
  }

  p {
    color: ${ (props) => {
    if (props.isTrial) {
      return props.theme.colors.mainGreen;
    } if (props.limitOver) {
      return props.theme.colors.mainRed;
    }
    return props.theme.colors.darkGray;
  } };
  }

  &:hover ${ TrialPopup } {
    display: block;
  }
`;

const ContactCountWrapper = styled.div`
  display: flex;
  align-items: center;
`;

const AdditionalLimitP = styled.p`
  margin-left: 5px;

  ${ (props) => {
    if (props.custom) {
      return `color: ${ props.theme.colors.mainBlue } !important;`;
    } if (props.trial) {
      return `color: ${ props.theme.colors.mainGreen } !important;`;
    }
    return '';
  } };

  &:hover ~ ${ Tooltip } {
    visibility: visible;
  }
`;

const ContactDivider = styled.div`
  width: 1px;
  height: 30px;
  background-color: ${ props => props.theme.colors.lightGray };
  margin-left: 15px;
  margin-right: 15px;
`;

const mapStateToProps = state => ({
  organization: state.manager.organization,
  contactCounts: state.organization.contactCounts,
});

class BillingDisplay extends Component {
  toggleUpdatingPlan () {
    const { organization, history } = this.props;
    return history.push(`/${ organization.id }/settings/billing`);
  }

  render () {
    const { organization, contactCounts } = this.props;
    if (!organization) return null;

    const format = v => (v !== undefined && v !== null ? v.toLocaleString() : '');

    const isTrial = organization.trial_end_at
      && moment.utc(organization.trial_end_at).isAfter();

    let AdditionalLimitDisplay = null;
    if (organization.custom_contact_limit) {
      AdditionalLimitDisplay = [
        <AdditionalLimitP custom>
          { `+ ${ format(organization.custom_contact_limit) }` }
        </AdditionalLimitP>,
        <TrialPopup custom>Custom</TrialPopup>,
      ];
    } else if (isTrial) {
      AdditionalLimitDisplay = [
        <AdditionalLimitP trial>+ 400</AdditionalLimitP>,
        <TrialPopup>Trial</TrialPopup>,
      ];
    }

    return (
      <Wrapper>
        <ContactCount>
          <ContactCountNumber
            limitOver={ contactCounts.total > organization.bill_contact_count }
          >
            <h6>Used</h6>
            <p>{ format(contactCounts.total) }</p>
          </ContactCountNumber>
          <ContactDivider />
          <ContactCountNumber isTrial={ isTrial }>
            <h6>Limit</h6>
            <ContactCountWrapper>
              <p>{ format(organization.bill_contact_count) }</p>
              { AdditionalLimitDisplay }
            </ContactCountWrapper>
          </ContactCountNumber>
        </ContactCount>
        { !organization.custom_plan ? (
          <Button small light onClick={ this.toggleUpdatingPlan.bind(this) }>
            { t('Upgrade') }
          </Button>
        ) : null }
      </Wrapper>
    );
  }
}

export default withRouter(connect(mapStateToProps)(BillingDisplay));
