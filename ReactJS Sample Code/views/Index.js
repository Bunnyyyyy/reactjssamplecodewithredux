import React from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router';
import { Redirect } from 'react-router-dom';

const mapStateToProps = state => ({
  user: state.me.user,
  organizations: state.manager.organizations,
});

const IndexView = ({ user, organizations }) => {
  let redirectUrl;
  if (organizations.length) {
    const targetOrganization = user.last_managed_organization
      ? organizations.find(o => o.id === user.last_managed_organization)
        || organizations[0]
      : organizations[0];
    redirectUrl = `/${ targetOrganization.id }`;
  } else if (user.id) {
    redirectUrl = user.verified ? '/new' : '/verify';
  } else {
    redirectUrl = '/login';
  }

  return <Redirect to={ redirectUrl } />;
};

export default withRouter(connect(mapStateToProps)(IndexView));
