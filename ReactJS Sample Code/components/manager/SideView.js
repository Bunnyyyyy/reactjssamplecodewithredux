import React from 'react';
import styled from 'styled-components';

import juvenWhiteLogo from '../../assets/manager/juven_white_logo.png';
import hkruWhiteLogo from '../../assets/manager/hkru_white_logo.png';
import esliteWhiteLogo from '../../assets/manager/eslite_white_logo.png';
import maximWhiteLogo from '../../assets/manager/maxim_white_logo.png';

const JuvenLogo = styled.img`
  height: 45px;
`;

const SideViewWrapper = styled.div`
  background: url('${ props => props.imgSrc }');
  background-size: cover;
  min-height: 100vh;
  padding: 70px 60px 0 60px;
  width: 33.333%;

  @media (max-width: 900px) {
    display: none;
  }
`;

const SideViewContent = styled.div`
  align-items: center;
  height: calc(100% - 115px);
  display: flex;
`;

const ContentWrapper = styled.div`
  text-align: center;
`;

const Title = styled.h3`
  color: ${ props => props.theme.colors.lightGray };
  font-size: 20px;
  font-weight: 900;
  margin: 24px 0;
  line-height: 1.5;
`;

const Description = styled.p`
  color: ${ props => props.theme.colors.lightGray };
  font-size: 15px;
  font-weight: 300;
`;

const Divider = styled.hr`
  color: ${ props => props.theme.colors.lightGray };
  height: 2px;
  margin: 36px 0;
`;

const LogoContainer = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  padding-top: 32px;
`;

const Logo = styled.img`
  align-self: center;
  width: ${ props => props.width }px;
  height: ${ props => props.height }px;
`;

const TrustedByContainer = () => {
  const logos = [
    {
      alt: 'HKRFU logo',
      src: hkruWhiteLogo,
      height: 50,
      width: 40,
    },
    {
      alt: 'eslite logo',
      src: esliteWhiteLogo,
      width: 118,
      height: 30,
    },
    {
      alt: 'Maxim\'s logo',
      src: maximWhiteLogo,
      width: 75,
      height: 30,
    },
  ];

  return [
    <Description key='description'>Trusted by</Description>,
    <LogoContainer key='logo-container'>
      { logos.map((logo, i) => (
        <Logo
          alt={ logo.alt }
          key={ `${ logo.alt }-${ i }` }
          src={ logo.src }
          width={ logo.width }
          height={ logo.height }
        />
      )) }
    </LogoContainer>,
  ];
};

const SideView = ({ description, imgSrc, title }) => (
  <SideViewWrapper imgSrc={ imgSrc }>
    <JuvenLogo alt='Juven logo' src={ juvenWhiteLogo } />
    <SideViewContent>
      <ContentWrapper>
        <Title>{ title }</Title>
        <Description>{ description }</Description>
        <Divider />
        <TrustedByContainer />
      </ContentWrapper>
    </SideViewContent>
  </SideViewWrapper>
);

export default SideView;
