import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { withRouter } from 'react-router';
import styled from 'styled-components';
import { changeToUrlable } from '../../../../utils';
import * as ManagerActionCreators from '../../../../redux/manager';
import * as OrganizationActionCreators from '../../../../redux/organization';
import * as UiActionCreators from '../../../../redux/ui';

import ManagerContainer from '../../../../components/layouts/ManagerContainer';
import ElementsBuilder from '../../../../components/manager/ElementsBuilder';
import Input from '../../../../components/common/Input';
import JuvenIdInput from '../../../../components/common/JuvenIdInput';
import Label from '../../../../components/common/inputs/Label';

import { t as translate } from '../../../../../i18n';

const t = a => translate(a, 'manager/forms/edit');

const { FRONTEND_URL } = process.env;

const LinkWrapper = styled.div`
  margin-bottom: 40px;
`;

const FormLinkP = styled.p`
  padding-bottom: 20px;
`;
const Divide = styled.div`
height: 10px;
background-color: #e1e8ed;
margin-left: -20px;
margin-right: -20px;
`;

const ELEMENTS_BUILDER_NAME = 'ELEMENTS_BUILDER_NAME';
const TYPES = [
  {
    data_type: 'string',
    required: true,
    label: t('Short Text Input'),
    validate: e => e.label && e.label.length,
  },
  {
    data_type: 'text',
    required: true,
    label: t('Long Text Input'),
    validate: e => e.label && e.label.length,
  },
  {
    data_type: 'number',
    required: true,
    label: t('Number Input'),
    validate: e => e.label && e.label.length,
  },
  {
    data_type: 'string',
    required: true,
    options: [],
    label: t('Single Choice Options'),
    validate: e => e.label && e.label.length,
  },
  {
    data_type: 'string',
    required: true,
    options: [],
    multiple_options: true,
    label: t('Multiple Choice Options'),
    validate: e => e.label && e.label.length,
  },
  {
    data_type: 'boolean',
    required: true,
    label: t('Yes / No'),
    validate: e => e.label && e.label.length,
  },
  {
    data_type: 'file',
    required: true,
    label: t('File Upload'),
    validate: e => e.label && e.label.length,
  },
  {
    display_type: 'section',
    label: t('Section'),
    validate: e => e.display_text && e.display_text.length,
  },
  {
    display_type: 'title',
    label: t('Displayed Title'),
    validate: e => e.display_text && e.display_text.length,
  },
  {
    display_type: 'text',
    label: t('Displayed Paragraph'),
    validate: e => e.display_text && e.display_text.length,
  },
  {
    display_type: 'image',
    label: t('Displayed Image'),
    validate: e => e.file || e.image_url,
  },
  {
    display_type: 'contacts',
    label: t('Contact Information'),
    hidden: true,
  },
];

const DEFAULT_ELEMENTS = [
  {
    display_type: 'contacts',
    order: 0,
  },
];

const Standard_Elements_Types = [
  {
    label: 'Gender',
    checked: false,
    required: false,
    standard_field_name: 'gender',
    data_type: 'string',
    display_type: null,

  },
  {
    label: 'Date of birth',
    checked: false,
    required: false,
    standard_field_name: 'date_of_birth',
    data_type: 'date',
    display_type: null,
  },
  {
    label: 'Country',
    checked: false,
    required: false,
    standard_field_name: 'country',
    data_type: 'string',
    display_type: null,
  },
  {
    label: 'Address',
    checked: false,
    required: false,
    standard_field_name: 'address',
    data_type: 'string',
    display_type: null,
  },
  {
    label: 'Level of education',
    checked: false,
    required: false,
    standard_field_name: 'education',
    data_type: 'string',
    display_type: null,
  },
  {
    label: 'Occupation',
    checked: false,
    required: false,
    standard_field_name: 'occupation',
    data_type: 'string',
    display_type: null,
  },
  {
    label: 'Office address',
    checked: false,
    required: false,
    standard_field_name: 'company_address',
    data_type: 'string',
    display_type: null,
  },
  {
    label: 'Company name',
    checked: false,
    required: false,
    standard_field_name: 'company_name',
    data_type: 'string',
    display_type: null,
  },
  {
    label: 'Industry',
    checked: false,
    required: false,
    standard_field_name: 'industry',
    data_type: 'string',
    display_type: null,
  },
];
const mapStateToProps = state => ({
  organization: state.manager.organization,
  form: state.organization.form,
});
const NEW_FORM_ELEMENTS = [];
const ActionCreators = Object.assign(
  {},
  ManagerActionCreators,
  OrganizationActionCreators,
  UiActionCreators,
);
const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators(ActionCreators, dispatch),
});

class FormEditView extends Component {
  constructor () {
    super();

    this.state = {
      title: '',
      icered_id: '',
      iceredIdAvailable: null,
    };
  }

  async componentDidMount () {
    const { actions, match } = this.props;
    const { oId, foId } = match.params;

    actions.setLoading(true);

    try {
      if (foId) {
        await actions.getOrganizationForm(oId, foId);

        const { form: { title } } = this.props;
        this.setState({ title });
      } else {
        actions.setOrganizationForm({});
        actions.addSaveAction(null, ELEMENTS_BUILDER_NAME);
      }
    } catch (e) {
      console.error('ERROR: FormsEditView', e);
    } finally {
      actions.setLoading(false);
    }
  }

  componentWillUnmount () {
    const { actions } = this.props;
    actions.resetSaveActions();
  }

  async onFinishSaving () {
    try {
      const {
        actions, history, match: { params: { foId } }, organization, form,
      } = this.props;
      await actions.getOrganizationForm(organization.id, form.id);
      if (!foId) {
        return history.push(`/${ organization.id }/forms/${ form.id }`);
      }
    } catch (e) {
      console.error(e);
    }
  }

  onCancelSaving () {
    const { history, organization, form } = this.props;

    if (form.id) {
      this.setState({
        title: form.title,
      });
    } else {
      history.push(`/${ organization.id }/forms`);
    }
  }

  async createFormAction (data) {
    try {
      const { actions, organization } = this.props;
      const oId = organization.id;

      const form = await actions.createOrganizationForm(oId, data);
      await actions.getOrganizationForm(oId, form.id);

      return;
    } catch (e) {
      throw e;
    }
  }

  changeTitle (field, value) {
    const { changeLinkUrl } = this.state;
    this.setState({ title: value }, this.updateTitle);
    if (!changeLinkUrl && value) {
      this.setState({ icered_id: changeToUrlable(value) });
      this.setState({
        iceredIdAvailable: null,
      });
      const newIceredId = value;
      if (!newIceredId.length) {
        this.setState({
          iceredIdAvailable: false,
        });
        return;
      }
      this.checkIceredId(newIceredId);
    }
  }

  updateTitle () {
    const { actions, organization, form } = this.props;
    const { title, icered_id, iceredIdAvailable } = this.state;

    const isInvalid = !title || !title.length;

    if (isInvalid || (!iceredIdAvailable && !form.id)) {
      actions.addSaveAction(null, ELEMENTS_BUILDER_NAME);
    } else {
      const action = form.id
        ? actions.updateOrganizationForm.bind(this, organization.id, form.id, {
          title,
        })
        : this.createFormAction.bind(this, { title, icered_id });

      actions.addSaveAction(action, ELEMENTS_BUILDER_NAME, null, 0);
    }
  }

  createElementAction (element) {
    const { actions, organization, form } = this.props;

    return actions.createOrganizationFormDataField(
      organization.id,
      form.id,
      element,
    );
  }

  updateElementAction (eleId, element) {
    const { actions, organization, form } = this.props;

    return actions.updateOrganizationFormDataField(
      organization.id,
      form.id,
      eleId,
      element,
    );
  }

  removeElementAction (eleId) {
    const { actions, organization, form } = this.props;

    return actions.removeOrganizationFormDataField(
      organization.id,
      form.id,
      eleId,
    );
  }

  async changeIceredId (field, value) {
    await this.changeInput(field, value);
    this.setState({
      iceredIdAvailable: null,
      changeLinkUrl: true,
    });
    const newIceredId = value;
    if (!newIceredId.length) {
      this.setState({
        iceredIdAvailable: false,
      });
      return;
    }
    this.checkIceredId(newIceredId);
  }

  changeInput (field, value) {
    return new Promise((resolve) => {
      this.setState(
        { [field]: value },
        resolve,
      );
    });
  }

  checkIceredId (id) {
    const { actions } = this.props;
    clearTimeout(this.iceredIdTimer);
    this.iceredIdTimer = setTimeout(() => {
      actions
        .checkIceredId(id)
        .then((isAvailable) => {
          this.setState({
            iceredIdAvailable: isAvailable,
          });
          this.updateTitle();
        })
        .catch(console.error);
    }, 300);
  }

  render () {
    const { match: { params: { foId } }, organization, form } = this.props;
    const { title, icered_id, iceredIdAvailable } = this.state;
    if (!form) return null;
    const standardFields = [];
    const customFields = [];
    if (form.fields) {
      for (let i = 0; i < form.fields.length; i++) {
        if (form.fields[i].standard_field_name || form.fields[i].display_type === 'contacts') {
          standardFields.push(form.fields[i]);
        } else {
          customFields.push(form.fields[i]);
        }
      }
    }
    const formUrl = form.id ? `${ FRONTEND_URL }/${ organization.id }/forms/${ form.id }` : null;
    const LeftComponents = [
      <Input
        field='title'
        label={ t('Title') }
        value={ title }
        onChange={ this.changeTitle.bind(this) }
        required
        invalid={ !!form.id && (!title || !title.length) }
      />,
      form.id ? (
        <LinkWrapper>
          <Label label={ t('Form URL') } customCss='opacity: 1;' />

          <FormLinkP>
            <a href={ formUrl } target='_blank' rel='noopener noreferrer'>
              { formUrl }
            </a>
          </FormLinkP>
        </LinkWrapper>
      ) : (
        <JuvenIdInput
          field='icered_id'
          label={ t('Form URL') }
          subLabel={ t('Cannot be changed after the form is created') }
          displayedUrl={ `${ FRONTEND_URL }/${ organization.id }/forms/` }
          value={ icered_id }
          error={ iceredIdAvailable === false ? true : null }
          success={ iceredIdAvailable ? true : null }
          onChange={ this.changeIceredId.bind(this) }
          required
          css='margin-bottom: 40px;'
          innerCss='flex-wrap: wrap;'
        />
      ),
      <Divide />,
    ];

    return (
      <ManagerContainer
        noSubheader={ !foId }
        contentCss='padding: 0;overflow: hidden;'
      >
        <ElementsBuilder
          organization={ organization }
          newObject={ !foId }
          LeftComponents={ LeftComponents }
          elementTypes={ TYPES }
          elements={ form.id ? customFields : NEW_FORM_ELEMENTS }
          standardElements={ form.id ? standardFields : DEFAULT_ELEMENTS }
          Standard_Elements_Types={ Standard_Elements_Types }
          createElementAction={ this.createElementAction.bind(this) }
          updateElementAction={ this.updateElementAction.bind(this) }
          removeElementAction={ this.removeElementAction.bind(this) }
          onFinish={ this.onFinishSaving.bind(this) }
          onCancel={ this.onCancelSaving.bind(this) }
          emptyText={ `${ t(
            'There are no Elements in this Form yet.\nYou may start adding elements by selecting one of the Element Types on the left and drag it here.',
          ) }` }
          saveActionsCss='
          position: absolute;
            z-index: 799;
            width: 100%;
            top: 0;
            right: 0;
            box-shadow: 0 0 6px 0 rgba(0, 0, 0, 0.15);
          '
        />
      </ManagerContainer>
    );
  }
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(FormEditView));
