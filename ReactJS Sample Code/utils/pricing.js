export const calculateBillAmount = (brackets, count = 0) => {
  let amount = 0;

  brackets.forEach(b => {
    const countInBracket = b.to
      ? Math.min(b.to, count) - b.from
      : count - b.from;

    if (countInBracket <= 0) return;

    const unitsInBracket = Math.ceil(countInBracket / b.unit_size);
    amount += unitsInBracket * b.amount_per_unit;
  });

  return amount;
};

export const humanizeCost = cost => {
  if (cost < 0) return 'Custom';
  if (cost === 0) return 'Free';

  return Math.floor(cost).toLocaleString();
};
