import React from 'react';
import { Transition } from 'react-transition-group';
import styled from 'styled-components';

const LoadingDivStyled = styled.div`
  position: absolute;
  width: 100%;
  height: 100%;
  left: 0;
  top: 0;
  background-color: rgba(0, 0, 0, 0.5);
  min-height: ${ props => (props.fullScreen ? '100vh' : 'auto') };

  i {
    position: absolute;
    left: 50%;
    top: 50%;
    width: 72px;
    height: 72px;
    margin: -36px 0 0 -36px;
    font-size: 72px;
    color: white;
  }

  @media (max-width: 900px) {
    position: fixed !important;
  }
`;

const Loading = ({
  isLoading,
  fullScreen,
  mobileOnly,
  style = {},
  iStyle = {}
}) => {
  const LoadingDiv = (
    <LoadingDivStyled
      style={ style }
      fullScreen={ fullScreen }
      className={ mobileOnly ? '_mobile' : '' }
    >
      <i className='fas fa-spinner fa-pulse fa-3x fa-fw' style={ iStyle } />
    </LoadingDivStyled>
  );

  const defaultStyle = {
    transition: `opacity 300ms ease`,
    position: fullScreen ? 'fixed' : 'absolute',
    display: 'none',
    width: '100%',
    height: '100%',
    left: 0,
    top: 0,
    zIndex: 1000,
    opacity: 0
  };
  if (fullScreen) defaultStyle.minHeight = '100vh';

  const transitionStyles = {
    entering: { display: 'block', opacity: 0 },
    entered: { display: 'block', opacity: 1 }
  };

  return (
    <Transition in={ isLoading } timeout={ 0 }>
      { state => (
        <div
          style={ {
            ...defaultStyle,
            ...transitionStyles[state],
            ...style
          } }
        >
          { LoadingDiv }
        </div>
      ) }
    </Transition>
  );
};

export default Loading;
