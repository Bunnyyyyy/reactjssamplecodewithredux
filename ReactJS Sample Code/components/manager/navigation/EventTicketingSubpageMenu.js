import React, { Component } from 'react';
import { connect } from 'react-redux';
import styled from 'styled-components';
import SubpageMenu from './../../manager/SubpageMenu';

import i18n from '.././../../../i18n';
const t = a => i18n.t([`components/manager:${ a }`]);

const EventTicketingSubpageMenu = ({ organization, event }) => {
  const baseUrl = `/${ organization.id }/events/${ event.id }/ticketing`;
  const pages = [
    {
      title: 'Ticket Types',
      route: `${ baseUrl }/types`
    },
    {
      title: 'Ticket Packages',
      route: `${ baseUrl }/packages`
    },
    {
      title: 'Promo Codes',
      route: `${ baseUrl }/promo-codes`
    },
    {
      title: 'Guest Data Collection',
      route: `${ baseUrl }/data-collection`
    },
    {
      title: 'Additional Information',
      route: `${ baseUrl }/additional-info`
    },
    {
      title: 'Customized Label',
      route: `${ baseUrl }/label`
    }
  ];

  return <SubpageMenu pages={ pages } />;
};

const mapStateToProps = state => ({
  organization: state.manager.organization,
  event: state.manager.event
});

export default connect(mapStateToProps)(EventTicketingSubpageMenu);
