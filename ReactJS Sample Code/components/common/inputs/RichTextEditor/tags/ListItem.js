import React from 'react';
import styled from 'styled-components';

const Wrapper = styled.li`
  font-size: inherit;
`;

function ListItem ({ children, attributes }) {
  return (
    <Wrapper { ...attributes }>
      { children }
    </Wrapper>
  );
}

export default ListItem;
