import React from 'react';
import styled from 'styled-components';

import { A } from '../constants';

const Icon = styled.i`
  display: inline-block;
  cursor: pointer;
  text-align: center;
  height: 44px;
  width: 40px;
  font-size: 14px;
  line-height: 44px;
  transform: rotate(90deg);
  color: ${ props => (
    props.selected ? props.theme.colors.mainBlue : props.theme.colors.darkGray
  ) };

  &:hover {
    color: ${ props => props.theme.colors.mainBlue };
  }
`;

function Link ({ onClick, selected }) {
  return (
    <Icon
      onClick={ onClick }
      className='fas fa-link'
      selected={ selected(A) }
    />
  );
}

export default Link;
