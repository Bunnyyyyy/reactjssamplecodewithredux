import React from 'react';
import styled from 'styled-components';

const Wrapper = styled.em`
  font-style: italic;
`;

function Italic ({ children, attributes }) {
  return (
    <Wrapper { ...attributes }>
      { children }
    </Wrapper>
  );
}

export default Italic;
