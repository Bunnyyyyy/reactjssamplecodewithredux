import styled from 'styled-components';

export default styled.div`
  cursor: pointer;
  width: 50%;
  background-color: white;
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  line-height: 32px;
  padding: 0 8px;
  height: 32px;
  border: solid
    ${ ({ theme, selected }) => (selected
    ? `2px ${ theme.colors.mainBlue }`
    : `1px ${ theme.colors.mainGray }`) };

  &:first-child {
    border-radius: 5px 0 0 5px;
  }
  &:last-child {
    border-radius: 0 5px 5px 0;
  }

  &:hover,
  &:active {
    border-color: rgba(0, 167, 255);
    box-shadow: none;
  }

  & > h6 {
    text-align: center;
    font-weight: 500;
    color: ${ ({ theme }) => theme.colors.darkGray };
  }

  & > div {
    height: 32px;
    color: ${ props => props.theme.colors.darkGray };
    text-align: center;

    & i {
      font-size: 30px;
      line-height: 32px;
    }
  }

  ${ props => (props.selected
    ? `
      box-shadow: none;
    `
    : '') };

  ${ props => props.customCss || '' };
`;
