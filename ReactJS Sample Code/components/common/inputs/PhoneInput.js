import React, { Component } from 'react';
import styled from 'styled-components';
import { callingCountries } from 'country-data';
import { components } from 'react-select';
import { userSelect } from '../../../utils/styles';
import Tooltip from '../Tooltip';

import Select from './Select';
import Input from '../Input';
import InvalidMessage from '../InvalidMessage';
import Label from './Label';

const { Option, SingleValue } = components;

const Div = styled.div`
  position: relative;
  display: inline-block;
  width: ${ props => (props.fullWidth ? '100%' : '390px') };
  margin-top: 20px;
  margin-bottom: 20px;
  margin-right: 20px;
  ${ props => props.customCss || '' };
`;

const InputWrapper = styled.div`
  position: relative;
  display: flex;
`;

const Message = styled.p`
  display: inline-block;
  color: ${ props => props.theme.colors.darkGray };
  line-height: 32px;
  margin-right: 5px;

  ${ userSelect('none') };
`;

const ErrorSuccessIcon = styled.i`
  position: absolute;
  bottom: 5px;
  height: 32px;
  font-size: 20px;
  line-height: 32px !important;
  position: relative;
  display: inline-block;
  color: ${ props => (props.error ? '#fa6363' : '#47c166') };

  &:hover ~ ${ Tooltip } {
    visibility: visible;
  }
`;

const COUNTRY_CODE_OPTIONS = [];
callingCountries.all.forEach((c) => {
  const value = c.countryCallingCodes[0].slice(1);
  const existingOption = COUNTRY_CODE_OPTIONS.find(o => o.value === value);

  if (existingOption) {
    existingOption.label = existingOption.label.replace(')', ` / ${ c.name })`);
  } else {
    COUNTRY_CODE_OPTIONS.push({
      label: `${ c.countryCallingCodes[0] } (${ c.name })`,
      value: c.countryCallingCodes[0].slice(1),
    });
  }
});

const COUNTRY_CODES = callingCountries.all.map(c => c.countryCallingCodes[0].slice(1));

export default class InputPhone extends Component {
  async onKeyDownPhone (e) {
    const { value } = this.props;
    // when pressing Delete key
    if (e.keyCode === 8 && value) {
      const countryCallingCode = COUNTRY_CODES.slice()
        .sort((c1, c2) => c2.length - c1.length)
        .find(c => value.indexOf(c) === 1);

      if (countryCallingCode) {
        const phoneValue = value.replace(`+${ countryCallingCode }`, '');
        // when the phone value is empty
        if (!phoneValue.length) {
          e.preventDefault();
          await this.changeCallingCode(null);
        }
      }
    }
  }

  async changeCallingCode (callingCode) {
    try {
      const { onChange, field } = this.props;
      let { value } = this.props;
      value = value || '';

      const oldCallingCode = COUNTRY_CODES.slice()
        .sort((c1, c2) => c2.length - c1.length)
        .find(c => value.indexOf(c) === 1);

      const phoneValue = oldCallingCode
        ? value.replace(`+${ oldCallingCode }`, '')
        : value;
      const newValue = callingCode ? `+${ callingCode }${ phoneValue }` : null;

      if (onChange) await onChange(field, newValue);
    } catch (e) {
      console.error(e);
    }
  }

  async changePhone (phone) {
    try {
      const { onChange, field, value } = this.props;

      let countryCallingCode;
      if (value) {
        countryCallingCode = COUNTRY_CODES.slice()
          .sort((c1, c2) => c2.length - c1.length)
          .find(c => value.indexOf(c) === 1);
      }

      phone = phone || '';
      const newValue = countryCallingCode
        ? `+${ countryCallingCode }${ phone.replace(/\D/g, '') || '' }`
        : phone.replace(/\D/g, '') || '';

      if (onChange) await onChange(field, newValue);
    } catch (e) {
      console.error(e);
    }
  }

  render () {
    const {
      field,
      success,
      error,
      customCss,
      errorMessage,
      errorCss,
      label,
      value,
      divStyle,
      required,
      onBlur,
    } = this.props;

    let countryCallingCode;
    let phoneValue;
    if (value) {
      countryCallingCode = COUNTRY_CODES.slice()
        .sort((c1, c2) => c2.length - c1.length)
        .find(c => value.indexOf(c) === 1);

      if (countryCallingCode) {
        phoneValue = value.replace(`+${ countryCallingCode }`, '');
      }
    }

    return (
      <Div customCss={ customCss } style={ divStyle }>
        { label ? (
          <Label field={ field } label={ label } required={ required } />
        ) : null }
        <InputWrapper>
          <Select
            plain
            customCss={ `
              min-width: auto;
              flex: 0 0 100px;
              padding-right: 10px;

              margin-top: 0;
              margin-bottom: 0;

              & .select {
                &__placeholder,
                &__single-value,
                &__option {
                  font-size: 12px;
                  font-weight: 400;
                }

                &__dropdown-indicator {
                  padding: 0;
                }
              }
            ` }
            clearable={ false }
            required={ required }
            placeholder='+...'
            field={ field }
            value={ countryCallingCode || null }
            options={ COUNTRY_CODE_OPTIONS }
            components={ {
              Option: props => <Option { ...props }>{ props.data.label }</Option>,
              SingleValue: props => (
                <SingleValue { ...props }>
                  { props.data.value }
                </SingleValue>
              ),
            } }
            onChange={ v => this.changeCallingCode(v) }
            onBlur={ onBlur }
          />
          <Input
            fullWidth
            customCss='
              flex: 0 0 calc(100% - 100px);
              margin-top: 0;
              margin-bottom: 0;
            '
            value={ phoneValue || null }
            onChange={ (_, v) => this.changePhone(v) }
            onKeyDown={ this.onKeyDownPhone.bind(this) }
            disabled={ !countryCallingCode }
            errorMessage={ errorMessage ? ' ' : null }
            onBlur={ onBlur }
          />
        </InputWrapper>
        { errorMessage ? (
          <InvalidMessage customCss={ errorCss }>{ errorMessage }</InvalidMessage>
        ) : null }
        { error || success
          ? [
            <Message>{ error || success }</Message>,
            <ErrorSuccessIcon
              error={ error }
              className={
                error ? 'fas fa-times-circle' : 'fas fa-check-circle'
              }
              aria-hidden='true'
            />,
          ]
          : null }
      </Div>
    );
  }
}
