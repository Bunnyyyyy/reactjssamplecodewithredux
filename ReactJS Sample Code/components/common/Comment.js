import React from 'react';
import styled from 'styled-components';
import UserAvatar from './UserAvatar';
import moment from 'moment';

const Container = styled.div`
  display: flex;
  flex-direction: row;
  margin: 10px 20px;
`;

const CommentText = styled.div`
  background-color: ${ props => props.theme.colors.lightGray };
  padding: 12px 18px;
  margin-left: 20px;
  font-size: 12px;
  line-height: 18px;
  border-radius: 2px;
  width: auto;
`;

const CommentInfo = styled.div`
  padding-bottom: 12px;
  display: flex;
  justify-content: flex-start;
  > .createdBy {
    font-size: 12px;
    font-weight: bold;
  }
  > .createdAt {
    color: #7d909a;
    margin-left: auto;
    padding-left: 20px;
  }
`;

export default ({ first_name, last_name, comment, created_at }) => (
  <Container>
    <UserAvatar size={ 45 } user={ { first_name, last_name } } />
    <CommentText>
      <CommentInfo>
        <div className='createdBy'>{ `${ first_name } ${ last_name }` }</div>
        <div className='createdAt'>
          { created_at
            ? moment(created_at).from(moment())
            : moment()
              .subtract(45, 'minutes')
              .from(moment()) }
        </div>
      </CommentInfo>
      { comment }
    </CommentText>
  </Container>
);
