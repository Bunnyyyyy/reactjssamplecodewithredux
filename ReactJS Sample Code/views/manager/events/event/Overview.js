import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { withRouter } from 'react-router';
import styled from 'styled-components';

import { formatDateTime } from '../../../../utils';
import * as UiActionCreators from '../../../../redux/ui';
import * as ManagerActionCreators from '../../../../redux/manager';
import * as EventActionCreators from '../../../../redux/event';
import * as ProfileActionCreators from '../../../../redux/profile';

import currencies from '../../../../utils/currencies';

import ManagerContainer from '../../../../components/layouts/ManagerContainer';
import ShareCard from '../../../../components/manager/cards/ShareCard';
import NumbersCard from '../../../../components/manager/cards/NumbersCard';
import TabsCard from '../../../../components/manager/cards/TabsCard';
import MiniTable from '../../../../components/common/MiniTable';
import PieChart from '../../../../components/manager/charts/PieChart';
import DateGuestsChart from '../../../../components/manager/charts/DateGuestsChart';
import TicketSalesChart from '../../../../components/manager/charts/TicketSalesChart';
import DownloadApp from '../../../../components/manager/cards/DownloadApp';
import ticketIcon from '../../../../assets/manager/icon_ticket.svg';
import pieIcon from '../../../../assets/manager/icon_pie.svg';
import tableIcon from '../../../../assets/manager/icon_table.svg';
import pieBlueIcon from '../../../../assets/manager/icon_pie_blue.svg';
import tableBlueIcon from '../../../../assets/manager/icon_table_blue.svg';
import { t as translate } from '../../../../../i18n';

const t = a => translate(a, 'manager/events/overview');

const ContentWrapper = styled.div`
  text-align: center;
  padding: 60px 0;

  p {
    padding-top: 15px;
  }
`;

const CardImage = styled.img`
  width: 140px;
  height: 80px;
`;

const EmptyCard = ({ src, label }) => (
  <ContentWrapper>
    <CardImage src={ src } />
    <p>{ label }</p>
  </ContentWrapper>
);


const mapStateToProps = state => ({
  organization: state.manager.organization,
  event: state.manager.event,
  guestCounts: state.event.guestCounts,
  ticketSales: state.event.ticketSales,
  tickets: state.event.tickets,
});

const ActionCreators = Object.assign(
  {},
  UiActionCreators,
  ManagerActionCreators,
  EventActionCreators,
  ProfileActionCreators,
);
const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators(ActionCreators, dispatch),
});

class EventOverviewView extends Component {
  async componentDidMount () {
    const { actions, match } = this.props;
    const { eId } = match.params;

    actions.setLoading(true);

    try {
      const promises = [
        actions.getEventGuestCounts(eId),
        actions.getEventTicketSales(eId),
        actions.getEventTickets(eId),
      ];
      await Promise.all(promises);
    } catch (e) {
      console.error('ERROR: EventOverviewView', e);
    } finally {
      actions.setLoading(false);
    }
  }

  render () {
    const {
      event,
      // guests,
      guestCounts,
      ticketSales,
      tickets,
    } = this.props;

    const currency = currencies[event.currency || 'hkd'];

    const performanceItems = [
      {
        title: t('Total sales'),
        number: ticketSales.total,
        format: v => (v ? `${ currency.symbol }${ v.toLocaleString() }` : ''),
      },
      {
        title: t('Total no. of guests'),
        number: guestCounts.total,
        format: v => (v ? `${ v.toLocaleString() }` : ''),
      },
      {
        title: t('Total no. of check-ins'),
        number:
          guestCounts.total > 0
            ? `${ guestCounts.checkedInCount } (${ (guestCounts.checkedInCount / guestCounts.total * 100).toFixed(1) }%)`
            : 0,
      },
      {
        type: 'custom',
        renderComponent: () => (
          <DownloadApp />
        ),
      },
    ];

    const TicketTypeDetails = () => {
      const columns = [
        {
          heading: t('Ticket Type'),
          field: 'name',
          flex: '2 0 0',
        },
        {
          heading: t('Price'),
          field: 'price',
          format: value => (value !== null ? `${ currency.symbol }${ value.toLocaleString() }` : ''),
        },
        {
          heading: t('Sold'),
          field: 'quantity_sold',
          format: (value, { quantity }) => (
            (value !== null && quantity !== null)
              ? `${ value.toLocaleString() } (${ (value / quantity * 100).toFixed(1) }%)`
              : value.toLocaleString() : '0'
          ),
        },
        {
          heading: t('Qty'),
          field: 'quantity',
          format: value => (value !== null ? value.toLocaleString() : t('Unlimited')),
        },
        {
          heading: t('Selling Start Date'),
          field: 'start_selling_at',
          format: value => (value ? formatDateTime(value) : '-'),
        },
        {
          heading: t('Selling End Date'),
          field: 'end_selling_at',
          format: value => (value ? formatDateTime(value) : '-'),
        },
      ];

      return (
        <MiniTable
          title={ t('Ticket Type Details') }
          columns={ columns }
          rows={ tickets }
        />
      );
    };

    const TicketTypeCountsChart = () => {
      const { ticket_types = [] } = guestCounts;

      const data = ticket_types
        ? ticket_types.slice(0, 10).map(type => ({
          label: type.name,
          value: type.count,
        }))
        : [];
      const totalQuantity = ticket_types.reduce(
        (pv, type) => pv + type.count,
        0,
      );
      return data.length === 0 || totalQuantity === 0 ? (
        <EmptyCard
          src={ ticketIcon }
          label={ t('No ticket sales yet') }
        />
      ) : (
        <PieChart data={ data } />
      );
    };

    const typeBreakdownTabs = [
      {
        iconUrl: pieIcon,
        iconBlueUrl: pieBlueIcon,
        render: <TicketTypeCountsChart />,
      },
      {
        iconUrl: tableIcon,
        iconBlueUrl: tableBlueIcon,
        render: <TicketTypeDetails />,
      },
    ];

    const freeEvent = typeof tickets.find(value => value.price !== 0)
  === 'undefined';

    const salesTabs = [
      {
        label: t('Ticket sales over time'),
        render: <TicketSalesChart event={ event } salesData={ ticketSales } isFreeEvent={ freeEvent } />,
      },
      {
        label: t('Guests per event date'),
        render: <DateGuestsChart event={ event } data={ guestCounts.dates } />,
      },
    ];

    return (
      <ManagerContainer>
        <ShareCard title={ t('Share event') } event={ event } />
        <NumbersCard
          title={ t('Overall performance') }
          items={ performanceItems }
        />
        <TabsCard
          useTabIcons
          title={ t('Ticket type breakdown') }
          tabs={ typeBreakdownTabs }
          customCss='padding-bottom: 5px;'
        />
        { /*
        <ActivityListCard
          title={ t('Recent Updates') }
          detailsUrl={ `/${ organization.id }/events/${
            event.id
          }/guests` }
          rows={ guests.filter(g => !!g.purchased_at).slice(0, 6) }
          emptyText={ t('There are no recent updates.') }
          rowImage={ r =>
            r.profile_image_url ? (
              <img src={ r.profile_image_url } />
            ) : (
              `${ r.first_name[0] }${ r.last_name ? r.last_name[0] : '' }`
            )
          }
          rowContent={ r => (
            <p>
              <strong>{ r.name }</strong> { t('Has Purchased A') }{ ' ' }
              <strong>{ r.ticket_name }</strong> { t('Ticket to') }{ ' ' }
              <strong>{ event.name }</strong>.
            </p>
          ) }
          rowTimestamp={ r => formatDateTime(r.purchased_at) }
        />
        */ }
        <TabsCard tabs={ salesTabs } />
        { /* <BarChartForTags entities={ guests } label='Guests with Tag' /> */ }
        { /**
        <BarChartForTickets
          guests={ guests }
          tickets={ tickets }
          label={ t('Daily Ticketing Revenue (HKD)') }
          type='DAY'
        />
        <LineChartForGrowth
          entities={ guests }
          title={ t('Growth of Guest') }
          type={ 'DAY' }
        />
        * */ }
      </ManagerContainer>
    );
  }
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(EventOverviewView));
