import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import styled from 'styled-components';

import * as UiActionCreators from '../../redux/ui';
import * as MeActionCreators from '../../redux/me';

// import LanguageToggle from '../common/LanguageToggle';

import CurrentPageNav from './CurrentPageNav';
import BillingDisplay from '../manager/BillingDisplay';
import UserProfile from './UserProfile';

import juvenLogo from '../../assets/common/logo_juven.svg';

const ActionCreators = Object.assign({}, UiActionCreators, MeActionCreators);

const HeaderRightDiv = styled.div`
  display: flex;
  flex-shrink: 0;
  align-items: center;
  height: 56px;
  position: relative;
  float: right;
  text-align: right;
  border-left: 1px solid ${ props => props.theme.colors.lightGray };
`;

const HeaderLeft = styled.div`
  display: flex;
  align-items: center;
  flex-grow: 1;
  flex-shrink: 1;
`;

const HeaderLogo = styled.div`
  position: relative;
  z-index: 101;
  height: 56px;
  padding: 11px 0;

  & img {
    height: 34px;
  }
`;

// NEW STYLES
const HeaderDiv = styled.div`
  position: ${ props => (props.fixed ? 'fixed' : 'absolute') };
  z-index: 900;
  width: 100%;
  top: 0;
  box-shadow: 0 0 6px 0 rgba(0, 0, 0, 0.09);
  background-color: white;

  transition: background 0.3s ease;
  @media (max-width: 900px) {
    box-shadow: none;
  }

  p {
    font-size: 12px;
  }
`;

const HeaderBody = styled.div`
  display: flex;
  align-items: center;
  width: 100%;
  height: 56px;
  padding-left: 35px;

  @media (max-width: 900px) {
    padding-left: 15px;
  }
`;

const LanguageWrapper = styled.div`
  margin-left: -20px;
  width: 100px;

  @media (max-width: 900px) {
    display: none;
  }
`;

const mapStateToProps = state => ({
  user: state.me.user,
  organizations: state.manager.organizations,
  organization: state.manager.organization,
  event: state.manager.event,
});
const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators(ActionCreators, dispatch),
});

class Header extends Component {
  logout () {
    const { actions } = this.props;
    actions.logout();
  }

  render () {
    const {
      organization,
      navPages = [],
      fixed,
      // showLanguage
    } = this.props;

    const Right = (
      <HeaderRightDiv>
        <UserProfile />
        <LanguageWrapper>{ /* <LanguageToggle /> */ }</LanguageWrapper>
      </HeaderRightDiv>
    );

    const Left = (
      <HeaderLeft>
        { !organization ? (
          <HeaderLogo>
            <Link to='/'>
              <img alt='Juven logo' src={ juvenLogo } />
            </Link>
          </HeaderLogo>
        ) : null }
        { navPages.length ? <CurrentPageNav pages={ navPages } /> : null }
        <BillingDisplay />
      </HeaderLeft>
    );

    return (
      <HeaderDiv fixed={ fixed }>
        <HeaderBody>
          { Left }
          { Right }
        </HeaderBody>
      </HeaderDiv>
    );
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Header);
