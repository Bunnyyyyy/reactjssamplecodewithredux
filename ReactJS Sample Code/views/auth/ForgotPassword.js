import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { withRouter } from 'react-router';
import { Link } from 'react-router-dom';
import styled from 'styled-components';
import { Formik } from 'formik';
import * as Yup from 'yup';

import { validateEmail } from '../../utils';

import * as MeActionCreators from '../../redux/me';

import Input from '../../components/common/Input';
import Button from '../../components/common/Button';

import { t as translate } from '../../../i18n';

const t = a => translate(a, 'manager/index');

const Title = styled.h3`
  text-align: center;
  margin-bottom: 30px;
`;

const Message = styled.p`
  text-align: center;
  margin-bottom: 30px;
`;

const SuccessMessages = styled.div`
  text-align: center;

  p {
    margin-bottom: 10px;
  }
`;

const UserEmail = styled.span`
  font-weight: 700;
  color: ${ props => props.theme.colors.mainBlue };
`;

const StyledInput = styled(Input)`
  margin-bottom: 30px;
`;

const ToggleWrapper = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  margin: 30px 0;
  
  p {
    margin-right: 10px;
  }

  a {
    color: ${ props => props.theme.colors.mainBlue };
  }
`;

const mapStateToProps = state => ({
  user: state.me.user,
});

const ActionCreators = Object.assign(
  {},
  MeActionCreators,
);
const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators(ActionCreators, dispatch),
});

const RESET_ERROR_MESSAGE = t('There was an error when you were signing up.');

class AuthForgotPasswordView extends Component {
  constructor () {
    super();

    this.state = {
      sentEmail: false,
      sendError: null,
    };
  }

  componentDidMount () {
    if (this.input) {
      this.input.focus();
    }
  }

  async forgotPassword (state) {
    const { actions } = this.props;

    try {
      this.setState({ sendError: null });
      await actions.forgotPassword(state);
      this.setState({ sentEmail: state.email });
    } catch (e) {
      const sendError = e.message || RESET_ERROR_MESSAGE;
      return this.setState({ sendError });
    }
  }

  render () {
    const { sentEmail, sendError } = this.state;
    const ForgotPasswordFormSchema = Yup.object().shape({
      email: Yup.string()
        .nullable()
        .test(
          'is-valid-email',
          t('Please fill in a valid email'),
          v => !v || validateEmail(v),
        ),
    });

    const ViewContent = sentEmail
      ? (
        <SuccessMessages>
          <p>
            { `${ t('An email with the reset link has been sent to') } ` }
            <UserEmail>{ sentEmail }</UserEmail>
            { '.' }
          </p>
          <Message>{ t('Please follow the instructions there to reset your password.') }</Message>
        </SuccessMessages>
      )
      : (
        <Formik
          initialValues={ {} }
          validationSchema={ ForgotPasswordFormSchema }
          onSubmit={ this.forgotPassword.bind(this) }
        >
          { (props) => {
            const {
              values, errors, setFieldValue, handleSubmit, submitCount,
            } = props;
            const validateOnChange = submitCount > 0;

            return (
              <div>
                <Message>{ t('Fill in your email address and we will send you a link to reset your password.') }</Message>
                <StyledInput
                  key='email'
                  field='email'
                  fullWidth
                  label={ t('Email') }
                  inputRef={ input => (this.input = input) }
                  name='email'
                  value={ values.email }
                  errorMessage={ sendError || errors.email }
                  onChange={ (_, value) => setFieldValue('email', value, validateOnChange) }
                />
                <Button fullWidth onClick={ handleSubmit }>
                  { t('Request reset link') }
                </Button>
              </div>
            );
          } }
        </Formik>
      );

    return (
      <div>
        <Title>{ t('Recover your password') }</Title>
        { ViewContent }
        <ToggleWrapper>
          <Link to='/login'>
            { t('Back to login') }
          </Link>
        </ToggleWrapper>
      </div>
    );
  }
}

export default withRouter(
  connect(mapStateToProps, mapDispatchToProps)(AuthForgotPasswordView),
);
