import React, { Component } from 'react';
import styled from 'styled-components';
import CookieBanner from 'react-cookie-banner';
import Cookies from 'universal-cookie';
import i18n from '../../../i18n';

const t = a => i18n.t([`translations:${ a }`]);

const cookieMessage = t(
  'Juven uses cookies to ensure you have the best experience. If you continue to use this site, you are consenting to our use of cookies.',
);
const BannerDiv = styled.div`
  .react-cookie-banner {
    height: auto !important;
    background-color: ${ props => props.theme.colors.darkGray } !important;
    padding: 10px !important;
    z-index: 998 !important;

    @media (min-width: 901px) {
      display: flex !important;
      align-items: center !important;
      justify-content: space-between !important;
      padding: 10px 30px !important;
    }
  }

  .cookie-message {
    font-size: 15px !important;
    line-height: 21px !important;
    color: white !important;

    @media (max-width: 900px) {
      display: block !important;
      font-size: 12px !important;
      line-height: 14px !important;
    }

    @media (min-width: 901px) {
      text-align: left !important;
      margin-right: 20px !important;
    }
  }

  .button-close {
    position: relative !important;
    top: auto !important;
    right: auto !important;
    background: none !important;
    color: ${ props => props.theme.colors.darkBlue } !important;
    font-size: 15px !important;
    font-weight: 700 !important;
    line-height: 21px !important;
    height: 21px !important;
    padding: 0 !important;
    border: none !important;
    border-radius: 0 !important;
    margin-top: 0 !important;

    @media (max-width: 900px) {
      margin-top: 5px !important;
    }

    @media (min-width: 901px) {
      flex-shrink: 0 !important;
    }
  }

  ${ props => props.customCss || '' };
`;

function userAcceptCookies () {
  const cookies = new Cookies();
  cookies.set('user-has-accepted-cookies', true, { path: '/' });
}

class CookieBannerComponent extends Component {
  constructor () {
    super();
    this.state = {
      componentMount: false,
    };
  }

  componentDidMount () {
    this.setState({
      componentMount: true,
    });
  }

  componentWillUnmount () {
    userAcceptCookies();
  }

  render () {
    const { customCss } = this.props;
    const { componentMount } = this.state;
    const cookies = new Cookies();
    const cookiesValue = cookies.get('user-has-accepted-cookies');
    return (
      <BannerDiv customCss={ customCss }>
        { !cookiesValue && componentMount ? (
          <CookieBanner
            message={ cookieMessage }
            buttonMessage={ t('Accept cookies') }
            onAccept={ () => userAcceptCookies() }
            dismissOnScroll={ false }
            dismissOnClick
            cookie='user-has-accepted-cookies'
          />
        ) : null }
      </BannerDiv>
    );
  }
}
export default CookieBannerComponent;
