import { handleApiError, generateUrl } from '../utils';
import { getRequest } from './index';
import { setMeta } from './manager';

const SET_DONATIONS = 'SET_DONATIONS';
const SET_DONATIONS_TOTAL = 'SET_PROFILE_DONATIONS_TOTAL';
const SET_SUBSCRIPTIONS = 'SET_PROFILE_SUBSCRIPTIONS';
const SET_SUBSCRIPTIONS_TOTAL = 'SET_PROFILE_SUBSCRIPTIONS_TOTAL';
const SET_DONORS = 'SET_DONORS';
const SET_DONORS_TOTAL = 'SET_PROFILE_DONORS_TOTAL';
const SET_DONATION_AMOUNTS = 'SET_DONATION_AMOUNTS';
const SET_DONATION_CAUSES = 'SET_PROFILE_CAUSES';

export default function reducer (
  state = {
    donations: [],
    donationsTotal: {},
    subscriptions: [],
    subscriptionsTotal: {},
    donors: [],
    donorsTotal: {},
    amounts: [],
    causes: [],
  },
  action,
) {
  switch (action.type) {
    case SET_DONATIONS:
      return { ...state, donations: action.payload };

    case SET_DONATIONS_TOTAL:
      return { ...state, donationsTotal: action.payload };

    case SET_SUBSCRIPTIONS:
      return { ...state, subscriptions: action.payload };

    case SET_SUBSCRIPTIONS_TOTAL:
      return { ...state, subscriptionsTotal: action.payload };

    case SET_DONORS:
      return { ...state, donors: action.payload };

    case SET_DONORS_TOTAL:
      return { ...state, donorsTotal: action.payload };

    case SET_DONATION_AMOUNTS:
      return {
        ...state,
        amounts: action.payload.sort((x, y) => x.amount < y.amount),
      };

    case SET_DONATION_CAUSES:
      return { ...state, causes: action.payload };

    default:
      return state;
  }
}

export function setDonations (donations) {
  return {
    type: SET_DONATIONS,
    payload: donations,
  };
}

export function setDonationsTotal (total) {
  return {
    type: SET_DONATIONS_TOTAL,
    payload: total,
  };
}

export function setSubscriptions (subscriptions) {
  return {
    type: SET_SUBSCRIPTIONS,
    payload: subscriptions,
  };
}

export function setSubscriptionsTotal (total) {
  return {
    type: SET_SUBSCRIPTIONS_TOTAL,
    payload: total,
  };
}

export function setDonors (donors) {
  return {
    type: SET_DONORS,
    payload: donors,
  };
}

export function setDonorsTotal (total) {
  return {
    type: SET_DONORS_TOTAL,
    payload: total,
  };
}

export function setDonationAmounts (amounts) {
  return {
    type: SET_DONATION_AMOUNTS,
    payload: amounts,
  };
}

export function setDonationCauses (causes) {
  return {
    type: SET_DONATION_CAUSES,
    payload: causes,
  };
}

export function getDonations (
  oId,
  limit = 10,
  offset = 0,
  sortBy = 'created_at',
  sortOrder = 'desc',
  queryFilter,
) {
  return dispatch => new Promise(async (resolve) => {
    try {
      // add request loading UI for table
      const request = getRequest(dispatch, true);

      const baseUrl = `/organizations/${ oId }/donations`;
      const url = generateUrl(
        baseUrl,
        limit,
        offset,
        sortBy,
        sortOrder,
        queryFilter,
      );
      const response = await request.get(url);

      const donations = response.data;
      const meta = {
        count: Number(response.headers['juven-count']) || 0,
        from: Number(response.headers['juven-from']) || 0,
        to: Number(response.headers['juven-to']) || 0,
      };
      if (response.headers['juven-limited'] === 'true') {
        meta.limited = true;
      }

      dispatch(setDonations(donations));
      dispatch(setMeta(meta));

      return resolve(donations);
    } catch (e) {
      handleApiError(e);
      return resolve();
    }
  });
}

export function getDonationsTotal (oId, params) {
  return dispatch => new Promise(async (resolve) => {
    try {
      const request = getRequest(dispatch);
      const url = `/organizations/${ oId }/donations/total`;

      const response = await request.get(url, { params });
      const total = response.data;

      dispatch(setDonationsTotal(total));
      return resolve(total);
    } catch (e) {
      handleApiError(e);
      return resolve();
    }
  });
}

export function createDonation (oId, data) {
  return dispatch => new Promise(async (resolve, reject) => {
    try {
      const request = getRequest(dispatch);

      const url = `/organizations/${ oId }/donations`;
      const response = await request.post(url, data);

      return resolve(response.data);
    } catch (e) {
      const error = handleApiError(e);
      return reject(error);
    }
  });
}

export function getSubscriptions (
  oId,
  limit = 10,
  offset = 0,
  sortBy = 'created_at',
  sortOrder = 'desc',
) {
  return dispatch => new Promise(async (resolve) => {
    try {
      // add request loading UI for table
      const request = getRequest(dispatch, true);

      const baseUrl = `/organizations/${ oId }/subscriptions`;
      const url = generateUrl(baseUrl, limit, offset, sortBy, sortOrder);
      const response = await request.get(url);

      const subscriptions = response.data;
      const meta = {
        count: Number(response.headers['juven-count']) || 0,
        from: Number(response.headers['juven-from']) || 0,
        to: Number(response.headers['juven-to']) || 0,
      };
      if (response.headers['juven-limited'] === 'true') {
        meta.limited = true;
      }

      dispatch(setSubscriptions(subscriptions));
      dispatch(setMeta(meta));

      return resolve(subscriptions);
    } catch (e) {
      handleApiError(e);
      return resolve();
    }
  });
}

export function getSubscriptionsTotal (oId, params = {}) {
  return dispatch => new Promise(async (resolve) => {
    try {
      const request = getRequest(dispatch);
      const url = `/organizations/${ oId }/subscriptions/total`;
      const response = await request.get(url, { params });
      const total = response.data;

      dispatch(setSubscriptionsTotal(total));
      return resolve(total);
    } catch (e) {
      handleApiError(e);
      return resolve();
    }
  });
}

export function createSubscription (oId, data) {
  return dispatch => new Promise(async (resolve, reject) => {
    try {
      const request = getRequest(dispatch);

      const url = `/organizations/${ oId }/subscriptions`;
      const response = await request.post(url, data);

      return resolve(response.data);
    } catch (e) {
      const error = handleApiError(e);
      return reject(error);
    }
  });
}

export function getDonors (
  oId,
  limit = 10,
  offset = 0,
  sortBy = 'amount',
  sortOrder = 'desc',
) {
  return dispatch => new Promise(async (resolve) => {
    try {
      // add request loading UI for table
      const request = getRequest(dispatch, true);

      const baseUrl = `/organizations/${ oId }/donors`;
      const url = generateUrl(baseUrl, limit, offset, sortBy, sortOrder);
      const response = await request.get(url);

      const donors = response.data;
      const meta = {
        count: Number(response.headers['juven-count']) || 0,
        from: Number(response.headers['juven-from']) || 0,
        to: Number(response.headers['juven-to']) || 0,
      };

      dispatch(setDonors(donors));
      dispatch(setMeta(meta));

      return resolve(donors);
    } catch (e) {
      handleApiError(e);
      return resolve();
    }
  });
}

export function getDonorsTotal (oId) {
  return dispatch => new Promise(async (resolve) => {
    try {
      const request = getRequest(dispatch);
      const url = `/organizations/${ oId }/donors/total`;
      const response = await request.get(url);
      const total = response.data;

      dispatch(setDonorsTotal(total));
      return resolve(total);
    } catch (e) {
      handleApiError(e);
      return resolve();
    }
  });
}

export function createOrganizationDonation (oId, data) {
  return dispatch => new Promise(async (resolve, reject) => {
    try {
      const request = getRequest(dispatch);

      const url = `/organizations/${ oId }/donations`;
      const response = await request.post(url, data);

      const charge = response.data;
      return resolve(charge);
    } catch (e) {
      const error = handleApiError(e);
      return reject(error);
    }
  });
}

export function createOrganizationSubscription (oId, data) {
  return dispatch => new Promise(async (resolve, reject) => {
    try {
      const request = getRequest(dispatch);

      const url = `/organizations/${ oId }/subscriptions`;
      const response = await request.post(url, data);

      const charge = response.data;
      return resolve(charge);
    } catch (e) {
      const error = handleApiError(e);
      return reject(error);
    }
  });
}

export function updateOrganizationSubscription (oId, suId, data) {
  return dispatch => new Promise(async (resolve, reject) => {
    try {
      const request = getRequest(dispatch);

      const url = `/organizations/${ oId }/subscriptions/${ suId }`;
      await request.put(url, data);

      return resolve();
    } catch (e) {
      const error = handleApiError(e);
      return reject(error);
    }
  });
}

export function cancelOrganizationSubscription (oId, suId) {
  return dispatch => new Promise(async (resolve, reject) => {
    try {
      const request = getRequest(dispatch);

      const url = `/organizations/${ oId }/subscriptions/${ suId }`;
      await request.delete(url);

      return resolve();
    } catch (e) {
      const error = handleApiError(e);
      return reject(error);
    }
  });
}

export function getDonationAmounts (oId) {
  return dispatch => new Promise(async (resolve) => {
    try {
      const request = getRequest(dispatch);

      const url = `/organizations/${ oId }/donations/amounts`;
      const response = await request.get(url);

      const amounts = response.data;
      dispatch(setDonationAmounts(amounts));

      return resolve();
    } catch (e) {
      handleApiError(e);
      return resolve();
    }
  });
}

export function createDonationAmount (oId, data) {
  return dispatch => new Promise(async (resolve, reject) => {
    try {
      const request = getRequest(dispatch);

      const url = `/organizations/${ oId }/donations/amounts`;
      await request.post(url, data);

      return resolve();
    } catch (e) {
      const error = handleApiError(e);
      return reject(error);
    }
  });
}

export function updateDonationAmount (oId, amId, data) {
  return dispatch => new Promise(async (resolve, reject) => {
    try {
      const request = getRequest(dispatch);

      const url = `/organizations/${ oId }/donations/amounts/${ amId }`;
      await request.put(url, data);

      return resolve();
    } catch (e) {
      const error = handleApiError(e);
      return reject(error);
    }
  });
}

export function removeDonationAmount (oId, amId) {
  return dispatch => new Promise(async (resolve, reject) => {
    try {
      const request = getRequest(dispatch);

      const url = `/organizations/${ oId }/donations/amounts/${ amId }`;
      await request.delete(url);

      return resolve();
    } catch (e) {
      const error = handleApiError(e);
      return reject(error);
    }
  });
}

export function getDonationCauses (oId) {
  return dispatch => new Promise(async (resolve) => {
    try {
      const request = getRequest(dispatch);

      const url = `/organizations/${ oId }/donations/causes`;
      const response = await request.get(url);

      const causes = response.data;
      dispatch(setDonationCauses(causes));
      return resolve(causes);
    } catch (e) {
      handleApiError(e);
      return resolve();
    }
  });
}

export function createDonationCause (oId, data) {
  return dispatch => new Promise(async (resolve, reject) => {
    try {
      const request = getRequest(dispatch);

      const url = `/organizations/${ oId }/donations/causes`;
      await request.post(url, data);

      return resolve();
    } catch (e) {
      const error = handleApiError(e);
      return reject(error);
    }
  });
}

export function updateDonationCause (oId, camId, data) {
  return dispatch => new Promise(async (resolve, reject) => {
    try {
      const request = getRequest(dispatch);

      const url = `/organizations/${ oId }/donations/causes/${ camId }`;
      await request.put(url, data);

      return resolve();
    } catch (e) {
      const error = handleApiError(e);
      return reject(error);
    }
  });
}

export function removeDonationCause (oId, camId) {
  return dispatch => new Promise(async (resolve, reject) => {
    try {
      const request = getRequest(dispatch);

      const url = `/organizations/${ oId }/donations/causes/${ camId }`;
      await request.delete(url);

      return resolve();
    } catch (e) {
      const error = handleApiError(e);
      return reject(error);
    }
  });
}
