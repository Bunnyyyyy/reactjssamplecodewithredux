import React, { Component } from 'react';
import styled from 'styled-components';

import Button from '../../common/Button';
import TagsDropdown from './TagsDropdown';
import Tooltip from '../../common/Tooltip';

import tagIcon from '../../../assets/manager/bulk-actions/icon-tag.svg';

import i18n from '../../../../i18n';

const t = a => i18n.t([`translations:${ a }`]);

const TagDropdownWrapper = styled.div`
  display: block;
  position: absolute;
  top: 0px;
`;

const ButtonWrapper = styled.div`
  position: inherit;
  flex-shrink: 0;
  margin-right: 6px;
`;

const ToggleButton = styled(Button)`
  position: relative;
  font-size: 12px;
  height: 48px;
  padding: 12px;
  margin: 0;

  &:disabled {
    background: transparent;
    color: rgb(47, 46, 46);
    opacity: 0.3;
  }

  &:hover ~ ${ Tooltip } {
    visibility: visible;
  }

  ${ props => props.customCss || '' };
`;

export default class EditTag extends Component {
  constructor () {
    super();
    this.state = {
      isShowingDropdown: false,
      isCreatingTag: false,
      isUpdatingTag: false,
    };

    this.hideDropdownHandler = this.hideDropdown.bind(this);
  }

  componentDidMount () {
    document.addEventListener('mousedown', this.hideDropdownHandler, false);
  }

  componentWillUnmount () {
    document.removeEventListener('mousedown', this.hideDropdownHandler, false);
  }

  onCloseDropdown () {
    this.setState({
      isShowingDropdown: false,
      isCreatingTag: false,
      isUpdatingTag: false,
    });
  }

  hideDropdown (e) {
    const { isCreatingTag, isUpdatingTag } = this.state;
    if ((this.dropdownWrapper && this.dropdownWrapper.contains(e.target)) || isCreatingTag || isUpdatingTag) return;

    this.onCloseDropdown();
  }

  render () {
    const {
      tags, reloadData, customCss, actionParams,
    } = this.props;
    const { isShowingDropdown } = this.state;
    const disableToggle = !actionParams.apply_to_all
      && (!actionParams.filters
        || (actionParams.filters.id && !actionParams.filters.id.length)
        || (actionParams.filters.user_id && !actionParams.filters.user_id.length));

    const tooltipText = t('Edit Tags');
    return (
      <ButtonWrapper
        onClick={ () => this.setState({ isShowingDropdown: true }) }
      >
        <ToggleButton plain customCss={ customCss } disabled={ disableToggle }>
          <img alt={ t('Tag') } src={ tagIcon } />
        </ToggleButton>
        { isShowingDropdown && !disableToggle ? (
          <TagDropdownWrapper innerRef={ wrapper => (this.dropdownWrapper = wrapper) }>
            <TagsDropdown
              tags={ tags }
              reloadData={ reloadData }
              actionParams={ actionParams }
              onClose={ this.onCloseDropdown.bind(this) }
              onCreateTag={ () => this.setState({ isCreatingTag: true }) }
              onUpdateTag={ () => this.setState({ isUpdatingTag: true }) }
            />
          </TagDropdownWrapper>
        ) : null }
        <Tooltip>{ tooltipText }</Tooltip>
      </ButtonWrapper>
    );
  }
}
