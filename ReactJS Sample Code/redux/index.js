import { combineReducers } from 'redux';
import axios from 'axios';
// import { persistStore, persistReducer } from 'redux-persist';
// import storage from 'redux-persist/lib/storage';
// import autoMergeLevel2 from 'redux-persist/lib/stateReconciler/autoMergeLevel2';

import request from './request';
import ui, { setRequestLoading } from './ui';
import me from './me';
import home from './home';
import search from './search';
import profile from './profile';
import organization from './organization';
import donations from './donations';
import event from './event';
import emailTemplates from './emailTemplates';
import eventLive from './eventLive';
import eventPromoCodes from './eventPromoCodes';
import contacts from './contacts';
import manager from './manager';
import checkout from './checkout';
import pricing from './pricing';
import enquiries from './enquiries';
import affiliates from './affiliates';
import config from './config';

const { API_URL } = process.env;

const reducer = combineReducers({
  request,
  ui,
  me,
  home,
  search,
  profile,
  organization,
  donations,
  event,
  emailTemplates,
  eventLive,
  eventPromoCodes,
  contacts,
  manager,
  checkout,
  pricing,
  enquiries,
  affiliates,
  config,
});

export function getRequest (dispatch, hasLoadingUi = null) {
  const clientRequest = axios.create({
    baseURL: API_URL,
    withCredentials: true,
  });

  clientRequest.interceptors.request.use(
    (conf) => {
      const { method, url } = conf;

      const setLoading = hasLoadingUi !== null ? hasLoadingUi : method !== 'get';
      if (setLoading) {
        dispatch(setRequestLoading(url, true));
      }

      return conf;
    },
    (error) => {
      const trimmedUrl = error.request.responseURL.slice(API_URL.length);

      const setLoading = hasLoadingUi !== null ? hasLoadingUi : true;
      if (setLoading) {
        dispatch(setRequestLoading(trimmedUrl, false));
      }

      return Promise.reject(error);
    },
  );
  clientRequest.interceptors.response.use(
    (conf) => {
      const { config: { method, url, baseURL } } = conf;
      const trimmedUrl = url.slice(baseURL.length);

      const setLoading = hasLoadingUi !== null ? hasLoadingUi : method !== 'get';
      if (setLoading) {
        dispatch(setRequestLoading(trimmedUrl, false));
      }

      return conf;
    },
    (error) => {
      const trimmedUrl = error.request.responseURL.slice(API_URL.length);

      const setLoading = hasLoadingUi !== null ? hasLoadingUi : true;
      if (setLoading) {
        dispatch(setRequestLoading(trimmedUrl, false));
      }

      return Promise.reject(error);
    },
  );

  return clientRequest;
}

export default reducer;
