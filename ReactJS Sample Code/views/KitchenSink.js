import React, { Component } from 'react';
import styled from 'styled-components';

import Button from '../components/common/Button';
import Checkbox from '../components/common/Checkbox';
import ConfirmModal from '../components/common/ConfirmModal';
import DatePicker from '../components/common/DatePicker';
import FacebookLoginButton from '../components/common/FacebookLoginButton';
import GoogleLoginButton from '../components/common/GoogleLoginButton';
import LineChart from '../components/manager/charts/LineChart';
import PieChart from '../components/manager/charts/PieChart';
import BarChart from '../components/manager/charts/BarChart';
import HorizontalBarChart from '../components/manager/charts/HorizontalBarChart';
import MultiLineChart from '../components/manager/charts/MultiLineChart';
import Select from '../components/common/inputs/Select';
import Input from '../components/common/Input';
import PhoneInput from '../components/common/inputs/PhoneInput';
import MultiInput from '../components/common/inputs/MultiInput';
import TabbedPeopleList from '../components/manager/TabbedPeopleList';
import ColorPickerV2 from '../components/common/ColorPicker';
import i18n from '../../i18n';

// const t = a => i18n.t([`translations:${ a }`]);

const KitchenSinkContainer = styled.div`
  padding: 0 20px;
`;

const CurrencyContainer = styled.div`
  display: flex;
  justify-content: space-between;
  font-weight: bold;
`;
const CurrencyLabel = styled.span`
  padding-top: 3px;
`;
const CurrencySelect = styled.select`
  font-size: 16px;
  font-weight: bold;
  border: none;
`;
const guestsMock = [
  {
    id: 1,
    avatar: 'https://randomuser.me/api/portraits/women/0.jpg',
    fullName: 'Ida P.McClain',
    title: 'Customer Usability Developer',
    status: 'CHECK-IN',
  },
  {
    id: 2,
    avatar: 'https://randomuser.me/api/portraits/men/0.jpg',
    fullName: 'David H.Thoman',
    title: 'Computer Software Engineer',
    status: 'CHECK-IN',
  },
  {
    id: 3,
    avatar: 'https://randomuser.me/api/portraits/men/3.jpg',
    fullName: 'David H.Thoman',
    title: 'Computer Software Engineer',
    status: 'WALK-IN',
  },
  {
    id: 4,
    avatar: 'https://randomuser.me/api/portraits/men/2.jpg',
    fullName: 'David H.Thoman',
    title: 'Computer Software Engineer',
    status: 'INVITED',
  },
  {
    id: 5,
    avatar: 'https://randomuser.me/api/portraits/women/2.jpg',
    fullName: 'David H.Thoman',
    title: 'Computer Software Engineer',
    status: 'INVITED',
  },
  {
    id: 6,
    avatar: 'https://randomuser.me/api/portraits/women/0.jpg',
    fullName: 'Ida P.McClain',
    title: 'Computer Software Engineer',
    status: 'DECLINED',
  },
  {
    id: 7,
    avatar: 'https://randomuser.me/api/portraits/men/0.jpg',
    fullName: 'David H.Thoman',
    title: 'Computer Software Engineer',
    status: 'CHECK-IN',
  },
  {
    id: 8,
    avatar: 'https://randomuser.me/api/portraits/men/3.jpg',
    fullName: 'David H.Thoman',
    title: 'Computer Software Engineer',
    status: 'WALK-IN',
  },
  {
    id: 9,
    avatar: 'https://randomuser.me/api/portraits/men/2.jpg',
    fullName: 'David H.Thoman',
    title: 'Computer Software Engineer',
    status: 'JOINED',
  },
  {
    id: 10,
    avatar: 'https://randomuser.me/api/portraits/women/2.jpg',
    fullName: 'David H.Thoman',
    title: 'Computer Software Engineer',
    status: 'INVITED',
  },
  {
    id: 11,
    avatar: 'https://randomuser.me/api/portraits/men/2.jpg',
    fullName: 'Thiago T. Dallacqua',
    title: 'Computer Software Engineer',
  },
  {
    id: 12,
    avatar: 'https://randomuser.me/api/portraits/women/2.jpg',
    fullName: 'Arslam Arshad',
    title: 'Computer Software Engineer',
  },
];

const guests = guestsMock.concat(guestsMock);

export default class JuvenDesignKitchenSink extends Component {
  state = {
    selectedCurrency: 'USD',
    selectValue: 4,
    color: '#ff9e58',
    tagName: 'VIP Client',
    hobbies: ['fishing', 'shopping'],
  };

  render () {
    const {
      color, tagName, selectValue, demoDate, phoneNumber, hobbies, selectedCurrency, showModal,
    } = this.state;

    const changeLanguage = (lng) => {
      i18n.changeLanguage(lng);
      window.location.reload();
    };

    const t = a => i18n.t([`translations:${ a }`, `kitchen_sink:${ a }`]);
    return (
      <KitchenSinkContainer>
        <div md={ 12 }>
          <div className='App'>
            <div className='App-header'>
              <h2>{ t('Testing') }</h2>
              <button type='button' onClick={ () => changeLanguage('pt') }>pt</button>
              <button type='button' onClick={ () => changeLanguage('en') }>en</button>
              <button type='button' onClick={ () => changeLanguage('zh') }>zh-cn</button>
              <button type='button' onClick={ () => changeLanguage('zh-hk') }>zh-hk</button>
            </div>
          </div>
          <i className='fas fa-exchange-alt' />
        </div>
        <div>
          <ColorPickerV2
            field='color'
            value={ color }
            tagName={ tagName }
            onTagChange={ value => this.setState({ tagName: value }) }
            onChange={ value => this.setState({ color: value }) }
          />
        </div>
        <div md={ 12 }>
          <p>Buttons</p>
          <Button onClick={ () => this.setState({ showModal: true }) }>
            <i className='fas fa-compass' aria-hidden='true' />
            { ' ' }
Show Confirm
            Modal
          </Button>
        </div>
        <hr />
        <div md={ 12 }>
          <p>Checkboxes</p>
          <Checkbox checked />
          <Checkbox />
          <Checkbox checked />
          <Checkbox />
          <Checkbox checked />
        </div>
        <hr />
        <div style={ { display: 'flex' } }>
          <Select
            label='React Select V2 Testing'
            field='react-select-test'
            customCss='flex: 0 0 50%;'
            value={ selectValue }
            options={ [
              { value: 1, label: 'USD' },
              { value: 2, label: 'HKD' },
              { value: 3, label: 'EUR' },
              { value: 4, label: 'JPY' },
              { value: 5, label: 'CNY' },
            ] }
            onChange={ (value) => {
              this.setState({ selectValue: value });
            } }
          />
          <Input
            label='React Select V2 Input Comparison'
            field='react-select-input-comparison'
            customCss='flex: 0 0 50%'
          />
        </div>
        <hr />
        <div>
          <div md={ 4 }>
            <p>Facebook login button</p>
            <FacebookLoginButton />
          </div>
          <div md={ 4 } offset={ { md: 2 } }>
            <p>Google login button</p>
            <GoogleLoginButton />
          </div>
        </div>
        <div>
          <p>Date picker</p>
          <DatePicker
            date={ demoDate }
            onChange={ date => this.setState({ demoDate: date }) }
          />
        </div>
        <div>
          <div md={ 6 } style={ { overflow: 'visible' } }>
            <PhoneInput
              placeholder='e.g: 64601900'
              field='phoneNumber'
              label='Phone number'
              value={ phoneNumber }
              onChange={ (filed, value) => {
                this.setState({ phoneNumber: value });
              } }
            />
          </div>
        </div>
        <div>
          <div md={ 6 } style={ { overflow: 'visible' } }>
            <MultiInput
              underline
              fullWidth
              field='hobbies'
              label='What are your hobbies?'
              value={ hobbies }
              placeholder='Add hobbies'
              onChange={ value => this.setState({ hobbies: value }) }
            />
          </div>
        </div>
        <hr />
        <div style={ { backgroundColor: '#D3D3D3', padding: 20 } }>
          <p>Charts</p>
          <div lg={ 4 } md={ 4 } sm={ 6 } xs={ 12 }>
            <LineChart
              data={ [
                { value: 10, label: 'Mon' },
                { value: 12, label: 'Tue' },
                { value: 3, label: 'Wed' },
                { value: 7, label: 'Thu' },
                { value: 160, label: 'Fri' },
                { value: 30, label: 'Weeeeeekend!!!' },
              ] }
              summaryValue={ 1350 }
              summaryDirection='up'
              title='Organization Profile Views Past 2 Weeks'
            />
          </div>
        </div>
        <div style={ { backgroundColor: 'white', margin: 20 } }>
          <PieChart
            data={ [
              {
                label: 'Red',
                value: 300,
              },
              {
                label: 'Green',
                value: 150,
              },
              {
                label: 'Hello',
                value: 50,
              },
              {
                label: 'Yellow',
                value: 0,
                color: '#FFCE56',
              },
            ] }
          />
        </div>
        <hr />
        <div>
          <p>Multi lines chart</p>
          <div lg={ 8 } md={ 8 } sm={ 12 } xs={ 12 }>
            <MultiLineChart
              title='TICKET SALES'
              labels={ [31, 'JAN', 2, 3, 4, 5, 6, 7, 8] }
              datasets={ [
                {
                  color: 'rgba(63, 63, 191,1)',
                  data: [
                    52000,
                    52004,
                    49070,
                    50123,
                    79001,
                    90092,
                    130091,
                    180182,
                    204101,
                  ],
                  label: 'TOTAL REVENUE',
                },
                {
                  color: 'rgba(63, 191, 63, 1)',
                  data: [
                    152102,
                    172208,
                    99091,
                    40201,
                    209109,
                    190190,
                    110101,
                    120201,
                    274251,
                  ],
                  label: 'DONATION',
                },
                {
                  color: 'rgba(191, 63, 63, 1)',
                  data: [
                    62102,
                    81208,
                    249091,
                    109201,
                    180109,
                    118190,
                    87101,
                    201201,
                    174251,
                  ],
                  label: 'TICKETING',
                },
                {
                  color: 'rgba(19, 163, 132, 1)',
                  data: [
                    12102,
                    21208,
                    49091,
                    69201,
                    50109,
                    30190,
                    21101,
                    12201,
                    8251,
                  ],
                  label: 'MERCHANDISE',
                },
              ] }
              yAxesDisplayFormat={ value => (value ? `${ Math.floor(value / 1000) }K` : value) }
              controlsElem={ (
                <CurrencyContainer>
                  <CurrencyLabel>CURRENCY</CurrencyLabel>
                  <CurrencySelect
                    value={ selectedCurrency }
                    onChange={ (field, value) => this.setState({ selectedCurrency: value })
                    }
                  >
                    <option value='USD'>USD</option>
                    <option value='HKD'>HKD</option>
                  </CurrencySelect>
                </CurrencyContainer>
) }
            />
          </div>
        </div>
        <hr />
        <div>
          <TabbedPeopleList title='Guest List' people={ guests } />
        </div>
        <hr />
        { showModal ? (
          <ConfirmModal
            message='Please confirm you want to delete selected files!'
            onConfirm={ () => {
              this.setState({ showModal: false });
            } }
            onCancel={ () => this.setState({ showModal: false }) }
          />
        ) : null }
        <div>
          <div md={ 6 }>
            <BarChart
              data={ [
                {
                  label: 'JAN',
                  value: 1000,
                },
                {
                  label: 'FEB',
                  value: 1500,
                },
                {
                  label: 'MAR',
                  value: 800,
                },
                {
                  label: 'APR',
                  value: 1750,
                },
                {
                  label: 'MAY',
                  value: 1300,
                },
                {
                  label: 'JUN',
                  value: 700,
                },
                {
                  label: 'JUL',
                  value: 400,
                },
              ] }
              // xLabels={ ['JAN', 'FEB', 'MAR', 'APR', 'MAY', 'JUN', 'JUL'] }
              // datasets={ [
              //   {
              //     label: 'Example 1',
              //     data: [1000, 1500, 1700, 1750, 1650, 1400, 1300],
              //     backgroundColor: '#dedede'
              //   },
              //   {
              //     label: 'Example 2',
              //     data: [1500, 1700, 1705, 1605, 2000, 2100, 1400],
              //     backgroundColor: '#dedede'
              //   },
              //   {
              //     label: 'Example 3',
              //     data: [700, 805, 800, 900, 1300, 700, 400],
              //     backgroundColor: '#dedede'
              //   }
              // ] }
            />
          </div>
          <HorizontalBarChart
            data={ [
              {
                label: 'JAN',
                value: 1000,
              },
              {
                label: 'FEB',
                value: 1500,
              },
              {
                label: 'MAR',
                value: 800,
              },
              {
                label: 'APR',
                value: 1750,
              },
              {
                label: 'MAY',
                value: 1300,
              },
              {
                label: 'JUN',
                value: 700,
              },
              {
                label: 'JUL',
                value: 400,
              },
            ] }
          />
        </div>
      </KitchenSinkContainer>
    );
  }
}
