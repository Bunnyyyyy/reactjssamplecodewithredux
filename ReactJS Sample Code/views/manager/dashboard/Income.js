import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { withRouter } from 'react-router';

import {
  formatDateTime,
  stateHasSelectedRows,
} from '../../../utils';
import currencies from '../../../utils/currencies';

import * as UiActionCreators from '../../../redux/ui';
import * as MeActionCreators from '../../../redux/me';
import * as ManagerActionCreators from '../../../redux/manager';
import * as OrganizationActionCreators from '../../../redux/organization';
import * as EmailTemplatesActionCreators from '../../../redux/emailTemplates';

import Table from '../../../components/common/Table';
import ManagerContainer from '../../../components/layouts/ManagerContainer';
import BasicContactInfo from '../../../components/manager/BasicContactInfo';
import Tag from '../../../components/manager/tags/Tag';
import EditTag from '../../../components/manager/tags/EditTag';

import { t as translate } from '../../../../i18n';

const t = a => translate(a, 'manager/home/incomes');

const mapStateToProps = state => ({
  user: state.me.user,
  organizations: state.manager.organizations,
  organization: state.manager.organization,
  purchasedTickets: state.manager.purchasedTickets,
  customFields: state.organization.customFields,
  tags: state.organization.tags,
});

const ActionCreators = Object.assign(
  {},
  UiActionCreators,
  MeActionCreators,
  ManagerActionCreators,
  OrganizationActionCreators,
  EmailTemplatesActionCreators,
);
const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators(ActionCreators, dispatch),
});

class DashboardIncomeView extends Component {
  constructor () {
    super();

    // default table settings
    this.state = {
      sortBy: 'purchased_at',
      sortOrder: 'desc',
    };
  }

  async componentDidMount () {
    const { actions, match } = this.props;
    const { oId } = match.params;

    actions.setLoading(true);

    try {
      const promises = [
        actions.getPurchasedTickets(oId),
        actions.getOrganizationTags(oId),
        actions.getOrganizationCustomFields(oId),
        actions.getEmailTemplates(oId),
      ];

      await Promise.all(promises);
    } catch (e) {
      console.error('ERROR: DashboardIncomeView', e);
    } finally {
      actions.setLoading(false);
    }
  }

  async reloadPurchasedTickets () {
    const { actions, organization } = this.props;
    const { sortBy, sortOrder, sortType } = this.state;
    try {
      await actions.getPurchasedTickets(organization.id, {
        sort_by: sortBy,
        sort_order: sortOrder,
        sort_type: sortType,
      });
    } catch (e) {
      console.error(e);
    }
  }

  sortTickets (newSortBy, sortType) {
    const { sortBy, sortOrder } = this.state;
    const newSortOrder = sortBy === newSortBy ? (sortOrder === 'asc' ? 'desc' : 'asc') : 'desc';
    this.setState({
      sortBy: newSortBy,
      sortOrder: newSortOrder,
      sortType,
    }, this.reloadPurchasedTickets);
  }

  render () {
    const {
      organization,
      purchasedTickets,
      tags,
      customFields,
    } = this.props;
    const { sortBy, sortOrder } = this.state;
    // add up to only 85% due to checkbox and edit button
    let tableColumns = [
      {
        title: t('Purchased By'),
        field: 'name',
        compulsory: true,
        unscrollable: true,
        flex: '1.5 0 0',
        format: (d, r) => (
          <BasicContactInfo
            type='event_guest'
            organization={ organization }
            contact={ r }
          />
        ),
        tooltipFormat: d => d,
      },
      {
        title: t('Ref. ID'),
        field: 'id',
        format: v => `#G${ v }`,
      },
      {
        title: t('Tags'),
        field: 'tags',
        css: 'overflow: visible;line-height: normal;',
        bodyCss: 'overflow: visible;',
        format: (d, r) => {
          const contactTags = tags.filter(
            tag => r.tags && r.tags.indexOf(tag.id) > -1,
          );

          return contactTags.length
            ? contactTags.map(tag => <Tag value={ tag.tag } color={ tag.color } />)
            : '-';
        },
      },
      {
        title: t('Email'),
        field: 'email',
      },
      {
        title: t('Phone'),
        field: 'phone',
      },
      {
        title: t('Purchased At'),
        field: 'purchased_at',
        format: d => formatDateTime(d),
      },
      {
        title: t('Event Name'),
        field: 'event_name',
      },
      {
        title: t('Amount'),
        field: 'total_amount',
        format: (d, r) => {
          const currency = currencies[r.currency || 'hkd'];
          return `${ currency ? currency.symbol : '' }${ d ? d.toLocaleString() : 0 }`;
        },
      },
      {
        title: t('Quantity'),
        field: 'quantity',
      },
      {
        title: t('Ticket Type'),
        field: 'ticket_name',
      },
    ];

    tableColumns = tableColumns.concat(
      customFields.map(f => ({
        title: f.name,
        field: f.id,
        customField: true,
        format: (d, r) => {
          let value = r.custom_fields ? r.custom_fields[f.id] : null;
          if (!value) return '-';
          if (f.data_type === 'boolean') value = value === 1 ? 'Yes' : 'No';
          return value;
        },
      })),
    );
    const tableBulkActions = [
      {
        render: (params) => {
          const actionParams = Object.assign(params, { contact_type: 'user' });

          return (
            <EditTag
              tags={ tags }
              reloadData={ this.reloadPurchasedTickets.bind(this) }
              actionParams={ actionParams }
            />
          );
        },
        condition: state => stateHasSelectedRows(state),
      },
    ];

    return (
      <ManagerContainer>
        <Table
          id='income_tickets'
          noPagination
          columns={ tableColumns }
          rows={ purchasedTickets }
          onSort={ this.sortTickets.bind(this) }
          sortBy={ sortBy }
          sortOrder={ sortOrder }
          bulkActions={ tableBulkActions }
          emptyText={ t('There are no tickets sold yet.') }
        />
      </ManagerContainer>
    );
  }
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(DashboardIncomeView));
