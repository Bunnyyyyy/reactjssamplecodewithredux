import React, { Component } from 'react';
import styled from 'styled-components';

const SectionToggleDiv = styled.div`
  z-index: 101;
  position: relative;
  border-bottom: 1px solid rgb(227, 227, 227);
`;

const SectionToggleButton = styled.button`
  width: 150px;
  height: 47px;
  line-height: 40px;
  font-size: 16px;
  font-weight: 400;
  color: ${ props => props.theme.colors.mainGray };
  background: none;
  border-radius: 0;
  border-bottom: 3px solid transparent;
  margin-bottom: -1.5px;

  &:hover,
  &:active {
    background: none;
    color: ${ props => props.theme.colors.darkGray };
  }

  ${ props =>
    props.active
      ? `font-weight: 700;
      color: ${ props => props.theme.colors.darkBlue };
      border-color: ${ props => props.theme.colors.darkBlue };

      &:hover, &:active {
        color: ${ props => props.theme.colors.mainBlue };
        border-color: ${ props => props.theme.colors.mainBlue };
      }`
      : '' };
`;

const SectionToggle = ({ sections, currentSection, onChangeSection }) => {
  const SectionToggleButtons = sections.map(s => (
    <SectionToggleButton
      active={ currentSection === s.section }
      onClick={ onChangeSection.bind(this, s.section) }
    >
      { s.label }
    </SectionToggleButton>
  ));

  return <SectionToggleDiv>{ SectionToggleButtons }</SectionToggleDiv>;
};

export default SectionToggle;
