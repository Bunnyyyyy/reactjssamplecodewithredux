import React from 'react';
import styled from 'styled-components';

import Label from './Label';
import InvalidMessage from '../InvalidMessage';

const OptionsWrapper = styled.div`
  margin-top: 20px;
  margin-bottom: 20px;
  max-width: 100%;
  ${ props => props.customCss || '' };
`;

const AllOptionsContainer = styled.div`
  display: flex;
  flex-direction: row;
  width: 100%;
  flex-wrap: wrap;
`;

const H7 = styled.h6`
  vertical-align: bottom;
  ${ props => (props.subLabelCss ? props.subLabelCss : '') };
`;

export default function Options ({
  field,
  options,
  custom,
  value,
  label,
  subLabel,
  subLabelCss,
  divStyle,
  customCss,
  onChange = () => {},
  required,
  errorMessage,
}) {
  const OptionComponents = options.map((opt, i) => {
    if (!opt.component) return null;
    const component = typeof opt.component === 'function'
      ? opt.component(onChange, value)
      : opt.component;

    const elementProps = { key: `${ field }-option-${ i }` };
    if (!custom) {
      const selected = opt.value === value;
      elementProps.selected = selected;
      elementProps.onClick = () => {
        if (selected) {
          if (!required) onChange(null);
        } else if (opt.value !== undefined) {
          onChange(opt.value);
        }
      };
    }

    return React.cloneElement(component, elementProps);
  });

  return (
    <OptionsWrapper style={ divStyle } customCss={ customCss }>
      { label ? (
        <Label
          field={ field }
          label={ label }
          invalid={ !!errorMessage }
          required={ required }
        />
      ) : null }
      { subLabel ? <H7 subLabelCss={ subLabelCss }>{ subLabel }</H7> : null }
      <AllOptionsContainer>{ OptionComponents }</AllOptionsContainer>
      { errorMessage ? <InvalidMessage>{ errorMessage }</InvalidMessage> : null }
    </OptionsWrapper>
  );
}
