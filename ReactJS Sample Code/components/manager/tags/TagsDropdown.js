import React, { Component } from 'react';
import styled from 'styled-components';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { get, uniq } from 'lodash';

import Dropdown from '../../common/Dropdown';
import Tag from './Tag';
import Radio from '../../common/Radio';
import Checkbox from '../../common/Checkbox';
import Button from '../../common/Button';
import SettingModal from '../SettingModalV2';

import * as ManagerActionCreators from '../../../redux/manager';
import * as OrganizationActionCreators from '../../../redux/organization';
import * as ContactsActionCreators from '../../../redux/contacts';
import theme from '../../../utils/theme';

import i18n from '../../../../i18n';

import { REPEAT_ALERT_OPTIONS } from '../../../constants';

const t = a => i18n.t([`components/manager:${ a }`]);

const Container = styled.div``;

const TagWithCheckboxContainer = styled.div`
  display: flex;
  flex-direction: column;
  padding-right: 20px;
  padding-left: 50px;
  border-bottom: 0.3px solid ${ props => props.theme.colors.lightGray };
  margin-top: 10px;
  margin-bottom: 10px;
  > p {
    color: ${ props => props.theme.colors.darkGray };
    font-size: 12px;
    margin-bottom: 10px;
  }
`;

const TagWithCheckbox = styled.div`
  display: flex;
  flex-direction: row;
  margin-top: 5px;
  margin-bottom: 8px;
  min-height: 30px;
  > h5 {
    font-size: 12px;
    font-weight: normal;
    text-transform: capitalize;
  }
`;

const H5 = styled.h5`
  flex-grow: 1;
  text-align: left;
  margin-left: 8px;
`;

const CustomRadioWrapper = styled.p`
  margin: 20px;
  font-size: 12px;
`;

const ConfirmButton = Button.extend`
  width: 240px;
  height: 30px;
  border-radius: 42px;
  box-shadow: #00a7ff;
  margin: 20px;
  font-size: 12px;
`;
const ButtonWraper = styled.div`
  border-top: 1px solid ${ props => props.theme.colors.lightGray };
`;
const TagScrollWrapper = styled.div`
  height: auto;
  overflow-y: scroll;
  max-height: 180px;
  margin-bottom: 20px;
  &::-webkit-scrollbar {
    width: 6px;
    display: block !important;
  }

  /* Track */
  &::-webkit-scrollbar-track {
    box-shadow: inset 0 0 5px #e1e8ed;
    border-radius: 10px;
    background: #e1e8ed;
  }

  /* Handle */
  &::-webkit-scrollbar-thumb {
    background: #bcd4e0;
    border-radius: 10px;
  }
`;
const mapStateToProps = state => ({
  organization: state.manager.organization,
  admins: state.manager.admins,
  templates: state.emailTemplates.templates,
});

const ActionCreators = Object.assign(
  {},
  ManagerActionCreators,
  OrganizationActionCreators,
  ContactsActionCreators,
);

const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators(ActionCreators, dispatch),
});

class TagsDropdown extends Component {
  constructor () {
    super();
    this.state = {
      isCreatingTag: false,
      updatingTag: null,
      userAction: '',
      tagIds: [],
    };
  }

  onToggleTagCheckbox (tagId, checked) {
    const { tagIds } = this.state;
    const currentPos = tagIds.indexOf(tagId);

    if (checked) {
      if (currentPos === -1) tagIds.push(tagId);
    } else if (currentPos > -1) tagId.splice(currentPos, 1);

    this.setState({ tagIds });
  }

  async addTagToContacts (tagIds) {
    try {
      const {
        organization, actions, reloadData, actionParams,
      } = this.props;
      const oId = organization.id;
      const promises = tagIds.map(tag => actions.bulkAddTagToContacts(oId, tag, actionParams));
      await Promise.all(promises);
      await reloadData();
    } catch (e) {
      throw e;
    }
  }

  async removeTagFromContacts (tagIds) {
    try {
      const {
        organization, actions, reloadData, actionParams,
      } = this.props;
      const oId = organization.id;
      const promises = tagIds.map(tag => actions.bulkRemoveTagFromContacts(oId, tag, actionParams));
      await Promise.all(promises);
      await reloadData();
    } catch (e) {
      throw e;
    }
  }

  async createTag (data, addToContacts = false) {
    try {
      const { organization, actions, reloadData } = this.props;
      const oId = organization.id;

      delete data.set_alert;

      const tagData = await actions.createOrganizationTag(oId, data);
      await actions.getOrganizationTags(oId);

      if (!addToContacts) {
        await reloadData();
      }

      return tagData;
    } catch (e) {
      throw e;
    }
  }

  async updateTag (data) {
    try {
      const { organization, actions, reloadData } = this.props;
      const { updatingTag } = this.state;
      const oId = organization.id;

      delete data.set_alert;

      await actions.updateOrganizationTag(oId, updatingTag.id, data);
      await reloadData();
      await actions.getOrganizationTags(oId);
    } catch (e) {
      throw e;
    }
  }

  changeRadio (e) {
    this.setState({
      userAction: e.target.value,
      tagIds: [],
    });
  }

  startCreatingTag () {
    const { onCreateTag } = this.props;
    this.setState({ isCreatingTag: true });

    if (onCreateTag) onCreateTag();
  }

  cancelCreatingTag () {
    const { onClose } = this.props;
    this.setState({ isCreatingTag: false });

    if (onClose) onClose();
  }

  cancelUpdatingTag () {
    const { onClose } = this.props;
    this.setState({
      updatingTag: null,
      isUpdatingTag: false,
    });

    if (onClose) onClose();
  }

  async removeTag () {
    try {
      const {
        organization, actions, reloadData, onClose,
      } = this.props;
      const { updatingTag } = this.state;
      const oId = organization.id;
      await actions.removeOrganizationTag(oId, updatingTag.id);
      await reloadData();
      await actions.getOrganizationTags(oId);
      this.setState({
        updatingTag: null,
        isUpdatingTag: false,
      });
      if (onClose) onClose();
    } catch (e) {
      throw e;
    }
  }

  async actionOnTags () {
    const { userAction, tagIds } = this.state;
    if (userAction === 'create_tag') {
      this.startCreatingTag();
    } else if (userAction === 'add_tag') {
      await this.addTagToContacts(tagIds);
    } else {
      await this.removeTagFromContacts(tagIds);
    }
  }

  render () {
    const {
      tags, admins, dropdownCss, templates,
    } = this.props;
    const {
      isCreatingTag,
      isUpdatingTag,
      updatingTag,
      userAction,
      tagIds,
    } = this.state;
    const disableConfirm = (userAction === 'add_tag' || userAction === 'remove_tag')
      && !tagIds.length;
    const TagsWithCheckboxs = tags.map(tag => (
      <TagWithCheckbox>
        <Checkbox
          customCss='margin-bottom: 0; width: 25px;'
          field={ tag.id }
          checked={ tags.selected }
          onChange={ value => this.onToggleTagCheckbox(tag.id, value) }
        />
        <Tag color={ tag.color } />
        <H5>{ tag.tag }</H5>
      </TagWithCheckbox>
    ));

    const templateOptions = templates.map(tag => ({
      label: tag.title,
      value: tag.id,
    }));

    const createTagFields = [
      {
        type: 'section',
        customCss: `
          border: 1px solid ${ theme.colors.lightGray };
          border-radius: 5px;
          margin: 20px 0;
        `,
        fields: [
          {
            type: 'custom',
            flex: '0 0 80px',
            customCss: `
            border-right: 1px solid ${ theme.colors.lightGray };
            text-align: center;
            padding-top: 80px;
          `,
            renderComponent: (value, state) => (
              <Tag size={ 40 } color={ state.color } />
            ),
          },
          {
            type: 'section',
            flex: '0 0 calc(100% - 80px)',
            customCss: 'padding-left: 20px;',
            fields: [
              {
                type: 'input',
                field: 'tag',
                label: 'Tag Name',
                required: true,
                getErrorMessage: (formState, value, label) => {
                  if (
                    value
                    && value.length
                    && tags.filter(tag => tag.tag === value).length === 0
                  ) {
                    return null;
                  }
                  return `Please enter a valid ${ label }`;
                },
              },
              {
                type: 'color',
                field: 'color',
                label: t('Tag Color'),
                default: theme.colors.lightGray,
                modal: true,
              },
            ],
          },
        ],
      },
      {
        field: 'set_alert',
        type: 'checkbox',
        label: 'Set Alert',
        default: false,
      },
      {
        type: 'input',
        number: true,
        field: 'alert_in_days',
        label: 'Alert in how many days?',
        condition: state => state.set_alert,
        placeholder: t('Immediate'),
      },
      {
        type: 'select',
        field: 'alert_recurrence',
        label: 'Repeat Alert?',
        options: REPEAT_ALERT_OPTIONS,
        searchable: false,
        condition: state => state.set_alert,
        required: true,
      },
      {
        type: 'rte',
        field: 'alert_message',
        label: 'Alert Message',
        condition: state => state.set_alert,
        required: true,
      },
      {
        type: 'multiSelect',
        field: 'admins_to_alert',
        label: 'Select Admins to alert',
        options: admins.map(m => ({ value: m.id, label: m.email })),
        condition: state => state.set_alert,
        required: true,
      },
      {
        field: 'contact_template_id',
        type: 'select',
        label: t('Email Template to Send to Tagged Contact (Optional)'),
        default: null,
        options: templateOptions,
        condition: state => state.set_alert,
      },
    ];

    const updateTagFields = [
      {
        type: 'section',
        customCss: `
          border: 1px solid #e1e8ed;
          border-radius: 5px;
          margin: 20px 0;
        `,
        fields: [
          {
            type: 'custom',
            flex: '0 0 80px',
            customCss: `
            border-right: 1px solid #e1e8ed;
            text-align: center;
            padding-top: 80px;
          `,
            renderComponent: (value, state) => (
              <Tag size={ 40 } color={ state.color } />
            ),
          },
          {
            type: 'section',
            flex: '0 0 calc(100% - 80px)',
            customCss: 'padding-left: 20px;',
            fields: [
              {
                type: 'input',
                field: 'tag',
                label: 'Tag Name',
                default: get(updatingTag, 'tag'),
                required: true,
                getErrorMessage: (formState, value, label) => {
                  const filteredTags = tags.filter(
                    tag => tag.id !== updatingTag.id,
                  );
                  if (
                    value
                    && value.length
                    && filteredTags.filter(tag => tag.tag === value).length === 0
                  ) {
                    return null;
                  }

                  return `Please enter a valid ${ label }`;
                },
              },
              {
                type: 'color',
                field: 'color',
                label: t('Tag Color'),
                default: get(updatingTag, 'color'),
                modal: true,
              },
            ],
          },
        ],
      },
      {
        field: 'set_alert',
        type: 'checkbox',
        label: 'Set Alert',
        default: !!get(updatingTag, 'alert_message'),
        condition: state => state.set_alert,
      },
      {
        type: 'input',
        number: true,
        field: 'alert_in_days',
        label: 'Alert in how many days?',
        default: get(updatingTag, 'alert_in_days'),
        condition: state => state.set_alert,
        placeholder: t('Immediate'),
      },
      {
        type: 'select',
        field: 'alert_recurrence',
        label: 'Repeat Alert?',
        options: REPEAT_ALERT_OPTIONS,
        default: get(updatingTag, 'alert_recurrence'),
        searchable: false,
        condition: state => state.set_alert,
        required: true,
      },
      {
        type: 'rte',
        field: 'alert_message',
        label: 'Alert Message',
        default: get(updatingTag, 'alert_message'),
        condition: state => state.set_alert,
        required: true,
      },
      {
        type: 'multiSelect',
        field: 'admins_to_alert',
        label: 'Select Admins to alert',
        options: admins.map(m => ({ value: m.id, label: m.email })),
        default: get(updatingTag, 'admins_to_alert'),
        condition: state => state.set_alert,
        required: true,
      },
      {
        field: 'contact_template_id',
        type: 'select',
        label: t('Email Template to Send to Tagged Contact (Optional)'),
        default: get(updatingTag, 'contact_template_id'),
        options: templateOptions,
        condition: state => state.set_alert,
      },
    ];

    return (
      <Dropdown
        customCss={ `
          top: 40px;
          ${ dropdownCss };
        ` }
      >
        <Container>
          <CustomRadioWrapper>
            <Radio
              field='add_tag'
              onChange={ e => this.changeRadio(e) }
              value='add_tag'
              checked={ userAction === 'add_tag' }
            />
            { t('Add tag to selected guests') }
          </CustomRadioWrapper>
          { tags.length && userAction === 'add_tag' ? (
            <TagWithCheckboxContainer>
              <p>Select tag</p>
              <TagScrollWrapper>{ TagsWithCheckboxs }</TagScrollWrapper>
            </TagWithCheckboxContainer>
          ) : null }
          <CustomRadioWrapper>
            <Radio
              field='remove_tag'
              onChange={ e => this.changeRadio(e) }
              value='remove_tag'
              checked={ userAction === 'remove_tag' }
            />
            { t('Remove tag to selected guests') }
          </CustomRadioWrapper>
          { tags.length && userAction === 'remove_tag' ? (
            <TagWithCheckboxContainer>
              <p>Select tag</p>
              <TagScrollWrapper>{ TagsWithCheckboxs }</TagScrollWrapper>
            </TagWithCheckboxContainer>
          ) : null }
          <CustomRadioWrapper>
            <Radio
              field='create_tag'
              onChange={ e => this.changeRadio(e) }
              value='create_tag'
              checked={ userAction === 'create_tag' }
            />
            { t('Create new tag for selected guests') }
          </CustomRadioWrapper>
          <ButtonWraper>
            <ConfirmButton
              fullHeight
              onClick={ () => this.actionOnTags() }
              disabled={ disableConfirm }
            >
              { t('Confirm') }
            </ConfirmButton>
          </ButtonWraper>
        </Container>
        <SettingModal
          medium
          isOpen={ isCreatingTag }
          title='Create New Tag'
          fields={ createTagFields }
          action={ async (formState) => {
            const {
              tag,
              color,
              set_alert,
              alert_in_days,
              alert_recurrence,
              alert_message,
              admins_to_alert,
              contact_template_id,
            } = formState;

            const createData = { tag, color };
            if (set_alert) {
              createData.alert_in_days = alert_in_days || 0;
              createData.alert_recurrence = alert_recurrence;
              createData.alert_message = alert_message;
              createData.admins_to_alert = uniq(admins_to_alert);
              createData.contact_template_id = contact_template_id;
            }

            const tagData = await this.createTag(createData, true);
            await this.addTagToContacts([tagData.id]);
          } }
          formCss='align-items: center;'
          onFinish={ this.cancelCreatingTag.bind(this) }
          onCancel={ this.cancelCreatingTag.bind(this) }
          saveButtonText='Create Tag'
        />
        <SettingModal
          medium
          isOpen={ isUpdatingTag }
          title='Update Tag'
          fields={ updateTagFields }
          action={ (formState) => {
            const {
              tag,
              color,
              set_alert,
              alert_in_days,
              alert_recurrence,
              alert_message,
              admins_to_alert,
              contact_template_id,
            } = formState;

            const updateData = {
              tag,
              color,
              alert_in_days: set_alert ? alert_in_days || 0 : null,
              alert_recurrence: set_alert ? alert_recurrence : null,
              alert_message: set_alert ? alert_message : null,
              admins_to_alert: set_alert ? admins_to_alert : [],
              contact_template_id: set_alert ? contact_template_id : null,
            };

            return this.updateTag(updateData);
          } }
          formCss='align-items: center;'
          onFinish={ this.cancelUpdatingTag.bind(this) }
          onCancel={ this.cancelUpdatingTag.bind(this) }
          onDelete={ this.removeTag.bind(this) }
          saveButtonText={ t('Update Tag') }
          deleteButtonText={ t('Delete Tag') }
        />
      </Dropdown>
    );
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(TagsDropdown);
