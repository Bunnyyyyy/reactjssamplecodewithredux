import React, { Component } from 'react';

import Modal from '../common/ModalV2';
import Form from '../common/inputs/Form';
import Button from '../common/Button';

import i18n from '../../../i18n';

import { validateFormFields } from '../../utils';

const t = a => i18n.t([`components/manager:${ a }`]);

const ActionButton = Button.extend`
  width: 103px;
  height: 30px;
  line-height: 30px;
  border-radius: 15px;
  font-size: 12px;
  text-transform: none;
  padding: 0;
  margin: 0 0 0 20px;

  ${ props => (props.disabled
    ? `
    cursor: default;
    box-shadow: none;
    background-color: ${ props.theme.colors.lightGray };
    color: white;

    &:hover, &:active {
      box-shadow: none;
      background-color: ${ props.theme.colors.lightGray };
      color: white;
    }
  `
    : '') };
`;

export default class SettingModal extends Component {
  constructor () {
    super();

    this.state = {
      formState: null,
      validateForm: false,
      resetForm: false,
    };
  }

  async onSave () {
    try {
      const { action, onFinish } = this.props;
      const { formState } = this.state;

      if (!action) return;
      if (!formState) {
        return this.setState({
          validateForm: true,
        });
      }

      await action(formState);
      if (onFinish) await onFinish();
      await this.onCancel();
    } catch (e) {
      throw e;
    }
  }

  async onDelete () {
    try {
      const { onDelete } = this.props;
      this.resetForm();
      if (onDelete) await onDelete();
    } catch (e) {
      throw e;
    }
  }

  async onCancel () {
    try {
      const { onCancel } = this.props;

      this.resetForm();
      if (onCancel) await onCancel();
    } catch (e) {
      throw e;
    }
  }

  setFormState (state) {
    const { fields } = this.props;

    const isValid = validateFormFields(state, fields);
    if (isValid) {
      this.setState({ formState: state });
    } else {
      this.setState({ formState: null });
    }
  }

  resetForm () {
    this.setState({ resetForm: true, validateForm: false }, () => {
      this.setState({ resetForm: false });
    });
  }

  render () {
    const {
      title,
      fields,
      validateForm,
      formCss,
      saveButtonText,
      deleteButtonText,
      onDelete,
      ...props
    } = this.props;
    const { formState, resetForm } = this.state;
    // eslint-disable-next-line
    const stateValidationForm = this.state.validateForm;

    const Buttons = [
      <ActionButton gray onClick={ this.onCancel.bind(this) }>
        Cancel
      </ActionButton>,
      <ActionButton
        light
        disabled={ !formState }
        onClick={ this.onSave.bind(this) }
      >
        { saveButtonText || t('Save') }
      </ActionButton>,
    ];
    if (onDelete) {
      Buttons.push(
        <ActionButton red onClick={ this.onDelete.bind(this) }>
          { deleteButtonText || t('Delete') }
        </ActionButton>,
      );
    }

    return (
      <Modal
        onClose={ this.onCancel.bind(this) }
        title={ title }
        headerCss={ `
          & h4 {
            font-size: 15px;
          }
        ` }
        footerContent={ Buttons }
        footerCss={ `
          box-shadow: none;
          display: flex;
          padding-top: 30px;
          padding-bottom: 30px;
          padding-right: 30px;
          justify-content: flex-end;
          @media (min-width: 901px) {
            height: auto;
          }
        ` }
        isOpen
        { ...props }
      >
        <Form
          validate={ validateForm || stateValidationForm }
          fields={ fields }
          bottomModal
          reset={ resetForm }
          onUpdate={ this.setFormState.bind(this) }
          customCss={ formCss }
        />
      </Modal>
    );
  }
}
