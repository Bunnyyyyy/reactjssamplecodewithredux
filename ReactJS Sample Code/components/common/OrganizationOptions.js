import React from 'react';
import styled from 'styled-components';

import Radio from './Radio';

const OptionsWrapper = styled.div`
  display: flex;
  flex-wrap: wrap;
  margin-bottom: 20px;
  div:nth-child(odd) {
    margin-right: 20px;
  }
`;

const RadioBox = styled.div`
  cursor: pointer;
  width: 240px;
  height: 44px;
  border-radius: 5px;
  border: ${ props => (props.checked ? `2px solid ${ props.theme.colors.mainBlue }` : `1px solid ${ props.theme.colors.lightBlue }`) };
  display: flex;
  align-items: center;
  padding-left: 10px;
  font-size: 12px;
  flex-wrap: wrap;
  margin-bottom: 10px;
`;

const OrgTypeImage = styled.img`
  width: 24px;
  height: 24px;
  margin-right: 10px;
`;

const Subtitle = styled.div`
  font-size: 12px;
  font-weight: normal;
  font-style: normal;
  font-stretch: normal;
  line-height: 1.5;
  letter-spacing: normal;
  margin-top: 30px;
  margin-bottom: 20px;
`;

export default (categories, formikProps) => {
  const {
    values, setFieldValue,
  } = formikProps;
  return (
    <div>
      <Subtitle>Organization type</Subtitle>
      <OptionsWrapper>
        {
      categories.map((c, i) => (
        <RadioBox
          key={ `radio-box-${ i }` }
          checked={ values.category_id === c.id }
          onClick={ () => {
            setFieldValue('category_id', c.id);
          } }
        >
          <Radio
            field='org_type'
            name='org_type'
            value={ c.id }
            checked={ values.category_id === c.id }
          />
          <OrgTypeImage src={ `../../../assets/manager/${ c.icon }.svg` } />
          { c.name }
        </RadioBox>
      ))
    }
      </OptionsWrapper>
    </div>
  );
};
