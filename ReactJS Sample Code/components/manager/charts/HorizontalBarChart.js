import React from 'react';
import styled from 'styled-components';
import { HorizontalBar, defaults } from 'react-chartjs-2';

import theme from './../../../utils/theme';
import i18n from '.././../../../i18n';
const t = a => i18n.t([`components/manager:${ a }`]);

defaults.global.responsive = true;

const ChartWrapper = styled.div`
  width: 100%;
  padding: 15px;
  background-color: white;
`;

const EmptyText = styled.p`
  font-size: 20px;
  line-height: 25px;
  text-align: center;
  padding: 30px 0;
`;

const CHART_OPTIONS = {
  title: {
    display: false
  },
  legend: {
    display: false
  },
  scales: {
    xAxes: [
      {
        ticks: {
          beginAtZero: true,
          // only show integer ticks
          callback: value => {
            if (Math.floor(value) === value) return value.toLocaleString();
          },
          fontFamily: '"Muli", sans-serif',
          fontColor: theme.colors.mainGray
        },
        gridLines: {
          display: false
        }
      }
    ],
    yAxes: [
      {
        ticks: {
          autoSkip: false,
          fontFamily: '"Muli", sans-serif',
          fontColor: theme.colors.mainGray
        },
        gridLines: {
          display: false
        },
        maxBarThickness: 20
      }
    ]
  }
};

const TOOLTIP_OPTIONS = {
  mode: 'index',
  intersect: false,
  titleFontFamily: '"Muli", sans-serif',
  titleFontColor: theme.colors.darkGray,
  titleSpacing: 0,
  titleMarginBottom: 0,
  backgroundColor: 'white',
  borderColor: theme.colors.lightGray,
  borderWidth: 1,
  xPadding: 20,
  yPadding: 10,
  callbacks: {
    title: (tooltipItem, data) => {
      const index = tooltipItem[0].index;
      return `${ data.labels[index] }: ${ tooltipItem[0].xLabel.toLocaleString() }`;
    },
    label: () => null
  }
};

const HorizontalBarChart = ({
  data = [],
  tooltipFormat,
  emptyText = t('No data yet.')
}) => {
  if (!data.length) return <EmptyText>{ emptyText }</EmptyText>;

  const labels = data.map(d => d.label || '');
  const values = data.map(d => d.value);

  const tooltipOptions =
    typeof tooltipFormat === 'function'
      ? Object.assign({}, TOOLTIP_OPTIONS, {
        callbacks: {
          title: (tooltipItem, data) => {
            const index = tooltipItem[0].index;
            const dataObj = {
              label: data.labels[index],
              value: tooltipItem[0].xLabel
            };

            return tooltipFormat(dataObj);
          },
          label: () => null
        }
      })
      : TOOLTIP_OPTIONS;

  return (
    <ChartWrapper>
      <HorizontalBar
        data={ {
          labels,
          datasets: [
            {
              data: values,
              backgroundColor: theme.colors.mainBlue
            }
          ]
        } }
        options={ {
          ...CHART_OPTIONS,
          tooltips: tooltipOptions
        } }
      />
    </ChartWrapper>
  );
};

export default HorizontalBarChart;
