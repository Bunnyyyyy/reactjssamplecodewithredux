import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import styled from 'styled-components';

import * as MeActionCreators from '../../redux/me';

import Dropdown from '../common/Dropdown';

import platformIcon from '../../assets/header/icon_platform.svg';
import userPlaceholder from '../../assets/common/placeholder_user.png';

import i18n from '../../../i18n';

const t = a => i18n.t([`components/header:${ a }`]);

const { FRONTEND_URL } = process.env;

const ProfileImage = styled.img`
  width: 36px;
  height: 36px;
  border-radius: 25px;
  box-shadow: 0 3px 6px 0 rgba(0, 0, 0, 0.1);
`;

const ProfileName = styled.p`
  font-size: 15px;
  line-height: 21px;
  font-weight: 700;
  margin-left: 15px;

  ${ props => (props.transparent
    ? `
    color: white;

    &:hover {
      color: white;
      opacity: 0.6;
    }
  `
    : '') };
`;

const ServiceLink = styled.div`
  cursor: pointer;
  font-size: 12px;
  line-height: 33px;
  font-weight: 700;

  ${ props => (props.active ? `color: ${ props.theme.colors.darkBlue };` : '') };
`;

const ServiceLinkIcon = styled.i`
  width: 20px;
  text-align: center;
  margin-right: 5px;
`;

const ServiceLinkImageWrapper = styled.div`
  display: inline-block;
  width: 20px;
  height: 20px;
  text-align: center;
  margin-right: 5px;

  & img {
    height: 12px;
  }
`;

const UserProfileDiv = styled.div`
  order: 1;
  display: flex;
  align-items: center;
  cursor: pointer;
  height: 56px;
  padding: 0 30px;

  &:hover {
    ${ ProfileImage } {
      box-shadow: 0 3px 6px 0 rgba(0, 0, 0, 0.2);
    }

    ${ ProfileName } {
      color: ${ props => props.theme.colors.darkBlue };
    }
  }

  @media (max-width: 900px) {
    display: none;
  }
`;

const mapStateToProps = state => ({
  user: state.me.user,
  organizations: state.manager.organizations,
});

const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators(MeActionCreators, dispatch),
});

class UserProfile extends Component {
  constructor () {
    super();

    this.state = {
      showingDropdown: false,
    };

    this.hideDropdownHandler = this.hideDropdown.bind(this);
  }

  componentDidMount () {
    document.addEventListener('mousedown', this.hideDropdownHandler, false);
  }

  componentWillUnmount () {
    document.removeEventListener('mousedown', this.hideDropdownHandler, false);
  }

  hideDropdown (e) {
    if (this.dropdownWrapper && this.dropdownWrapper.contains(e.target)) return;
    this.setState({ showingDropdown: false });
  }

  showDropdown () {
    this.setState({ showingDropdown: true });
  }

  logout () {
    const { actions } = this.props;
    actions.logout();
  }

  render () {
    const { user, transparent } = this.props;
    const { showingDropdown } = this.state;

    const ProfileDropdown = (
      <Dropdown
        refHandler={ wrapper => (this.dropdownWrapper = wrapper) }
        customCss='
          left: auto;
          right: 0;
          width: 300px;
          top: 56px;
          text-align: left;
          padding: 10px 15px 20px 15px;
        '
      >
        <a href={ `${ FRONTEND_URL }/me` }>
          <ServiceLink>
            <ServiceLinkIcon className='fas fa-user' aria-hidden='true' />
            { t('My Profile') }
          </ServiceLink>
        </a>
        <a href={ `${ FRONTEND_URL }/events` }>
          <ServiceLink>
            <ServiceLinkImageWrapper>
              <img alt='' src={ platformIcon } />
            </ServiceLinkImageWrapper>
            { t('Juven Platform') }
          </ServiceLink>
        </a>
        { /* <LanguageToggle loggedIn /> */ }
        <ServiceLink onClick={ this.logout.bind(this) }>
          <ServiceLinkIcon
            className='fas fa-sign-out-alt'
            aria-hidden='true'
          />
          { t('Logout') }
        </ServiceLink>
      </Dropdown>
    );

    const profileImgUrl = user.profile_image_url || userPlaceholder;

    return [
      <div>
        <UserProfileDiv
          transparent={ transparent }
          onClick={ this.showDropdown.bind(this) }
        >
          <ProfileImage src={ profileImgUrl } />
          <ProfileName transparent={ transparent }>{ user.first_name }</ProfileName>
        </UserProfileDiv>
        { showingDropdown ? ProfileDropdown : null }
      </div>,
    ];
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(UserProfile);
