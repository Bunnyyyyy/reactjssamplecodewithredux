import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import styled, { ThemeProvider } from 'styled-components';
import { DragDropContext } from 'react-dnd';
import HTML5Backend from 'react-dnd-html5-backend';

import * as ManagerActionCreators from '../../../redux/manager';
import * as OrganizationActionCreators from '../../../redux/organization';
import * as EmailTemplatesActionCreators from '../../../redux/emailTemplates';
import ConfirmModal from '../../common/ConfirmModal';
import SaveActions from '../SaveActions';
import NewElementDragger from './NewElementDragger';
import Element from './Element';
import ElementTarget from './ElementTarget';
import Checkbox from '../../common/Checkbox';
import { transform } from '../../../utils/styles';
import i18n from '../../../../i18n';

const t = a => i18n.t([`components/manager:${ a }`]);

const ELEMENTS_BUILDER_ELEMENTS = 'ELEMENTS_BUILDER_ELEMENTS';

const BuilderDiv = styled.div`
  height: calc(
    100vh - ${ props => (props.theme.withSubheader ? '100px' : '56px') }
  );
  padding-left: 300px;
`;

const LeftWrapper = styled.div`
  position: absolute;
  top: 0;
  left: 0;
  height: calc(
    100vh - ${ props => (props.theme.withSubheader ? '100px' : '56px') }
  );
  width: 300px;
  overflow: hidden scroll;
  z-index: 100;
  background-color: white;
  box-shadow: 0 3px 6px 0 rgba(0, 0, 0, 0.16);
`;

const LeftInnerWrapper = styled.div`
  padding: 0 0 80px 0;
  overflow: hidden;
`;

const RightWrapper = styled.div`
  position: relative;
  width: 100%;
`;

const SaveActionsContainer = styled.div`
  background-color: white;
  text-align: right;
  padding: 0 15px;

  ${ props => props.customCss || '' };
`;

const ElementsWrapper = styled.div`
  background-color: ${ props => props.theme.colors.lightGray };
  height: calc(
    100vh - ${ props => (props.theme.withSubheader ? '100px' : '56px') }
  );
  overflow-y: scroll;
`;

const ElementsInnerWrapper = styled.div`
  position: relative;
  padding: ${ props => (props.noSave ? '30px' : '80px') } 20px 160px 20px;
  ${ props => (props.empty ? 'padding-bottom: 0;' : '') };
`;

const ElementTargetWrapper = styled.div`
  position: relative;

  ${ props => (props.empty
    ? `
    position: absolute;
    top: 0;
    left: 0;
    width: 100%;
    height: calc(100vh - ${ props.theme.newObject ? '190px' : '240px' });
    z-index: 300;
  `
    : '') };
`;

const EmptyWrapper = styled.div`
  background-color: ${ props => props.theme.colors.darkBlue };
  width: 100%;
  height: calc(100vh - ${ props => (props.theme.newObject ? '190px' : '240px') });
  padding: 30px;
`;
const StandardFieldContainer = styled.div`
 padding: 20px 0px;
 padding: 20px 20px;
 border-bottom: 1px solid #e1e8ed;
 position : relative;
 >p{
  font-size: 12px;
  font-weight: bold;
 }
 >span{
  font-size: 12px;
  color : #9c9c9c;
 }
 >i{
  position: absolute;
  right: 20px;
  top: 20px;
  color : #9c9c9c;
 }

`;
const ElementLeftDots = styled.div`
  position: absolute;
  top: 0;
  left: -30px;
  height: 100%;
  border-left: 5px dotted ${ props => props.theme.colors.mainBlue };
  opacity: 0.5;
`;

const EmptyInnerWrapper = styled.div`
  position: relative;
  width: 100%;
  height: 100%;
  padding: 0 20px;
  border: 2px dashed white;
`;

const EmptyText = styled.div`
  position: relative;
  max-width: 600px;
  top: 50%;
  text-align: center;
  margin: 0 auto;
  ${ transform('translateY(-50%)') } h3, h4 {
    color: white;
  }

  h3 {
    margin-bottom: 20px;
  }

  h4 {
    font-weight: 400;
  }
`;

const ConditionalFieldList = styled.ul`
  padding-left: 20px;
  margin: 20px 0;
  list-style-type: circle;
`;
const StandardFieldWrapper = styled.div`
border-bottom: 1px solid #e1e8ed;
padding: 20px;
`;
const PaddingWrapper = styled.div`
padding: 0 20px;
`;
const DeviderWithText = styled.hr`
line-height: 1em;
position: relative;
outline: 0;
border: 0;
color: black;
text-align: center;
height: 1.5em;
opacity: .5;
margin-bottom:40px;
&:before {

  content: '';
  // use the linear-gradient for the fading effect
  // use a solid background color for a solid bar
  background: #bcd4e0;
  position: absolute;
  left: 0;
  top: 50%;
  width: 100%;
  height: 2px;
}
&:after {
  content: attr(data-content);
  position: relative;
  display: inline-block;
  color: black;
  padding: 0 .5em;
  line-height: 1.5em;
  color: #818078;
  background-color: #e1e8ed;
}

`;

const CustomFieldPlaceholder = styled.p`
height: 91px;
line-height : 91px;
font-size : 12px;
border : 2px dashed #bcd4e0;
border-radius : 2px;
text-align : center;
color : #7d909a;
position: relative;
margin-left: 40px;

`;
// TODO: use better cloning
function getDefaultElements (oldElements) {
  const elements = [];
  if (oldElements) {
    oldElements.forEach((e) => {
      const newElement = Object.assign({}, e);
      if (newElement.condition_field_id) {
        newElement.conditional = true;
      }
      elements.push(newElement);
    });
  }

  return elements;
}

const ActionCreators = Object.assign(
  {},
  ManagerActionCreators,
  OrganizationActionCreators,
  EmailTemplatesActionCreators,
);

const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators(ActionCreators, dispatch),
});

class ElementsBuilder extends Component {
  constructor (props) {
    super(props);
    const { elements, Standard_Elements_Types, standardElements } = props;
    this.state = {
      elements: getDefaultElements(elements),
      newId: 0,
      focusedElement: null,
      elementsWrapper: null,
      removingField: [],
      removingConditionalFields: [],
      showStandardField: false,
      showCustomField: false,
      standardElements: getDefaultElements(standardElements),
      StandardElementsTypes: Standard_Elements_Types,
    };
  }

  componentDidMount () {
    // this.updateSaveActions();

    if (this.elementsWrapper) {
      this.setState({
        elementsWrapper: this.elementsWrapper,
      });
    }
  }

  componentWillReceiveProps (nextProps) {
    const { elements } = this.props;
    if (elements === nextProps.elements) return;

    this.setState({
      elements: getDefaultElements(nextProps.elements),
    });
  }

  onCancelSaving () {
    const {
      actions, newObject, elements, onCancel,
    } = this.props;
    actions.resetSaveActions();

    if (!newObject) {
      this.setState({
        elements: getDefaultElements(elements),
        newId: 0,
        focusedElement: null,
      });
    }

    if (onCancel) onCancel();
  }

  onDropNewElement (element, order) {
    if (element && !element.standard_field_name) {
      const { elements } = this.state;
      // HACK: to keep key of the new elements
      let { newId } = this.state;
      const newElement = {
        newId,
        order,
        ...element,
      };
      delete newElement.label;
      elements.splice(order, 0, newElement);
      newId++;

      this.setState({ elements, newId }, async () => {
        await this.updateElementOrders();
        this.updateSaveActions();
      });
    }
  }

  onDropNewElementStandard (element, order) {
    if (element && element.standard_field_name) {
      const { standardElements } = this.state;
      // HACK: to keep key of the new standardElements
      let { newId } = this.state;
      const newElement = {
        newId,
        order,
        ...element,
      };
      delete newElement.label;
      standardElements.splice(order, 0, newElement);
      newId++;

      this.setState({ standardElements, newId }, async () => {
        await this.updateStandardElementOrders();
        this.updateSaveActions();
      });
    }
  }

  onDropExistingElement (element, oldOrder, order) {
    if (element && !element.standard_field_name) {
      const { elements } = this.state;
      elements.splice(oldOrder, 1);
      elements.splice(order, 0, element);
      this.setState({ elements }, async () => {
        await this.updateElementOrders();
        this.updateSaveActions();
      });
    }
  }

  onDropExistingStandardElement (element, oldOrder, order) {
    if (element && element.standard_field_name && order) {
      const { standardElements } = this.state;
      standardElements.splice(oldOrder, 1);
      standardElements.splice(order, 0, element);

      this.setState({ standardElements }, async () => {
        await this.updateStandardElementOrders();
        this.updateSaveActions();
      });
    }
  }

  onUpdateElementField (order, field, value) {
    const { elements } = this.state;
    const updatedElement = elements[order];
    if (updatedElement) updatedElement[field] = value;

    this.setState({ elements }, this.updateSaveActions);
  }

  onUpdateStandardElementField (order, field, value) {
    const { standardElements } = this.state;
    const updatedElement = standardElements[order];
    if (updatedElement) updatedElement[field] = value;

    this.setState({ standardElements }, this.updateSaveActions);
  }

  onRemoveElement (order) {
    const { elements } = this.state;
    const removingConditionalFields = [];
    elements.forEach((field, i) => {
      if (field.condition_field_id && field.condition_field_id === elements[order].id && i !== order) {
        removingConditionalFields.push(field);
      }
    });
    if (removingConditionalFields.length) {
      this.setState({
        removingField: elements[order],
        removingConditionalFields,
      });
    } else {
      elements.splice(order, 1);
      this.setState({ elements }, async () => {
        await this.updateElementOrders();
        this.updateSaveActions();
      });
    }
  }

  onRemoveStandardElement (order) {
    const { standardElements, StandardElementsTypes } = this.state;
    const removingConditionalFields = [];
    standardElements.forEach((field, i) => {
      if (field.condition_field_id && field.condition_field_id === standardElements[order].id && i !== order) {
        removingConditionalFields.push(field);
      }
    });
    let indexUnCheck = 0;
    StandardElementsTypes.forEach((ele, index) => {
      if (ele.standard_field_name === standardElements[order].standard_field_name) {
        indexUnCheck = index;
      }
    });
    StandardElementsTypes[indexUnCheck].checked = false;
    this.setState({
      StandardElementsTypes,
    });
    if (removingConditionalFields.length) {
      this.setState({
        removingField: standardElements[order],
        removingConditionalFields,
      });
    } else {
      standardElements.splice(order, 1);
      this.setState({ standardElements }, async () => {
        await this.updateStandardElementOrders();
        this.updateSaveActions();
      });
    }
  }

  async onFinishSaving () {
    try {
      const { actions, onFinish } = this.props;
      actions.resetSaveActions();

      if (onFinish) await onFinish();
    } catch (e) {
      throw e;
    }
  }

  setFocusedElement (eleId) {
    this.setState({
      focusedElement: eleId,
    });
  }


  updateElementOrders () {
    return new Promise((resolve) => {
      const { elements } = this.state;
      elements.forEach((e, i) => (e.order = i));

      this.setState({ elements }, resolve);
    });
  }

  updateStandardElementOrders () {
    return new Promise((resolve) => {
      const { standardElements } = this.state;
      standardElements.forEach((e, i) => (e.order = i));

      this.setState({ standardElements }, resolve);
    });
  }

  updateSaveActions () {
    const {
      actions,
      newObject,
      elementTypes,
      createElementAction = () => { },
      updateElementAction = () => { },
      removeElementAction = () => { },
    } = this.props;
    const { elements, standardElements } = this.state;
    actions.removeSaveAction(ELEMENTS_BUILDER_ELEMENTS);

    const updatedElements = [...elements, ...standardElements];
    updatedElements.forEach((e) => {
      const data = Object.assign({}, e);
      if (data.conditional) {
        delete data.conditional;
      } else {
        data.condition_field_id = null;
        data.condition_field_values = null;
      }

      const matchingType = elementTypes.find(et => (
        (et.type && et.type === e.type)
        || (et.display_type && et.display_type === e.display_type)
        || (et.data_type
          && et.data_type === e.data_type
          && !!et.options === !!e.options
          && et.multiple_options === e.multiple_options)
      ));

      if (
        matchingType
        && typeof matchingType.validate === 'function'
        && !matchingType.validate(e)
      ) {
        // invalid element
        return actions.addSaveAction(
          null,
          ELEMENTS_BUILDER_ELEMENTS,
          e.id || `new-${ e.newId }`,
          1,
        );
      }

      if (data.type === 'image' || data.display_type === 'image') {
        const formData = new FormData();
        formData.append('image', data.file);
        if (data.type) formData.append('type', data.type);
        if (data.display_type) {
          formData.append('display_type', data.display_type);
        }
        formData.append('id', data.id);
        formData.append('order', data.order);

        const elementAction = e.id
          ? updateElementAction.bind(this, data.id, formData)
          : createElementAction.bind(this, formData);

        actions.addSaveAction(
          elementAction,
          ELEMENTS_BUILDER_ELEMENTS,
          e.id || `new-${ e.newId }`,
          1,
        );
      } else {
        const actionData = Object.assign({}, data);
        delete actionData.validate;

        const elementAction = e.id
          ? updateElementAction.bind(this, e.id, actionData)
          : createElementAction.bind(this, actionData);

        actions.addSaveAction(
          elementAction,
          ELEMENTS_BUILDER_ELEMENTS,
          e.id || `new-${ e.newId }`,
          1,
        );
      }
    });

    // for removing existing elements
    if (!newObject) {
      // eslint-disable-next-line
      const oldElements = this.props.elements;
      oldElements
        .filter(oldE => !updatedElements.find(e => e.id === oldE.id))
        .forEach((e) => {
          actions.addSaveAction(
            removeElementAction.bind(this, e.id),
            ELEMENTS_BUILDER_ELEMENTS,
            e.id,
            1,
          );
        });
    }
  }

  closeConfirm () {
    this.setState({
      removingField: null,
      removingConditionalFields: [],
    });
  }

  toggleStandardField () {
    const { showStandardField } = this.state;
    this.setState({
      showStandardField: !showStandardField,
    });
  }

  toggleCustomField () {
    const { showCustomField } = this.state;
    this.setState({
      showCustomField: !showCustomField,
    });
  }

  checkStandardField (value, i, et) {
    let indexToRemove = 0;
    const { StandardElementsTypes, standardElements } = this.state;
    let { newId } = this.state;
    const order = standardElements.length;

    StandardElementsTypes[i].checked = value;
    if (value) {
      const newElement = {
        newId,
        order,
        ...StandardElementsTypes[i],
      };
      delete newElement.checked;
      // delete newElement.label;
      standardElements.push(newElement);
      newId++;
    } else {
      standardElements.forEach((ele, index) => {
        if (ele.display_type === et.display_type) {
          indexToRemove = index;
        }
      });
      standardElements.splice(indexToRemove, 1);
    }

    this.setState({
      StandardElementsTypes, standardElements, newId,
    }, async () => {
      this.updateSaveActions();
    });
  }

  render () {
    const {
      isEmail,
      elementTypes = [],
      emptyText,
      newObject = false,
      withSubheader,
      saveActionsCss,
      noSaveActions,
      LeftComponents = null,
      extraElement,
    } = this.props;
    const {
      elements = [], focusedElement, elementsWrapper,
      removingField, standardElements, removingConditionalFields, showStandardField, showCustomField, StandardElementsTypes,
    } = this.state;
    const removingFieldMessage = removingField ? [
      <p>
        { `${ t('The following fields have conditional logic that is dependent on the field') } ` }
        <b>{ removingField.label }</b>
        { ':' }
      </p>,
      <ConditionalFieldList>
        { removingConditionalFields.map(field => <li>{ field.label }</li>) }
      </ConditionalFieldList>,
      <p>{ t('You must remove all associated conditional logic first before you can remove this field.') }</p>,
    ] : null;
    const NewElementDraggers = elementTypes
      .filter(et => !et.hidden)
      .map((et, i) => (
        <NewElementDragger
          key={ `type-dragger-${ i }` }
          element={ et }
          elementsWrapper={ elementsWrapper }
        />
      ));
    const StandardElementsDragger = StandardElementsTypes && StandardElementsTypes
      .filter(et => !et.hidden)
      .map((et, i) => (
        <Checkbox
          name={ `standard_field_${ i }` }
          checked={ et.checked }
          onChange={ value => this.checkStandardField(value, i, et) }
          field={ et.checked }
          label={ et.label }
        />
      ));

    const Elements = elements.map((e, i) => {
      const eId = e.id || `new-${ e.newId }`;

      return (
        <ElementTargetWrapper key={ `elements-${ eId }` }>
          <Element
            isEmail={ isEmail }
            key={ `elements-${ eId }` }
            order={ i }
            elements={ elements }
            element={ e }
            focused={ focusedElement === eId }
            onSetFocusedElement={ this.setFocusedElement.bind(this) }
            onUpdateField={ this.onUpdateElementField.bind(this) }
            onRemoveElement={ this.onRemoveElement.bind(this) }
          />
          <ElementTarget
            onDropNewElement={ this.onDropNewElement.bind(this) }
            onDropExistingElement={ this.onDropExistingElement.bind(this) }
            order={ i + 1 }
          />
        </ElementTargetWrapper>
      );
    });
    const StandardElements = standardElements && standardElements.map((e, i) => {
      const eId = e.id || `new-${ e.newId }`;

      return (
        <ElementTargetWrapper key={ `elements-${ eId }-standard` }>
          <Element
            isEmail={ isEmail }
            key={ `elements-${ eId }-standard` }
            order={ i }
            elements={ elements }
            element={ e }
            focused={ focusedElement === eId }
            onSetFocusedElement={ this.setFocusedElement.bind(this) }
            onUpdateField={ this.onUpdateStandardElementField.bind(this) }
            onRemoveElement={ this.onRemoveStandardElement.bind(this) }
          />
          <ElementTarget
            onDropNewElement={ this.onDropNewElementStandard.bind(this) }
            onDropExistingElement={ this.onDropExistingStandardElement.bind(this) }
            order={ i + 1 }
          />
        </ElementTargetWrapper>
      );
    });
    return (
      <ThemeProvider theme={ { newObject, withSubheader } }>
        <BuilderDiv>
          <LeftWrapper>
            <LeftInnerWrapper>
              <PaddingWrapper>
                { extraElement }
              </PaddingWrapper>
              <PaddingWrapper>
                { LeftComponents }
              </PaddingWrapper>
              { StandardElementsTypes ? (
                <StandardFieldContainer>
                  <p>Standard fields</p>
                  <span>Placed in Standard fields only</span>
                  <i className='fa fa-chevron-down' aria-hidden='true' onClick={ this.toggleStandardField.bind(this) } />
                </StandardFieldContainer>
              ) : null }
              { showStandardField ? (
                <StandardFieldWrapper>
                  { StandardElementsDragger }
                </StandardFieldWrapper>
              ) : null }
              <StandardFieldContainer>
                <p>Custom fields</p>
                <span>Placed in custom fields only</span>
                <i className='fa fa-chevron-down' aria-hidden='true' onClick={ this.toggleCustomField.bind(this) } />
              </StandardFieldContainer>
              { showCustomField ? (
                <StandardFieldWrapper>
                  { NewElementDraggers }
                </StandardFieldWrapper>
              ) : null }
            </LeftInnerWrapper>
          </LeftWrapper>
          <RightWrapper>
            { !noSaveActions ? (
              <SaveActionsContainer customCss={ saveActionsCss }>
                <SaveActions
                  onFinish={ this.onFinishSaving.bind(this) }
                  onCancel={ this.onCancelSaving.bind(this) }
                  saveNewItem={ newObject }
                />
              </SaveActionsContainer>
            ) : null }
            <ElementsWrapper
              innerRef={ wrapper => (this.elementsWrapper = wrapper) }
            >

              <ElementsInnerWrapper
                empty={ !Elements.length }
                noSave={ noSaveActions }
              >
                { StandardElementsTypes ? <DeviderWithText data-content='Standard fields' /> : null }
                { !Elements.length && (!standardElements || standardElements.length === 0) ? (
                  <EmptyWrapper>
                    <EmptyInnerWrapper>
                      <EmptyText>
                        <h3>Drag element here</h3>
                        <h4>{ emptyText || '' }</h4>
                      </EmptyText>
                    </EmptyInnerWrapper>
                  </EmptyWrapper>
                ) : null }
                <ElementTargetWrapper
                  key='elements-new-standard'
                  empty={ !Elements.length && (!standardElements || standardElements.length === 0) }
                >
                  <ElementTarget
                    onDropNewElement={ this.onDropNewElementStandard.bind(this) }
                    onDropExistingElement={ this.onDropExistingStandardElement.bind(
                      this,
                    ) }
                    empty={ !Elements.length && (!standardElements || standardElements.length === 0) }
                    order={ 0 }
                  />
                </ElementTargetWrapper>
                { StandardElements }
                { StandardElementsTypes && <DeviderWithText data-content='Custom fields' /> }
                { StandardElementsTypes && Elements.length === 0 ? (
                  <CustomFieldPlaceholder>
Place elements from custom fields here
                    <ElementLeftDots />
                  </CustomFieldPlaceholder>
                ) : null }
                <ElementTargetWrapper
                  key='elements-new'
                  empty={ !Elements.length && (!standardElements || standardElements.length === 0) }
                >
                  <ElementTarget
                    onDropNewElement={ this.onDropNewElement.bind(this) }
                    onDropExistingElement={ this.onDropExistingElement.bind(
                      this,
                    ) }
                    empty={ !Elements.length && (!standardElements || standardElements.length === 0) }
                    order={ 0 }
                  />
                </ElementTargetWrapper>
                { Elements }
              </ElementsInnerWrapper>
            </ElementsWrapper>
          </RightWrapper>
          { removingField && removingConditionalFields.length ? (
            <ConfirmModal
              message={ removingFieldMessage }
              onConfirm={ this.closeConfirm.bind(this) }
              title={ t('You cannot remove this element yet') }
              confirmButtonText={ t('Okay') }
            />
          ) : null }
        </BuilderDiv>
      </ThemeProvider>
    );
  }
}

export default connect(null, mapDispatchToProps)(
  DragDropContext(HTML5Backend)(ElementsBuilder),
);
