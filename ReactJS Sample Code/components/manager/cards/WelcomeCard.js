import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import styled from 'styled-components';

import Card from './Card';
import * as OrganizationActionCreators from '../../../redux/organization';
import * as ManagerActionCreators from '../../../redux/manager';
import * as UiActionCreators from '../../../redux/ui';

import circleCheck from '../../../assets/manager/check-circle.svg';
import paymentLogo from '../../../assets/common/actions/payments.svg';
import contactsLogo from '../../../assets/common/actions/contacts.svg';

import { t as translate } from '../../../../i18n';

const t = a => translate(a, 'manager/home/index');

const CardWrapper = styled.div`
  padding: 30px;
`;

const Title = styled.div`
  h4 {
    text-align: center;
  }

  p {
    text-align: center;
    font-size: 12px;
  }
`;

const Wrapper = styled.div`
  display: flex;
  margin-top: 30px;
`;

const ImageWrapper = styled.img`
  width: 100px;
`;

const CardTitle = styled.h5`
  margin-bottom: 5px;
`;

const CardItemWrapper = styled.div`
  min-height: 90px;
  flex-basis: calc(50% - 15px);
  margin-right: 30px;
  padding: 20px 30px;

  &:nth-child(2n) {
    margin-right: 0;
  }

  border-radius: 5px;
  border: solid 1px ${ props => props.theme.colors.lightGray };
  display: flex;
  align-items: center;
  cursor: pointer;

  &:hover {
    box-shadow: 0 0 6px 0 rgba(0, 0, 0, 0.2);
  }

  img {
    height: 100px;
    margin-right: 15px;
  }
`;

const CardDoneWrapper = styled(CardItemWrapper)`
  cursor: auto;

  &:hover {
    box-shadow: none;
  }

  img {
    width: 50px;
  }
`;

const ActionCreators = Object.assign(
  {},
  ManagerActionCreators,
  OrganizationActionCreators,
  UiActionCreators,
);

const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators(ActionCreators, dispatch),
});

const mapStateToProps = state => ({
  organization: state.manager.organization,
});

const { API_URL } = process.env;

const CardItem = ({
  title, src, description, onClick,
}) => (
  <CardItemWrapper onClick={ onClick }>
    <ImageWrapper src={ src } />
    <div>
      <CardTitle>{ title }</CardTitle>
      <p>{ description }</p>
    </div>
  </CardItemWrapper>
);

const CardDone = ({ title }) => (
  <CardDoneWrapper>
    <img alt={ t('Done') } src={ circleCheck } />
    <div>
      <h5>{ title }</h5>
    </div>
  </CardDoneWrapper>
);

class WelcomeCard extends Component {
  connectStripe () {
    const { actions, organization } = this.props;
    const oId = organization.id;

    const psuedoRequestUrl = '/custom/connect-stripe';

    actions.setRequestLoading(psuedoRequestUrl, true);
    const authorizeStripeUrl = `${ API_URL }/organizations/${ oId }/stripe_account/authorize`;
    const windowRef = window.open(
      authorizeStripeUrl,
      'stripe',
      'height=600,width=768',
    );

    this.checkClose = setInterval(async () => {
      if (windowRef.closed) {
        clearInterval(this.checkClose);
        actions.setRequestLoading(psuedoRequestUrl, false);
      } else if (windowRef.location.href === `${ API_URL }/stripe/success`) {
        clearInterval(this.checkClose);
        windowRef.close();

        await Promise.all([
          actions.getOrganizationStripeAccount(oId),
          actions.getOrganization(oId),
        ]);

        actions.setRequestLoading(psuedoRequestUrl, false);
      }
    }, 1000);
  }

  render () {
    const { organization } = this.props;

    const cardDone = organization.payment_ready;
    const organizationDone = organization.name
      && organization.logo_image_url
      && organization.description;

    if (!organization.id || (cardDone && organizationDone)) {
      return null;
    }

    return (
      <Card>
        <CardWrapper>
          <Title>
            <h4>{ t('Welcome to Juven') }</h4>
          </Title>
          <Wrapper>
            { cardDone ? (
              <CardDone title={ t('Stripe account connected!') } />
            ) : (
              <CardItem
                title={ t('Receive payments with Stripe') }
                description={ t(
                  'Accept credit card payments in a few clicks with our trusted global partner.',
                ) }
                src={ paymentLogo }
                onClick={ this.connectStripe.bind(this) }
              />
            ) }
            { organizationDone ? (
              <CardDone title={ t('Organization profile completed!') } />
            ) : (
              <Link to={ `/${ organization.id }/settings` }>
                <CardItem
                  title={ t('Complete organization profile') }
                  description={ t(
                    'Add a banner image, logo and your organization contact details.',
                  ) }
                  src={ contactsLogo }
                />
              </Link>
            ) }
          </Wrapper>
        </CardWrapper>
      </Card>
    );
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(WelcomeCard);
