import React from 'react';

import NavBar from '../../common/NavBar';
import i18n from '../../../../i18n';

const t = a => i18n.t([`components/manager:${ a }`]);

const EventNavBar = ({ organization, event, inline }) => {
  const navLinks = [
    {
      title: t('Overview'),
      route: `/${ organization.id }/events/${ event.id }`,
      exact: true,
    },
    {
      title: t('Event Info'),
      route: `/${ organization.id }/events/${ event.id }/info`,
    },
    {
      title: t('Ticketing'),
      route: `/${ organization.id }/events/${ event.id }/ticketing/types`,
      baseRoute: `/${ organization.id }/events/${ event.id }/ticketing`,
    },
    {
      title: t('Guests'),
      route: `/${ organization.id }/events/${ event.id }/guests`,
    },
    {
      title: t('Helpers'),
      route: `/${ organization.id }/events/${ event.id }/helpers`,
    },
  ];

  // if (event.premium) {
  //   navLinks.push({
  //     title: t('Live Interactions'),
  //     route: `/${ organization.id }/events/${ event.id }/live-interactions`,
  //   });
  // }

  return (
    <NavBar inline={ inline } links={ navLinks } />
  );
};

export default EventNavBar;
