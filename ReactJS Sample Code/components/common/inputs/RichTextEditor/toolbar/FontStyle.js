import React from 'react';
import styled from 'styled-components';
import { components } from 'react-select';

import Select from '../../Select';
import {
  P,
  H5,
  // QUOTE,
} from '../constants';

const SortIcon = styled.i`
  font-size: 12px;
`;

const { Option } = components;

const DropdownIndicator = props => components.DropdownIndicator && (
  <components.DropdownIndicator { ...props }>
    <SortIcon className='fas fa-sort' />
  </components.DropdownIndicator>
);

const css = `
  min-width: auto;
  flex: 0 0 100px;
  height: 44px;
  line-height: 44px;
  padding: 0 20px;
  width: 145px;

  margin-top: 0;
  margin-bottom: 0;

  & .select {
    &__placeholder,
    &__single-value,
    &__option {
      font-size: 13px;
      font-weight: 700;
    }

    &__dropdown-indicator {
      padding: 0;
    }
  }
`;

const options = [
  {
    label: 'Heading',
    value: H5,
  },
  {
    label: 'Paragraph',
    value: P,
  },
  // {
  //   label: 'Quote',
  //   value: QUOTE,
  // },
];

function FontStyle ({ onChange, selected }) {
  const option = options.find(v => selected(v.value));
  return (
    <Select
      plain
      customCss={ css }
      clearable={ false }
      required={ false }
      searchable={ false }
      options={ options }
      value={ option ? option.value : P }
      components={ {
        Option: (props) => {
          const { data } = props;
          return <Option { ...props }>{ data.label }</Option>;
        },
        DropdownIndicator,
      } }
      onChange={ type => onChange(type) }
      onBlur={ false }
    />
  );
}

export default FontStyle;
