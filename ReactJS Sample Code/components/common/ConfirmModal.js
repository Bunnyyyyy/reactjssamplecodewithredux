import React from 'react';
import styled from 'styled-components';

import Modal from './ModalV2';
import Button from './Button';

import i18n from '../../../i18n';

const t = a => i18n.t([`components/common:${ a }`]);

const Message = styled.p`
  margin-bottom: 10px;
`;

const ConfirmButton = styled(Button)`
  margin-right: 15px;
`;

const ActionButtons = styled.div`
  text-align: right;
`;

const ConfirmModal = ({
  message, title, onConfirm, onCancel, warning, confirmButtonText,
}) => (
  <Modal
    isOpen
    medium
    onClose={ onCancel || onConfirm }
    title={ title || (warning ? t('Are you sure?') : t('Please Confirm')) }
    bodyCss='padding-bottom: 10px;'
  >
    <Message>{ message || '' }</Message>
    <ActionButtons>
      { onCancel
        ? (
          <ConfirmButton small light gray onClick={ onCancel }>
            { t('Cancel') }
          </ConfirmButton>
        ) : '' }
      { onConfirm ? (
        <ConfirmButton light red={ !!warning } small onClick={ onConfirm }>
          { confirmButtonText || `${ t('Confirm') }` }
        </ConfirmButton>
      ) : '' }
    </ActionButtons>
  </Modal>
);

export default ConfirmModal;
