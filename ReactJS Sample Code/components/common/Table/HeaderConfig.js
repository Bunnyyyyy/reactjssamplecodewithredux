import React, { Component } from 'react';
import styled from 'styled-components';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import Checkbox from '../Checkbox';
import { boxShadow, nowrap } from '../../../utils/styles';

import * as ConfigActionCreators from '../../../redux/config';

import i18n from '../../../../i18n';

const t = a => i18n.t([`components/common:${ a }`]);

const Container = styled.div`
  display: flex;
  position: absolute;
  background-color: white;
  height: auto;
  top: 42px;
  z-index: 1;
  right: 0;
  text-align: left;
  padding: 20px 0;
  ${ boxShadow('0 1px 6px 0 rgba(0, 0, 0, 0.16)') };
`;

const CogIcon = styled.i`
  width: 24px;
  height: 24px;
  line-height: 24px;
  color: ${ props => props.theme.colors.mainGray };
`;

const CogIconWrapper = styled.div`
  display: inline-block;
  cursor: pointer;
  width: 36px;
  padding: 6px;

  &:hover {
    ${ CogIcon } {
      color: ${ props => props.theme.colors.mainBlue };
    }
  }
`;

const Wrapper = styled.div`
  position: relative;
  width: 100%;
  height: 48px;
  text-align: center;
  padding: 6px 0;
`;

const ColumnsWrapper = styled.div`
  flex: 1 0 0;
  padding: 0 20px;
  border-right: 1px solid ${ props => props.theme.colors.lightGray };

  &:last-child {
    border-right: none;
  }
`;

const ColumnsTitle = styled.h6`
  font-weight: 400;
  color: #9c9c9c;
`;

const ColumnLabel = styled.p`
  font-size: 12px;
  line-height: 1.5;
  flex-basis: 140px;
  padding-right: 10px;
  ${ nowrap };
`;

const ColumnToggle = styled.div`
  display: flex;
  align-items: center;
  width: 160px;
  margin: 7.5px 0;
`;

const mapStateToProps = state => ({
  tableConfigs: state.config.tableConfigs,
});

const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators(ConfigActionCreators, dispatch),
});

function getColumnField (column) {
  if (column.dataField) {
    return `data-field-${ column.field }`;
  } if (column.customField) {
    return `custom-field-${ column.field }`;
  }
  return column.field;
}

class HeaderConfig extends Component {
  constructor (props) {
    super(props);

    this.state = {
      showingContainer: false,
    };

    this.hideContainerHandler = this.hideContainer.bind(this);
  }

  componentDidMount () {
    document.addEventListener('mousedown', this.hideContainerHandler, false);
  }

  componentWillUnmount () {
    document.removeEventListener('mousedown', this.hideContainerHandler, false);
  }

  getColumnToggle (column, i) {
    const { tableId, tableConfigs } = this.props;
    const tableConfig = tableConfigs[tableId] || {};

    let key;
    if (column.dataField) {
      key = `data-column-${ i }`;
    } else if (column.customField) {
      key = `custom-column-${ i }`;
    } else {
      key = `standard-column-${ i }`;
    }

    const columnField = getColumnField(column);
    const checked = tableConfig[columnField] !== undefined
      ? tableConfig[columnField]
      : !(column.dataField || column.customField);

    return (
      <ColumnToggle key={ key }>
        <ColumnLabel>{ column.title }</ColumnLabel>
        <Checkbox
          customCss='margin-right: 0;margin-bottom: 0;'
          field={ columnField }
          checked={ checked }
          onChange={ value => this.updateColumn(column, value) }
        />
      </ColumnToggle>
    );
  }

  hideContainer (e) {
    if (this.containerWrapper && this.containerWrapper.contains(e.target)) return;
    this.setState({ showingContainer: false });
  }

  showContainer () {
    this.setState({ showingContainer: true });
  }

  updateColumn (column, value) {
    const { tableId, actions, tableConfigs } = this.props;

    const tableConfig = Object.assign({}, tableConfigs[tableId] || {});
    const columnField = getColumnField(column);

    tableConfig[columnField] = value;
    actions.setTableConfig(tableId, tableConfig);
  }


  render () {
    const { columns = [] } = this.props;
    const { showingContainer } = this.state;
    const filteredColumns = columns.filter(c => !c.compulsory);
    const standardColumns = filteredColumns.filter(
      c => !c.dataField && !c.customField,
    );
    const dataColumns = filteredColumns.filter(c => c.dataField);
    const customColumns = filteredColumns.filter(c => c.customField);

    const StandardColumnsWrapper = standardColumns.length ? (
      <ColumnsWrapper>
        <ColumnsTitle>{ t('Standard Columns') }</ColumnsTitle>
        <div>{ standardColumns.map(this.getColumnToggle.bind(this)) }</div>
      </ColumnsWrapper>
    ) : null;
    const DataColumnsWrapper = dataColumns.length ? (
      <ColumnsWrapper>
        <ColumnsTitle>{ t('Data Collection Columns') }</ColumnsTitle>
        <div>{ dataColumns.map(this.getColumnToggle.bind(this)) }</div>
      </ColumnsWrapper>
    ) : null;
    const CustomColumnsWrapper = customColumns.length ? (
      <ColumnsWrapper>
        <ColumnsTitle>{ t('Custom Field Columns') }</ColumnsTitle>
        <div>{ customColumns.map(this.getColumnToggle.bind(this)) }</div>
      </ColumnsWrapper>
    ) : null;

    return (
      <Wrapper>
        <CogIconWrapper innerRef={ wrapper => (this.containerWrapper = wrapper) }>
          <CogIcon className='fas fa-cog' onClick={ this.showContainer.bind(this) } />
          { showingContainer ? (
            <Container>
              { StandardColumnsWrapper }
              { DataColumnsWrapper }
              { CustomColumnsWrapper }
            </Container>
          ) : null }
        </CogIconWrapper>
      </Wrapper>
    );
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(HeaderConfig);
