import styled from 'styled-components';

export default styled.div`
  cursor: pointer;
  height: 180px;
  width: calc(50% - 15px);
  border-radius: 5px;
  margin: 0 30px 10px 0;
  border: solid 1px ${ props => props.theme.colors.lightBlue };
  box-shadow: 0 0 6px 0 rgba(0, 0, 0, 0.09);

  & div {
    height: 100px;
    width: 100%;
    background-color: ${ props => props.theme.colors.lightGray };
    background-position: center;
    background-size: cover;
  }

  & h6 {
    height: 80px;
    padding: 10px 20px;
  }

  &:nth-child(2n) {
    margin-right: 0;
  }

  ${ props =>
    props.selected
      ? `
      border-width: 2px;
      border-color: ${ props.theme.colors.mainBlue } !important;
      box-shadow: none;
    `
      : '' };
`;
