import React, { Component } from 'react';
import styled from 'styled-components';

import { find } from 'lodash';
import Modal from '../common/ModalV2';
import Form from '../common/inputs/Form';
import Button from '../common/Button';

import billingBanner from '../../assets/home/billing_banner.png';

import i18n from '../../../i18n';

const t = a => i18n.t([`components/manager:${ a }`]);

const BillingContainer = styled.div`
  > img {
    height: 300px;
    width: 70vw;
  }
`;

const YourOrderContainer = styled.div`
  display: flex;
  flex-direction: column;
  margin-left: 100px;
  margin-right: 100px;
  margin-top: 30px;
`;

const YourOrderHeader = styled.div`
  display: flex;
  justify-content: space-between;

  > p {
    font-size: 15px;
    font-weight: normal;
    font-style: normal;
    font-stretch: normal;
    line-height: normal;
    letter-spacing: normal;
    color: #0085cc;
  }
`;
const TotalDiv = styled.div`
  display: flex;
  justify-content: flex-end;
  margin-top: 20px;
  > h5 {
    margin-left: 30px;
  }
`;
const YourOrderContain = styled.div`
  display: flex;
  justify-content: space-between;
  margin-top: 20px;
  padding-bottom: 20px;
  border-bottom: 1px solid ${ props => props.theme.colors.lightGray };

  > p {
    font-size: 15px;
    font-weight: normal;
    font-style: normal;
    font-stretch: normal;
    line-height: normal;
    letter-spacing: normal;
    color: #2f2e2e;
  }
`;
const SaveButtonStyled = styled(Button)`
  width: 100%;
  font-size: 20px;

  @media (max-width: 900px) {
    height: 54px;
  }

  ${ props => (props.disable
    ? `
    cursor: default;
    box-shadow: none;
    background-color: ${ props.theme.colors.lightGray };
    color: white;

    &:hover, &:active {
      box-shadow: none;
      background-color: ${ props.theme.colors.lightGray };
      color: white;
    }
  `
    : '') };
`;

export default class BillingPaymentModal extends Component {
  constructor () {
    super();

    this.state = {
      formState: null,
      validateForm: false,
      resetForm: false,
    };
  }

  async onSave () {
    try {
      const { action, onFinish } = this.props;
      const { formState } = this.state;
      if (!action) return;
      if (!formState) {
        return this.setState({
          validateForm: true,
        });
      }

      // const card = {
      //   number: formState.card_number,
      //   exp_month: exp_date.value.replace('/', '').substring(0, 2),
      //   exp_year: exp_date.value.replace('/', '').substring(2, 4),
      //   cvc: cvv.value
      // };
      // await onSubmit(card);
      if (onFinish) await onFinish();
      await this.onCancel();
    } catch (e) {
      throw e;
    }
  }

  async onCancel () {
    try {
      const { onCancel } = this.props;

      this.resetForm();
      if (onCancel) await onCancel();
    } catch (e) {
      throw e;
    }
  }

  setFormState (state) {
    const { fields } = this.props;

    let isInvalid = false;
    fields.forEach((f) => {
      const {
        field, label, required, getErrorMessage, hidden, multiple,
      } = f;

      if (
        (getErrorMessage && getErrorMessage(state, state[field], label))
        || (!hidden && required && !state[field] && state[field] === null)
        || (!hidden && required && multiple && state[field].length === 0)
      ) {
        isInvalid = true;
      }
    });

    if (!isInvalid) {
      this.setState({ formState: state });
    } else {
      this.setState({ formState: null });
    }
  }

  resetForm () {
    this.setState({ resetForm: true, validateForm: false }, () => {
      this.setState({ resetForm: false });
    });
  }

  render () {
    const {
      title,
      fields,
      validateForm,
      formCss,
      contactCounts,
      organization,
      brackets,
      ...props
    } = this.props;
    const { formState, resetForm } = this.state;
    // eslint-disable-next-line
    const stateValidateForm = this.state.validateForm;

    let billingPricePerMonth = 0;
    const priceDetails = find(brackets, (bracket) => {
      if (bracket.to == null) return bracket;
      if (organization.bill_contact_count <= bracket.to) return bracket;
    });
    if (organization.bill_contact_count !== 100) {
      brackets.forEach((bracket) => {
        if (bracket.id < priceDetails.id) {
          billingPricePerMonth
            += (bracket.to - bracket.from)
              / bracket.unit_size
              * bracket.amount_per_unit;
        } else if (bracket.id === priceDetails.id) {
          billingPricePerMonth
            += organization.bill_contact_count
              / priceDetails.unit_size
              * priceDetails.amount_per_unit;
        }
      });
    }

    const SaveButton = (
      <SaveButtonStyled
        fullHeight
        // HACK: use fake disable prop here
        disable={ !formState }
        onClick={ this.onSave.bind(this) }
      >
        { t('PAY NOW') }
      </SaveButtonStyled>
    );

    return (
      <Modal
        onClose={ this.onCancel.bind(this) }
        headerCss={ `
          text-align: center;

          & h4 {
            font-size: 15px;
            text-transform: uppercase;
          }
        ` }
        footerContent={ SaveButton }
        footerCss={ `
          box-shadow: none;

          @media (min-width: 901px) {
            height: 54px;
          }
        ` }
        isOpen
        large
        noPadding
        { ...props }
      >
        <BillingContainer>
          <img alt='' src={ billingBanner } />
          <YourOrderContainer>
            <YourOrderHeader>
              <h4>YOUR ORDER</h4>
              <p>Amend</p>
            </YourOrderHeader>
            <YourOrderContain>
              <h5>
                { organization.bill_contact_count }
                { ' ' }
Contacts
              </h5>
              <p>
USD $
                { billingPricePerMonth }
              </p>
            </YourOrderContain>
            <TotalDiv>
              <p>Total</p>
              <h5>
USD $
                { billingPricePerMonth }
              </h5>
            </TotalDiv>
            <Form
              validate={ validateForm || stateValidateForm }
              fields={ fields }
              reset={ resetForm }
              onUpdate={ this.setFormState.bind(this) }
              customCss={ formCss }
            />
          </YourOrderContainer>
        </BillingContainer>
      </Modal>
    );
  }
}
