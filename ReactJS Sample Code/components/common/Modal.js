import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import styled from 'styled-components';

import Button from './Button';
import * as ActionCreators from '../../redux/ui';

const ModalDiv = styled.div`
  position: fixed;
  z-index: 999;
  width: 100%;
  height: 100%;
  left: 0;
  top: 0;
  background-color: rgba(0, 0, 0, 0.7);
  color: ${ props => props.theme.colors.darkGray };
  text-align: left;

  @media (max-width: 900px) {
    width: 100% !important;
    overflow-y: scroll;
  }

  & .modal-section {
    position: relative;
    padding: 20px 40px;
    border-bottom: 1px solid ${ props => props.theme.colors.lightGray };

    &:nth-last-child(2) {
      border-bottom: none;
    }
  }

  & .modal-section-title {
    margin-bottom: 10px;
    font-size: 14px;
    line-height: 20px;
    font-weight: 600;
    color: ${ props => props.theme.colors.darkGray };
  }

  & .change {
    position: absolute;
    top: 20px;
    right: 40px;
    height: 20px;
    font-size: 12px;
    line-height: 20px;
    font-weight: 600;
    background: none;
    color: ${ props => props.theme.colors.mainBlue };
  }

  & .instruction {
    padding: 0 40px;
    text-align: center;
  }

  & .appendix {
    margin-top: 20px;
    font-size: 12px;
    line-height: 12px;
  }
`;

const Body = styled.div`
  position: absolute;
  overflow-y: ${ props => (props.disableScrolling ? 'initial' : 'auto') };
  width: 640px;
  max-height: calc(100vh - 120px);
  left: 50%;
  top: 50%;
  transform: translate(-50%, calc(-50% - 60px));
  background-color: white;
  border-radius: 6px;

  @media (max-width: 900px) {
    position: fixed;
    width: 100% !important;
    height: 100vh;
    max-height: 100vh;
    left: 0;
    top: 0;
    bottom: 0;
    transform: none;
    margin-left: 0;
    margin-top: 0 !important;
    border-radius: 0;
    padding-bottom: 60px;
  }
`;

const Cancel = styled(Button)`
  position: absolute;
  top: ${ props => (props.text ? '16px' : '10px') };
  right: 20px;
  height: 32px;
  background-color: transparent;
  font-size: ${ props => (props.text ? '16px' : '22px') };
  line-height: 32px;
  padding: 0;
  color: ${ props => props.theme.colors.darkGray };
  opacity: 0.7;
  box-shadow: none;

  @media (max-width: 900px) {
    left: 20px;
    top: 6px;
    right: auto;
    font-size: 16px;
  }

  &:hover,
  &:active {
    background-color: transparent;
    box-shadow: none;
    color: ${ props => props.theme.colors.mainBlue };
  }

  &:hover {
    opacity: 0.85;
  }

  &:active {
    opacity: 1;
  }
`;

const Title = styled.p`
  display: block;
  padding: 20px 36px;
  font-size: 19px;
  color: ${ props => props.theme.colors.darkGray };
  font-weight: 600;
  line-height: 27px;
  text-align: center;

  @media (max-width: 900px) {
    padding: 10px 36px;
    font-size: 16px;
    line-height: 22px;
  }
`;

const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators(ActionCreators, dispatch),
});

class Modal extends Component {
  componentDidMount () {
    const { actions } = this.props;
    actions.setScrollable(false);
  }

  componentWillUnmount () {
    const { actions } = this.props;
    actions.setScrollable(true);
  }

  render () {
    const {
      title,
      noTitle,
      children,
      onCancel,
      style,
      bodyStyle = {},
      contentStyle,
      disableScrolling,
      backButton,
      cancelButtonText,
    } = this.props;

    const titleStyle = {};
    if (title) titleStyle.borderBottom = '1px solid rgb(225, 232, 237)';

    return (
      <ModalDiv style={ style }>
        <Body style={ bodyStyle } disableScrolling={ disableScrolling }>
          <div style={ { position: 'relative', ...contentStyle } }>
            { onCancel ? (
              <Cancel onClick={ onCancel } text={ !!cancelButtonText }>
                { cancelButtonText || (
                  <i
                    className={ `fas fa-${ backButton ? 'arrow-left' : 'times' }` }
                  />
                ) }
              </Cancel>
            ) : null }
            { !noTitle ? <Title style={ titleStyle }>{ title }</Title> : null }
            { children }
          </div>
        </Body>
      </ModalDiv>
    );
  }
}

export default connect(null, mapDispatchToProps)(Modal);
