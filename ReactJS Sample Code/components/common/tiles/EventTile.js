import React from 'react';
import { Link } from 'react-router-dom';
import styled from 'styled-components';

import TagLabel from '../TagLabel';
import Button from '../Button';

import { formatDateTime } from '../../../utils';
import bannerPlaceholder from '../../../assets/common/placeholder_banner.png';
import Image from '../Image';

import i18n from '../../../../i18n';

const t = a => i18n.t([`components/common:${ a }`]);

const EventTileWrapper = styled.div`
  position: relative;
  flex: 0 0 30%;
  margin: 0 3.3333% 25px 0;
  border-radius: 5px;
  background-color: white;
  box-shadow: 0 0 6px 0 rgba(0, 0, 0, 0.16);

  &:nth-child(3n) {
    margin-right: 0;
  }
`;

const EventTileContent = styled.div`
  position: relative;

  &:before {
    display: block;
    content: '';
  }

  & > div {
    padding: 20px 15px;
    top: 0;
    bottom: 0;
    left: 0;
    right: 0;
  }
`;

const RowContainer = styled.div`
  display: flex;
  width: 100%;
  margin-bottom: 10px;
  align-items: flex-start;
`;

const EventDate = styled.div`
  font-size: 12px;
  font-weight: 700;
  line-height: 17px;
  flex: 1;
  text-align: right;
`;

const ButtonsContainer = styled.div`
  display: flex;
  width: 100%;
  justify-content: space-between;
`;

const IconButton = styled(Button)`
  font-size: 15px;
  line-height: 21px;
  font-weight: 700;
  letter-spacing: 0.5px;
  margin-left: 5px;
  min-width: 80px;
  color: ${ props => (props.red ? props.theme.colors.mainRed : props.theme.colors.mainGray) };
  opacity: ${ props => (props.red ? '0.4' : '1.0') };
  text-transform: none;
  margin: 0;

  & i {
    margin-right: 10px;
  }

  &:hover,
  &:active {
    color: ${ props => (props.red ? props.theme.colors.mainRed : props.theme.colors.darkGray) };
    opacity: 1;
  }
`;

const IconButtonLink = IconButton.extend`
  height: 100%;
  padding: 0;
  margin: 0;
  font-size: 20px;
  line-height: 20px;
`;

const EventTile = ({
  event, pageLink, manageLink, onDelete,
}) => (
  <EventTileWrapper>
    <Link to={ manageLink }>
      <Image
        src={ event.cover_image_url }
        width={ 440 }
        height={ 220 }
        defaultImage={ bannerPlaceholder }
        description={ event.name }
        customCss='border-top-left-radius: 5px; border-top-right-radius: 5px;'
      />
    </Link>
    <EventTileContent>
      <div>
        <RowContainer>
          <a href={ pageLink }>
            <IconButtonLink plain>
              <i className='fa fa-link' aria-hidden='true' />
            </IconButtonLink>
          </a>
          { event.private ? (
            <TagLabel label={ t('Private') } bgColor='mainRed' />
          ) : (
            <TagLabel label={ t('Public') } bgColor='mainGreen' />
          ) }
          { event.premium ? (
            <TagLabel label={ t('Premium') } bgColor='mainBlue' />
          ) : null }
          <EventDate>
            { formatDateTime(event.start_at, event.timezone) }
          </EventDate>
        </RowContainer>
        <ButtonsContainer>
          <Link to={ manageLink }>
            <h5>{ event.name }</h5>
          </Link>
          <IconButton plain red onClick={ onDelete.bind(event) }>
            <i className='fas fa-trash' aria-hidden='true' />
            { t('Delete') }
          </IconButton>
        </ButtonsContainer>
      </div>
    </EventTileContent>
  </EventTileWrapper>
);

export default EventTile;
