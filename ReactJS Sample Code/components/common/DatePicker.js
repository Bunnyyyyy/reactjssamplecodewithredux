// this import has to be on top of the file, due to a quirk in react-dates
import 'react-dates/initialize';

import React, { Component } from 'react';
import { SingleDatePicker } from 'react-dates';
import moment from 'moment-timezone';
import styled from 'styled-components';
import defaultCss from 'react-dates/lib/css/_datepicker.css';

import Label from './inputs/Label';
import InvalidMessage from './InvalidMessage';

const DatePickerDiv = styled.div`
  ${ defaultCss } position: relative;
  display: inline-block;
  width: ${ props => (props.fullWidth ? '100%' : '270px') };
  margin-top: 20px;
  margin-bottom: 20px;
  margin-right: 20px;

  &._half {
    width: 49%;
    margin-right: 2%;

    &:nth-child(2n) {
      margin-right: 0;
    }
  }

  & .SingleDatePicker,
  & .SingleDatePickerInput,
  & .DateInput,
  & .DateInput_input {
    width: 100%;
    background: none;
  }

  & .DateInput_input {
    height: 32px;
    width: 100%;
    font-size: 15px;
    line-height: 28px;
    font-weight: 400;
    border: 0px solid transparent;
    border-bottom: 1px solid
      ${ props => (props.theme.light
    ? 'rgba(255, 255, 255, .25)'
    : 'rgba(47, 46, 46, .25)') };
    border-radius: 0;
    padding-left: 0;
    margin-top: 0px;
    background-color: transparent;
    color: ${ props => (props.theme.light ? 'white' : props.theme.colors.darkGray) };
  }

  & .DateInput_input__focused {
    border-color: ${ props => (props.theme.light ? 'white' : props.theme.colors.mainBlue) };
  }

  & .DateInput_fang {
    display: none;
  }

  & .SingleDatePickerInput {
    padding-right: 0;
  }

  & .SingleDatePickerInput_clearDate {
    height: 32px;
    padding: 5px;
    margin: 0;

    &:hover,
    &:active {
      background-color: transparent;

      & svg {
        fill: ${ props => props.theme.colors.mainBlue };
      }
    }
  }

  & .SingleDatePicker_picker {
    z-index: 901;

    ${ props => (props.menuOnTop
    ? `
      top: auto !important;
      bottom: 35px !important;
      `
    : 'top: 50px !important;') };
  }

  & .DayPicker {
    border: none;
    border-radius: 4px;
    box-shadow: 0 0 20px 0 rgba(0, 0, 0, 0.1);
  }

  & .CalendarMonth_caption {
    font-size: 20px;
    line-height: 28px;
    color: ${ props => props.theme.colors.mainGray };
    padding-bottom: 57px;

    & strong {
      font-weight: 500;
    }
  }

  & .DayPickerNavigation_button {
    width: 28px;
    height: 28px;
    background: none;
    color: rgb(186, 194, 199);

    & i {
      font-size: 20px;
    }

    &:hover,
    &:active {
      background: none;
    }
    &:hover,
    &:active {
      color: ${ props => props.theme.colors.mainBlue };
    }
  }

  & .DayPicker_weekHeader {
    top: 70px;
  }

  & .DayPicker_weekHeader_li {
    color: rgb(146, 146, 146);
    font-weight: 600;
  }

  & .CalendarDay {
    line-height: 38px;
    border-radius: 20px;
    border: none !important;
    color: ${ props => props.theme.colors.mainGray };

    &:hover,
    &:active {
      border: none;
    }
  }

  & .CalendarDay__selected {
    background-color: ${ props => props.theme.colors.mainBlue };
    color: white;

    &:hover,
    &:active {
      background-color: ${ props => props.theme.colors.darkBlue };
    }
  }

  ${ props => props.customCss || '' };
`;

const NavIcon = styled.i`
  position: absolute;
  top: 25px;
  ${ props => (props.left ? 'left: 35px;' : 'right: 35px;') }
`;
export default class DatePicker extends Component {
  constructor () {
    super();

    this.state = {
      focused: false,
    };
  }

  onFocusChange ({ focused }) {
    this.setState({ focused });
  }

  changeDate (date) {
    const { onChange, timezone } = this.props;
    const dateObj = date && timezone ? date.tz(timezone) : date;
    if (onChange) onChange(dateObj, timezone);
  }

  render () {
    const {
      field,
      label,
      date,
      defaultDate,
      timezone,
      className,
      fullWidth,
      divStyle,
      customCss,
      required,
      placeholder,
      errorMessage,
      menuOnTop,
      onClose,
    } = this.props;
    const { focused } = this.state;
    const momentDate = date
      ? timezone ? moment.tz(date, timezone) : moment(date).local()
      : null;

    const initialVisibleDate = defaultDate
      ? timezone
        ? moment.tz(defaultDate, timezone)
        : moment(defaultDate).local()
      : timezone ? moment.tz(timezone) : moment().local();

    return (
      <DatePickerDiv
        menuOnTop={ menuOnTop }
        className={ className }
        fullWidth={ fullWidth }
        style={ divStyle }
        customCss={ customCss }
      >
        { label ? (
          <Label field={ field } label={ label } required={ required } />
        ) : null }
        <SingleDatePicker
          noBorder
          readOnly
          showClearDate
          hideKeyboardShortcutsPanel
          isOutsideRange={ () => false }
          numberOfMonths={ 1 }
          displayFormat='DD MMM YYYY (ddd)'
          placeholder={ placeholder || '' }
          date={ momentDate }
          onDateChange={ this.changeDate.bind(this) }
          focused={ focused }
          onFocusChange={ this.onFocusChange.bind(this) }
          initialVisibleMonth={ () => initialVisibleDate }
          navPrev={ <NavIcon left className='fas fa-angle-left' aria-hidden='true' /> }
          navNext={ <NavIcon className='fas fa-angle-right' aria-hidden='true' /> }
          onClose={ onClose }
        />
        { errorMessage ? <InvalidMessage>{ errorMessage }</InvalidMessage> : null }
      </DatePickerDiv>
    );
  }
}
