import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import styled from 'styled-components';
import UnsavedPrompt from '../forms/UnsavedPrompt';
import * as ManagerActionCreators from '../../redux/manager';

import Button from '../common/Button';
import i18n from '../../../i18n';

const t = a => i18n.t([`components/manager:${ a }`]);

const Wrapper = styled.div`
  position: relative;
  overflow: hidden;
  transition: 0.75s;

  &:hover {
    overflow: visible;
  }

  ${ props => (props.hasPopup ? 'overflow: visible;' : '') };
`;

const CancelButton = Button.extend`
  margin-top: 7px;
  margin-bottom: 7px;
`;

const SaveButton = styled(Button)`
  margin-top: 7px;
  margin-bottom: 7px;

  ${ props => (props.disabled
    ? `
    &:disabled, &:hover, &:active {
      background-color: ${ props.theme.colors.darkBlue } !important;
    }
  `
    : '') };

  ${ props => (props.empty ? 'opacity: 0.4;' : '') } ${ props => (props.invalid
  ? `
    background-color: ${ props.theme.colors.mainRed } !important;

    &:hover, &:active {
      background-color: ${ props.theme.colors.mainRed } !important;
      color: white;
    }
  `
  : '') };
`;

const HoverPopup = styled.div`
  position: absolute;
  min-width: 330px;
  bottom: 0;
  right: 0;
  transform: translateY(100%);
  box-shadow: 0 3px 6px 0 rgba(0, 0, 0, 0.16);
  background-color: white;
  text-align: left;
  padding: 15px 15px 10px 15px;
  transition: 0.75s;

  & h6 {
    margin-bottom: 5px;
  }

  & a,
  & p {
    font-size: 12px;
    line-height: 18px;
    margin-bottom: 5px;
  }
`;

const ActionCreators = Object.assign(
  {},
  ManagerActionCreators,
);

const mapStateToProps = state => ({
  saveActions: state.manager.saveActions,
});

const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators(ActionCreators, dispatch),
});

class SaveActions extends Component {
  getActions () {
    const { saveActions } = this.props;

    const saveActionSections = Object.values(saveActions);
    saveActionSections.sort((s1, s2) => {
      if (s1.order === undefined && s2.order === undefined) {
        return 0;
      } if (s1.order !== undefined && s2.order === undefined) {
        return -1;
      } if (s1.order === undefined && s2.order !== undefined) {
        return 1;
      }
      return s1.order - s2.order;
    });

    const actionArr = [];
    saveActionSections.forEach((s) => {
      if (typeof s.subsections === 'object') {
        const subActions = [];
        for (const itemId in s.subsections) {
          subActions.push(s.subsections[itemId]);
        }
        actionArr.push(subActions);
      } else if (typeof s.action === 'function') {
        actionArr.push([s.action]);
      }
    });

    return actionArr;
  }

  // HACK: use null to determine invalid actions for now
  hasInvalidAction () {
    const { saveActions } = this.props;
    const saveActionSections = Object.values(saveActions);

    let hasInvalid = false;
    saveActionSections.forEach((s) => {
      if (typeof s.subsections === 'object') {
        for (const itemId in s.subsections) {
          if (s.subsections[itemId] === null) hasInvalid = true;
        }
      } else if (s.action === null) {
        hasInvalid = true;
      }
    });

    return hasInvalid;
  }

  async executeSaveActions () {
    try {
      const { actions, onFinish } = this.props;
      const executeActions = this.getActions();
      for (let i = 0; i < executeActions.length; i++) {
        await Promise.all(executeActions[i].map(a => a()));
      }

      if (onFinish) await onFinish();
      actions.resetSaveActions();
    } catch (e) {
      console.error(e);
    }
  }

  async resetSaveActions () {
    try {
      const { actions, onCancel } = this.props;
      actions.resetSaveActions();

      if (onCancel) await onCancel();
    } catch (e) {
      console.error(e);
    }
  }

  async clickInvalidSaveActions () {
    try {
      const { onInvalid } = this.props;
      if (onInvalid) await onInvalid();
    } catch (e) {
      console.error(e);
    }
  }

  render () {
    const {
      className,
      saveActions,
      buttonText = t('Save'),
      buttonDoneText = t('Saved'),
      saveNewItem,
      // TODO: kill saveActive prop after New Organization page moves to Form component
      saveActive,
      hoverPopup,
      popup,
      submit,
    } = this.props;

    const saveActionsCount = Object.keys(saveActions).length;
    const hasInvalid = this.hasInvalidAction();
    const actions = this.getActions();
    return (
      <Wrapper className={ className } hasPopup={ !!popup }>
        <CancelButton
          gray
          small
          light
          disabled={ saveNewItem ? false : !saveActionsCount }
          onClick={ this.resetSaveActions.bind(this) }
        >
          { t('Cancel') }
        </CancelButton>
        { hasInvalid ? (
          <SaveButton
            small
            light
            invalid
            onClick={ this.clickInvalidSaveActions.bind(this) }
            type={ submit ? 'submit' : 'button' }
          >
            { buttonText }
          </SaveButton>
        ) : (
          <SaveButton
            small
            light
            disabled={ saveActive ? false : !actions.length }
            empty={ !saveActive && saveNewItem && !saveActionsCount }
            onClick={ this.executeSaveActions.bind(this) }
            type={ submit ? 'submit' : 'button' }
          >
            { saveActionsCount || saveNewItem ? buttonText : buttonDoneText }
          </SaveButton>
        ) }
        { popup || hoverPopup ? (
          <HoverPopup>{ popup || hoverPopup || null }</HoverPopup>
        ) : null }
        <UnsavedPrompt formikProps={ { dirty: !!(saveActionsCount || saveNewItem) } } />
      </Wrapper>
    );
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(SaveActions);
