import React from 'react';
import styled from 'styled-components';
import sanitizeHtml from 'sanitize-html';

import { getImageUrl } from '../../utils';

const OLD_RATIO_TIMESTAMP = 1544156823479;

const PictureStyled = styled.picture`
  display: block;
  position: relative;
  width: 100%;
  height: 0;
  padding-bottom: ${ props => (`${ props.ratio }%`) };
  background-image: ${ props => (props.defaultImage ? `url(${ props.defaultImage })` : 'none') };
  background-size: cover;
  overflow: hidden;

  ${ props => props.customCss || '' };
`;

const ImgStyled = styled.img`
  position: absolute;
  width: 100%;
  text-indent: 100%;
  white-space: nowrap;
  overflow: hidden;
`;

const Image = ({
  src = '',
  width = 1000,
  height,
  defaultImage = '',
  description = '',
  customCss,
}) => {
  let ratio = height ? 100 * height / width : 50;
  if (!src) {
    return (
      <PictureStyled ratio={ ratio } defaultImage={ defaultImage } customCss={ customCss } />
    );
  }
  let filters = '';
  const timestamp = src ? src.split('_')[2] : null;
  // eslint-disable-next-line radix
  if (timestamp && parseInt(timestamp) < OLD_RATIO_TIMESTAMP) {
    // TODO: replace 1543221581479 by timestamp of ratio migration date
    // hardcode old ratio
    if (!height) ratio = 28;
    // ad empty filter to avoid applying no_upscale() by default
    filters = 'FORCE_UPSCALE';
  }
  height = height || width * ratio / 100;
  description = description
    ? sanitizeHtml(description, { allowedTags: [], allowedAttributes: [] })
    : '';
  // TODO: add Picturefill lib to support old browser
  const src1x = getImageUrl(src, `${ width }x${ height }${ filters }`);
  const src2x = getImageUrl(
    src,
    `${ 2 * width }x${ height ? 2 * height : '' }${ filters }`,
  );
  const sources = [];
  if (width > 500) {
    // add special small size for mobile
    const src1xsmall = getImageUrl(
      src,
      `500x${ height ? Math.floor(500 * ratio / 100) : '' }${ filters }`,
    );
    const src2xsmall = getImageUrl(
      src,
      `1000x${ height ? Math.floor(1000 * ratio / 100) : '' }${ filters }`,
    );
    sources.push(
      <source
        srcSet={ `${ src1xsmall }, ${ src2xsmall } 2x` }
        media='(max-width: 500px)'
      />,
    );
  }
  sources.push(<source srcSet={ `${ src1x }, ${ src2x } 2x` } />);
  return (
    <PictureStyled ratio={ ratio } customCss={ customCss }>
      { sources }
      <ImgStyled src={ src1x } alt={ description } />
    </PictureStyled>
  );
};

export default Image;
