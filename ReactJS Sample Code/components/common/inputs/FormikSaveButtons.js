import React from 'react';
import styled from 'styled-components';
import Button from '../Button';

import i18n from '../../../../i18n';

const t = a => i18n.t([`translations:${ a }`]);

const Wrapper = styled.div`
  // display: inline-block;
`;

const FormikSaveButtons = ({ newItem, formikProps = {} }) => {
  const {
    errors = {}, dirty, handleSubmit, handleReset,
  } = formikProps;

  return (
    <Wrapper>
      <Button
        small
        gray
        onClick={ handleReset }
        disabled={ !dirty }
        customCss='margin: 0 20px 0 0;'
      >
        { newItem ? t('Clear') : t('Reset') }
      </Button>
      <Button
        small
        light
        onClick={ handleSubmit }
        disabled={ !dirty || !!Object.keys(errors).length }
      >
        { !newItem && !dirty ? t('Saved') : t('Save') }
      </Button>
    </Wrapper>
  );
};

export default FormikSaveButtons;
