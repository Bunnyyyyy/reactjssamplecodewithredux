import React from 'react';
import styled from 'styled-components';

const Wrapper = styled.strong`
  font-weight: bold;
`;

function Bold ({ children, attributes }) {
  return (
    <Wrapper { ...attributes }>
      { children }
    </Wrapper>
  );
}

export default Bold;
