import React from 'react';
import { Link } from 'react-router-dom';
import { withRouter } from 'react-router';
import styled, { ThemeProvider } from 'styled-components';

const NavBarWrapper = styled.div`
  display: flex;

  ${ props => (props.inline ? `
    flex-grow: 1;
    padding: 0 5px;
  ` : `
    position: absolute;
    z-index: 899;
    top: 56px;
    width: 100%;
    height: 44px;
    padding: 0 30px;
    background-color: white;
    box-shadow: 0 2px 4px 0 rgba(0, 0, 0, 0.1);
  `) }
`;

const LinkWrapper = styled.div`
  margin-right: 30px;
`;

const LinkDiv = styled.div`
  cursor: pointer;
  display: flex;
  align-items: center;
  height: 44px;
  padding: 5px 0 0 0;
  border-bottom: 5px solid transparent;
  opacity: ${ props => (props.disabled ? '0.5' : '1') };

  ${ props => (props.theme.steps
    ? `
    padding-top: 2px;
    border-bottom-width: 2px;
  `
    : '') } ${ props => (props.theme.active
  ? `border-color: ${ props.theme.colors.mainBlue };`
  : '') };
`;

const StepText = styled.div`
  color: ${ props => (props.theme.active
    ? props.theme.colors.darkGray
    : props.theme.colors.lightGray) };
  font-size: 12px;
  line-height: ${ props => (props.theme.steps ? '46px' : '42px') };
  font-weight: 700;
  margin-right: 4px;
`;

const StepNumber = styled.div`
  text-align: center;
  color: ${ props => (props.theme.active
    ? props.theme.colors.mainBlue
    : props.theme.colors.lightGray) };
  height: 26px;
  width: 26px;
  display: flex;
  align-items: center;
  justify-content: center;
  border-radius: 13px;
  margin-right: 8px;
  border: 2px solid
    ${ props => (props.theme.active ? props.theme.colors.mainBlue : 'transparent') };
`;

const LinkTitle = styled.div`
  font-size: 12px;
  font-weight: 600;
  line-height: ${ props => (props.theme.steps ? '46px' : '42px') };
  ${ props => (props.theme.active ? `color: ${ props.theme.colors.mainBlue };` : '') };
`;

const NavBar = ({
  location: { pathname, search }, steps, links, inline,
}) => {
  const Links = links.map((l, i) => {
    const comparedRoute = l.baseRoute || l.route;

    const currentRoute = `${ pathname }${ search }`;
    const active = l.exact
      ? comparedRoute === currentRoute
      : currentRoute.indexOf(comparedRoute) === 0;

    return (
      <ThemeProvider theme={ { active } }>
        <LinkWrapper>
          <Link to={ l.route } disabled={ l.disabled }>
            <LinkDiv>
              { steps
                ? [
                  <StepText>Step</StepText>,
                  <StepNumber>{ i + 1 }</StepNumber>,
                ]
                : null }
              <LinkTitle>{ l.title }</LinkTitle>
            </LinkDiv>
          </Link>
        </LinkWrapper>
      </ThemeProvider>
    );
  });

  return (
    <ThemeProvider theme={ { steps } }>
      <NavBarWrapper inline={ inline }>{ Links }</NavBarWrapper>
    </ThemeProvider>
  );
};

export default withRouter(NavBar);
