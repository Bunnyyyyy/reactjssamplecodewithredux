import React, { Component } from 'react';

export default class UserAvatar extends Component {
  render () {
    const { size, user } = this.props;

    return (
      <div
        style={ {
          flexShrink: 0,
          position: 'relative',
          textAlign: 'center',
          width: size,
          height: size,
          overflow: 'hidden',
          borderRadius: `${ size / 2 }px`,
          color: 'white',
          backgroundColor: 'rgb(0, 133, 204)'
        } }
      >
        { user.profile_image_url ? (
          <img
            style={ {
              position: 'absolute',
              width: '100%',
              height: '100%',
              left: 0,
              top: 0,
              borderRadius: `${ size / 2 }px`
            } }
            src={ user.profile_image_url }
          />
        ) : (
          <span
            style={ {
              display: 'inline-block',
              fontSize: size / 2.4,
              fontWeight: 400,
              lineHeight: `${ size * 1.05 }px`
            } }
          >
            { user.first_name ? user.first_name.charAt(0) : '' }
            { user.last_name ? user.last_name.charAt(0) : '' }
          </span>
        ) }
      </div>
    );
  }
}
