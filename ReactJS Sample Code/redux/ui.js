import React from 'react';
import { toast } from 'react-toastify';
import { ToastBody } from '../components/common/ToastNotification';

const SET_LOADING = 'SET_LOADING';
const SET_REQUEST_LOADING = 'SET_REQUEST_LOADING';
const SET_CURRENT_ROUTE = 'SET_CURRENT_ROUTE';
const SET_CURRENT_PATH = 'SET_CURRENT_PATH';
const SET_FORM_HANDLE_SUBMIT = 'SET_FORM_HANDLE_SUBMIT';
const SET_FORM_HANDLE_RESET = 'SET_FORM_HANDLE_RESET';
const SET_FORM_ERRORS = 'SET_FORM_ERRORS';
const SET_FORM_DIRTY = 'SET_FORM_DIRTY';

const defaultState = {
  loading: false,
  requestsLoading: {},
  currentRoute: null,
  currentPath: null,
  formHandleSubmits: {},
  formHandleResets: {},
  formErrors: {},
  formDirties: {},
};

export default function reducer (state = defaultState, action) {
  let requestsLoading;
  let formHandleSubmits;
  let formHandleResets;
  let formErrors;
  let formDirties;

  switch (action.type) {
    case SET_LOADING:
      return { ...state, loading: action.payload };

    case SET_REQUEST_LOADING:
      requestsLoading = Object.assign({}, state.requestsLoading);
      if (action.payload.loading) {
        requestsLoading[action.payload.url] = true;
      } else {
        delete requestsLoading[action.payload.url];
      }

      return { ...state, requestsLoading };

    case SET_CURRENT_ROUTE:
      return { ...state, currentRoute: action.payload };

    case SET_CURRENT_PATH:
      return { ...state, currentPath: action.payload };

    case SET_FORM_HANDLE_SUBMIT:
      if (!action.payload.id) return state;
      formHandleSubmits = Object.assign({}, state.formHandleSubmits);

      if (action.payload.handleSubmit) {
        formHandleSubmits[action.payload.id] = action.payload.handleSubmit;
      } else {
        delete formHandleSubmits[action.payload.id];
      }

      return { ...state, formHandleSubmits };

    case SET_FORM_HANDLE_RESET:
      if (!action.payload.id) return state;
      formHandleResets = Object.assign({}, state.formHandleResets);

      if (action.payload.handleReset) {
        formHandleResets[action.payload.id] = action.payload.handleReset;
      } else {
        delete formHandleResets[action.payload.id];
      }

      return { ...state, formHandleResets };

    case SET_FORM_ERRORS:
      if (!action.payload.id) return state;
      formErrors = Object.assign({}, state.formErrors);

      if (action.payload.errors) {
        formErrors[action.payload.id] = action.payload.errors;
      } else {
        delete formErrors[action.payload.id];
      }

      return { ...state, formErrors };

    case SET_FORM_DIRTY:
      if (!action.payload.id) return state;
      formDirties = Object.assign({}, state.formDirties);

      if (action.payload.dirty !== null) {
        formDirties[action.payload.id] = !!action.payload.dirty;
      } else {
        delete formDirties[action.payload.id];
      }

      return { ...state, formDirties };

    default:
      return state;
  }
}

export function setLoading (isLoading) {
  return {
    type: SET_LOADING,
    payload: isLoading,
  };
}

export function setRequestLoading (url, loading) {
  return {
    type: SET_REQUEST_LOADING,
    payload: {
      url,
      loading,
    },
  };
}

export function setCurrentRoute (route) {
  return {
    type: SET_CURRENT_ROUTE,
    payload: route,
  };
}

export function setCurrentPath (path) {
  return {
    type: SET_CURRENT_PATH,
    payload: path,
  };
}

export function setScrollable (scrollable) {
  return () => (scrollable
    ? document.body.classList.remove('_unscrollable')
    : document.body.classList.add('_unscrollable'));
}

const NOTIFICATION_TYPES = ['success', 'error', 'warn', 'info'];
export function showNotification (notification) {
  return () => {
    const { type } = notification;
    if (NOTIFICATION_TYPES.indexOf(type) === -1) return;

    const ToastComponent = <ToastBody notification={ notification } />;
    toast[type](ToastComponent, {
      position: toast.POSITION.TOP_CENTER,
    });
  };
}

export function setFormHandleSubmit (id, handleSubmit) {
  return {
    type: SET_FORM_HANDLE_SUBMIT,
    payload: { id, handleSubmit },
  };
}

export function setFormHandleReset (id, handleReset) {
  return {
    type: SET_FORM_HANDLE_RESET,
    payload: { id, handleReset },
  };
}

export function setFormErrors (id, errors) {
  return {
    type: SET_FORM_ERRORS,
    payload: { id, errors },
  };
}

export function setFormDirty (id, dirty) {
  return {
    type: SET_FORM_DIRTY,
    payload: { id, dirty },
  };
}
