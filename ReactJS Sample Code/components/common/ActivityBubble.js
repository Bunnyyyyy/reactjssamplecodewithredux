import React, { Component } from 'react';
import { connect } from 'react-redux';
import styled from 'styled-components';

const Bubble = styled.div`
  display: inline-block;
  width: 30px;
  height: 30px;
  line-height: 30px;
  border-radius: 15px;
  margin: 5px 10px 5px 0;
  background-color: ${ props => props.bgColor || props.theme.colors.mainBlue };
  box-shadow: 0 0 6px 0 rgba(0, 0, 0, 0.16);
  color: white;
  text-align: center;
`;

const BACKGROUND_COLORS = {
  organization_member: 'rgb(244, 181, 119)',
  event_guest: 'rgb(0, 133, 204)',
  profile_donation: 'rgb(255, 108, 108)',
  form_respondent: 'rgb(229, 223, 115)'
};

const mapStateToProps = state => ({
  organization: state.manager.organization
});

const ActivityBubble = ({
  contactType,
  contactId,
  sourceType,
  sourceId,
  organization
}) => {
  return (
    <Bubble bgColor={ BACKGROUND_COLORS[contactType] }>
      { contactType[0].toUpperCase() }
    </Bubble>
  );
};

export default connect(mapStateToProps)(ActivityBubble);
