import { handleApiError } from '../utils';
import { getRequest } from './index';

const SET_EMAIL_TEMPLATES = 'SET_EMAIL_TEMPLATES';
const SET_EMAIL_TEMPLATE = 'SET_EMAIL_TEMPLATE';

const defaultState = {
  templates: [],
  template: {},
};

export default function reducer (state = defaultState, action) {
  switch (action.type) {
    case SET_EMAIL_TEMPLATES:
      return { ...state, templates: action.payload };

    case SET_EMAIL_TEMPLATE:
      return { ...state, template: action.payload };

    default:
      return state;
  }
}

export function setEmailTemplates (templates) {
  return {
    type: SET_EMAIL_TEMPLATES,
    payload: templates,
  };
}

export function setEmailTemplate (template) {
  return {
    type: SET_EMAIL_TEMPLATE,
    payload: template,
  };
}

export function getEmailTemplates (oId) {
  return dispatch => new Promise(async (resolve) => {
    try {
      const request = getRequest(dispatch);

      const url = `/organizations/${ oId }/email_templates`;
      const response = await request.get(url);

      const templates = response.data;
      dispatch(setEmailTemplates(templates));

      return resolve(templates);
    } catch (e) {
      handleApiError(e);
      return resolve();
    }
  });
}

export function getEmailTemplate (oId, emId) {
  return dispatch => new Promise(async (resolve) => {
    try {
      const request = getRequest(dispatch);

      const url = `/organizations/${ oId }/email_templates/${ emId }`;
      const response = await request.get(url);

      const template = response.data;
      dispatch(setEmailTemplate(template));

      return resolve(template);
    } catch (e) {
      handleApiError(e);
      return resolve();
    }
  });
}

export function createEmailTemplate (oId, data) {
  return dispatch => new Promise(async (resolve, reject) => {
    try {
      const request = getRequest(dispatch);

      const url = `/organizations/${ oId }/email_templates`;
      const response = await request.post(url, data);

      const email = response.data;
      return resolve(email);
    } catch (e) {
      const error = handleApiError(e);
      return reject(error);
    }
  });
}

export function updateEmailTemplate (oId, emId, data) {
  return dispatch => new Promise(async (resolve, reject) => {
    try {
      const request = getRequest(dispatch);

      const url = `/organizations/${ oId }/email_templates/${ emId }`;
      await request.put(url, data);

      return resolve();
    } catch (e) {
      const error = handleApiError(e);
      return reject(error);
    }
  });
}

export function createEmailTemplateElement (oId, emId, data) {
  return dispatch => new Promise(async (resolve, reject) => {
    try {
      const request = getRequest(dispatch);

      const url = `/organizations/${ oId }/email_templates/${ emId }/elements`;
      await request.post(url, data);

      return resolve();
    } catch (e) {
      const error = handleApiError(e);
      return reject(error);
    }
  });
}

export function updateEmailTemplateElement (oId, emId, eleId, data) {
  return dispatch => new Promise(async (resolve, reject) => {
    try {
      const request = getRequest(dispatch);

      const url = `/organizations/${ oId }/email_templates/${ emId }/elements/${ eleId }`;
      await request.put(url, data);

      return resolve();
    } catch (e) {
      const error = handleApiError(e);
      return reject(error);
    }
  });
}

export function removeEmailTemplateElement (oId, emId, eleId) {
  return dispatch => new Promise(async (resolve, reject) => {
    try {
      const request = getRequest(dispatch);

      const url = `/organizations/${ oId }/email_templates/${ emId }/elements/${ eleId }`;
      await request.delete(url);

      return resolve();
    } catch (e) {
      const error = handleApiError(e);
      return reject(error);
    }
  });
}

export function sendTemplateEmails (oId, emId, data) {
  return dispatch => new Promise(async (resolve, reject) => {
    try {
      const request = getRequest(dispatch);

      const url = `/organizations/${ oId }/email_templates/${ emId }/send`;
      await request.post(url, data);

      return resolve();
    } catch (e) {
      const error = handleApiError(e);
      return reject(error);
    }
  });
}
