import React, { Component } from 'react';
import styled from 'styled-components';

import { nowrap } from '../../utils/styles';

const TableWrapper = styled.div`
  padding: 0 !important;
`;

const TableHeader = styled.div`
  display: flex;
  align-items: center;
  height: 48px;
  background-color: white;
  border-bottom: 1px solid ${ props => props.theme.colors.lightGray };
  padding: 0 30px;
`;

const HeaderCell = styled.div`
  flex: ${ props => props.flex || '1 0 0' };
  cursor: ${ props => (props.unsortable ? 'default' : 'pointer') };
  height: 100%;
  display: flex;
  align-items: center;
  text-transform: capitalize;
  color: ${ props => props.theme.colors.mainGray };
  padding-right: 10px;
  ${ nowrap } &:hover {
    color: ${ props => (props.unsortable
    ? props.theme.colors.mainGray
    : props.theme.colors.mainBlue) };
  }
`;

const HeaderTitle = styled.div`
  flex-shrink: 1;
  font-size: 12px;
  line-height: 18px;
  height: 18px;
  ${ nowrap };
`;

const ArrowWrapper = styled.div`
  flex-shrink: 0;
  padding-left: 10px;
`;

const ArrowDown = styled.div`
  flex-shrink: 0;
  width: 0;
  height: 0;
  border-left: 4.5px solid transparent;
  border-right: 4.5px solid transparent;
  border-top: 6px solid
    ${ props => (props.active ? props.theme.colors.darkBlue : props.theme.colors.mainGray) };
  margin-top: 3px;
`;

const ArrowUp = styled.div`
  width: 0;
  height: 0;
  border-left: 4.5px solid transparent;
  border-right: 4.5px solid transparent;
  border-bottom: 6px solid
    ${ props => (props.active ? props.theme.colors.darkBlue : props.theme.colors.mainGray) };
`;

const BodyRow = styled.div`
  display: flex;
  align-items: center;
  height: 66px;
  padding: 0 30px;
  background-color: ${ props => (props.colored ? props.theme.colors.bgGray : 'white') };
`;

const RowCell = styled.div`
  flex: ${ props => props.flex || '1 0 0' };
  height: 18px;
  color: ${ props => props.theme.colors.darkGray };
  font-size: 12px;
  line-height: 1.5;
  padding-right: 10px;
  ${ nowrap } &:first-child {
    font-weight: 700;
  }

  ${ props => props.customCss || '' };
`;

class MiniTable extends Component {
  constructor (props) {
    super(props);

    const { columns } = props;

    this.state = {
      activeSortOrder: 'asc',
      sortingField: columns.length ? columns[0].field : null,
    };
  }

  onSort (newSortingField) {
    const { sortingField } = this.state;
    let { activeSortOrder } = this.state;
    activeSortOrder = sortingField === newSortingField
      && activeSortOrder === 'asc'
      ? 'desc'
      : 'asc';

    this.setState({
      activeSortOrder,
      sortingField: newSortingField,
    });
  }

  render () {
    const { columns, rows } = this.props;
    const { activeSortOrder, sortingField } = this.state;

    const sortedRows = rows.slice();
    if (sortingField) {
      const column = columns.find(c => c.field === sortingField);

      if (column) {
        sortedRows.sort((r1, r2) => {
          if (r1[sortingField] !== null && r2[sortingField] === null) {
            return -1;
          }
          return activeSortOrder === 'desc'
            ? r1[sortingField] > r2[sortingField] ? -1 : 1
            : r1[sortingField] < r2[sortingField] ? -1 : 1;
        });
      }
    }

    const TableRows = sortedRows.map((r, i) => {
      const Cells = columns.map((c, j) => {
        const displayedValue = typeof c.format === 'function' ? c.format(r[c.field], r) : r[c.field];

        return (
          <RowCell key={ `mini-table-cell-${ i }-${ j }` } flex={ c.flex }>
            { displayedValue }
          </RowCell>
        );
      });

      return (
        <BodyRow key={ `mini-table-row-${ i }` } colored={ i % 2 }>
          { Cells }
        </BodyRow>
      );
    });

    return (
      <TableWrapper>
        <TableHeader>
          { columns.map((column, i) => (
            <HeaderCell
              flex={ column.flex }
              customCss={ column.customCss }
              unsortable={ column.unsortable }
              key={ i }
              onClick={
                  !column.unsortable
                    ? this.onSort.bind(this, column.field)
                    : null
                }
            >
              <HeaderTitle>{ column.heading }</HeaderTitle>
              { !column.unsortable ? (
                <ArrowWrapper>
                  <ArrowUp
                    active={
                        sortingField === column.field
                        && activeSortOrder === 'asc'
                      }
                  />
                  <ArrowDown
                    active={
                        sortingField === column.field
                        && activeSortOrder === 'desc'
                      }
                  />
                </ArrowWrapper>
              ) : null }
            </HeaderCell>
          )) }
        </TableHeader>
        <div>{ TableRows }</div>
      </TableWrapper>
    );
  }
}
export default MiniTable;
