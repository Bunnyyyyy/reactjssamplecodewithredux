import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { withRouter } from 'react-router';
import styled from 'styled-components';

import * as UiActionCreators from '../../../../../redux/ui';
import * as ManagerActionCreators from '../../../../../redux/manager';
import * as EventActionCreators from '../../../../../redux/event';

import SaveActions from '../../../../../components/manager/SaveActions';
import ElementsBuilder from '../../../../../components/manager/ElementsBuilder';
import Form from '../../../../../components/common/inputs/Form';

import { t as translate } from '../../../../../../i18n';

const t = a => translate(a, 'manager/events/ticketing/dataCollection');

const SaveActionsContainer = styled.div`
  background-color: white;
  z-index: 1;
  text-align: right;
  position: absolute;
  height: 44px;
  top: 0;
  left: 0;
  width: 100%;
  padding-right: 45px;
`;

const mapStateToProps = state => ({
  events: state.manager.events,
  event: state.manager.event,
  organization: state.manager.organization,
  fields: state.event.dataFields,
});

const ActionCreators = Object.assign(
  {},
  UiActionCreators,
  ManagerActionCreators,
  EventActionCreators,
);
const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators(ActionCreators, dispatch),
});

const TYPES = [
  {
    data_type: 'string',
    required: true,
    label: t('Short Text Input'),
    validate: e => e.label && e.label.length,
  },
  {
    data_type: 'text',
    required: true,
    label: t('Long Text Input'),
    validate: e => e.label && e.label.length,
  },
  {
    data_type: 'number',
    required: true,
    label: t('Number Input'),
    validate: e => e.label && e.label.length,
  },
  {
    data_type: 'string',
    required: true,
    options: [],
    label: t('Single Choice Options'),
    validate: e => e.label && e.label.length,
  },
  {
    data_type: 'string',
    required: true,
    options: [],
    multiple_options: true,
    label: t('Multiple Choice Options'),
    validate: e => e.label && e.label.length,
  },
  {
    data_type: 'boolean',
    required: true,
    label: t('Yes / No'),
    validate: e => e.label && e.label.length,
  },
  {
    data_type: 'file',
    required: true,
    label: t('File Upload'),
    validate: e => e.label && e.label.length,
  },
  {
    display_type: 'section',
    label: t('Section'),
    validate: e => e.display_text && e.display_text.length,
  },
  {
    display_type: 'title',
    label: t('Displayed Title'),
    validate: e => e.display_text && e.display_text.length,
  },
  {
    display_type: 'text',
    label: t('Displayed Paragraph'),
    validate: e => e.display_text && e.display_text.length,
  },
  {
    display_type: 'contacts',
    label: t('Contact Information'),
    hidden: true,
  },
];

const DEFAULT_ELEMENTS = [
  {
    display_type: 'contacts',
    order: 0,
  },
];

class EventTicketingDataCollectionView extends Component {
  async componentDidMount () {
    const { actions, match } = this.props;
    const { eId } = match.params;

    actions.setLoading(true);


    try {
      await actions.getEventDataFields(eId);
    } catch (e) {
      console.error('ERROR: EventTicketingDataCollectionView', e);
    } finally {
      actions.setLoading(false);
    }
  }

  componentWillUnmount () {
    const { actions } = this.props;
    actions.resetSaveActions();
  }

  async onFinishSaving () {
    try {
      const { actions, event } = this.props;
      const eId = event.id;
      await Promise.all([
        actions.getOrganizationEvent(eId),
        actions.getEventDataFields(eId),
      ]);
    } catch (e) {
      throw e;
    }
  }

  async onUpdateForm (data) {
    const { actions, event } = this.props;

    delete data._iceredIdAvailable;
    actions.updateEvent(event.id, data);
  }

  render () {
    const {
      actions,
      event = {},
      fields,
    } = this.props;
    const DATA_COLLECTION = [
      {
        type: 'checkbox',
        field: 'data_fields_editable',
        label: 'Allow guest to change their info after submit the form?',
        default: event.data_fields_editable,
      },
    ];

    const EDIT_DATA = [
      <Form
        name='data-collection'
        fields={ DATA_COLLECTION }
        onUpdate={ this.onUpdateForm.bind(this) }
      />,
    ];

    return [
      <SaveActionsContainer>
        <SaveActions onFinish={ this.onFinishSaving.bind(this) } />
      </SaveActionsContainer>,
      <ElementsBuilder
        object={ event }
        withSubheader
        noSaveActions
        elementTypes={ TYPES }
        elements={ fields.length ? fields : DEFAULT_ELEMENTS }
        createElementAction={ element => actions.createEventDataField(event.id, element)
        }
        updateElementAction={ (eleId, element) => actions.updateEventDataField(event.id, eleId, element)
        }
        removeElementAction={ eleId => actions.removeEventDataField(event.id, eleId)
        }
        onFinish={ this.onFinishSaving.bind(this) }
        saveActionsCss='
          position: absolute;
          z-index: 799;
          width: 100%;
          top: 0;
          right: 0;
          box-shadow: 0 0 6px 0 rgba(0, 0, 0, 0.15);
        '
        extraElement={ EDIT_DATA }
      />,
    ];
  }
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(EventTicketingDataCollectionView));
