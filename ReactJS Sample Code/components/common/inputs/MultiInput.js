import React, { Component } from 'react';
import styled from 'styled-components';
import Creatable from 'react-select/lib/Creatable';
import Color from 'color';

import Label from './Label';
import InvalidMessage from '../InvalidMessage';
import { nowrap } from '../../../utils/styles';

const Wrapper = styled.div`
  position: relative;
  display: inline-block;
  width: ${ props => (props.fullWidth ? '100%' : '400px') };
  margin-top: 20px;
  margin-bottom: 20px;

  &__single-value {
    position: absolute;
    top: 50%;
    max-width: calc(100% - 8px);
    -webkit-transform: translateY(-50%);
    -ms-transform: translateY(-50%);
    transform: translateY(-50%);
    font-size: 15px;
    line-height: 1.4;
    font-weight: 400;
    margin: 0;
    ${ nowrap };
  }

  ${ props => props.customCss || '' };
`;

const InnerWrapper = styled.div`
  .control__control,
  .control__control:focus,
  .control__control--is-focused {
    min-height: 32px;
    background-color: transparent;
    border: none;
    border-bottom: 1px solid rgba(47, 46, 46, 0.25);
    border-radius: 0px;
    box-shadow: none;
    padding: 0px;
  }

  .control__control--is-focused {
    border-color: ${ props => props.theme.colors.mainBlue } !important;
  }

  .control__control .control__value-container {
    padding: 0px;
    max-width: calc(100% - 10px);
    display: flex;

    > div {
      max-width: 100%;
      height: 26px;
      margin-top: 0;
      margin-bottom: 2px;
      padding: 5px;

      > div:first-child {
        ${ nowrap } overflow: hidden;
        padding-left: 5px;
        padding-right: 5px;
      }
    }
  }

  .control__control .control__multi-value__remove {
    color: #6b8dad;
    background: none;
  }

  .control__value-container > div {
    padding-left: 0 !important;
  }

  .control__placeholder {
    padding: 5px 0 !important;
    color: rgba(47, 46, 46, 0.25);
  }

  .control__input {
    height: 32px;
    padding: 0 !important;
  }

  .control__control input {
    font-size: 12px !important;
    min-width: 50px !important;
    caret-color: ${ ({ theme }) => theme.colors.gray };
  }
`;

const EmptyComponent = () => null;

const MultiValueContainer = styled.div`
  background-color: #f2f6fa;
  display: flex;
  padding: 5px 15px;
  border-radius: 5px;
  border: 1px solid #adceef;
  cursor: pointer;
  z-index: 101;
  margin: 0 10px 0 0;
`;

const MultiValueLabel = styled.div`
  font-size: 13px;
  padding-left: 15px;
  color: ${ ({ theme }) => Color(theme.lightGray)
    .fade(0.3)
    .string() };
`;

export default class MultiInput extends Component {
  constructor () {
    super();

    this.state = {
      inputValue: '',
    };
  }

  onChange (options = []) {
    const { onChange } = this.props;

    const value = options.map(o => o.value);
    if (onChange) onChange(value);
  }

  onInputChange (inputValue) {
    this.setState({ inputValue });
  }

  onKeyDown (e) {
    const { onChange, value } = this.props;
    const { inputValue } = this.state;
    if (!inputValue || !inputValue.length) return;

    // press Tab or Enter keys
    if (e.keyCode === 9 || e.keyCode === 13) {
      this.setState({ inputValue: '' });
      if (onChange) onChange(value.concat([inputValue]));

      e.preventDefault();
      // press Esc key
    } else if (e.keyCode === 27) {
      this.setState({ inputValue: '' });
      e.preventDefault();
    }
  }

  render () {
    const {
      underline,
      placeholder,
      value = [],
      fullWidth,
      field,
      label,
      customCss,
      errorMessage,
      required,
      onChange,
      ...rest
    } = this.props;
    const { inputValue } = this.state;
    const valueOptions = value
      ? value.map(v => ({
        label: v,
        value: v,
      }))
      : [];

    return (
      <Wrapper customCss={ customCss } fullWidth={ fullWidth }>
        { label ? (
          <Label
            field={ field }
            label={ label }
            invalid={ !!errorMessage }
            required={ required }
          />
        ) : null }
        <InnerWrapper underline={ underline }>
          <Creatable
            classNamePrefix='control'
            components={ {
              DropdownIndicator: EmptyComponent,
              IndicatorSeparator: EmptyComponent,
              MultiValueContainer,
              MultiValueLabel,
            } }
            inputValue={ inputValue }
            isClearable={ false }
            isMulti
            menuIsOpen={ false }
            onChange={ this.onChange.bind(this) }
            onInputChange={ this.onInputChange.bind(this) }
            onKeyDown={ this.onKeyDown.bind(this) }
            value={ valueOptions }
            { ...rest }
          />
          { errorMessage ? (
            <InvalidMessage>{ errorMessage }</InvalidMessage>
          ) : null }
        </InnerWrapper>
      </Wrapper>
    );
  }
}
