import React, { Component } from 'react';
import styled from 'styled-components';

import InvalidMessage from './InvalidMessage';

const Wrapper = styled.div`
  position: relative;
  line-height: 1.4;
  margin-bottom: 20px;
  margin-right: 20px;

  ${ props => props.customCss || '' };
`;

const Box = styled.label`
  position: absolute;
  left: 0;
  top: 1.5px;
  width: 18px;
  height: 18px;
  cursor: pointer;
  border-radius: 2px;
  border: 1px solid ${ props => props.theme.colors.deadGray };

  &:after {
    opacity: 0;
    content: '';
    position: absolute;
    width: 9px;
    height: 5px;
    left: 3.5px;
    top: 4px;
    border: 1px solid white;
    border-top: none;
    border-right: none;
    background: transparent;
    transform: rotate(-45deg);
  }

  ${ props => (!props.disabled
    ? `
    &:hover,
    &:active {
      background-color: ${ props.theme.colors.mainBlue };
      border-color: ${ props.theme.colors.lightGray };
      opacity: 0.15;
    }

    &:hover:after {
      opacity: 0.7;
    }

    &:active:after {
      opacity: 0.85;
    }
  `
    : `
    cursor: default;
  `) } ${ props => props.customCss };
`;

const BoxWrapper = styled.div`
  position: relative;
  display: inline-block;
  vertical-align: middle;
  width: 31px;
  height: 21px;
`;

const CheckboxStyled = styled.input`
  display: none;
  user-select: none;
  margin-right: 10px;
  vertical-align: middle;

  &:checked + ${ BoxWrapper } > label {
    background-color: ${ props => props.theme.colors.mainBlue };
    border-color: ${ props => props.theme.colors.mainBlue };
    opacity: 1;

    &:after {
      opacity: 1;
      border-color: white;
    }

    ${ props => (!props.disabled
    ? `
      &:hover {
        opacity: 0.85;
      }
    `
    : '') };
  }

  &:disabled + ${ BoxWrapper } > label {
    opacity: 0.5;
  }
`;

const CheckboxText = styled.p`
  display: inline-block;
  vertical-align: middle;
`;

const Label = styled.label`
  display: inline-block;
  cursor: pointer;
  user-select: none;
`;

export default class Checkbox extends Component {
  onChange (e) {
    const { onChange } = this.props;
    const value = e.target.checked;

    if (onChange) onChange(value);
  }

  render () {
    const {
      id,
      name,
      field,
      style,
      spanStyle,
      labelCss,
      readOnly,
      onChange,
      customCss = '',
      errorCss = '',
      label,
      errorMessage,
      children,
      ...props
    } = this.props;
    const checkboxId = id || name || field;

    return (
      <Wrapper customCss={ customCss }>
        <Label htmlFor={ checkboxId }>
          <CheckboxStyled
            type='checkbox'
            id={ checkboxId }
            name={ checkboxId }
            data-field={ field }
            disabled={ readOnly }
            onChange={ this.onChange.bind(this) }
            { ...props }
          />
          <BoxWrapper>
            <Box
              htmlFor={ checkboxId }
              style={ style }
              customCss={ labelCss }
              disabled={ readOnly }
            />
          </BoxWrapper>
          { label ? <CheckboxText>{ label }</CheckboxText> : null }
          { children }
        </Label>
        { errorMessage ? (
          <InvalidMessage customCss={ errorCss }>{ errorMessage }</InvalidMessage>
        ) : null }
      </Wrapper>
    );
  }
}
