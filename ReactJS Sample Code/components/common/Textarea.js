import React from 'react';
import styled from 'styled-components';

import Label from './inputs/Label';
import InvalidMessage from './InvalidMessage';

const Div = styled.div`
  position: relative;
  display: inline-block;
  margin-top: 20px;
  margin-bottom: 20px;
  margin-right: 20px;
`;

const TextareaStyled = styled.textarea`
  margin-top: 10px;
  resize: none;
  max-width: 100%;
  width: 620px;
  height: 111px;
  line-height: 22px;
  padding: 10px 15px;
`;

const Textarea = ({
  className,
  type,
  label,
  field,
  onChange,
  error,
  children,
  divStyle = {},
  required,
  errorMessage,
  ...props
}) => (
  <Div className={ className } style={ divStyle }>
    { label ? <Label field={ field } label={ label } required={ required } /> : null }
    <TextareaStyled
      id={ field }
      data-field={ field }
      onChange={ onChange }
      { ...props }
    />
    { errorMessage ? <InvalidMessage>{ errorMessage }</InvalidMessage> : null }
    { children }
  </Div>
);

export default Textarea;
