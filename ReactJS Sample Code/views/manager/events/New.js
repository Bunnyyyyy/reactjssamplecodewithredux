import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { withRouter } from 'react-router';
import styled from 'styled-components';
import moment from 'moment';

import * as UiActionCreators from '../../../redux/ui';
import * as ManagerActionCreators from '../../../redux/manager';
import * as EventActionCreators from '../../../redux/event';
import * as ProfileActionCreators from '../../../redux/profile';

import ManagerContainer from '../../../components/layouts/ManagerContainer';
import SaveActions from '../../../components/manager/SaveActions';
import EditInfoCard from '../../../components/manager/cards/EditInfoCard';
import CategoryOption from '../../../components/common/inputs/CategoryOption';
import BiOption from '../../../components/common/inputs/BiOption';
import EventDatesCard from '../../../components/manager/events/EventDatesCard';

import currencies from '../../../utils/currencies';

import { t as translate } from '../../../../i18n';

const t = a => translate(a, 'manager/events/new');

const CURRENCY_OPTIONS = [];
for (const currency in currencies) {
  CURRENCY_OPTIONS.push({
    value: currency,
    label: `${ currencies[currency].name } (${ currencies[currency].symbol })`,
  });
}

const timezone_filters = ['Etc/GMT', 'Etc/GMT0', 'Etc/GMT+0', 'Etc/GMT-0'];
const TIMEZONE_OPTIONS = moment.tz
  .names()
  .map(tz => ({
    label: tz
      .replace('Etc/', '')
      .replace(/\//g, ' - ')
      .replace(/_/g, ' '),
    value: tz,
  }))
  .filter(tz => !timezone_filters.includes(tz.value));

const EVENT_TYPE_OPTIONS = [
  {
    value: false,
    label: t('Public'),
    description: t('Event is searchable on our public platform juven.co.'),
  },
  {
    value: true,
    label: t('Private'),
    description: t('Event is only reachable through direct URL link.'),
  },
];

const FormSubtitle = styled.div`
  margin: 0 -30px;
  padding: 20px 30px;
  border-top: 1px solid ${ props => props.theme.colors.lightGray };
  border-bottom: 1px solid ${ props => props.theme.colors.lightGray };

  ${ props => (props.first ? 'border-top: none;' : 'margin-top: 20px;') } h5 {
    font-weight: bold;
  }
`;

const BiOptionTitle = styled.p`
  margin-bottom: 5px;
`;

const BiOptionDescription = styled.p`
  font-size: 12px;
  line-height: 18px;
  color: ${ props => props.theme.colors.deadGray };
`;

const RsvpAppendix = styled.p`
  font-size: 12px;
  line-height: 14px;
`;

const mapStateToProps = state => ({
  events: state.manager.events,
  event: state.manager.event,
  organization: state.manager.organization,
  categories: state.manager.categories,
});
const ActionCreators = Object.assign(
  {},
  UiActionCreators,
  ManagerActionCreators,
  EventActionCreators,
  ProfileActionCreators,
);
const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators(ActionCreators, dispatch),
});

class EventsNewView extends Component {
  constructor () {
    super();

    this.state = {
      validateForms: false,
    };
  }

  async componentDidMount () {
    const { actions } = this.props;
    actions.setLoading(true);

    try {
      actions.setOrganizationEvent();
      await actions.getCategories('event');
    } catch (e) {
      console.error('ERROR: ManagerEventNew', e);
    } finally {
      actions.setLoading(false);
    }
  }

  componentWillUnmount () {
    const { actions } = this.props;
    actions.resetSaveActions();
  }

  async onFinishSaving () {
    try {
      const { organization, history, event } = this.props;
      // something went wrong if there is no event.id

      const targetUrl = `/${ organization.id }/events/${
        event.id
      }/ticketing/types/new`;

      return history.push(targetUrl);
    } catch (e) {
      throw e;
    }
  }

  onCancelSaving () {
    const { organization, history } = this.props;

    const targetUrl = `/${ organization.id }/events`;
    return history.push(targetUrl);
  }

  onInvalidSave () {
    this.setState({ validateForms: true });
  }

  // To be used later
  async saveImage (eId, file, type) {
    const { actions } = this.props;

    const formData = new FormData();
    formData.append('image', file);
    formData.append('type', type);

    try {
      await actions.uploadProfileImage(eId, formData);
    } catch (e) {
      throw e;
    }
  }

  async createEvent (state) {
    try {
      const { actions, organization } = this.props;
      const eId = state.icered_id;
      const data = {
        ...state,
        organization_id: organization.id,
      };

      await actions.createEvent(data);

      try {
        if (state.cover_image) {
          await this.saveImage(eId, state.cover_image, 'cover');
        }

        return Promise.all([
          actions.getOrganizationEvents(organization.id),
          actions.getOrganizationEvent(eId),
        ]);
      } catch (e) {
        // allow image upload failure
        console.error(e);
      }
    } catch (e) {
      throw e;
    }
  }

  async createEventDates (eventDates) {
    try {
      const { event, actions } = this.props;

      for (let i = 0; i < eventDates.length; i++) {
        const { timeslots } = eventDates[i];
        const dateData = Object.assign({}, eventDates[i]);

        // format the start and end dates properly
        dateData.start_date = dateData.start_date
          ? moment(dateData.start_date).format('YYYY-MM-DD')
          : null;
        dateData.end_date = dateData.end_date
          ? moment(dateData.end_date).format('YYYY-MM-DD')
          : null;
        delete dateData.timeslots;

        const date = await actions.createEventDate(event.id, dateData);

        // HACK: must do the timeslot adding one by one, otherwise race condition occurs and everything turns into shit
        for (let j = 0; j < timeslots.length; j++) {
          await actions.addEventDateTimeslot(event.id, date.id, timeslots[j]);
        }
      }
    } catch (e) {
      throw e;
    }
  }

  render () {
    const { categories } = this.props;
    const { validateForms } = this.state;

    const eventDetailsFields = [
      {
        type: 'custom',
        field: 'title-1',
        readOnly: true,
        renderComponent: () => (
          <FormSubtitle first>
            <h5>{ t('1. Event Info*') }</h5>
          </FormSubtitle>
        ),
      },
      {
        type: 'section',
        field: 'event-name-section',
        flex: '0 0 50%',
        fields: [
          {
            type: 'input',
            field: 'name',
            label: t('Event Name'),
            customCss: 'margin-bottom: 0;',
            errorCss: 'position: relative;',
            required: true,
          },
          {
            type: 'juvenId',
            field: 'icered_id',
            label: t('Event URL'),
            required: true,
            getErrorMessage: (formState, value, label) => {
              if (
                value
                && value.length
                && formState
                && formState._iceredIdAvailable
              ) {
                return null;
              }
              return `${ t('Please enter a valid') } ${ label }`;
            },
          },
        ],
      },
      {
        type: 'image',
        field: 'cover_image',
        label: `${ t('Event Banner') } (2:1 ratio | recommended size: 2000 x 1000 px)`,
        flex: '0 0 50%',
        width: 500,
        height: 250,
      },
      {
        type: 'rte',
        field: 'description',
        placeholder: t('Fill in description here...'),
        label: t('Event Description'),
        required: true,
        getErrorMessage: (formState, value, label) => {
          if (value && value.replace(/(<([^>]+)>)/gi, '').length) {
            return null;
          }
          return `${ t('Please enter a valid') } ${ label }`;
        },
      },
      {
        type: 'options',
        field: 'category_id',
        label: t('Select Event Category (Most relevant one)'),
        options: categories.map(c => ({
          value: c.id,
          component: (
            <CategoryOption>
              <div>
                <i className={ `fas fa-${ c.icon }` } />
              </div>
              <h6>{ c.name }</h6>
            </CategoryOption>
          ),
        })),
      },
      {
        type: 'options',
        field: 'private',
        label: t('Event Type'),
        default: false,
        required: true,
        options: EVENT_TYPE_OPTIONS.map(c => ({
          value: c.value,
          component: () => (
            <BiOption customCss='height: auto; padding: 15px;'>
              <BiOptionTitle>{ c.label }</BiOptionTitle>
              <BiOptionDescription>{ c.description }</BiOptionDescription>
            </BiOption>
          ),
        })),
      },
      {
        type: 'select',
        field: 'currency',
        label: t('Ticketing Currency'),
        customCss: 'padding-right: calc(50% + 20px);',
        options: CURRENCY_OPTIONS,
        default: 'hkd',
        required: true,
      },
      {
        type: 'checkbox',
        field: 'includes_fee',
        label: t('Includes Stripe Fee in Ticket Prices'),
        default: true,
      },
      {
        type: 'checkbox',
        field: 'requires_approval',
        label: t('Requires admin approval for ticket reservations*'),
        default: false,
      },
      {
        type: 'custom',
        field: 'requires_approval-appendix',
        readOnly: true,
        renderComponent: () => (
          <RsvpAppendix>
            { `*${ t('If you choose this option, it will require administrator approval before particpants can pay/confirm their tickets.') }` }
          </RsvpAppendix>
        ),
      },
      {
        type: 'custom',
        field: 'title-2',
        readOnly: true,
        renderComponent: () => (
          <FormSubtitle>
            <h5>{ t('2. Event Location*') }</h5>
          </FormSubtitle>
        ),
      },
      {
        type: 'section',
        field: 'event-location-section',
        flex: '0 0 50%',
        fields: [
          {
            type: 'input',
            field: 'location_address',
            label: t('Location'),
            required: true,
          },
          {
            type: 'input',
            field: 'location',
            label: t('Venue'),
          },
          {
            type: 'select',
            field: 'timezone',
            label: t('Timezone'),
            default: 'Asia/Hong_Kong',
            options: TIMEZONE_OPTIONS,
            required: true,
          },
        ],
      },
    ];

    return (
      <ManagerContainer
        contentCss='height: calc(100vh - 56px);transform: translateY(-44px);padding-top: 78px;'
        subheader={ (
          <SaveActions
            onFinish={ this.onFinishSaving.bind(this) }
            onCancel={ this.onCancelSaving.bind(this) }
            onInvalid={ this.onInvalidSave.bind(this) }
            saveNewItem
          />
        ) }
        subheaderCss='justify-content: right;'
      >
        <EditInfoCard
          name='event-details'
          fields={ eventDetailsFields }
          action={ this.createEvent.bind(this) }
          actionOrder={ 0 }
          validateForm={ validateForms }
          customCss='
            padding-top: 0;
            margin-bottom: 0;
          '
        />
        <EventDatesCard
          name='event-dates'
          handleEventDates={ this.createEventDates.bind(this) }
          actionOrder={ 1 }
          validateForm={ validateForms }
        />
      </ManagerContainer>
    );
  }
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(EventsNewView));
