import React from 'react';
import styled from 'styled-components';

import Tooltip from '../Tooltip';
import { nowrap } from '../../../utils/styles';

const Wrapper = styled.div`
  position: relative;
  flex-grow: 1;
  flex-basis: 0;
  height: 64px;
  &:hover ${ Tooltip } {
    visibility: visible;
  }

  ${ props => props.customCss || '' };
`;

// HACK: crazy hack to facilitate overflow clip and tooltip
const ColumnBody = styled.div`
  position: absolute;
  top: 0;
  bottom: 0;
  left: 0;
  right: 0;
  padding: ${ props => (props.fixed ? '0 15px 0 30px' : '0 15px') };
  display: flex;
  align-items: center;
  font-weight: 400;
  cursor: default;
  ${ props => (!props.wrapText ? nowrap : '') };
`;

const InnerWrapper = styled.div`
  display: inline-block;
  max-width: 100%;
  font-size: 12px;
  ${ nowrap } ${ props => props.customCss || '' };

  & p {
    ${ nowrap } font-size: 12px;
    line-height: 1.5;
  }
`;

const RowColumn = ({
  fixed,
  style,
  customCss,
  bodyCss,
  value,
  tooltipValue = null,
  tooltipCss = '',
  showTooltip,
  wrapText,
  parentRef,
  bodyRef,
}) => (
  <Wrapper style={ style } customCss={ customCss } innerRef={ parentRef }>
    <ColumnBody fixed={ fixed } wrapText={ wrapText }>
      <InnerWrapper innerRef={ bodyRef } customCss={ bodyCss }>
        { typeof value === 'string' ? <p>{ value }</p> : value }
      </InnerWrapper>
    </ColumnBody>
    { showTooltip && (
    <Tooltip customCss={ tooltipCss }>{ tooltipValue || value }</Tooltip>
    ) }
  </Wrapper>
);

export default RowColumn;
