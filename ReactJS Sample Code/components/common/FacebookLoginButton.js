import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import styled from 'styled-components';

import Button from './Button';
import { boxShadow } from '../../utils/styles';

import * as MeActionCreators from '../../redux/me';
import * as ManagerActionCreators from '../../redux/manager';

import i18n from '../../../i18n';

const t = a => i18n.t([`components/common:${ a }`]);

const FACEBOOK_COLOR = 'rgb(59, 89, 152)';

const FacebookLoginStyledButton = styled(Button)`
  box-shadow: 0 6px 6px 0 rgba(0, 0, 0, 0.16);
  background-color: ${ FACEBOOK_COLOR };
  color: white;
  border-radius: 21px;
  ${ boxShadow('0 6px 6px 0 rgba(0, 0, 0, 0.16)') } &:hover, &:active {
    background-color: ${ props => props.theme.colors.mainBlue };
  }

  display: flex;
  align-items: center;
  justify-content: center;

  & p {
    color: white;
  }

  & i {
    margin-right: 20px;
    font-size: 20px;
  }

  @media (max-width: 900px) {
    height: 42px;
    line-height: 42px;
  }

  ${ props => props.customCss || '' };
`;

const SmallScreenHiddenText = styled.span`
  @media (max-width: 450px) {
    display: none;
  }
`;

const ActionCreators = Object.assign(
  {},
  MeActionCreators,
  ManagerActionCreators,
);

const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators(ActionCreators, dispatch),
});

class FacebookLoginButton extends Component {
  async onClick () {
    try {
      if (typeof FB === 'undefined') return;
      const { actions, onFinish } = this.props;
      const accessToken = await new Promise((resolve) => {
        FB.login(
          (response) => {
            const { status, authResponse } = response;

            if (status === 'connected' && authResponse.accessToken) {
              return resolve(authResponse.accessToken);
            }
          },
          // TEMP: add auth_type for now due to Facebook bug:
          // https://stackoverflow.com/questions/50124462/facebook-graph-api-rejects-newly-created-access-token
          {
            auth_type: 'reauthorize',
            scope: 'public_profile,email',
          },
        );
      });

      if (!accessToken) return;

      const data = { input_token: accessToken };
      await actions.loginWithFacebook(data);
      await actions.getMyManagedOrganizations();
      await actions.getMe();
      if (onFinish) await onFinish();
    } catch (e) {
      console.error(e);
    }
  }

  render () {
    const { signup, ...props } = this.props;

    return (
      <FacebookLoginStyledButton
        fullWidth
        type='button'
        { ...props }
        onClick={ this.onClick.bind(this) }
      >
        <i className='fab fa-facebook-f' aria-hidden='true' />
        <p>
          <SmallScreenHiddenText>{ `${ signup ? t('Sign up with') : t('Log in with') } ` }</SmallScreenHiddenText>
          <span>Facebook</span>
        </p>
      </FacebookLoginStyledButton>
    );
  }
}

export default connect(null, mapDispatchToProps)(FacebookLoginButton);
