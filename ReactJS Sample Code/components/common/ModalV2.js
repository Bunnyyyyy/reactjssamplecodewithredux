import React from 'react';
import ReactModal from 'react-modal';
import styled, { ThemeProvider } from 'styled-components';
import closeIcon from '../../assets/common/close_icon.svg';

const customStyles = {
  overlay: {
    zIndex: 999,
    backgroundColor: 'rgba(0, 0, 0, 0.5)',
  },
};

const StyledModal = styled(ReactModal)`
  position: absolute;
  left: 50%;
  top: 50%;
  transform: translate(-50%, -50%);
  overflow: scroll;
  max-height: calc(100vh - 180px);
  width: ${ props => (props.small ? '418px' : props.medium ? '530px' : '778px') };
  border: none;
  border-radius: 5px;
  background-color: white;
  -webkit-overflow-scrolling: touch;
  outline: none;

  h2 {
    text-transform: none;
  }

  ${ props => (props.theme.light
    ? `
    background-color: ${ props.theme.colors.darkBlue };

    h2, h3, h4, h5, h6, p, a, li {
      color: white;
    }
  `
    : null) };

  ${ props => props.customCss || '' };

  @media (max-width: 900px) {
    left: 0;
    top: 0;
    transform: none;
    width: 100%;
    height: 100vh;
    max-height: 100vh;
    border-radius: 0;
  }
`;

const Header = styled.div`
  position: absolute;
  display: flex;
  justify-content: space-between;
  align-items: center;
  width: 100%;
  left: 0;
  top: 0;
  background-color: ${ props => (props.theme.light ? props.theme.colors.darkBlue : 'white') };
  padding: 20px 30px 20px 30px;
  border-bottom: 1px solid ${ props => props.theme.colors.lightGray };
  z-index: 999;

  @media (max-width: 900px) {
    padding: 20px;
  }

  ${ props => props.customCss || '' };
`;

const CloseButton = styled.img`
  font-weight: 600;
  z-index: 999;
  height: 36px;
  padding: 10px;
  margin: 0 -5px 0 0;
  cursor: pointer;
  color: ${ props => (props.theme.light ? 'white' : props.theme.colors.darkGray) };

  &:hover {
    opacity: 0.7;
  }
`;

const Title = styled.h4`
  color: ${ props => (props.theme.light ? 'white' : props.theme.colors.darkGray) };
`;

const Body = styled.div`
  max-height: calc(100vh - 180px);

  padding: ${ props => (props.noPadding
    ? '0'
    : props.noHeader ? '0 30px 0 30px' : '85px 30px 0 30px') };
  position: relative;
  overflow-y: scroll;

  > *:last-child {
    margin-bottom: ${ props => (props.hasFooter ? '90px' : '0') };
  }

  @media (max-width: 900px) {
    max-height: calc(100vh - 140px);
    padding: ${ props => (props.noHeader ? '0' : '84') }px 20px 25px 20px;
  }

  ${ props => props.customCss || '' };
`;

const Footer = styled.div`
  position: fixed;
  left: 0;
  bottom: 0;
  width: 100%;
  background-color: white;
  box-shadow: 0 -3px 6px 0 rgba(0, 0, 0, 0.15);

  @media (min-width: 901px) {
    position: absolute;
    height: 78px;
  }

  ${ props => props.customCss || '' };
`;

export default function Modal ({
  onClose,
  isOpen,
  children,
  light = false,
  title = '',
  headerCss,
  footerContent,
  footerCss,
  bodyCss,
  noHeader,
  noPadding,
  bodyInnerRef = () => { },
  ...props
}) {
  const TitleComponent = typeof title === 'string' ? <Title>{ title }</Title> : title;
  return (
    <ThemeProvider theme={ { light } }>
      <StyledModal
        isOpen={ isOpen }
        onRequestClose={ onClose }
        ariaHideApp={ false }
        style={ customStyles }
        { ...props }
      >
        { !noHeader ? (
          <Header customCss={ headerCss }>
            { TitleComponent }
            { onClose ? (
              <CloseButton plain onClick={ onClose } src={ closeIcon } />
            ) : null }
          </Header>
        ) : null }
        <Body
          noHeader={ noHeader }
          noPadding={ noPadding }
          hasFooter={ !!footerContent }
          customCss={ bodyCss }
          innerRef={ bodyInnerRef }
        >
          { children }
        </Body>
        { footerContent ? (
          <Footer customCss={ footerCss }>{ footerContent }</Footer>
        ) : null }
      </StyledModal>
    </ThemeProvider>
  );
}
