import React, { Component } from 'react';
import styled from 'styled-components';

import Button from '../Button';
import Input from '../Input';

import { inputPlaceholder } from '../../../utils/styles';

import i18n from '../../../../i18n';

const t = a => i18n.t([`components/common:${ a }`]);

const SearchDiv = styled.div`
  flex-grow: 1;
  display: flex;
  align-items: center;
  height: 100%;
`;

const SearchIcon = styled.div`
  width: 40px;
  height: 32px;
  text-align: center;

  & i {
    line-height: 32px;
    color: ${ props => props.theme.colors.mainGray };
    margin-right: 0;
  }

  &:hover,
  &:active {
    & i {
      color: ${ props => props.theme.colors.darkGray };
    }
  }
`;

const CancelButton = styled(Button)`
  color: ${ props => props.theme.colors.mainGray };
  padding: 0 10px;

  &:hover,
  &:active {
    color: ${ props => props.theme.colors.darkGray };
  }
`;

const FilterCount = styled.p`
  font-size: 12px;
  margin: 0 10px;
  color: ${ props => props.theme.colors.mainGray };
`;

export default class Search extends Component {
  constructor () {
    super();
    this.state = {
      searchValue: null,
    };
  }

  onInputKeyDown (e) {
    // close Search if user clicks Esc
    if (e.keyCode === 27) this.changeValue(null, null);
  }

  changeValue (f, value) {
    const { onChangeSearchValue } = this.props;

    if (this.timer) clearTimeout(this.timer);
    this.timer = setTimeout(() => onChangeSearchValue(value), 300);

    this.setState({ searchValue: value });
  }

  render () {
    const { searchValue } = this.state;
    const { placeholder, filters = {} } = this.props;

    const filterCount = Object.keys(filters).length;

    const SearchMode = [
      <SearchIcon>
        <i className='fas fa-search' />
      </SearchIcon>,
      <Input
        fullWidth
        placeholder={ placeholder }
        value={ searchValue }
        inputRef={ input => (this.searchInput = input) }
        onChange={ this.changeValue.bind(this) }
        onKeyDown={ this.onInputKeyDown.bind(this) }
        customCss={ `
          margin: 0;

          & input {
            color: ${ props => props.theme.colors.darkBlue };
            border-bottom: none;
            opacity: 1;

            ${ inputPlaceholder(`
              font-size: 12px;
              font-weight: 400;
            `) };
          }
        ` }
      />,
      searchValue && searchValue.length ? (
        <CancelButton plain onClick={ this.changeValue.bind(this, null, null) }>
          ✕
        </CancelButton>
      ) : null,
    ];

    const FilterMode = (
      <FilterCount>
        { `${ filterCount } ${
          filterCount.length > 1 ? t('filters applied.') : t('filter applied.')
        }` }
      </FilterCount>
    );

    return <SearchDiv>{ filterCount > 0 ? FilterMode : SearchMode }</SearchDiv>;
  }
}
