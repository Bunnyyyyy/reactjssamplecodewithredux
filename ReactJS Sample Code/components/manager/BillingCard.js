import React, { Component } from 'react';
import styled, { ThemeProvider } from 'styled-components';

import Button from '../common/Button';
import Input from '../common/Input';

import i18n from '../../../i18n';

const t = a => i18n.t([`translations:${ a }`]);

const ModalBodyWrapper = styled.div`
  padding-bottom: 30px;
`;

const YourChoiceWrapper = styled.div`
  display: flex;
  align-items: center;
`;

const YourChoiceText = styled.p`
  color: rgb(125, 144, 154);
  height: 50px;
  text-align: center;
  width: 100%;
  line-height: 50px;
  border-bottom: 1px solid #e1e8ed;
  border-radius: 0px;
  font-size: 15px;
  font-weight: bold;
  color: black;
`;

const ViewFullPricing = styled(Button)`
  display: block;
  margin: 0 auto;
  font-size: 15px;
  font-weight: 400;
  text-transform: none;
  width: 100%;

  color: ${ props => props.theme.colors.mainGray };
  margin-top: 10px;
  margin-bottom: 10px;
  i {
    padding-left: 10px;
  }
`;

const PriceWrapper = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  padding: 20px 0;
`;

const PriceWrapperHalf = styled.div`
  flex: 0 0 50%;
  display: flex;
  align-items: center;
  padding: 0 20px;

  ${ props => (props.left
    ? `
    justify-content: flex-end;
    border-right: 1px solid ${ props.theme.colors.lightGray };
  `
    : '') } h2 {
    color: ${ props => props.theme.colors.mainBlue };
    line-height: 35px;
    margin-right: 10px;
    margin-bottom: 5px;
    font-weight: normal;
  }
  h4 {
    font-weight: normal;
  }

  p {
    font-size: 20px;
  }
`;

const SliderInput = styled.input`
  -webkit-appearance: none;
  cursor: pointer;
  outline: none;
  border-radius: 7.5px;
  background-color: ${ props => props.theme.colors.lightGray };
  margin: 20px 0;
  width: 611px;
  height: 9px;
  & :focus {
    outline: none;
  }

  &::-webkit-slider-thumb {
    width: 20px;
    height: 20px;
    border-radius: 50%;
    background-color: ${ props => props.theme.colors.darkBlue };
    cursor: pointer;
    -webkit-appearance: none;
    appearance: none;
    opacity: 1;
  }

  &::-moz-range-thumb {
    width: 20px;
    height: 20px;
    border-radius: 50%;
    background-color: ${ props => props.theme.colors.darkBlue };
    cursor: pointer;
  }

  &::-ms-thumb {
    width: 20px;
    height: 20px;
    border-radius: 50%;
    background-color: ${ props => props.theme.colors.darkBlue };
    cursor: pointer;
  }

  &::-webkit-slider-runnable-track {
    cursor: pointer;
    background: transparent;
    border-color: transparent;
    box-shadow: none;
  }

  &::-moz-range-track {
    cursor: pointer;
    background: transparent;
    border-color: transparent;
    box-shadow: none;
  }

  &::-ms-track {
    cursor: pointer;
    background: transparent;
    border-color: transparent;
    box-shadow: none;
  }
`;

const SaveButtonStyled = styled(Button)`
  width: 172px;
  height: 30px;
  font-size: 12px;
  line-height: 30px;
  border-radius: 25px;
  background-color: #01a7ff;
  box-shadow: 0 0 6px 0 rgba(0, 0, 0, 0.16);
  background-color: white;
  margin-bottom: 30px;
  @media (max-width: 900px) {
    height: 54px;
  }

  ${ props => (props.disable
    ? `
    cursor: default;
    box-shadow: none;
    background-color: ${ props.theme.colors.lightGray };
    color: white;

    &:hover, &:active {
      box-shadow: none;
      background-color: ${ props.theme.colors.lightGray };
      color: white;
    }
  `
    : '') };
`;

const FullPricingTable = styled.div`
  margin: 60px 0;
`;

const FullPricingTableHeader = styled.div`
  display: flex;
  padding: 15px;
  > h5 {
    :first-child {
      flex: 0 0 60%;
    }
  }
  > h5 {
    :last-child {
      flex: 0 0 40%;
    }
  }
`;

const FullPricingTableBody = styled.div`
  display: flex;
  justify-content: space-around;
  align-items: center;
  padding: 15px;
  background-color: ${ props => (props.bgColor ? props.bgColor : '') };
`;

const ImageContactDiv = styled.div`
  display: flex;
  flex: 0 0 60%;
  > i {
    font-size: 20px;
  }
  > h5 {
    margin-left: 10px;
    font-weight: normal;
  }
`;
const PriceDiv = styled.div`
  flex: 0 0 40%;
`;

const CurrentContact = styled.div`
  border: solid 1px #e1e8ed;
  border-radius: 5px;
  text-align: center;

  ${ props => props.customCss || '' };
  > span {
    font-size: 30px;
  }
  > p {
    font-size: 20px;
  }
`;

const ContactNumber = styled.div`
  display: flex;
  > span {
    font-size: 30px;
  }
  > p {
    font-size: 20px;
    margin-left: 10px;
  }
  text-align: center;
  justify-content: center;
  align-items: flex-end;
  margin-top: 30px;
`;

const Free = styled.p`
  font-size: 15px;
  font-weight: bold;
  color: #7d909a;
  height: 50px;
  line-height: 50px;
  border-bottom: 1px solid #e1e8ed;
`;

const CurrentChoceButton = styled.div`
  width: 187px;
  height: 30px;
  border-radius: 21px;
  background-color: #e1e8ed;
  box-shadow: 0 0 6px 0 rgba(0, 0, 0, 0.16);
  margin: auto;
  line-height: 30px;
  color: white;
  margin-top: 20px;
  margin-bottom: 30px;
  font-size: 12px;
  font-weight: bold;
`;

export default class BillingCard extends Component {
  constructor (props) {
    const { organization: { bill_contact_count } } = props;
    super();

    this.state = {
      slideValue: Math.ceil((bill_contact_count - 100) / 50),
      billContactCount: bill_contact_count || 100,
      showFullPricing: false,
      currentChoiceContact: bill_contact_count || 100,
    };
  }

  async onSave () {
    try {
      const { action, onFinish } = this.props;
      const { billContactCount } = this.state;

      if (!action) return;
      await action({ bill_contact_count: billContactCount });
      if (onFinish) await onFinish();
    } catch (e) {
      throw e;
    }
  }

  getBillingPrice (count) {
    const { brackets } = this.props;

    let amount = 0;
    brackets.forEach((b) => {
      const countInBracket = b.to
        ? Math.min(b.to, count) - b.from
        : count - b.from;
      if (countInBracket <= 0) return;

      const unitsInBracket = Math.ceil(countInBracket / b.unit_size);
      amount += unitsInBracket * b.amount_per_unit;
    });
    return amount;
  }

  changeSliderValue = (event) => {
    const billContactCount = 100 + event.target.value * 50;

    this.setState({
      slideValue: event.target.value,
      billContactCount,
    });
  };

  changeBillContactCount (billContactCount) {
    const slideValue = Math.ceil((billContactCount - 100) / 100) * 2;

    this.setState({
      slideValue,
      billContactCount,
    });
  }

  showList () {
    const { showFullPricing } = this.state;
    this.setState({ showFullPricing: !showFullPricing });
  }

  render () {
    const {
      organization,
      brackets,
    } = this.props;
    const {
      slideValue,
      billContactCount,
      showFullPricing,
      currentChoiceContact,
    } = this.state;

    const pricingTableList = brackets.map((detail, key) => (
      <FullPricingTableBody key={ key } bgColor={ key % 2 === 0 ? '#e1e8ed' : '' }>
        <ImageContactDiv>
          <i className='fas fa-user' aria-hidden='true' />
          { detail.to && (
            <h5>
Next
              { detail.to - detail.from }
            </h5>
          ) }
          { !detail.to && <h5>Any Extra</h5> }
        </ImageContactDiv>
        <PriceDiv>
$
          { detail.amount_per_unit }
        </PriceDiv>
      </FullPricingTableBody>
    ));
    const SaveButton = (
      <SaveButtonStyled
        // HACK: use fake disable prop here
        disabled={ billContactCount === organization.bill_contact_count }
        onClick={ this.onSave.bind(this) }
      >
        { t('UPGRADE NOW') }
      </SaveButtonStyled>
    );

    const YourChoice = (
      <YourChoiceWrapper>
        <YourChoiceText>Make your choice</YourChoiceText>
      </YourChoiceWrapper>
    );

    const monthlyPrice = this.getBillingPrice(billContactCount);
    const PriceDisplay = (
      <PriceWrapper>
        <PriceWrapperHalf left>
          <Input
            number
            required
            field='billContactCount'
            placeholder='100'
            value={ billContactCount }
            onChange={ (_, value) => {
              this.changeBillContactCount(value);
            } }
            customCss='
              width: 173px;
              padding-right: 20px;
              margin-right: 0;
              margin-top: 0;
              margin-bottom: 0;
              font-weight: normal;
            '
            inputCss='
              height: 40px;
              font-size: 35px;
              text-align: center;
              opacity: 1;
              border-color: #bcd4e0;
              font-weight:normal;
            '
          />
          <h4>contacts for</h4>
        </PriceWrapperHalf>
        <PriceWrapperHalf>
          <h2>
            { !monthlyPrice ? 'Free' : `$${ monthlyPrice.toLocaleString() }` }
          </h2>
          <p>
            { !monthlyPrice ? '' : 'USD / ' }
            { ' ' }
per month
          </p>
        </PriceWrapperHalf>
      </PriceWrapper>
    );

    const currentPlanPrice = this.getBillingPrice(currentChoiceContact);
    const currentPlanPriceText = currentPlanPrice
      ? `US$${ currentPlanPrice.toLocaleString() } ${ t('per month') }`
      : t('Free');

    return (
      <ThemeProvider theme={ { smallInput: true } }>
        <React.Fragment>
          <ModalBodyWrapper>
            <CurrentContact customCss={ { 'margin-top': '30px' } }>
              { /* {CurrentContacts} */ }
              { YourChoice }
              { PriceDisplay }
              <div>
                <SliderInput
                  value={ slideValue }
                  type='range'
                  min='0'
                  max='100'
                  step='2'
                  onChange={ this.changeSliderValue }
                  style={ {
                    background: `linear-gradient(to right, #00a7ff 0%, #00a7ff ${ slideValue }%, #e1e8ed ${ slideValue }%,#e1e8ed 100%)`,
                  } }
                />
              </div>

              <ViewFullPricing plain onClick={ () => this.showList() }>
                <span>
                  { showFullPricing ? 'Hide' : 'View' }
                  { ' ' }
full pricing table
                </span>
                <i
                  className={ `fas fa-chevron-${
                    showFullPricing ? 'up' : 'down'
                  }` }
                  aria-hidden='true'
                />
              </ViewFullPricing>

              { showFullPricing && (
                <FullPricingTable>
                  <FullPricingTableHeader>
                    <h5>NO. OF CONTACTS</h5>
                    <h5>PER 100 CONTACTS (USD)</h5>
                  </FullPricingTableHeader>
                  <FullPricingTableBody>
                    <ImageContactDiv>
                      <i className='fas fa-user' aria-hidden='true' />
                      <h5>First 100</h5>
                    </ImageContactDiv>
                    <PriceDiv>Free</PriceDiv>
                  </FullPricingTableBody>
                  { pricingTableList }
                </FullPricingTable>
              ) }
              { SaveButton }
            </CurrentContact>
            <CurrentContact customCss={ { 'margin-top': '20px' } }>
              <Free>{ currentPlanPriceText }</Free>
              <ContactNumber>
                <span>{ currentChoiceContact.toLocaleString() }</span>
                { ' ' }
                <p>Contacts</p>
              </ContactNumber>
              <CurrentChoceButton>Current Choice</CurrentChoceButton>
            </CurrentContact>
          </ModalBodyWrapper>
        </React.Fragment>
      </ThemeProvider>
    );
  }
}
