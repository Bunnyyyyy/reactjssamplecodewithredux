import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { withRouter } from 'react-router';
import styled from 'styled-components';
import _ from 'lodash';
import moment from 'moment';

import * as UiActionCreators from '../../../../redux/ui';
import * as ManagerActionCreators from '../../../../redux/manager';
import * as EventActionCreators from '../../../../redux/event';
import * as ProfileActionCreators from '../../../../redux/profile';

import ManagerContainer from '../../../../components/layouts/ManagerContainer';
import EventNavBar from '../../../../components/manager/navigation/EventNavBar';
import PreviewPopup from '../../../../components/manager/events/PreviewPopup';
import SaveActions from '../../../../components/manager/SaveActions';
import EditInfoCard from '../../../../components/manager/cards/EditInfoCard';
import CategoryOption from '../../../../components/common/inputs/CategoryOption';
import Label from '../../../../components/common/inputs/Label';
import BiOption from '../../../../components/common/inputs/BiOption';
import EventDatesCard from '../../../../components/manager/events/EventDatesCard';

import currencies from '../../../../utils/currencies';

import { t as translate } from '../../../../../i18n';

const t = a => translate(a, 'manager/events/info');

const { FRONTEND_URL } = process.env;

const CURRENCY_OPTIONS = [];
for (const currency in currencies) {
  CURRENCY_OPTIONS.push({
    value: currency,
    label: `${ currencies[currency].name } (${ currencies[currency].symbol })`,
  });
}

const timezone_filters = ['Etc/GMT', 'Etc/GMT0', 'Etc/GMT+0', 'Etc/GMT-0'];
const TIMEZONE_OPTIONS = moment.tz
  .names()
  .map(tz => ({
    label: tz
      .replace('Etc/', '')
      .replace(/\//g, ' - ')
      .replace(/_/g, ' '),
    value: tz,
  }))
  .filter(tz => !timezone_filters.includes(tz.value));

const EVENT_TYPE_OPTIONS = [
  {
    value: false,
    label: t('Public'),
    description: t('Event is searchable on our public platform juven.co.'),
  },
  {
    value: true,
    label: t('Private'),
    description: t('Event is only reachable through direct URL link.'),
  },
];

const SaveActionsContainer = styled.div`
  flex-shrink: 0;
  background-color: white;
  text-align: right;
  align-items: center;
  justify-content: center;
  display: flex;
`;

const FormSubtitle = styled.div`
  margin: 0 -30px;
  padding: 20px 30px;
  border-top: 1px solid ${ props => props.theme.colors.lightGray };
  border-bottom: 1px solid ${ props => props.theme.colors.lightGray };

  ${ props => (props.first ? 'border-top: none;' : 'margin-top: 20px;') };
`;

const BiOptionTitle = styled.p`
  margin-bottom: 5px;
`;

const BiOptionDescription = styled.p`
  font-size: 12px;
  line-height: 18px;
  color: ${ props => props.theme.colors.deadGray };
`;

const EventLinkA = styled.a`
  color: ${ props => props.theme.colors.darkBlue };
`;

const RsvpAppendix = styled.p`
  font-size: 12px;
  line-height: 14px;
`;

const mapStateToProps = state => ({
  events: state.manager.events,
  event: state.manager.event,
  eventDates: state.event.dates,
  organization: state.manager.organization,
  categories: state.manager.categories,
});

const ActionCreators = Object.assign(
  {},
  UiActionCreators,
  ManagerActionCreators,
  EventActionCreators,
  ProfileActionCreators,
);
const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators(ActionCreators, dispatch),
});

class EventInfoView extends Component {
  constructor () {
    super();

    this.state = {
      validateForms: false,
    };
  }

  async componentDidMount () {
    const { actions, match } = this.props;
    const { eId } = match.params;

    actions.setLoading(true);

    try {
      const promises = [
        actions.getEventDates(eId),
        actions.getCategories('event'),
      ];
      await Promise.all(promises);
    } catch (e) {
      console.error('ERROR: EventInfoView', e);
    } finally {
      actions.setLoading(false);
    }
  }

  componentWillUnmount () {
    const { actions } = this.props;
    actions.resetSaveActions();
  }

  async onFinishSaving () {
    try {
      const { event, actions } = this.props;

      return Promise.all([
        actions.getOrganizationEvent(event.id),
        actions.getEventDates(event.id),
      ]);
    } catch (e) {
      throw e;
    }
  }

  onCancelSaving () {
    const { organization, history } = this.props;

    const targetUrl = `/${ organization.id }/events`;
    return history.push(targetUrl);
  }

  onInvalidSave () {
    this.setState({ validateForms: true });
  }

  // To be used later
  async saveImage (eId, file, type) {
    const { actions } = this.props;

    const formData = new FormData();
    formData.append('image', file);
    formData.append('type', type);

    try {
      await actions.uploadProfileImage(eId, formData);
    } catch (e) {
      throw e;
    }
  }

  async updateEvent (state) {
    try {
      const { event, actions, organization } = this.props;
      await actions.updateEvent(event.id, state);

      try {
        if (state.cover_image) {
          await this.saveImage(event.id, state.cover_image, 'cover');
        }

        return Promise.all([
          actions.getOrganizationEvents(organization.id),
          actions.getOrganizationEvent(event.id),
        ]);
      } catch (e) {
        // allow image upload failure
        console.error(e);
      }
    } catch (e) {
      throw e;
    }
  }

  async updateEventDates (newEventDates) {
    try {
      const { event, eventDates, actions } = this.props;

      for (let i = 0; i < newEventDates.length; i++) {
        const newDate = newEventDates[i];

        if (newDate.id) {
          const oldDate = eventDates.find(d => d.id === newDate.id);
          // TEMP: cannot update timeslots for now
          const timeslots = newDate.timeslots || [];
          const dateData = Object.assign({}, newDate);

          // format the start and end dates properly
          dateData.start_date = dateData.start_date
            ? moment(dateData.start_date).format('YYYY-MM-DD')
            : null;
          dateData.end_date = dateData.end_date
            ? moment(dateData.end_date).format('YYYY-MM-DD')
            : null;
          delete dateData.timeslots;

          await actions.updateEventDate(event.id, newDate.id, dateData);

          // updated single date
          if (!newDate.recurring_interval && !oldDate.recurring_interval) {
            const oldTimeslot = oldDate.timeslots[0];
            const newTimeslot = timeslots[0];

            if (oldTimeslot && newTimeslot) {
              await actions.updateEventDateTimeslot(
                event.id,
                newDate.id,
                oldTimeslot.id,
                newTimeslot,
              );
            } else if (oldTimeslot) {
              await actions.removeEventDateTimeslot(
                event.id,
                newDate.id,
                oldTimeslot.id,
              );
            } else if (newTimeslot) {
              await actions.addEventDateTimeslot(
                event.id,
                newDate.id,
                newTimeslot,
              );
            }
          } else {
            const newTimeslots = timeslots.filter(ts => !ts.id);
            const removedTimeslots = oldDate && oldDate.timeslots
              ? oldDate.timeslots.filter(ts => !timeslots.find(nt => nt.id === ts.id))
              : [];

            for (let j = 0; j < newTimeslots.length; j++) {
              await actions.addEventDateTimeslot(
                event.id,
                newDate.id,
                newTimeslots[j],
              );
            }
            for (let j = 0; j < removedTimeslots.length; j++) {
              await actions.removeEventDateTimeslot(
                event.id,
                newDate.id,
                removedTimeslots[j].id,
              );
            }
          }
        } else {
          const { timeslots } = newDate;
          const dateData = Object.assign({}, newDate);

          // format the start and end dates properly
          dateData.start_date = dateData.start_date
            ? moment(dateData.start_date).format('YYYY-MM-DD')
            : null;
          dateData.end_date = dateData.end_date
            ? moment(dateData.end_date).format('YYYY-MM-DD')
            : null;
          delete dateData.timeslots;

          const date = await actions.createEventDate(event.id, dateData);

          // HACK: must do the timeslot adding one by one, otherwise race condition occurs and everything turns into shit
          for (let j = 0; j < timeslots.length; j++) {
            await actions.addEventDateTimeslot(event.id, date.id, timeslots[j]);
          }
        }
      }

      const removePromises = eventDates
        .filter(d => !newEventDates.find(nd => nd.id === d.id))
        .map(d => actions.removeEventDate(event.id, d.id));

      return Promise.all(removePromises);
    } catch (e) {
      throw e;
    }
  }

  render () {
    const {
      organization,
      event = {},
      eventDates = [],
      categories,
    } = this.props;
    const { validateForms } = this.state;

    const eventLink = `${ FRONTEND_URL }/${ event.id }`;
    const eventDetailsFields = [
      {
        type: 'custom',
        field: 'title-1',
        readOnly: true,
        renderComponent: () => (
          <FormSubtitle first>
            <h5>{ t('1. Event Info*') }</h5>
          </FormSubtitle>
        ),
      },
      {
        type: 'section',
        field: 'event-name-section',
        flex: '0 0 50%',
        fields: [
          {
            type: 'input',
            field: 'name',
            label: t('Event Name'),
            default: event.name || '',
            errorCss: 'position: relative;',
            required: true,
          },
          {
            type: 'custom',
            readOnly: true,
            renderComponent: () => (
              <div>
                <Label label={ t('Event URL') } />
                <EventLinkA target='_blank' href={ eventLink }>
                  { eventLink }
                </EventLinkA>
              </div>
            ),
          },
        ],
      },
      {
        type: 'image',
        field: 'cover_image',
        label: `${ t('Event Banner') } (2:1 ratio | recommended size: 2000 x 1000 px)`,
        flex: '0 0 50%',
        width: 500,
        height: 250,
        default: event.cover_image_url,
      },
      {
        type: 'rte',
        field: 'description',
        placeholder: t('Fill in description here...'),
        label: t('Event Description'),
        default: event.description,
        required: true,
        getErrorMessage: (formState, value, label) => {
          if (value && value.replace(/(<([^>]+)>)/gi, '').length) {
            return null;
          }
          return `${ t('Please enter a valid') } ${ label }`;
        },
      },
      {
        type: 'options',
        field: 'category_id',
        label: t('Select Event Category (Most relevant one)'),
        default: event.category_id,
        options: categories.map(c => ({
          value: c.id,
          component: (
            <CategoryOption>
              <div>
                <i className={ `fas fa-${ c.icon }` } />
              </div>
              <h6>{ c.name }</h6>
            </CategoryOption>
          ),
        })),
      },
      {
        type: 'options',
        field: 'private',
        label: t('Event Type'),
        default: event.private,
        required: true,
        options: EVENT_TYPE_OPTIONS.map(c => ({
          value: c.value,
          component: () => (
            <BiOption customCss='height: auto; padding: 15px;'>
              <BiOptionTitle>{ c.label }</BiOptionTitle>
              <BiOptionDescription>{ c.description }</BiOptionDescription>
            </BiOption>
          ),
        })),
      },
      {
        type: 'select',
        field: 'currency',
        label: t('Ticketing Currency'),
        customCss: 'padding-right: calc(50% + 20px);',
        options: CURRENCY_OPTIONS,
        default: event.currency,
        required: true,
      },
      {
        type: 'checkbox',
        field: 'includes_fee',
        label: t('Includes Stripe Fee in Ticket Prices'),
        default: event.includes_fee,
      },
      {
        type: 'checkbox',
        field: 'requires_approval',
        label: t('Requires admin approval for ticket reservations*'),
        default: event.requires_approval,
      },
      {
        type: 'custom',
        field: 'requires_approval-appendix',
        readOnly: true,
        renderComponent: () => (
          <RsvpAppendix>
            { `*${ t('If you choose this option, it will require administrator approval before particpants can pay/confirm their tickets.') }` }
          </RsvpAppendix>
        ),
      },
      {
        type: 'custom',
        field: 'title-2',
        readOnly: true,
        renderComponent: () => (
          <FormSubtitle>
            <h5>{ t('2. Event Location & Timezone') }</h5>
          </FormSubtitle>
        ),
      },
      {
        type: 'section',
        field: 'event-location-section',
        flex: '0 0 50%',
        fields: [
          {
            type: 'input',
            field: 'location_address',
            label: t('Location'),
            default: event.location_address,
            required: true,
          },
          {
            type: 'input',
            field: 'location',
            label: t('Venue'),
            default: event.location,
          },
          {
            type: 'select',
            field: 'timezone',
            label: t('Timezone'),
            default: event.timezone || 'Asia/Hong_Kong',
            options: TIMEZONE_OPTIONS,
            required: true,
          },
        ],
      },
    ];

    return (
      <ManagerContainer
        contentCss='height: calc(100vh - 56px);transform: translateY(-44px);padding-top: 78px;'
        subheader={ [
          <EventNavBar inline organization={ organization } event={ event } />,
          <SaveActionsContainer>
            <SaveActions
              onFinish={ this.onFinishSaving.bind(this) }
              hoverPopup={ <PreviewPopup event={ event } /> }
            />
          </SaveActionsContainer>,
        ] }
      >
        <EditInfoCard
          name='event-details'
          fields={ eventDetailsFields }
          action={ this.updateEvent.bind(this) }
          actionOrder={ 0 }
          validateForm={ validateForms }
          customCss='padding-top: 0; margin-bottom: 0;'
        />
        <EventDatesCard
          name='event-dates'
          existingEventDates={ _.cloneDeep(eventDates) }
          handleEventDates={ this.updateEventDates.bind(this) }
          actionOrder={ 1 }
          validateForm={ validateForms }
        />
      </ManagerContainer>
    );
  }
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(EventInfoView));
