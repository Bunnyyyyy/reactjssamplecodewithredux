import React from 'react';
import { connect } from 'react-redux';
import styled, { ThemeProvider } from 'styled-components';

const ContentWrapper = styled.div`
  position: relative;
  height: 100vh;
  overflow: hidden;
  width: 100%;
  min-width: 900px;
`;

const BodyWrapper = styled.div`
  display: flex;
  padding-top: ${ props => (props.theme.noSubheader ? '56px' : '100px') };
`;

const ChildrenWrapper = styled.div`
  position: relative;
  height: calc(100vh - ${ props => (props.theme.noSubheader ? '56px' : '100px') });
  overflow-x: hidden;
  overflow-y: scroll;
  flex: 1;
  padding: 35px 25px 60px 25px;
  background-color: ${ props => props.theme.colors.lightGray };

  ${ props => props.customCss || '' };
`;

const LoadingDiv = styled.div`
  position: absolute;
  width: 100%;
  height: 100%;
  left: 0;
  top: 0;
  min-height: ${ props => (props.fullScreen ? '100vh' : 'auto') };
  background-color: ${ props => props.theme.colors.lightGray };

  i {
    position: absolute;
    left: 50%;
    top: 50%;
    width: 72px;
    height: 72px;
    margin: -36px 0 0 -36px;
    font-size: 72px;
    color: ${ props => props.theme.colors.mainBlue };
  }
`;

const Subheader = styled.div`
  display: flex;
  justify-content: space-between;
  position: absolute;
  text-align: right;
  z-index: 899;
  top: 56px;
  left: 0;
  width: 100%;
  height: 44px;
  padding: 0 25px;
  background-color: white;
  box-shadow: 0 2px 4px 0 rgba(0, 0, 0, 0.1);

  ${ props => props.customCss || '' }
`;

const mapStateToProps = state => ({
  user: state.me.user,
  loading: state.ui.loading,
});

const ManagerContainer = ({
  subpageMenu = null,
  children,
  contentCss = '',
  loading,
  noLoading = false,
  subheader = null,
  subheaderCss = '',
  noSubheader = false,
}) => (
  <ThemeProvider theme={ { noSubheader } }>
    <ContentWrapper>
      { subheader ? <Subheader customCss={ subheaderCss }>{ subheader }</Subheader> : null }
      <BodyWrapper>
        { subpageMenu }
        { loading && !noLoading
          ? (
            <LoadingDiv>
              <i className='fas fa-spinner fa-pulse fa-3x fa-fw' />
            </LoadingDiv>
          ) : <ChildrenWrapper customCss={ contentCss }>{ children }</ChildrenWrapper>
        }
      </BodyWrapper>
    </ContentWrapper>
  </ThemeProvider>
);

export default connect(mapStateToProps)(ManagerContainer);
