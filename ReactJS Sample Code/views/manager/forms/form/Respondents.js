import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { withRouter } from 'react-router';
import moment from 'moment';
import styled from 'styled-components';

import { stateHasSelectedRows } from '../../../../utils';
import * as UiActionCreators from '../../../../redux/ui';
import * as ManagerActionCreators from '../../../../redux/manager';
import * as OrganizationActionCreators from '../../../../redux/organization';
import * as EmailTemplatesActionCreators from '../../../../redux/emailTemplates';

import ManagerContainer from '../../../../components/layouts/ManagerContainer';
import Table from '../../../../components/common/Table';
import BasicContactInfo from '../../../../components/manager/BasicContactInfo';

import SettingModal from '../../../../components/manager/SettingModalV2';
import Tag from '../../../../components/manager/tags/Tag';
import EditTag from '../../../../components/manager/tags/EditTag';

import downloadIcon from '../../../../assets/manager/bulk-actions/icon-download.svg';
import copyContactIcon from '../../../../assets/manager/bulk-actions/icon-copy-contact.svg';
import emailIcon from '../../../../assets/manager/bulk-actions/icon-email.svg';

import { t as translate } from '../../../../../i18n';

const t = a => translate(a, 'manager/forms/respondents');

const { API_URL } = process.env;


const mapStateToProps = state => ({
  user: state.me.user,
  events: state.manager.events,
  organizations: state.manager.organizations,
  organization: state.manager.organization,
  form: state.organization.form,
  customFields: state.organization.customFields,
  respondents: state.organization.formRespondents,
  templates: state.emailTemplates.templates,
  meta: state.manager.meta,
  tags: state.organization.tags,
});

const ActionCreators = Object.assign(
  {},
  UiActionCreators,
  ManagerActionCreators,
  OrganizationActionCreators,
  EmailTemplatesActionCreators,
);
const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators(ActionCreators, dispatch),
});

const FileLink = styled.a`
  font-size: 12px;
  line-height: 18px;
  color: ${ props => props.theme.colors.mainBlue };
`;

class FormRespondentsView extends Component {
  constructor () {
    super();

    // default table settings
    this.state = {
      limit: 10,
      offset: 0,
      sortBy: 'name',
      sortOrder: 'asc',
      sortType: null,
      search: null,
      filters: {},
      templateEmailParams: null,
      cloningContactsParams: null,
    };
  }

  async componentDidMount () {
    const { actions, match } = this.props;
    const { oId, foId } = match.params;

    actions.setLoading(true);

    try {
      const promises = [
        actions.getOrganizationEvents(oId),
        actions.getOrganizationAdmins(oId),
        actions.getOrganizationForm(oId, foId),
        actions.getOrganizationCustomFields(oId),
        actions.getEmailTemplates(oId),
        actions.getOrganizationTags(oId),
        actions.getOrganizationFormRespondents(oId, foId, {
          limit: 10,
          offset: 0,
          sort_by: 'name',
          sort_order: 'asc',
        }),
      ];
      await Promise.all(promises);
    } catch (e) {
      console.error('ERROR: FormRespondentsView', e);
    } finally {
      actions.setLoading(false);
    }
  }


  setRespondentFilters (filters) {
    this.setState(
      {
        offset: 0,
        filters,
      },
      this.reloadRespondents,
    );
  }

  setTemplateEmailParams (templateEmailParams) {
    this.setState({ templateEmailParams });
  }

  setCloningContactsParams (cloningContactsParams) {
    this.setState({ cloningContactsParams });
  }


  getRespondentsFrom (newFrom) {
    this.setState({ offset: newFrom - 1 }, this.reloadRespondents);
  }

  sortRespondents (newSortBy, sortType) {
    const { sortBy } = this.state;
    let { sortOrder } = this.state;

    sortOrder = sortBy === newSortBy ? (sortOrder === 'asc' ? 'desc' : 'asc') : 'asc';
    this.setState({
      sortBy: newSortBy,
      sortOrder,
      sortType,
    }, this.reloadRespondents);
  }

  changeRespondentsPerPage (newPerPage) {
    this.setState(
      {
        offset: 0,
        limit: newPerPage,
      },
      this.reloadRespondents,
    );
  }

  changeSearchValue (value) {
    this.setState(
      {
        offset: 0,
        search: value,
      },
      this.reloadRespondents,
    );
  }

  async reloadRespondents () {
    const { actions, organization, form } = this.props;
    const {
      limit,
      offset,
      sortBy,
      sortOrder,
      sortType,
      search,
      filters,
    } = this.state;

    const params = {};
    if (limit !== undefined) params.limit = limit;
    if (offset !== undefined) params.offset = offset;
    if (sortBy !== undefined) params.sort_by = sortBy;
    if (sortOrder !== undefined) params.sort_order = sortOrder;
    if (sortType) params.sort_type = sortType;
    if (search !== undefined) params.search = search;
    if (filters !== undefined && Object.keys(filters).length) {
      params.filters = JSON.stringify(filters);
    }

    try {
      await actions.getOrganizationFormRespondents(
        organization.id,
        form.id,
        params,
      );
    } catch (e) {
      console.error(e);
    }
  }

  exportRespondents () {
    const { organization, form } = this.props;
    const { sortBy, sortOrder, sortType } = this.state;

    let url = `${ API_URL }/organizations/${ organization.id }/forms/${
      form.id
    }/respondents/export?sort_by=${ sortBy }&sort_order=${ sortOrder }`;
    if (sortType) url += `&sort_type=${ sortType }`;

    window.open(url, '_blank');
  }

  async sendTemplateEmails (state) {
    try {
      const { actions, organization, form } = this.props;
      const { templateEmailParams } = this.state;
      const { template_id } = state;

      const data = {
        contact_type: 'form_respondent',
        form_id: form.id,
        ...templateEmailParams,
      };

      await actions.sendTemplateEmails(organization.id, template_id, data);
      this.setTemplateEmailParams(null);
    } catch (e) {
      console.error(e);
    }
  }

  async cloneContacts (state) {
    const { actions, organization, form } = this.props;
    const { cloningContactsParams } = this.state;
    const { target_type, target_id } = state;

    const data = {
      source_type: 'form_respondent',
      source_id: form.id,
      target_type,
      ...cloningContactsParams,
    };
    if (target_id) data.target_id = target_id;

    try {
      return actions.cloneContacts(organization.id, data);
    } catch (e) {
      throw e;
    }
  }

  render () {
    const {
      organization,
      form,
      events,
      customFields,
      respondents,
      templates,
      meta,
      tags,
    } = this.props;
    const {
      limit,
      sortBy,
      sortOrder,
      search,
      filters,
      templateEmailParams,
      cloningContactsParams,
    } = this.state;

    let tableColumns = [
      {
        title: t('Name'),
        field: 'name',
        compulsory: true,
        unscrollable: true,
        flex: '1.5 0 0',
        format: (d, r) => (
          <BasicContactInfo
            type='form_respondent'
            organization={ organization }
            contact={ r }
          />
        ),
        tooltipFormat: d => d,
      },
      {
        title: t('Ref. ID'),
        field: 'id',
        format: v => `#R${ v }`,
      },
      {
        title: t('Tags'),
        field: 'tags',
        unsortable: true,
        customCss: 'overflow: visible;line-height: normal;',
        bodyCss: 'overflow: visible;',
        format: (d, r) => {
          const contactTags = tags.filter(
            tag => r.tags && r.tags.indexOf(tag.id) > -1,
          );

          return contactTags.length
            ? contactTags.map(tag => <Tag value={ tag.tag } color={ tag.color } />)
            : '-';
        },
      },
      {
        title: t('Email'),
        field: 'email',
      },
      {
        title: t('Phone'),
        field: 'phone',
      },
      {
        title: t('Submitted At'),
        field: 'created_at',
        format: d => (d
          ? moment
            .utc(d)
            .local()
            .format('YYYY MMM DD HH:mm')
          : 'Not Yet'),
      },
    ];

    tableColumns = tableColumns.concat(
      (form.fields || []).filter(f => !!f.data_type).map(f => ({
        title: f.label,
        field: f.id,
        dataField: true,
        format: (d, r) => {
          if (f.data_type === 'file') {
            const file = r.files && r.files[f.id];
            if (!file) return '-';

            const { file_url, file_name } = file;
            return (
              <FileLink target='_blank' href={ file_url }>
                { file_name }
              </FileLink>
            );
          }
          const value = r.data_fields ? r.data_fields[f.id] : null;
          if (!value) return '-';

          return Array.isArray(value) ? value.join(', ') : value;
        },
      })),
    );

    tableColumns = tableColumns.concat(
      customFields.map(f => ({
        title: f.name,
        field: f.id,
        customField: true,
        format: (d, r) => {
          const value = r.custom_fields ? r.custom_fields[f.id] : null;
          if (!value) return '-';
          return value;
        },
      })),
    );

    const tagFilterOptions = [
      {
        label: 'No Tag',
        value: null,
      },
    ].concat(
      tags.map(tag => ({
        label: tag.tag,
        value: tag.id,
      })),
    );

    const tableFilterFields = [
      {
        type: 'input',
        label: t('Name'),
        field: 'name',
      },
      {
        type: 'input',
        label: t('Email'),
        field: 'email',
      },
      {
        type: 'multiSelect',
        label: t('Tags'),
        field: 'tag_id',
        options: tagFilterOptions,
      },
    ];

    // const formFields = form.fields.filter(e => !!e.data_type);
    // tableColumns = tableColumns.concat(
    //   formFields.map(f => {
    //     return {
    //       title: f.label,
    //       field: f.id,
    //       format: (d, respondent) => {
    //         const value = respondent.data_fields ? respondent.data_fields[f.id] : null;
    //         if (!value) return '-';

    //         return value;
    //       }
    //     };
    //   })
    // );

    const tableBulkActions = [
      {
        function: this.exportRespondents.bind(this),
        name: t('Export Respondents'),
        icon: downloadIcon,
        condition: state => !stateHasSelectedRows(state),
      },
      {
        function: this.setCloningContactsParams.bind(this),
        name: t('Clone Contacts'),
        icon: copyContactIcon,
        condition: state => stateHasSelectedRows(state),
      },
      {
        function: this.setTemplateEmailParams.bind(this),
        name: t('Send Template Email'),
        icon: emailIcon,
        condition: state => stateHasSelectedRows(state),
      },
      {
        render: (params) => {
          const actionParams = Object.assign(params, {
            contact_type: 'form_respondent',
            form_id: form.id,
          });

          return (
            <EditTag
              tags={ tags }
              reloadData={ this.reloadRespondents.bind(this) }
              actionParams={ actionParams }
            />
          );
        },
        condition: state => stateHasSelectedRows(state),
      },
    ];

    const sendTemplateEmailsFields = [
      {
        type: 'select',
        field: 'template_id',
        label: `${ t('Select an Email Template you want to send') }:`,
        options: templates.map(tem => ({
          label: tem.title,
          value: tem.id,
        })),
        flex: '100% 0 0',
        required: true,
      },
    ];

    const copyContactsFields = [
      {
        type: 'select',
        field: 'target_type',
        label: t('Clone Target Type'),
        options: [
          {
            label: 'Membership',
            value: 'organization_member',
          },
          { label: t('Event (as Guests)'), value: 'event_guest' },
        ],
        searchable: false,
        required: true,
      },
      {
        type: 'select',
        field: 'target_id',
        label: t('Select the target event to clone selected contacts'),
        options: events.map(e => ({
          label: e.name,
          value: e.id,
        })),
        condition: state => state.target_type === 'event_guest',
        flex: '100% 0 0',
        required: true,
      },
    ];

    return (
      <ManagerContainer>
        <Table
          id='responses'
          title={ t('Respondent list') }
          subtitle={ `${ t('Total respondents:') } ${ meta.count }` }
          columns={ tableColumns }
          rows={ respondents }
          meta={ meta }
          perPage={ limit }
          sortBy={ sortBy }
          sortOrder={ sortOrder }
          search={ search }
          filters={ filters }
          filterFields={ tableFilterFields }
          onSetFilters={ this.setRespondentFilters.bind(this) }
          onReload={ this.reloadRespondents.bind(this) }
          onSort={ this.sortRespondents.bind(this) }
          onChangeFrom={ this.getRespondentsFrom.bind(this) }
          onChangePerPage={ this.changeRespondentsPerPage.bind(this) }
          onChangeSearchValue={ this.changeSearchValue.bind(this) }
          bulkActions={ tableBulkActions }
          emptyText={ t('There are no respondents yet.') }
        />
        { templateEmailParams ? (
          <SettingModal
            title={ t('Send Template Emails') }
            isOpen={ templateEmailParams }
            fields={ sendTemplateEmailsFields }
            action={ this.sendTemplateEmails.bind(this) }
            onCancel={ this.setTemplateEmailParams.bind(this, null) }
            saveButtonText={ t('Send') }
          />
        ) : null }
        { cloningContactsParams ? (
          <SettingModal
            title={ t('Clone Contacts') }
            isOpen={ cloningContactsParams }
            fields={ copyContactsFields }
            action={ this.cloneContacts.bind(this) }
            onCancel={ this.setCloningContactsParams.bind(this, null) }
          />
        ) : null }
      </ManagerContainer>
    );
  }
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(FormRespondentsView));
