import React from 'react';
import styled from 'styled-components';
import ReactTooltip from 'react-tooltip';

const StyledDiv = styled.div`
  display: inline-block;
  position: relative;
  overflow: hidden;
  cursor: pointer;
  flex-shrink: 0;
  border-radius: 50%;
  border: 1px solid ${ props => props.theme.colors.lightGray };
  width: ${ props => props.size }px;
  height: ${ props => props.size }px;
  padding: ${ props => (props.noPadding ? 0 : (props.size ? props.size / 20 : 2)) }px;

  &:hover {
    background-color: ${ props => props.theme.colors.bgGray };
  }

  ${ props => (props.active
    ? `
    background-color: ${ props.theme.colors.mainBlue };

    &:hover {
      background-color: ${ props.theme.colors.mainBlue };
    }
    `
    : '') }

  &:hover > span {
    display: flex;
  }
`;

const StyledImg = styled.img`
  width: 100%;
  height: 100%;
  ${ props => props.customCss || '' };
`;

const StyledText = styled.span`
  position: absolute;
  width: 100%;
  height: 100%;
  border-radius: 50%;
  top: 0;
  left: 0;
  color: white;
  background-color: black;
  opacity: 0.8;
  align-items: center;
  justify-content: center;
  display: none;
`;

const CircularImage = ({
  src = '',
  size = 30,
  noPadding = false,
  tooltip = '',
  onClick,
  customCss = '',
  active = false,
  hoverText = '',
}) => {
  if (src === '') {
    return '';
  }
  return (
    <StyledDiv onClick={ onClick } size={ size } active={ active } noPadding={ noPadding }>
      <StyledImg src={ src } customCss={ customCss } data-tip={ tooltip } data-place='right' data-effect='solid' />
      <ReactTooltip globalEventOff='click' />
      { hoverText && <StyledText>{ hoverText }</StyledText> }
    </StyledDiv>
  );
};

export default CircularImage;
