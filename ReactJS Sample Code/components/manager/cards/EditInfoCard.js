import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import * as ActionCreators from '../../../redux/manager';

import Card from './Card';
import Form from '../../common/inputs/Form';

const mapStateToProps = state => ({
  saveActions: state.manager.saveActions,
});

const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators(ActionCreators, dispatch),
});

class EditInfoCard extends Component {
  constructor () {
    super();

    this.state = {
      resetForm: false,
    };
  }

  componentWillReceiveProps (nextProps) {
    const { name, saveActions } = this.props;
    if (
      saveActions !== nextProps.saveActions
      && nextProps.saveActions[name] === undefined
    ) {
      this.resetForm();
    }
  }

  async onUpdateForm (state) {
    try {
      const {
        actions,
        fields,
        name,
        action,
        actionOrder,
        onUpdate,
      } = this.props;
      if (!action) return;

      let isInvalid = false;

      let allFields = fields;
      // recursively add all nested section fields
      const addSectionFields = (f) => {
        if (f.type !== 'section' || !f.fields || !f.fields.length) return;

        allFields = allFields.concat(
          f.fields.filter(f2 => f2.type !== 'section'),
        );
        f.fields.filter(f2 => f2.type === 'section').forEach(addSectionFields);
      };
      fields.filter(f => f.type === 'section').forEach(addSectionFields);

      allFields
        .filter(f => (typeof f.readOnly === 'function'
          ? !f.readOnly(state)
          : !f.readOnly))
        .forEach((f) => {
          const {
            field, label, required, getErrorMessage,
          } = f;

          if (
            (getErrorMessage && getErrorMessage(state, state[field], label))
            || (required && state[field] === null)
          ) {
            isInvalid = true;
          }
        });

      const stateData = Object.assign({}, state);
      delete stateData._iceredIdAvailable;

      allFields.forEach((f) => {
        const readOnly = typeof f.readOnly === 'function' ? f.readOnly(state) : f.readOnly;

        if (readOnly) delete stateData[f.field];
      });

      if (!isInvalid) {
        actions.addSaveAction(
          action.bind(this, stateData),
          name,
          null,
          actionOrder,
        );
      } else {
        // HACK: make action null but not deleting it from redux, hence not resetting the form
        actions.addSaveAction(null, name, null, actionOrder);
      }

      if (onUpdate) await onUpdate(state);
    } catch (e) {
      throw e;
    }
  }

  // hack to make the Form reset
  resetForm () {
    this.setState({ resetForm: true }, () => {
      this.setState({ resetForm: false });
    });
  }

  render () {
    const {
      name,
      title,
      fields = [],
      validateForm = true,
      customCss,
      formCss,
      children,
    } = this.props;
    const { resetForm } = this.state;

    return (
      <Card edit title={ title } customCss={ customCss }>
        { children }
        <Form
          name={ name }
          validate={ validateForm }
          reset={ resetForm }
          fields={ fields }
          onUpdate={ this.onUpdateForm.bind(this) }
          customCss={ formCss }
        />
      </Card>
    );
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(EditInfoCard);
