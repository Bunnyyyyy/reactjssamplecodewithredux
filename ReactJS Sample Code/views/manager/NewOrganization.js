import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { withRouter } from 'react-router';
import { Redirect } from 'react-router-dom';
import styled from 'styled-components';
import { Formik } from 'formik';
import * as Yup from 'yup';

import * as UiActionCreators from '../../redux/ui';
import * as MeActionCreators from '../../redux/me';
import * as ManagerActionCreators from '../../redux/manager';

import { changeToUrlable } from '../../utils';

import Header from '../../components/header/Header';
import Button from '../../components/common/Button';
import Input from '../../components/common/Input';
import OrganizationLogoUpload from '../../components/common/OrganizationLogoUpload';
import ErrorFocus from '../../components/common/inputs/ErrorFocus';
import JuvenIdInput from '../../components/common/JuvenIdInput';
import { t as translate } from '../../../i18n';
import OrganizationOptions from '../../components/common/OrganizationOptions';

const t = a => translate(a, 'manager/new');

const Container = styled.div`
  height: 100%;
  background-color: ${ props => props.theme.colors.lightGray };
  display: flex;
  justify-content: center;
  align-items: center;
`;

const RegisterContainer = styled.div`
  background-color: white;
  padding: 40px 60px 30px 60px;
  max-width: 620px;
  margin-top: 110px;
  border-radius: 5px;
  margin-bottom: 80px;

  @media (max-width: 900px) {
    padding-left: 30px;
    padding-right: 30px;
  }
`;

const Title = styled.h4`
  color: ${ props => props.theme.colors.mainBlue };
  text-align: center;
  margin-bottom: 20px;
`;

const Description = styled.p`
  text-align: center;
  margin-bottom: 10px;
`;

const SubmitButton = Button.extend`
  display: block;
  margin-bottom: 0px;
  margin-left: auto;
  margin-right: auto !important;
  padding-left: 30px;
  padding-right: 30px;
  font-size: 12px;
  width: 177px;
  height: 30px;
  line-height: normal;
`;


const mapStateToProps = state => ({
  user: state.me.user,
  organizations: state.manager.organizations,
  categories: state.manager.categories,
});

const ActionCreators = Object.assign(
  {},
  UiActionCreators,
  MeActionCreators,
  ManagerActionCreators,
);
const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators(ActionCreators, dispatch),
});

class NewOrganizationView extends Component {
  juvenIdInput = React.createRef();

  constructor () {
    super();

    this.state = {
      iceredIdAvailable: null,
    };
  }

  async componentDidMount () {
    const { history, actions, user } = this.props;

    actions.setLoading(true);
    if (!user.id) {
      return history.replace('/');
    }

    try {
      actions.setOrganization(null);
      actions.getCategories('organization');
    } catch (e) {
      console.error('ERROR: NewOrganizationView', e);
    } finally {
      actions.setLoading(false);
    }
  }

  async onSuccessfullyVerified () {
    const { actions, history } = this.props;

    try {
      await actions.getMyManagedOrganizations();
      actions.showNotification({
        type: 'success',
        text: t('Account verified'),
      });
      const { organizations } = this.props;

      if (organizations.length) {
        history.push('/');
      }
    } catch (e) {
      throw e;
    }
  }

  checkAndSetIdError (value, formikProps) {
    this.checkIceredId(value, (isAvailable) => {
      if (isAvailable) {
        this.setState({ iceredIdAvailable: true });
      } else {
        formikProps.setFieldError('icered_id', t('URL is already taken. Please choose another one.'));
        this.setState({ iceredIdAvailable: false });
      }
    });
  }

  checkIceredId (id, cb) {
    const { actions } = this.props;

    clearTimeout(this.iceredIdTimer);
    this.iceredIdTimer = setTimeout(() => {
      actions
        .checkIceredId(id)
        .then(cb)
        .catch(console.error);
    }, 300);
  }

  async saveImage (oId, file, type) {
    const { actions } = this.props;

    const formData = new FormData();
    formData.append('image', file);
    formData.append('type', type);

    try {
      await actions.uploadProfileImage(oId, formData);
    } catch (e) {
      throw e;
    }
  }

  async saveNewOrganization (state) {
    const { history, actions } = this.props;
    const { name, icered_id, category_id } = state;

    const organizationData = {
      name,
      icered_id,
      category_id,
    };

    try {
      const { id } = await actions.createOrganization(organizationData);

      if (state.logo_image_url) {
        await this.saveImage(id, state.logo_image_url, 'logo');
      }

      await actions.getMyManagedOrganizations();

      await actions.showNotification({
        type: 'success',
        text: t('Organization created.'),
      });

      // TEMP: directly redirect to dashboard
      const targetLink = `/${ icered_id }`;
      return history.push(targetLink);
    } catch (e) {
      await actions.setNotification({
        type: 'error',
        title: `${ t(
          'Oops! There is an error when creating the Organization',
        ) } - ${ name }.`,
        message: e,
      });
    }
  }

  render () {
    const { user, categories } = this.props;
    const { iceredIdAvailable } = this.state;

    if (!user.id || !user.verified) return <Redirect to='/' />;

    const FormSchema = Yup.object().shape({
      name: Yup.string()
        .nullable()
        .required(t('Please fill in an organization name')),
      icered_id: Yup.string()
        .nullable()
        .required(t('Please choose a valid organization URL')),
    });

    const Content = (
      <Formik
        initialValues={ {
          name: '',
          icered_id: '',
          category_id: null,
          logo_image_url: null,
        } }
        validationSchema={ FormSchema }
        onSubmit={ async (state) => {
          // Note: Need to handle juvenid validation manually.
          if (!iceredIdAvailable) {
            this.juvenIdInput.current.focus();
            return;
          }

          await this.saveNewOrganization(state);
        } }
      >
        { (props) => {
          const {
            values, setFieldValue, errors, handleSubmit,
          } = props;
          return [
            <div>
              <div>
                <Input
                  fullWidth
                  name='name'
                  field='name'
                  label={ t('Organization name') }
                  subLabel={ t(' - Don’t have an organization? You can use a club name, a team name, an association name or department name.') }
                  value={ values.name }
                  onChange={ (_, value) => {
                    setFieldValue('name', value);

                    const url = changeToUrlable(value);
                    setFieldValue('icered_id', url);
                    if (url) {
                      this.checkAndSetIdError(url, props);
                    }
                  } }
                  errorMessage={ errors.name }
                  required
                />
                <JuvenIdInput
                  fullWidth
                  required
                  name='icered_id'
                  label={ t('Custom URL') }
                  subLabel={ t(' - Customize a public URL for your organization') }
                  value={ values.icered_id }
                  onChange={ (_, value) => {
                    const url = changeToUrlable(value);
                    setFieldValue('icered_id', url);
                    if (url) {
                      this.checkAndSetIdError(url, props);
                    }
                  } }
                  customCss='margin-bottom: 40px;'
                  // Note: JuvenIdInput validation will be handled by seperated validation state.
                  error={ iceredIdAvailable === false }
                  errorMessage={ errors.icered_id }
                  errorCss='left: 116px;'
                  success={ values.icered_id && iceredIdAvailable }
                  inputRef={ this.juvenIdInput }
                />
              </div>
              <OrganizationLogoUpload
                label={ `${ t('Logo') }` }
                subLabel={ t(' (Recommended 320 x 320 px)') }
                image={ values.logo_image_url }
                onChange={ (_, image) => {
                  setFieldValue('logo_image_url', image);
                } }
                viewportType='circle'
                width={ 320 }
                height={ 320 }
              />
              { OrganizationOptions(categories, props) }
            </div>,
            <SubmitButton light onClick={ handleSubmit } type='submit'>
              Create organization
            </SubmitButton>,
            <ErrorFocus { ...props } />,
          ];
        } }
      </Formik>
    );

    return (
      <Container>
        <Header fixed />
        <RegisterContainer>
          <Title>{ t('Welcome to Juven') }</Title>
          <Description>
            { t(
              'Juven is an organization engagement plaform. To start, please tell us a little bit about your organization. It’ll only take a minute.',
            ) }
          </Description>
          { Content }
        </RegisterContainer>
      </Container>
    );
  }
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(NewOrganizationView));
