import React from 'react';
import styled from 'styled-components';
import Label from './inputs/Label';
import { userSelect } from '../../utils/styles';
import Tooltip from './Tooltip';

import InvalidMessage from './InvalidMessage';

const { FRONTEND_URL } = process.env;

const Exclamation = styled.i`
  position: relative !important;
  right: auto !important;
  bottom: auto !important;
  line-height: 14px !important;
  margin-right: 5px;
`;

const Div = styled.div`
  position: relative;
  display: inline-block;
  width: ${ props => (props.fullWidth ? '100%' : '270px') };
  margin-top: 20px;
  margin-bottom: 20px;

  & i {
    position: absolute;
    right: 10px;
    bottom: 0;
    line-height: 32px;
  }

  ${ props => props.customCss || '' };
`;

const InnerDiv = styled.div`
  display: flex;
  position: relative;
  ${ props => props.customCss || '' };
`;

const InputStyled = styled.input`
  outline: none;
  height: 32px;
  width: 100%;
  font-size: 15px;
  line-height: 28px;
  border: 0px solid transparent;
  border-bottom: 1px solid
    ${ props => (props.theme.light ? 'white' : props.theme.colors.darkGray) };
  border-radius: 0px;
  padding-left: ${ props => (props.prefix ? '30px' : '0px') };
  margin-top: 0px;
  background-color: transparent;
  padding-right: 30px;
  color: ${ props => (props.theme.light ? 'white' : props.theme.colors.darkGray) };
  opacity: 0.25;

  &:focus {
    border-color: ${ props => (props.theme.light ? 'white' : props.theme.colors.mainBlue) };
  }

  &:focus {
    opacity: 1;
  }
  ${ props => (props.value && props.value.length
    ? `
      opacity: 1;
      border-color: ${
    props.theme.light ? 'rgba(255, 255, 255, .25)' : 'rgba(47, 46, 46, .25)'
    };
    `
    : '') };

  ${ props => (props.readOnly
    ? `
    cursor: default;
    border-color: transparent;
  `
    : '') };

  ${ props => (props.invalid ? `border-color: ${ props.theme.colors.pinkRed } !important` : '') }
`;

const Message = styled.p`
  display: inline-block;
  color: #9c9c9c;
  line-height: 32px;
  margin-right: 5px;

  ${ userSelect('none') };
`;

const ErrorSuccessIcon = styled.i`
  position: absolute;
  bottom: 0;
  font-size: 14px;
  line-height: 32px;
  display: inline-block;
  color: ${ props => (props.error ? '#e84363' : '#47c166') };

  &:hover ~ ${ Tooltip } {
    visibility: visible;
  }
`;

const tooltipCss = `
  left: auto;
  top: -2px;
  right: 36px;
  max-width: 230px;
  line-height: 22px;
`;

const Link = styled.a`
  color: ${ props => props.theme.colors.mainBlue };
  font-size: 15px;
  font-weight: normal;
  margin-top: 5px;
`;

const JuvenIdInput = ({
  className,
  type,
  field,
  label,
  displayedUrl,
  onChange,
  success,
  error,
  inputRef,
  divStyle = {},
  juvenId,
  fullWidth,
  required,
  divCss,
  innerCss,
  disabled,
  readOnly,
  subLabel,
  errorMessage,
  errorCss,
  showLink,
  ...props
}) => {
  const ErrorMessage = errorMessage ? (
    <InvalidMessage customCss={ errorCss }>
      <Exclamation className='fas fa-exclamation-circle' />
      { errorMessage }
    </InvalidMessage>
  ) : null;

  const ErrorSuccess = error || success ? (
    <ErrorSuccessIcon
      error={ error }
      className={ error ? 'fa fa-exclamation-circle' : 'fas fa-check-circle' }
      aria-hidden='true'
    />
  ) : null;

  const ErrorTooltip = error && <Tooltip customCss={ tooltipCss }>{ errorMessage }</Tooltip>;

  return (
    <Div
      className={ `input${ className ? ` ${ className }` : '' }` }
      fullWidth={ fullWidth }
      style={ divStyle }
      juvenId={ juvenId }
      customCss={ divCss }
    >
      { label ? <Label field={ field } label={ label } required={ required } subLabel={ subLabel } /> : null }
      <InnerDiv customCss={ innerCss }>
        {
        showLink ? (<Link target='_blank' href={ showLink }>{ showLink }</Link>) : ([
          <Message>{ displayedUrl || `${ FRONTEND_URL }/` }</Message>,
          <InputStyled
            juvenId={ juvenId }
            type={ type || 'text' }
            id={ field }
            data-field={ field }
            onChange={ (e) => {
              // validate Juven ID format
              // only allow alphanumeric characters and restricted use of dots
              e.target.value = e.target.value
                .replace(/([^a-zA-Z\d.]|^[.])/g, '')
                .replace(/[.]{2,}/, '.');
              onChange(field, e.target.value);
            } }
            innerRef={ inputRef || null }
            disabled={ disabled || readOnly }
            readOnly={ readOnly }
            invalid={ !!errorMessage }
            { ...props }
          />,
          ErrorMessage,
          ErrorSuccess,
          ErrorTooltip,
        ])
      }
      </InnerDiv>
    </Div>
  );
};

export default JuvenIdInput;
