import React, { Component } from 'react';
import styled from 'styled-components';
import { DEFAULT_VERSION } from 'redux-persist';

const SliderContainer = styled.div`
  height: 530px;
  width: 100vw;
  box-shadow: 0 -7px 13px 2px rgba(0, 0, 0, 0.16);
  @media (max-width: 900px) {
    height: auto;
  }

  @media (min-width: 901px) {
    display: flex;
    flex-wrap: wrap;
    align-items: flex-end;
  }
`;

const SliderImageDiv = styled.div`
  height: 530px;
  width: 78vw;
  border: 1px solid transparent;
  border-radius: 0px 265px 265px 0px;
  overflow: hidden;

  @media (max-width: 900px) {
    display: none;
  }

  > div {
    background-repeat: no-repeat;
    background-size: cover;
    background-position-y: center;
    width: 100%;
    height: 100%;
  }
`;

const SliderImage = styled.img`
  width: 100%;
`;

const SliderDescDiv = styled.div`
  text-align: center;
  width: 22vw;

  & img {
    margin-right: auto;
    margin-left: auto;
  }
  & h4 {
    padding-top: 20px;
    padding-bottom: 20px;
  }
  & p {
    margin-left: 20px;
    margin-right: 33px;
    line-height: 1.62;
    @media (max-width: 900px) {
      margin-left: 5vw !important;
      margin-right: 5vw !important;
    }
  }

  @media (max-width: 900px) {
    min-height: 60vh;
    width: 100vw;
  }

  @media (min-width: 901px) {
    margin-bottom: 150px;
  }
`;

const DonateButton = styled.div`
  width: 187px;
  height: 42px;
  border-radius: 42px;
  background-color: #01a7ff;
  box-shadow: 0 0 6px 0 rgba(0, 0, 0, 0.16);
  font-size: 18px;
  font-weight: bold;
  color: #ffffff;
  padding-top: 16px;
  line-height: 11.5px;
  flex-shrink: 0;
  @media (max-width: 900px) {
    width: 162.6px;
    height: 40px;
    font-size: 15px;
    border-radius: 42px;
    line-height: 10px;
  }
`;

const DonateButtonWrapper = styled.div`
  cursor: default;
  opacity: 0.5;
  text-align: center;
  padding: 25px 0;

  & a {
    display: inline-block;
  }
`;

const GoodReadsDescSwiper = styled.div`
  display: flex;
  align-self: center;
  color: #7d909a;
  flex-direction: row;
  justify-content: space-between;
  width: 149.4px;
  margin: 0 auto;

  height: 45px;
  padding-top: 20px;
  line-height: 25px;

  @media (max-width: 900px) {
    width: 135.5px;
  }
`;

const SwiperButton = styled.div`
  cursor: pointer;
`;

const CompanyIcon = styled.img`
  height: 90px;

  @media (max-width: 900px) {
    ${ props => (props.isJuven ? 'display: none;' : '') } margin-top: 20px;
  }
`;

const MobileWrapper = styled.div`
  @media (min-width: 901px) {
    display: none;
  }
`;

export default class DescriptionSlider extends Component {
  constructor (props) {
    super();

    this.state = {
      index: 1,
      length: props.companys.length
    };
  }

  indexIncreaseHandler () {
    const { index, length } = this.state;
    if (index + 1 <= length) {
      this.setState({ index: index + 1 }, () => {
        // if (typeof window !== 'undefined' && !!window.scroll) {
        //   window.scroll({ left: 0, top: 0 });
        // }
      });
    }
  }

  indexDecreaseHandler () {
    const { index } = this.state;
    if (index > 1) {
      this.setState({ index: index - 1 }, () => {
        // if (typeof window !== 'undefined' && !!window.scroll) {
        //   window.scroll({ left: 0, top: 0 });
        // }
      });
    }
  }

  render () {
    const { index } = this.state;
    const { companys } = this.props;

    const currentCompany = companys[index - 1];

    return (
      <SliderContainer>
        <SliderImageDiv>
          <div
            style={ {
              backgroundImage: `url("${ (
                currentCompany.banner_url_desktop || currentCompany.banner_url
              ).replace('.png', '_large.jpg') }")`
            } }
          />
        </SliderImageDiv>
        <MobileWrapper>
          <SliderImage src={ currentCompany.banner_url } />
        </MobileWrapper>
        <SliderDescDiv noLink={ !currentCompany.id }>
          { currentCompany.icon_url ? (
            <CompanyIcon
              isJuven={ !currentCompany.id }
              src={ currentCompany.icon_url }
            />
          ) : null }
          <h4>{ currentCompany.name }</h4>
          <p>{ currentCompany.description }</p>
          <GoodReadsDescSwiper>
            <SwiperButton>
              <i
                class='fas fa-chevron-left'
                onClick={ () => this.indexDecreaseHandler() }
              />
            </SwiperButton>
            <div>
              { index }-{ companys.length }
            </div>
            <SwiperButton>
              <i
                class='fas fa-chevron-right'
                onClick={ () => this.indexIncreaseHandler() }
              />
            </SwiperButton>
          </GoodReadsDescSwiper>
        </SliderDescDiv>
      </SliderContainer>
    );
  }
}
