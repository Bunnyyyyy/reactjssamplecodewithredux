import React, { Component } from 'react';
import styled from 'styled-components';

import Label from './inputs/Label';
import InvalidMessage from './InvalidMessage';

const Exclamation = styled.i`
  font-size: 14px;
  position: initial !important;
  margin-right: 5px;
  padding-top: 5px;
`;

const Div = styled.div`
  position: relative;
  display: inline-block;
  width: ${ props => (props.fullWidth ? '100%' : '270px') };
  margin-top: 20px;
  margin-bottom: 20px;
  margin-right: 20px;

  &._half {
    margin-right: 2%;

    &:nth-child(2n) {
      margin-right: 0;
    }
  }

  & i {
    position: absolute;
    right: 10px;
    bottom: 0;
  }

  ${ props => props.customCss || '' };
`;

const InputWrapper = styled.div`
  position: relative;
  display: flex;
  flex-direction: column-reverse;
`;

const Currency = styled.p`
  position: absolute;
  left: 0;
  bottom: 8px;
  font-size: 12px;
  font-weight: 700;
  line-height: 14px;
  color: ${ props => (props.theme.light ? 'white' : props.theme.colors.darkGray) };
  text-transform: uppercase;
`;

const InputStyled = styled.input`
  outline: none;
  height: 32px;
  width: 100%;
  font-size: 15px;
  line-height: 28px;
  font-weight: 400;
  border: 0px solid transparent;
  border-bottom: 1px solid
    ${ props => (props.theme.light ? 'white' : props.theme.colors.darkGray) };
  border-radius: 0px;
  padding-left: ${ props => (props.prefix ? '30px' : '0px') };
  margin-top: 0px;
  background-color: transparent;
  color: ${ props => (props.theme.light ? 'white' : props.theme.colors.darkGray) };
  opacity: 0.25;

  &:focus {
    border-color: ${ props => (props.theme.light ? 'white' : props.theme.colors.mainBlue) };
  }

  &:focus,
  &:focus ~ label {
    opacity: 1;
  }

  ${ props => ((props.number && props.value !== '') || (props.value && props.value.length)
    ? `
    opacity: 1;
    border-color: ${
    props.theme.light ? 'rgba(255, 255, 255, .25)' : 'rgba(47, 46, 46, .25)'
    };

    & ~ label {
      opacity: 1;
    }
  `
    : '') };

  ${ props => (props.readOnly
    ? `
    cursor: default;
    border-color: transparent;
  `
    : '') } ::placeholder {
    color: ${ props => (props.theme.light ? 'white' : props.theme.colors.darkGray) };
    opacity: 0.25;
  }

  ${ props => (props.number
    ? `
      font-size: 25px;
      font-weight: 700;
    `
    : '') };

  ${ props => (props.invalid
    ? `
    border-color: ${
    props.theme.light
      ? props.theme.colors.mainYellow
      : props.theme.colors.pinkRed
    };

    &:focus {
      border-color: ${
    props.theme.light
      ? props.theme.colors.mainYellow
      : props.theme.colors.pinkRed
    };
    }
  `
    : '') };

  ${ props => props.inputCss || '' };
`;

const Message = styled.p`
  position: absolute;
  font-size: 12px;
  margin-top: 5px;
  color: ${ (props) => {
    if (props.error) {
      return props.theme.light
        ? props.theme.colors.mainYellow
        : props.theme.colors.pinkRed;
    }
    return props.theme.light ? 'white' : props.theme.colors.darkGray;
  } };
  line-height: 12px;
  letter-spacing: 1px;
`;

export default class Input extends Component {
  onFocus (e) {
    const { readOnly } = this.props;
    if (readOnly) {
      e.target.blur();
    }
  }

  onKeyDown (e) {
    // when pressing Enter or Esc
    if (e.keyCode === 13 || e.keyCode === 27) this.trimValue();
  }

  async changeInput (e) {
    const { onChange, number } = this.props;
    const { field } = e.target.dataset;
    let { value } = e.target;

    if (number) {
      value = value.replace(/,/g, '');
      // eslint-disable-next-line
      if (isNaN(Number(value)) || Number(value) < 0) return;
    }

    value = value === '' ? null : number ? Number(value) : value;
    await onChange(field, value);
  }

  async trimValue () {
    const {
      onChange, type, field, value, number, readOnly,
    } = this.props;

    if (
      onChange
      && !number
      && !readOnly
      && type !== 'password'
      && value !== null
      && value !== undefined
    ) {
      await onChange(field, value.trim());
    }
  }

  render () {
    const {
      className,
      type,
      number,
      currency,
      prefixElem,
      label,
      field,
      onChange,
      success,
      error,
      children,
      inputRef,
      divStyle = {},
      customCss,
      labelRight,
      fullWidth,
      required,
      disabled,
      readOnly,
      value,
      errorMessage,
      errorCss,
      subLabel,
      ...props
    } = this.props;

    let displayedValue = '';
    if (value !== null && value !== undefined) {
      displayedValue = number
        // eslint-disable-next-line
        ? !isNaN(value) ? value.toLocaleString() : null
        : value;
    }

    const prefix = prefixElem || currency ? <Currency>{ currency }</Currency> : null;
    return (
      <Div
        className={ `input${ className ? ` ${ className }` : '' }` }
        fullWidth={ fullWidth }
        style={ divStyle }
        customCss={ customCss }
      >
        <InputWrapper>
          { prefix }
          <InputStyled
            type={ type || 'text' }
            number={ number }
            prefix={ !!prefix }
            id={ field }
            data-field={ field }
            onChange={ this.changeInput.bind(this) }
            onFocus={ this.onFocus.bind(this) }
            onBlur={ this.trimValue.bind(this) }
            onKeyDown={ this.onKeyDown.bind(this) }
            innerRef={ inputRef || null }
            value={ displayedValue }
            disabled={ disabled || readOnly }
            readOnly={ readOnly }
            invalid={ !!errorMessage }
            { ...props }
          />
          { label ? (
            <Label
              field={ field }
              label={ label }
              required={ required }
              labelRight={ labelRight }
              subLabel={ subLabel }
            />
          ) : null }
        </InputWrapper>
        { errorMessage ? (
          <InvalidMessage customCss={ errorCss }>
            <Exclamation className='fas fa-exclamation-circle' />
            { errorMessage }
          </InvalidMessage>
        ) : null }
        { error || success ? (
          <Message error={ !!error }>{ error || success }</Message>
        ) : null }
        { children }
      </Div>
    );
  }
}
