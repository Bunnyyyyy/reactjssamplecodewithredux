import styled from 'styled-components';

export default styled.div`
  cursor: pointer;
  width: 50%;
  height: 54px;
  border: solid 1px ${ props => props.theme.colors.lightBlue };
  box-shadow: 0 0 6px 0 rgba(0, 0, 0, 0.09);
  text-align: center;

  &:first-child {
    border-radius: 5px 0 0 5px;
  }

  &:last-child {
    border-radius: 0 5px 5px 0;
  }

  & p {
    font-size: 15px;
    line-height: 54px;
  }

  ${ props =>
    props.selected
      ? `
      border-width: 2px;
      border-color: ${ props.theme.colors.mainBlue } !important;
      box-shadow: none;
    `
      : '' };
`;
