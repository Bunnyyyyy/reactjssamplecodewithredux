import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import styled from 'styled-components';

import Input from '../common/Input';
import Notification from './Notification';

import * as ManagerActionCreators from '../../redux/manager';

import logoPlaceholder from '../../assets/common/placeholder_logo.svg';
import bannerPlaceholder from '../../assets/common/placeholder_banner.png';

import { validateEmail, formatDate, formatTime } from '../../utils';

const ActionCreators = Object.assign(
  {},
  ManagerActionCreators,
);

const Background = styled.div`
  position: fixed;
  z-index: 1000;
  left: 0;
  top: 0;
  width: 100vw;
  height: 100vh;
  background-color: rgba(0, 0, 0, 0.5);
`;

const Wrapper = styled.div`
  position: absolute;
  right: 0;
  top: 0;
  width: 380px;
  height: 100%;
  background-color: rgb(251, 251, 251);
  box-shadow: -5px 0 20px 0 rgba(0, 0, 0, 0.1);
`;

const Header = styled.div`
  height: 80px;
  box-shadow: 0 3px 10px 0 rgba(0, 0, 0, 0.05);
  padding: 0 20px;

  & h3 {
    text-align: center;
    line-height: 80px;
    margin: 0;
  }

  & button {
    position: absolute;
    left: 0;
    width: 60px;
    height: 80px;
    background-color: transparent;
    color: ${ props => props.theme.colors.mainGray };

    &:hover,
    &:active {
      background-color: transparent;
    }

    & i {
      position: relative;
      top: 2px;
      font-size: 24px;
    }
  }
`;

const Testing = styled.div`
  height: 245px;
  padding: 20px 35px 30px 35px;
  border-bottom: 1px solid rgb(227, 227, 227);
`;

const Body = styled.div`
  height: calc(100% - ${ props => (props.hasTesting ? '325px' : '80px') });
  padding: 50px 10px;
  overflow-x: hidden;
  overflow-y: scroll;
`;

const LogoElement = styled.div`
  text-align: center;
  padding: 20px 10px 40px 10px;

  & img {
    height: 50px;
  }
`;

const TitleElement = styled.div`
  text-align: center;
  padding: 0 10px 30px 10px;

  & p {
    color: ${ props => props.theme.colors.darkGray };
  }

  & .title-main {
    font-size: 24px;
    line-height: 34px;
    font-weight: 600;
  }

  & .title-text {
    font-size: 14px;
    line-height: 20px;
    margin-bottom: 10px;
  }
`;

const GreetingElement = styled.div`
  text-align: center;
  padding: 0 10px 30px 10px;

  & p {
    font-size: 17px;
    line-height: 24px;
    font-weight: 600;
    color: rgb(61, 61, 79);
  }
`;

const ParagraphElement = styled.div`
  padding: 10px;

  & p {
    font-size: 14px;
    line-height: 20px;
    color: ${ props => props.theme.colors.darkGray };
    white-space: pre-line;
  }
`;

const ImageElement = styled.div`
  padding: 10px;

  & img {
    width: 100%;
  }
`;

const InformationElement = styled.div`
  padding: 0 10px 30px 10px;

  & p {
    font-size: 14px;
    line-height: 20px;
    color: ${ props => props.theme.colors.darkGray };
    white-space: pre-line;
  }

  & .information-description {
    margin-bottom: 10px;
  }

  & .information-title,
  & .information-value {
    display: inline-block;
    vertical-align: text-top;
  }

  & .information-title {
    width: 70px;
    font-weight: 600;
  }
`;

const ActionButtonElement = styled.div`
  padding: 10px 10px 30px 10px;

  & a.button {
    width: 100%;
    height: 56px;
    padding: 0 20px;
    background-color: ${ props => props.theme.colors.mainBlue };
    text-align: center;
    font-size: 13px;
    font-weight: 600;
    line-height: 56px;
    letter-spacing: 1.5px;
  }

  & .action-button-instruction {
    font-size: 14px;
    line-height; 20px;
    margin-bottom: 10px;
  }

  & .action-button-warning {
    font-size: 17px;
    line-height: 27px;
    font-weight: 500;
    color: rgb(239, 3, 32);
    margin: 30px 0 20px 0;

    & img {
      vertical-align: middle;
      height: 27px;
      margin-right: 5px;
    }
  }

  & .action-button-reminder {
    font-size: 13px;
    line-height: 18px;
    color: ${ props => props.theme.colors.mainGray };
    margin: 0;
  }
`;

const TicketElement = styled.div`
  max-width: 340px;
  width: 100%;
  margin: 0 auto;
  border-radius: 6.4px;
  border: 1px solid rgb(221, 221, 221);
  box-shadow: 0 4px 32px 0 rgba(0, 0, 0, 0.1);

  & .ticket-row {
    padding: 10px 15px 0 15px;
  }

  & .ticket-field {
    display: inline-block;
    vertical-align: text-top;
    width: 75px;
  }

  & .ticket-title {
    font-size: 8px;
    line-height: 11px;
    font-weight: 600;
    margin: 0;
    color: ${ props => props.theme.colors.mainBlue };
  }

  & .ticket-value {
    font-size: 14px;
    line-height: 17px;
    margin: 0;
    color: ${ props => props.theme.colors.darkGray };
  }
`;

const ContactElement = styled.div`
  padding: 20px 10px 10px 10px;

  & p {
    font-size: 12px;
    line-height: 18px;
    color: rgb(146, 146, 146);
  }
`;

const mapStateToProps = state => ({
  event: state.manager.event,
});
const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators(ActionCreators, dispatch),
});

class EmailPreview extends Component {
  constructor () {
    super();

    this.state = {
      email: '',
    };
  }

  changeInput (field, value) {
    this.setState({
      [field]: value,
    });
  }

  async sendTestingEmail () {
    const { actions, onSendTestEmail } = this.props;
    const { email } = this.state;

    if (!validateEmail(email)) {
      return actions.setNotification({
        type: 'error',
        title: 'Oops! There is an error when sending the Testing Email.',
        message: 'Please fill in a valid email address.',
      });
    }

    try {
      await onSendTestEmail(email);
      this.setState({ email: '' });
    } catch (e) {
      return actions.setNotification({
        type: 'error',
        title: 'Oops! There is an error when sending the Testing Email.',
        message: e.message,
      });
    }
  }

  render () {
    const {
      event, title, elements, onClose, onSendTestEmail,
    } = this.props;
    const { email } = this.state;

    const TestingEmail = (
      <div style={ { position: 'relative' } }>
        <Notification preview />
        <Testing>
          <h5>Testing email</h5>
          <p>
            You can send a testing email to your own address to see how the real
            email looks.
          </p>
          <Input
            fullWidth
            field='email'
            placeholder='Your email address'
            value={ email }
            onChange={ this.changeInput.bind(this) }
          />
          <button
            type='button'
            style={ { width: '100%' } }
            disabled={ !email || !email.length }
            onClick={ this.sendTestingEmail.bind(this) }
          >
            Send
          </button>
        </Testing>
      </div>
    );

    // eslint-disable-next-line
    const Elements = elements.map((e) => {
      const { element, text, image_url } = e;

      if (element === 'logo') {
        return (
          <LogoElement>
            <img
              alt='Logo'
              src={
                event.logo_image_url || logoPlaceholder
              }
            />
          </LogoElement>
        );
      } if (element === 'title') {
        return (
          <TitleElement>
            { text ? <p className='title-text'>{ text }</p> : null }
            <p className='title-main'>{ event.name }</p>
          </TitleElement>
        );
      } if (element === 'greeting') {
        return (
          <GreetingElement>
            <p>Dear John,</p>
          </GreetingElement>
        );
      } if (element === 'paragraph') {
        return (
          <ParagraphElement>
            <p>{ text }</p>
          </ParagraphElement>
        );
      } if (element === 'image') {
        return (
          <ImageElement>
            <img alt='' src={ image_url } />
          </ImageElement>
        );
      } if (element === 'information') {
        const startDate = formatDate(event.start_date);
        const startTime = formatTime(event.start_date);
        return (
          <InformationElement>
            <p className='information-description'>{ event.description }</p>
            <div style={ { marginLeft: 20 } }>
              { startDate.length ? (
                <div>
                  <p className='information-title'>Date:</p>
                  <p className='information-value'>{ startDate }</p>
                </div>
              ) : null }
              { startTime.length ? (
                <div>
                  <p className='information-title'>Time:</p>
                  <p className='information-value'>{ startTime }</p>
                </div>
              ) : null }
              <div>
                <p className='information-title'>Venue:</p>
                <p className='information-value'>{ event.location }</p>
              </div>
              { event.location_address ? (
                <div>
                  <p className='information-title'>Address:</p>
                  <p className='information-value'>{ event.location_address }</p>
                </div>
              ) : null }
            </div>
          </InformationElement>
        );
      } if (element === 'register_button') {
        return (
          <ActionButtonElement>
            <p className='action-button-instruction'>
              { `Please click the following button to register for ${ event.name }.` }
            </p>
            <button
              type='button'
              style={ { backgroundColor: event.primary_color } }
            >
              Register
            </button>
          </ActionButtonElement>
        );
      } if (element === 'login_button') {
        return (
          <ActionButtonElement>
            <p className='action-button-instruction'>
              <span>Please click the following button to access the </span>
              <br />
              <span>{ `${ event.name } Guest App.` }</span>
            </p>
            <button
              type='button'
              style={ { backgroundColor: event.primary_color } }
            >
              Log in as John
            </button>
            <p className='action-button-warning'>
              <img alt='' src='https://s3-ap-southeast-1.amazonaws.com/icered-prd/icon_warning.png' />
              This email is for you only!
            </p>
            <p className='action-button-reminder'>
              Please do NOT forward this email to anyone, otherwise those with
              this link will be able to participate in the live event activities
              under your identity.
            </p>
          </ActionButtonElement>
        );
      } if (element === 'ticket') {
        return (
          <TicketElement>
            <div style={ { height: 50, padding: '10px 15px' } }>
              <img
                alt=''
                style={ { height: 30 } }
                src={
                  event.logo_image_url || logoPlaceholder
                }
              />
              <div style={ { float: 'right' } }>
                <p
                  className='ticket-title'
                  style={ { color: event.primary_color } }
                >
                  Ref No.
                </p>
                <p className='ticket-value'>#G123</p>
              </div>
            </div>
            <div>
              <img
                alt=''
                style={ { width: '100%' } }
                src={
                  event.cover_image_url || bannerPlaceholder
                }
              />
            </div>
            <div style={ { padding: 15 } }>
              <p
                className='ticket-title'
                style={ { color: event.primary_color } }
              >
                Name
              </p>
              <p
                className='ticket-value'
                style={ { fontSize: 21, lineHeight: '28px' } }
              >
                John Doe
              </p>
            </div>
            <div>
              <div className='ticket-row'>
                <div>
                  <p
                    className='ticket-title'
                    style={ { color: event.primary_color } }
                  >
                    Ticket Type
                  </p>
                  <p className='ticket-value'>Premium</p>
                </div>
              </div>
              <div className='ticket-row'>
                <div className='ticket-field'>
                  <p
                    className='ticket-title'
                    style={ { color: event.primary_color } }
                  >
                    Date
                  </p>
                  <p className='ticket-value'>{ formatDate(event.start_date) }</p>
                </div>
                <div className='ticket-field'>
                  <p
                    className='ticket-title'
                    style={ { color: event.primary_color } }
                  >
                    Time
                  </p>
                  <p className='ticket-value'>{ formatDate(event.start_date) }</p>
                </div>
              </div>
              <div className='ticket-row'>
                <p
                  className='ticket-title'
                  style={ { color: event.primary_color } }
                >
                  Venue
                </p>
                <p className='ticket-value'>{ event.location }</p>
              </div>
              { event.location_address ? (
                <div className='ticket-row'>
                  <p
                    className='ticket-title'
                    style={ { color: event.primary_color } }
                  >
                    Address
                  </p>
                  <p className='ticket-value'>{ event.location_address }</p>
                </div>
              ) : null }
              <div className='ticket-row'>
                <div className='ticket-field'>
                  <p
                    className='ticket-title'
                    style={ { color: event.primary_color } }
                  >
                    Table
                  </p>
                  <p className='ticket-value'>A</p>
                </div>
                <div className='ticket-field'>
                  <p
                    className='ticket-title'
                    style={ { color: event.primary_color } }
                  >
                    Seat
                  </p>
                  <p className='ticket-value'>12</p>
                </div>
              </div>
              <div style={ { padding: '50px 15px 25px 15px' } }>
                <img
                  alt=''
                  src='http://api.qrserver.com/v1/create-qr-code/?size=90x90&data=1234567890'
                  style={ {
                    display: 'block',
                    margin: '0 auto',
                    height: 100,
                    width: 100,
                  } }
                />
              </div>
            </div>
          </TicketElement>
        );
      } if (element === 'contact') {
        return (
          <ContactElement>
            <p>
              <span>Please contact </span>
              <a href={ `mailto:${ event.organization_email }` }>
                { event.organization_email }
              </a>
              <span> for any enquiries.</span>
            </p>
          </ContactElement>
        );
      }
    });

    return (
      <Background>
        <Wrapper>
          <Header>
            <button type='button' onClick={ onClose ? onClose.bind(this) : null }>
              <i className='fas fa-angle-left' />
            </button>
            <h3>{ title }</h3>
          </Header>
          { onSendTestEmail ? TestingEmail : null }
          <Body hasTesting={ !!onSendTestEmail }>
            <div style={ { paddingBottom: 50 } }>{ Elements }</div>
          </Body>
        </Wrapper>
      </Background>
    );
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(EmailPreview);
