import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { withRouter } from 'react-router';
import styled, { ThemeProvider } from 'styled-components';

import theme from '../../../../../utils/theme';

import * as UiActionCreators from '../../../../../redux/ui';
import * as ManagerActionCreators from '../../../../../redux/manager';
import * as EventActionCreators from '../../../../../redux/event';

import SaveActions from '../../../../../components/manager/SaveActions';
import Card from '../../../../../components/manager/cards/Card';
import Form from '../../../../../components/common/inputs/Form';
import CustomizedLabel from '../../../../../components/manager/cards/CustomizedLabel';
import CategoryOption from '../../../../../components/common/inputs/CategoryOption';

import i18n from '../../../../../../i18n';

const t = a => i18n.t([`translations:${ a }`]);

const ButtonActions = styled.div`
  display: flex;
  align-items: center;
  padding: 10px 0;
  border-top: 1px solid ${ props => props.theme.colors.lightGray };
`;

const CardBodyWrapper = styled.div`
  display: flex;
`;

const LabelWrapper = styled.div`
  flex: 50% 0 0;
  padding: 30px;
`;

const SizeOption = CategoryOption.extend`
  width: 150px;
  height: 35px;

  ${ props => (props.selected
    ? `
      border: 2px solid ${ props.theme.colors.mainBlue } !important;
      color: ${ props.theme.colors.mainBlue } !important;
      > h6 {
        font-weight: 700;
      }
        `
    : '') };
`;

const formCss = `
  flex: 50% 0 0;
  border-right: 1px solid ${ theme.colors.lightGray };
  padding-bottom: 30px;
  margin-right: 0px !important;
`;

const LABEL_SIZE_OPTIONS = [
  {
    label: `90${ t('mm') } x 50 ${ t('mm') }`,
    value: '90,50',
  },
  {
    label: `90${ t('mm') } x 29 ${ t('mm') }`,
    value: '90,29',
  },
];

const DEFAULT_LABEL_SIZE = '90,29';

const mapStateToProps = state => ({
  events: state.manager.events,
  event: state.manager.event,
  organization: state.manager.organization,
  fields: state.event.dataFields,
  label: state.event.label,
});

const ActionCreators = Object.assign(
  {},
  UiActionCreators,
  ManagerActionCreators,
  EventActionCreators,
);
const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators(ActionCreators, dispatch),
});

class EventTicketingLabelView extends Component {
  constructor (props) {
    super();

    this.state = {
      labelData: this.getLabelData(props.label),
      isUpdated: false,
    };
  }

  async componentDidMount () {
    const { actions, match } = this.props;
    const { eId } = match.params;

    actions.setLoading(true);

    try {
      const promises = [
        actions.getEventTickets(eId),
        actions.getEventDates(eId),
        actions.getEventLabel(eId),
        actions.getEventDataFields(eId),
      ];
      await Promise.all(promises);
    } catch (e) {
      console.error('ERROR: EventTicketingLabelView', e);
    } finally {
      actions.setLoading(false);
    }
  }

  componentWillUnmount () {
    const { actions } = this.props;
    actions.resetSaveActions();
  }

  async onFinishSaving () {
    try {
      const { actions, event } = this.props;
      const eId = event.id;
      await Promise.all([actions.getOrganizationEvent(eId)]);
      await Promise.all[actions.getEventLabel(eId)];
    } catch (e) {
      throw e;
    }
  }

  onUpdateForm (state) {
    const { actions, event } = this.props;

    this.setState({ labelData: state }, () => {
      const saveData = Object.assign({}, state);
      const sizeArr = (saveData.size || LABEL_SIZE_OPTIONS[0].value).split(',');

      saveData.width = Number(sizeArr[0]);
      saveData.height = Number(sizeArr[1]);
      delete saveData.size;

      saveData.data_field_ids = saveData.has_data_fields
        ? saveData.data_field_ids
        : null;
      delete saveData.has_data_fields;

      actions.addSaveAction(
        actions.updateEventLabel.bind(this, event.id, saveData),
        'event-label',
        0,
      );
    });
  }

  getLabelData (newLabel) {
    let { label } = this.props || {};
    if (newLabel) label = newLabel;

    const labelData = {
      guest_name: label.guest_name !== false,
      guest_qr_code: label.guest_qr_code,
      event_name: label.event_name,
      tags: label.tags,
      has_data_fields: !!label.data_field_ids && !!label.data_field_ids.length,
      data_field_ids: label.data_field_ids,
    };
    if (label.width && label.height) {
      labelData.size = `${ label.width },${ label.height }`;
    } else {
      labelData.size = DEFAULT_LABEL_SIZE;
    }

    return labelData;
  }

  resetLabelData () {
    this.setState({
      labelData: this.getLabelData(),
      isUpdated: false,
    });
  }


  render () {
    const { event = {}, fields } = this.props;
    const { labelData, isUpdated } = this.state;

    const eventLabelFields = [
      {
        type: 'options',
        field: 'size',
        label: t('Label Size'),
        default: labelData.size || LABEL_SIZE_OPTIONS[0].value,
        required: true,
        options: LABEL_SIZE_OPTIONS.map(c => ({
          value: c.value,
          component: (
            <SizeOption>
              <h6>{ c.label }</h6>
            </SizeOption>
          ),
        })),
      },
      {
        type: 'title',
        text: t('Items'),
      },
      {
        field: 'guest_name',
        type: 'checkbox',
        label: t('Guest Name'),
        default: !!labelData.guest_name,
        required: true,
        disabled: true,
      },
      {
        field: 'guest_qr_code',
        type: 'checkbox',
        label: t('Guest QR Code'),
        default: !!labelData.guest_qr_code,
      },
      {
        field: 'event_name',
        type: 'checkbox',
        label: t('Event Name'),
        default: !!labelData.event_name,
      },
      {
        field: 'tags',
        type: 'checkbox',
        label: t('Tags'),
        default: !!labelData.tags,
      },
      {
        field: 'has_data_fields',
        type: 'checkbox',
        label: t('Data Collection Fields'),
        default: labelData.has_data_fields,
      },
      {
        field: 'data_field_ids',
        type: 'multiSelect',
        label: `${ t('Select fields to include') }:`,
        condition: state => !!state.has_data_fields,
        default: labelData.data_field_ids || [],
        getErrorMessage: (formState, value) => {
          if (formState.has_data_fields && !value.length) {
            return t(
              'Please select at least one field to include in the label',
            );
          }
          return null;
        },
        options: fields.filter(f => !!f.data_type).map(f => ({
          label: f.label,
          value: f.id,
        })),
      },
    ];

    return (
      <Card title={ t('Customize your label') }>
        <CardBodyWrapper>
          <ThemeProvider theme={ { smallInput: true } }>
            <Form
              name='event-label'
              validateForm={ isUpdated }
              fields={ eventLabelFields }
              onUpdate={ this.onUpdateForm.bind(this) }
              customCss={ formCss }
            />
          </ThemeProvider>
          <LabelWrapper>
            <p>
              { t(
                'Labels can be printed by using our Manager App when guests are being checked in.',
              ) }
            </p>
            <h5>
              { t('Sample Label Style') }
:
            </h5>
            <CustomizedLabel
              event={ event }
              fields={ fields }
              labelData={ labelData }
            />
          </LabelWrapper>
        </CardBodyWrapper>
        <ButtonActions>
          <SaveActions onFinish={ this.onFinishSaving.bind(this) } />
        </ButtonActions>
      </Card>
    );
  }
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(EventTicketingLabelView));
