import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { withRouter } from 'react-router';
import moment from 'moment-timezone';

import ManagerContainer from '../../../../components/layouts/ManagerContainer';
import BulkImportCard from '../../../../components/common/BulkImportCard';

import * as UiActionCreators from '../../../../redux/ui';
import * as ProfileActionCreators from '../../../../redux/profile';
import * as EventActionCreators from '../../../../redux/event';
import {
  validateEmail,
  validatePhone,
} from '../../../../utils';

import { t as translate } from '../../../../../i18n';

const t = a => translate(a, 'manager/events/bulkImportGuests');

const { API_URL } = process.env;

const mapStateToProps = state => ({
  organization: state.manager.organization,
  event: state.manager.event,
  ticketTypes: state.event.tickets,
  sessions: state.event.sessions,
  dataFields: state.event.dataFields,
});

const ActionCreators = Object.assign(
  {},
  UiActionCreators,
  ProfileActionCreators,
  EventActionCreators,
);
const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators(ActionCreators, dispatch),
});

class EventImporGuestsView extends Component {
  async componentDidMount () {
    const { actions, match } = this.props;
    const { eId } = match.params;

    actions.setLoading(true);

    try {
      const promises = [
        actions.getEventTickets(eId),
        actions.getEventSessions(eId),
        actions.getEventDataFields(eId),
      ];
      await Promise.all(promises);
    } catch (e) {
      console.error('ERROR: EventImporGuestsView', e);
    } finally {
      actions.setLoading(false);
    }
  }

  async importGuests (guests) {
    try {
      const { event, actions } = this.props;
      await actions.bulkAddEventGuests(event.id, { guests });

      this.returnToGuests();
    } catch (e) {
      console.error(e);
    }
  }

  returnToGuests () {
    const { organization, history, event } = this.props;
    history.push(`/${ organization.id }/events/${ event.id }/guests`);
  }

  render () {
    const {
      event,
      ticketTypes,
      sessions,
      dataFields,
    } = this.props;

    const timezone = event.timezone || 'Asia/Hong_Kong';
    let fields = [
      {
        label: t('First Name'),
        value: 'first_name',
        required: true,
      },
      {
        label: t('Last Name'),
        value: 'last_name',
      },
      {
        label: t('Email'),
        value: 'email',
        validate: v => !v || validateEmail(v),
      },
      {
        label: t('Phone'),
        value: 'phone',
        validate: v => !v || validatePhone(v),
      },
      {
        label: t('Ticket Type'),
        value: 'event_ticket_id',
        validate: (v) => {
          if (!v || !v.length) return true;

          return !!ticketTypes.find(tt => (
            tt.name
              .trim()
              .toLowerCase()
              .replace(/[ _]/g, '')
              === v
                .trim()
                .toLowerCase()
                .replace(/[ _]/g, '')
          ));
        },
        transform: (v) => {
          if (!v || !v.length) return null;

          const ticketType = ticketTypes.find(tt => (
            tt.name
              .trim()
              .toLowerCase()
              .replace(/[ _]/g, '')
              === v
                .trim()
                .toLowerCase()
                .replace(/[ _]/g, '')
          ));

          return ticketType ? ticketType.id : null;
        },
      },
      {
        label: t('Session'),
        value: 'session_id',
        validate: (v) => {
          if (!v || !v.length) return true;
          // HACK: get rid of Z to avoid erroneously assuming incoming date string as UTC
          const vDate = moment.tz(new Date(v.replace(/Z/g, '')), timezone);

          return !!sessions.find(s => moment.tz(s.start_at, timezone).isSame(vDate));
        },
        transform: (v) => {
          if (!v || !v.length) return null;
          // HACK: get rid of Z to avoid erroneously assuming incoming date string as UTC
          const vDate = moment.tz(new Date(v.replace(/Z/g, '')), timezone);

          const session = sessions.find(s => moment.tz(s.start_at, timezone).isSame(vDate));

          return session ? session.id : null;
        },
      },
    ];

    fields = fields.concat(
      dataFields.filter(f => !!f.data_type).map(f => ({
        label: f.label,
        value: f.id,
        custom: true,
        required: !!f.required && !f.condition_field_id && !f.specific_type_ids,
      })),
    );

    const sampleCsvUrl = `${ API_URL }/events/${ event.id }/guests/import/sample_csv`;

    return (
      <ManagerContainer>
        <BulkImportCard
          title={ t('Bulk Import Guests') }
          fields={ fields }
          onImport={ this.importGuests.bind(this) }
          onCancel={ this.returnToGuests.bind(this) }
          sampleCsvUrl={ sampleCsvUrl }
        />
      </ManagerContainer>
    );
  }
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(EventImporGuestsView));
