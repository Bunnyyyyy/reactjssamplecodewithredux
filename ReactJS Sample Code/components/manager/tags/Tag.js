import React from 'react';
import styled from 'styled-components';

import Tooltip from '../../common/Tooltip';

const PopupText = styled(Tooltip)`
  display: none;
  top: 15px;
  left: 0;
`;

const Container = styled.div`
  display: inline-flex;
  position: relative;
  &:hover ${ PopupText } {
    display: block;
  }
`;

const ColoredTag = styled.svg`
  width: ${ props => props.size }px;
  height: ${ props => props.size }px;
  margin-right: 5px;
  #icon-tag {
    fill: ${ props => props.color };
  }

  ${ props => props.customCss || '' };
`;

export default function Tag ({
  value,
  color = 'rgb(0, 167, 255)',
  customCss,
  size = 20,
}) {
  return (
    <Container>
      <ColoredTag
        size={ size }
        customCss={ customCss }
        color={ color }
        xmlns='http://www.w3.org/2000/svg'
        viewBox='-2678.782 4173 13.564 13.55'
      >
        <path
          id='icon-tag'
          className='cls-1'
          d='M20.6,13.78a2.311,2.311,0,0,0-1.636-.68h-3.53A2.338,2.338,0,0,0,13.1,15.435v3.53a2.249,2.249,0,0,0,.68,1.636l5.369,5.369a2.311,2.311,0,0,0,1.636.68,2.279,2.279,0,0,0,1.636-.68l3.567-3.512a2.342,2.342,0,0,0,0-3.291ZM17.733,16.8a.919.919,0,1,1-.919-.919A.934.934,0,0,1,17.733,16.8Z'
          transform='translate(-2691.883 4159.9)'
        />
      </ColoredTag>
      { value ? <PopupText>{ value }</PopupText> : null }
    </Container>
  );
}
