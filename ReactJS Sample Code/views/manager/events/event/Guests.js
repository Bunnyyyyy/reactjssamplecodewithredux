import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { withRouter } from 'react-router';
import styled from 'styled-components';
import moment from 'moment';

import {
  validateEmail,
  stateHasSelectedRows,
  formatDateTime,
} from '../../../../utils';
import * as UiActionCreators from '../../../../redux/ui';
import * as ManagerActionCreators from '../../../../redux/manager';
import * as ProfileActionCreators from '../../../../redux/profile';
import * as EventActionCreators from '../../../../redux/event';
import * as OrganizationActionCreators from '../../../../redux/organization';
import * as ContactsActionCreators from '../../../../redux/contacts';
import * as EmailTemplatesActionCreators from '../../../../redux/emailTemplates';

import ManagerContainer from '../../../../components/layouts/ManagerContainer';
import Table from '../../../../components/common/Table';
import Button from '../../../../components/common/Button';
import Select from '../../../../components/common/inputs/Select';
import BasicContactInfo from '../../../../components/manager/BasicContactInfo';

import Tag from '../../../../components/manager/tags/Tag';
import EditTag from '../../../../components/manager/tags/EditTag';
import SettingModal from '../../../../components/manager/SettingModalV2';

import ticketWhiteIcon from '../../../../assets/manager/icon_ticket_white.svg';
import eventWhiteIcon from '../../../../assets/header/icon_event_white.svg';
import deleteIcon from '../../../../assets/manager/icon_delete.svg';
import resendTicketIcon from '../../../../assets/manager/bulk-actions/icon-resend-ticket.svg';
import emailIcon from '../../../../assets/manager/bulk-actions/icon-email.svg';
import checkInIcon from '../../../../assets/manager/bulk-actions/icon-check-in.svg';
import uncheckInIcon from '../../../../assets/manager/bulk-actions/icon-uncheck-in.svg';
import copyContactIcon from '../../../../assets/manager/bulk-actions/icon-copy-contact.svg';
import trashIcon from '../../../../assets/manager/bulk-actions/icon-trash.svg';
import downloadIcon from '../../../../assets/manager/bulk-actions/icon-download.svg';
import addContactIcon from '../../../../assets/manager/bulk-actions/icon-contact-add.svg';

import { t as translate } from '../../../../../i18n';

const t = a => translate(a, 'manager/events/guests');

const { API_URL } = process.env;

const FileLink = styled.a`
  font-size: 12px;
  line-height: 18px;
  color: ${ props => props.theme.colors.mainBlue };
`;

const CheckIcon = styled.i`
  margin-right: 5px;
  color: #32a844;
`;

const GrayIcon = styled.i`
  margin-right: 5px;
  color: ${ props => props.theme.colors.mainGray };
`;

const LightBallWrapper = styled.div`
  display: inline-flex;
  font-size: 12px;
  justify-content: center;
  align-items: center;
`;

const LightBall = styled.div`
  width: 10px;
  height: 10px;
  background-color: ${ props => props.color };
  border-radius: 10px;
  margin-right: 10px;
`;

const ResendButton = Button.extend`
  font-size: 12px;
  height: 14px;
  line-height: 14px;
  padding: 0;
  margin: 0;
  color: ${ props => props.theme.colors.mainBlue };
`;

const mapStateToProps = state => ({
  user: state.me.user,
  organizations: state.manager.organizations,
  organization: state.manager.organization,
  events: state.manager.events,
  event: state.manager.event,
  guests: state.manager.eventGuests,
  tickets: state.event.tickets,
  tags: state.organization.tags,
  templates: state.emailTemplates.templates,
  dataFields: state.event.dataFields,
  customFields: state.organization.customFields,
  meta: state.manager.meta,
  sessions: state.event.sessions,
});

const ActionCreators = Object.assign(
  {},
  UiActionCreators,
  ManagerActionCreators,
  ProfileActionCreators,
  EventActionCreators,
  OrganizationActionCreators,
  ContactsActionCreators,
  EmailTemplatesActionCreators,
);
const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators(ActionCreators, dispatch),
});

class EventGuestsView extends Component {
  constructor (props) {
    super();
    const { guests } = props;
    const tooltips = {};
    guests.forEach((guest) => {
      tooltips[guest.id] = false;
    });

    // default table settings
    this.state = {
      limit: 10,
      offset: 0,
      sortBy: 'name',
      sortOrder: 'asc',
      sortType: null,
      search: null,
      filters: {},
      isAddingGuest: false,
      editingGuest: {},
      templateEmailParams: null,
      cloningContactsParams: null,
    };
  }

  async componentDidMount () {
    const { actions, match } = this.props;
    const { oId, eId } = match.params;

    actions.setLoading(true);

    try {
      const promises = [
        actions.getEventGuests(eId, {
          limit: 10,
          offset: 0,
          sort_by: 'name',
          sort_order: 'asc',
        }),
        actions.getEventTickets(eId),
        actions.getEventDataFields(eId),
        actions.getOrganizationCustomFields(oId),
        actions.getOrganizationTags(oId),
        actions.getEventSessions(eId),
        actions.getOrganizationAdmins(oId),
        actions.getEmailTemplates(oId),
      ];
      await Promise.all(promises);
    } catch (e) {
      console.error('ERROR: EventGuestsView', e);
    } finally {
      actions.setLoading(false);
    }
  }

  getGuestsFrom (newFrom) {
    this.setState({ offset: newFrom - 1 }, this.reloadGuests);
  }

  setEditingGuest (editingGuest) {
    this.setState({ editingGuest });
  }

  setTemplateEmailParams (templateEmailParams) {
    this.setState({ templateEmailParams });
  }

  setGuestFilters (filters) {
    this.setState(
      {
        offset: 0,
        filters,
      },
      this.reloadGuests,
    );
  }

  setCloningContactsParams (cloningContactsParams) {
    this.setState({ cloningContactsParams });
  }

  resetTooltips = () => {
    const { guests } = this.props;
    const tooltips = {};
    guests.forEach((guest) => {
      tooltips[guest.id] = false;
    });

    return tooltips;
  };

  routeToBulkImport = () => {
    const { organization, history, event } = this.props;
    history.push(`/${ organization.id }/events/${ event.id }/guests/import`);
  };

  sortGuests (newSortBy, sortType) {
    const { sortBy } = this.state;
    let { sortOrder } = this.state;

    sortOrder = newSortBy === sortBy ? (sortOrder === 'asc' ? 'desc' : 'asc') : 'asc';
    this.setState({
      sortBy: newSortBy,
      sortOrder,
      sortType,
    }, this.reloadGuests);
  }

  changeGuestsPerPage (newPerPage) {
    this.setState(
      {
        offset: 0,
        limit: newPerPage,
      },
      this.reloadGuests,
    );
  }

  async reloadGuests () {
    const { actions, event } = this.props;
    const {
      limit,
      offset,
      sortBy,
      sortOrder,
      sortType,
      search,
      filters,
    } = this.state;

    const params = {};
    if (limit !== undefined) params.limit = limit;
    if (offset !== undefined) params.offset = offset;
    if (sortBy !== undefined) params.sort_by = sortBy;
    if (sortOrder !== undefined) params.sort_order = sortOrder;
    if (sortType) params.sort_type = sortType;
    if (search !== undefined) params.search = search;
    if (filters !== undefined && Object.keys(filters).length) {
      params.filters = JSON.stringify(filters);
    }

    try {
      await actions.getEventGuests(event.id, params);
    } catch (e) {
      console.error(e);
    }
  }

  toggleAddingGuest () {
    const { isAddingGuest } = this.state;
    this.setState({ isAddingGuest: !isAddingGuest });
  }

  changeSearchValue (value) {
    this.setState(
      {
        offset: 0,
        search: value,
      },
      this.reloadGuests,
    );
  }

  exportGuest () {
    const { event } = this.props;
    const { sortBy, sortOrder, sortType } = this.state;

    let url = `${ API_URL }/events/${
      event.id
    }/guests/export?sort_by=${ sortBy }&sort_order=${ sortOrder }`;
    if (sortType) url += `&sort_type=${ sortType }`;

    window.open(url, '_blank');
  }

  async saveGuest (data) {
    const { actions, event } = this.props;
    const { first_name, image } = data;

    let errorMessage;
    if (!first_name.length) {
      errorMessage = t('Please fill in the First Name');
    }

    if (errorMessage) {
      throw new Error({ essage: errorMessage });
    }

    if (!image) delete data.image;

    const formData = new FormData();
    for (const field in data) {
      formData.append(field, data[field]);
    }

    try {
      await actions.addEventGuest(event.id, formData);
      await this.reloadGuests();
      this.toggleAddingGuest();
    } catch (e) {
      throw e;
    }
  }

  async removeGuests (actionParams) {
    const { actions, event } = this.props;

    try {
      await actions.removeEventGuests(event.id, actionParams);
      this.reloadGuests();
    } catch (e) {
      throw e;
    }
  }

  async sendGuestLoginEmails (guests) {
    const { actions, event } = this.props;
    const data = {
      emails: guests.filter(g => g.email).map(g => g.email),
      resend: true,
    };

    try {
      await actions.sendEventCheckinEmails(event.id, data);
    } catch (e) {
      return Promise.reject(e);
    }
  }

  async sendTemplateEmails (state) {
    try {
      const { actions, organization, event } = this.props;
      const { templateEmailParams } = this.state;
      const { template_id } = state;

      const data = {
        contact_type: 'event_guest',
        event_id: event.id,
        ...templateEmailParams,
      };

      await actions.sendTemplateEmails(organization.id, template_id, data);
      this.setTemplateEmailParams(null);
    } catch (e) {
      console.error(e);
    }
  }

  async sendEventTicketEmails (actionParams) {
    const { actions, event } = this.props;

    try {
      await actions.sendEventTicketEmails(event.id, actionParams);
    } catch (e) {
      return Promise.reject(e);
    }
  }

  async checkInGuests (actionParams) {
    const { actions, event } = this.props;

    const data = {
      ...actionParams,
      checked_in_at: moment.utc().toDate(),
    };

    try {
      await actions.updateEventGuests(event.id, data);
      this.reloadGuests();
    } catch (e) {
      throw e;
    }
  }

  async revertCheckInGuests (actionParams) {
    const { actions, event } = this.props;

    const data = {
      ...actionParams,
      checked_in_at: null,
    };

    try {
      await actions.updateEventGuests(event.id, data);
      this.reloadGuests();
    } catch (e) {
      throw e;
    }
  }

  async cloneContacts (state) {
    const { actions, organization, event } = this.props;
    const { cloningContactsParams } = this.state;
    const { target_type, target_id } = state;

    const data = {
      source_type: 'event_guest',
      source_id: event.id,
      target_type,
      ...cloningContactsParams,
    };
    if (target_id) data.target_id = target_id;
    try {
      return actions.cloneContacts(organization.id, data);
    } catch (e) {
      throw e;
    }
  }

  async updateGuest (data) {
    const { actions, event } = this.props;
    const { editingGuest } = this.state;

    try {
      await actions.updateEventGuest(event.id, editingGuest.id, data);
      await this.reloadGuests();
      this.setEditingGuest({});
    } catch (e) {
      throw e;
    }
  }

  async resendPaymentLink (guest) {
    const { actions, event } = this.props;

    try {
      await actions.resendEventPaymentEmail(event.id, guest.id);
      await this.reloadGuests();
    } catch (e) {
      console.error(e);
    }
  }

  render () {
    const {
      actions,
      organization,
      events,
      event,
      guests,
      tickets,
      templates,
      dataFields,
      customFields,
      meta,
      tags,
      sessions,
    } = this.props;
    const {
      limit,
      sortBy,
      sortOrder,
      sortType,
      search,
      filters,
      isAddingGuest,
      editingGuest,
      templateEmailParams,
      cloningContactsParams,
    } = this.state;

    let tableColumns = [
      {
        title: t('Name'),
        field: 'name',
        compulsory: true,
        unscrollable: true,
        flex: '1.5 0 0',
        format: (d, r) => (
          <BasicContactInfo
            type='event_guest'
            organization={ organization }
            contact={ r }
          />
        ),
        tooltipFormat: d => d,
      },
      {
        title: t('Ref. ID'),
        field: 'id',
        format: v => `#G${ v }`,
      },
    ];

    if (event.requires_approval) {
      const approvalStatusOptions = [
        {
          label: (
            <LightBallWrapper>
              <LightBall color='#fcca23' />
Pending
            </LightBallWrapper>
          ),
          value: null,
        },
        {
          label: (
            <LightBallWrapper>
              <LightBall color='#32a844' />
Approved
            </LightBallWrapper>
          ),
          value: true,
        },
        {
          label: (
            <LightBallWrapper>
              <LightBall color='#e84363' />
Rejected
            </LightBallWrapper>
          ),
          value: false,
        },
      ];

      tableColumns = tableColumns.concat([
        {
          title: t('Approval Status'),
          field: 'approved',
          wrapText: true,
          format: (v, r, i) => {
            if (v === true) {
              return (
                <LightBallWrapper>
                  <LightBall color='#32a844' />
Approved
                </LightBallWrapper>
              );
            } if (v === false) {
              return (
                <LightBallWrapper>
                  <LightBall color='#e84363' />
Rejected
                </LightBallWrapper>
              );
            }

            return (
              <Select
                fullWidth
                searchable={ false }
                value={ r.approved }
                customCss='
                  margin-top: 0px;
                  margin-bottom: 0px;
                '
                menuFixed={ guests.length <= 2 }
                menuOnTop={ guests.length > 2 && i >= guests.length - 2 }
                options={ approvalStatusOptions }
                onChange={ async (approved) => {
                  try {
                    await actions.updateEventGuest(event.id, r.id, {
                      approved,
                    });
                    await this.reloadGuests();
                    this.setEditingGuest({});
                  } catch (e) {
                    return Promise.reject(e);
                  }
                } }
              />
            );
          },
          noTooltip: true,
          bodyCss:
            'width: 100%;text-overflow: unset;overflow: unset;white-space: normal;',
        },
        {
          title: t('Payment Status'),
          field: 'payment_status',
          wrapText: true,
          format: (v, r) => {
            let totalPrice = r.ticket_price;
            if (r.childs) r.childs.forEach(c => (totalPrice += c.ticket_price));

            if (totalPrice === 0) {
              return (
                <div>
                  <GrayIcon className='fas fa-minus' />
                  { ' ' }
Free
                </div>
              );
            } if (r.charged) {
              return (
                <div>
                  <CheckIcon className='fas fa-check' />
                  { ' ' }
Paid
                </div>
              );
            } if (r.approved) {
              return (
                <div>
                  <p>
                    <GrayIcon className='far fa-clock' />
                    { t('Awaiting payment') }
                  </p>
                  <ResendButton
                    plain
                    onClick={ this.resendPaymentLink.bind(this, r) }
                  >
                    { t('Resend link') }
                  </ResendButton>
                </div>
              );
            }
            return null;
          },
        },
      ]);
    }

    tableColumns = tableColumns.concat([
      {
        title: t('Tags'),
        field: 'tags',
        unsortable: true,
        noTooltip: true,
        customCss: 'overflow: visible;line-height: normal;',
        bodyCss: 'overflow: visible;',
        format: (d, r) => {
          const contactTags = tags.filter(
            tag => r.tags && r.tags.indexOf(tag.id) > -1,
          );

          return contactTags.length
            ? contactTags.map(tag => <Tag value={ tag.tag } color={ tag.color } />)
            : '-';
        },
      },
      {
        title: t('Email'),
        field: 'email',
      },
      {
        title: t('Phone'),
        field: 'phone',
      },
      {
        title: t('Checked In At'),
        field: 'checked_in_at',
        format: d => (d
          ? moment
            .utc(d)
            .local()
            .format('YYYY MMM DD HH:mm')
          : t('Not Yet')),
      },
      {
        title: t('Ticket Re-delivered At'),
        field: 'ticket_delivered_at',
        format: d => (d
          ? moment
            .utc(d)
            .local()
            .format('YYYY MMM DD HH:mm')
          : '-'),
      },
      {
        title: t('Date'),
        field: 'date_start_at',
        format: (value, row) => {
          if (!value) return '-';
          let dateText = moment
            .utc(value)
            .local()
            .format('YYYY MMM DD HH:mm');

          if (row.date_title) dateText += ` (${ row.date_title })`;
          return dateText;
        },
      },
    ]);

    if (tickets.length) {
      tableColumns = tableColumns.concat([
        {
          title: t('Ticket Type'),
          field: 'ticket_name',
        },
        {
          title: t('Category'),
          field: 'category_name',
        },
        {
          title: t('Purchased At'),
          field: 'purchased_at',
          format: d => (d
            ? moment
              .utc(d)
              .local()
              .format('YYYY MMM DD HH:mm')
            : '-'),
        },
        {
          title: t('Purchased By'),
          field: 'purchased_by',
        },
      ]);
    }

    tableColumns = tableColumns.concat(
      dataFields.filter(f => !!f.data_type).map(f => ({
        title: f.label,
        field: f.id,
        dataField: true,
        format: (d, r) => {
          if (f.data_type === 'file') {
            const file = r.files && r.files[f.id];
            if (!file) return '-';

            const { file_url, file_name } = file;
            return (
              <FileLink target='_blank' href={ file_url }>
                { file_name }
              </FileLink>
            );
          }
          const value = r.data_fields ? r.data_fields[f.id] : null;
          if (!value) return '-';

          return Array.isArray(value) ? value.join(', ') : value;
        },
      })),
    );

    tableColumns = tableColumns.concat(
      customFields.map(f => ({
        title: f.name,
        field: f.id,
        customField: true,
        format: (d, g) => {
          const value = g.custom_fields ? g.custom_fields[f.id] : null;
          if (!value) return '-';
          return value;
        },
      })),
    );

    const ticketTypeFilterOptions = [
      {
        label: t('No Ticket Type'),
        value: null,
      },
    ].concat(
      tickets.map(ti => ({
        label: ti.name,
        value: ti.id,
      })),
    );

    const sessionFilterOptions = [
      {
        label: t('No Session'),
        value: null,
      },
    ].concat(
      sessions.map(s => ({
        label: s.title || formatDateTime(s.start_at),
        value: s.id,
      })),
    );
    const tagFilterOptions = [
      {
        label: t('No Tag'),
        value: null,
      },
    ].concat(
      tags.map(tag => ({
        label: tag.tag,
        value: tag.id,
      })),
    );

    const tableFilterFields = [
      {
        type: 'input',
        label: t('Name'),
        field: 'name',
      },
      {
        type: 'input',
        label: t('Email'),
        field: 'email',
      },
      {
        type: 'multiSelect',
        label: t('Ticket Type'),
        field: 'ticket_type_id',
        options: ticketTypeFilterOptions,
      },
      {
        type: 'multiSelect',
        label: t('Date Session'),
        field: 'session_id',
        options: sessionFilterOptions,
      },
      {
        type: 'multiSelect',
        label: t('Tags'),
        field: 'tag_id',
        options: tagFilterOptions,
      },
    ];

    const rowActions = [
      {
        label: t('PDF'),
        icon: ticketWhiteIcon,
        onClick: (row) => {
          const pdfUrl = `${ API_URL }/checkout/tickets/pdf/${ row.token }`;
          window.open(pdfUrl, '_blank');
        },
      },
      {
        label: t('Edit'),
        icon: eventWhiteIcon,
        onClick: row => this.setEditingGuest(row),
      },
      {
        label: t('Remove'),
        icon: deleteIcon,
        onClick: row => this.setRemovingGuest(row),
      },
    ];

    const tableBulkActions = [
      {
        function: this.sendEventTicketEmails.bind(this),
        name: t('Resend Tickets'),
        icon: resendTicketIcon,
        condition: state => stateHasSelectedRows(state),
        confirmMessage: `${ t(
          'Are you sure you want to re-send the Tickets to the selected Guests from',
        ) } ${ event.name }?`,
        successMessage: `${ t(
          'You have successfully re-sent the Tickets to the selected Guests from',
        ) } ${ event.name }.`,
        errorTitle: `${ t(
          'Oops! There is an error when re-sending the Tickets to the selected Guests from',
        ) } ${ event.name }.`,
      },
      {
        function: this.setTemplateEmailParams.bind(this),
        name: t('Send Template Email'),
        icon: emailIcon,
        condition: state => stateHasSelectedRows(state),
      },
      {
        function: this.checkInGuests.bind(this),
        name: t('Check In Guests'),
        icon: checkInIcon,
        condition: state => stateHasSelectedRows(state),
        confirmMessage: `${ t(
          'Are you sure you want to check in the selected Guests for',
        ) } ${ event.name }?`,
        successMessage: `${ t(
          'You have successfully checked in the selected Guests for',
        ) } ${ event.name }.`,
        errorTitle: `${ t(
          'Oops! There is an error when checking in the selected Guests for',
        ) } ${ event.name }.`,
      },
      {
        function: this.revertCheckInGuests.bind(this),
        name: t('Undo Checking-in Guests'),
        icon: uncheckInIcon,
        condition: state => stateHasSelectedRows(state),
        confirmMessage: `${ t(
          'Are you sure you want to revert the check-in status of the selected Guests for',
        ) } ${ event.name }?`,
        successMessage: `${ t(
          'You have successfully reverted the check-in status of the selected Guests for',
        ) } ${ event.name }.`,
        errorTitle: `${ t(
          'Oops! There is an error when reverting the check-in status of the selected Guests for',
        ) } ${ event.name }.`,
      },
      {
        function: this.setCloningContactsParams.bind(this),
        name: t('Clone Contacts'),
        icon: copyContactIcon,
        condition: state => stateHasSelectedRows(state),
      },
      {
        render: (params) => {
          const actionParams = Object.assign(params, {
            contact_type: 'event_guest',
            event_id: event.id,
          });

          return (
            <EditTag
              tags={ tags }
              reloadData={ this.reloadGuests.bind(this) }
              actionParams={ actionParams }
            />
          );
        },
        condition: state => stateHasSelectedRows(state),
      },
      {
        function: this.removeGuests.bind(this),
        name: t('Remove Guest'),
        icon: trashIcon,
        condition: state => stateHasSelectedRows(state),
        confirmMessage: `${ t(
          'Are you sure you want to remove the selected Guests from',
        ) } ${ event.name }?`,
        successMessage: `${ t(
          'You have successfully removed the selected Guests from',
        ) } ${ event.name }.`,
        errorTitle: `${ t(
          'Oops! there is an error when removing the selected Guests from',
        ) } ${ event.name }.`,
      },
      {
        function: this.exportGuest.bind(this),
        icon: downloadIcon,
        name: t('Export Guest'),
        condition: state => !stateHasSelectedRows(state),
      },
    ];

    // if (event.premium) {
    //   tableBulkActions.splice(2, 0, {
    //     title: t('Re-send Guest App Links'),
    //     function: this.sendGuestLoginEmails.bind(this),
    //     condition: state => stateHasSelectedRows(state),
    //     confirmMessage: `${ t(
    //       'Are you sure you want to re-send the Check-in Emails to the selected Guests from'
    //     ) } ${ event.name }?`,
    //     successMessage: `${ t(
    //       'You have successfully re-sent the Check-in Emails to the selected Guests from'
    //     ) } ${ event.name }.`,
    //     errorTitle: `${ t(
    //       'Oops! There is an error when re-sending the Check-in Emails to the selected Guests from'
    //     ) } ${ event.name }.`
    //   });
    // }

    const tableAddButtonActions = [
      {
        title: t('Add Individual Guest'),
        icon: addContactIcon,
        function: this.toggleAddingGuest.bind(this),
      },
      {
        title: t('Bulk Import Guests'),
        icon: addContactIcon,
        function: this.routeToBulkImport,
      },
    ];

    const ticketOptions = tickets.map(ti => ({
      label: ti.name,
      value: ti.id,
    }));

    const sessionOptions = sessions.map((d) => {
      let dateText = moment
        .utc(d.start_at)
        .local()
        .format('YYYY MMM DD HH:mm');

      if (d.title) dateText += ` (${ d.title })`;

      return {
        label: dateText,
        value: d.id,
      };
    });

    const sendTemplateEmailsFields = [
      {
        type: 'select',
        field: 'template_id',
        label: `${ t('Select an Email Template you want to send') }:`,
        options: templates.map(tem => ({
          label: tem.title,
          value: tem.id,
        })),
        flex: '100% 0 0',
        required: true,
      },
    ];

    const addGuestFields = [
      {
        type: 'input',
        field: 'first_name',
        label: t('First Name'),
        flex: '0 0 50%',
        required: true,
      },
      {
        type: 'input',
        field: 'last_name',
        label: t('Last Name'),
        flex: '0 0 50%',
      },
      {
        type: 'input',
        field: 'email',
        label: t('Email'),
        flex: '0 0 50%',
        getErrorMessage: (formState, value, label) => {
          if (!value || !value.length || validateEmail(value)) {
            return null;
          }
          return `Please enter a valid ${ label }`;
        },
      },
      {
        type: 'input',
        field: 'phone',
        label: t('Phone'),
        flex: '0 0 50%',
      },
      {
        type: 'select',
        field: 'event_ticket_id',
        label: t('Ticket Type'),
        options: ticketOptions,
        flex: '0 0 50%',
        required: true,
      },
      {
        type: 'select',
        field: 'session_id',
        label: t('Date'),
        options: sessionOptions,
        flex: '0 0 50%',
        required: true,
      },
    ];

    const copyContactsFields = [
      {
        type: 'select',
        field: 'target_type',
        label: t('Clone Target Type'),
        options: [
          { label: t('Membership'), value: 'organization_member' },
          { label: t('Event (as Guests)'), value: 'event_guest' },
        ],
        searchable: false,
        required: true,
      },
      {
        type: 'select',
        field: 'target_id',
        label: t('Select the target event to clone selected contacts'),
        options: events.filter(e => e.id !== event.id).map(e => ({
          label: e.name,
          value: e.id,
        })),
        condition: state => state.target_type === 'event_guest',
        flex: '100% 0 0',
        required: true,
      },
    ];

    const editingGuestFields = [
      {
        type: 'input',
        field: 'first_name',
        label: t('First Name'),
        flex: '0 0 50%',
        default: editingGuest.first_name,
        required: true,
      },
      {
        type: 'input',
        field: 'last_name',
        label: t('Last Name'),
        flex: '0 0 50%',
        default: editingGuest.last_name,
      },
      {
        type: 'input',
        field: 'email',
        label: t('Email'),
        flex: '0 0 50%',
        default: editingGuest.email,
        getErrorMessage: (formState, value, label) => {
          if (!value || !value.length || validateEmail(value)) {
            return null;
          }
          return `Please enter a valid ${ label }`;
        },
      },
      {
        type: 'input',
        field: 'phone',
        label: t('Phone'),
        flex: '0 0 50%',
        default: editingGuest.phone,
      },
      {
        type: 'select',
        field: 'event_ticket_id',
        label: t('Ticket Type'),
        options: tickets.map(ti => ({
          label: ti.name,
          value: ti.id,
        })),
        flex: '0 0 50%',
        default: editingGuest.event_ticket_id,
        required: true,
      },
      {
        type: 'select',
        field: 'session_id',
        label: t('Session'),
        options: sessions.map((d) => {
          let dateText = moment
            .utc(d.start_at)
            .local()
            .format('YYYY MMM DD HH:mm');

          if (d.title) dateText += ` (${ d.title })`;

          return {
            label: dateText,
            value: d.id,
          };
        }),
        flex: '0 0 50%',
        default: editingGuest.session_id,
        required: true,
      },
    ];
    return (
      <ManagerContainer>
        <Table
          id='guests'
          title={ t('Guest list') }
          subtitle={ `${ t('Total guest number') }: ${ meta.count }` }
          columns={ tableColumns }
          search={ search }
          filters={ filters }
          filterFields={ tableFilterFields }
          onSetFilters={ this.setGuestFilters.bind(this) }
          rows={ guests }
          meta={ meta }
          perPage={ limit }
          sortBy={ sortBy }
          sortOrder={ sortOrder }
          sortType={ sortType }
          onReload={ this.reloadGuests.bind(this) }
          onSort={ this.sortGuests.bind(this) }
          onChangeFrom={ this.getGuestsFrom.bind(this) }
          onChangePerPage={ this.changeGuestsPerPage.bind(this) }
          onChangeSearchValue={ this.changeSearchValue.bind(this) }
          rowActions={ rowActions }
          bulkActions={ tableBulkActions }
          singleItemText={ t('guest') }
          pluralItemText={ t('guests') }
          addButtonActions={ tableAddButtonActions }
          emptyText={ t('There are no guests yet.') }
          // HACK for adding tag dropdown visibility
          bulkActionsCss='
            overflow-x: visible;
            overflow-y: visible;
          '
          searchHideCondition={ state => !stateHasSelectedRows(state) }
        />
        <SettingModal
          medium
          isOpen={ isAddingGuest }
          title={ t('Add Guest') }
          fields={ addGuestFields }
          action={ state => actions.addEventGuest(event.id, state) }
          onFinish={ this.reloadGuests.bind(this) }
          onCancel={ this.toggleAddingGuest.bind(this) }
        />
        <SettingModal
          medium
          isOpen={ !!editingGuest.id }
          title={ `${ t('Edit Guest') } - ${ editingGuest.name }` }
          fields={ editingGuestFields }
          action={ this.updateGuest.bind(this) }
          onFinish={ this.setEditingGuest.bind(this, {}) }
          onCancel={ this.setEditingGuest.bind(this, {}) }
        />
        { templateEmailParams ? (
          <SettingModal
            title={ t('Send Template Emails') }
            isOpen={ templateEmailParams }
            fields={ sendTemplateEmailsFields }
            action={ this.sendTemplateEmails.bind(this) }
            onCancel={ this.setTemplateEmailParams.bind(this, null) }
          />
        ) : null }
        { cloningContactsParams ? (
          <SettingModal
            title={ t('Clone Contacts') }
            isOpen={ cloningContactsParams }
            fields={ copyContactsFields }
            action={ this.cloneContacts.bind(this) }
            onCancel={ this.setCloningContactsParams.bind(this, null) }
          />
        ) : null }
      </ManagerContainer>
    );
  }
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(EventGuestsView));
