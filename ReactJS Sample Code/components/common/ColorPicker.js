import React, { Component } from 'react';
import styled from 'styled-components';
import { PhotoshopPicker } from 'react-color';
import { EditableInput } from 'react-color/lib/components/common';
import Color from 'color';

import Label from './inputs/Label';
import theme from '../../utils/theme';

import i18n from '../../../i18n';

const t = a => i18n.t([`components/common:${ a }`]);

const DEFAULT_COLOR_OPTIONS = [
  '#e1e8ed',
  '#bcd4e0',
  '#0085cc',
  '#00a7ff',
  '#2f2f2f',
  '#e84363',
  '#f6acac',
  '#ff9e58',
  '#fcca23',
  '#32a844',
];

const DEFAULT_COLOR = '#00a7ff';

const Wrapper = styled.div`
  ${ props => props.customCss || '' };
`;

const CustomWrapper = styled.div`
  position: relative;
  display: flex;
  flex-direction: column;
  align-items: normal;
`;

const PhotoshopColorPicker = styled(PhotoshopPicker)`
  background: transparent !important;
  box-shadow: none !important;
  width: 100% !important;

  & > div:first-child {
    display: none;
  }

  & .flexbox-fix {
    > div:last-child {
      display: none;
      width: 0 !important;
    }
  }
`;

const ColorCircle = styled.div`
  display: inline-block;
  border-radius: 50%;
  cursor: pointer;
  background: ${ ({ color }) => color };

  ${ props => (props.selected
    ? `
    width: 26px;
    height: 26px;
    margin: 3px 4px 0 0;
    border: 3px solid ${ props.theme.colors.lightBlue };
  `
    : `
    width: 22px;
    height: 22px;
    margin: 5px 6px 2px 2px;
    border: 1px solid ${ props.theme.colors.lightBlue };
  `) };
`;

const StandardColorsWrapper = styled.div`
  padding-bottom: 10px;
`;

const CustomColorModal = styled.div`
  background: #ffffff;
  box-shadow: 0 0 6px 0 ${ theme.colors.lightGray };
  border-radius: 5px;
  position: absolute;
  z-index: 2;
  ${ props => (props.modal ? 'bottom: -140px;' : '') };
`;

const CustomColorInnerWrapper = styled.div`
  z-index: 2;
  position: relative;
`;

const Cover = styled.div`
  position: fixed;
  top: 0px;
  right: 0px;
  bottom: 0px;
  left: 0px;
`;

const BottomWrapper = styled.div`
  border-top: 1px solid ${ props => props.theme.colors.lightGray };
  padding: 10px;
  display: flex;
  cursor: pointer;
`;

const P = styled.p`
  margin: auto 10px;
`;

const inputStyles = {
  wrap: {
    padding: '10px',
  },
  input: {
    width: '100%',
    padding: '10px',
    borderRadius: '5px',
    border: `1px solid ${ theme.colors.lightBlue }`,
    outline: 'none',
  },
  label: {
    display: 'none',
  },
};

class CustomColorPicker extends Component {
  state = {
    displayColorPicker: false,
  };

  async onChangeInput (value) {
    try {
      const { onChange } = this.props;
      const colorValue = value ? Color(value).hex() : value;
      if (onChange) await onChange(colorValue);
    } catch (e) {
      // don't do anything if it's invalid
    }
  }

  togglePickingColor () {
    const { displayColorPicker } = this.state;
    this.setState({
      displayColorPicker: !displayColorPicker,
    });
  }

  render () {
    const {
      onChange, selected, modal, value = DEFAULT_COLOR,
    } = this.props;
    const { displayColorPicker } = this.state;

    return (
      <CustomWrapper modal={ modal }>
        <Label label={ t('Or select a custom color') } />
        <div>
          <ColorCircle
            color={ value }
            selected={ selected }
            onClick={ this.togglePickingColor.bind(this) }
          />
        </div>
        { displayColorPicker ? (
          <CustomColorModal modal={ modal }>
            <Cover onClick={ this.togglePickingColor.bind(this) } />
            <CustomColorInnerWrapper>
              <PhotoshopColorPicker
                color={ value }
                onChange={ v => onChange(v.hex) }
              />
              <EditableInput
                style={ inputStyles }
                value={ value }
                onChange={ this.onChangeInput.bind(this) }
              />
              <BottomWrapper onClick={ this.togglePickingColor.bind(this) }>
                <ColorCircle color={ value } />
                <P>Select a custom color</P>
              </BottomWrapper>
            </CustomColorInnerWrapper>
          </CustomColorModal>
        ) : null }
      </CustomWrapper>
    );
  }
}

// eslint-disable-next-line
class ColorPickerV2 extends Component {
  render () {
    const {
      value,
      label = t('Pick one of the standard colors'),
      colors = DEFAULT_COLOR_OPTIONS,
      onChange = () => {},
      modal,
      ...props
    } = this.props;
    const isCustomColor = value
      ? !colors.find(
        c => !!c && Color(c).hex() === Color(value).hex(),
      )
      : false;

    return (
      <Wrapper { ...props }>
        <StandardColorsWrapper>
          <Label label={ label } />
          { colors.map(c => (
            <ColorCircle
              color={ c }
              selected={ !!value && !!c && Color(c).hex() === Color(value).hex() }
              onClick={ onChange.bind(this, c) }
            />
          )) }
        </StandardColorsWrapper>
        <CustomColorPicker
          modal={ modal }
          value={ value }
          selected={ isCustomColor }
          onChange={ v => onChange(v) }
        />
      </Wrapper>
    );
  }
}

export default ColorPickerV2;
