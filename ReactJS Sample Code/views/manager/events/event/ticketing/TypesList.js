import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { withRouter } from 'react-router';

import * as UiActionCreators from '../../../../../redux/ui';
import * as ManagerActionCreators from '../../../../../redux/manager';
import * as EventActionCreators from '../../../../../redux/event';

import ItemTable from '../../../../../components/common/ItemTableV2';

import { t as translate } from '../../../../../../i18n';

const t = a => translate(a, 'manager/events/ticketing/types/index');

const mapStateToProps = state => ({
  events: state.manager.events,
  event: state.manager.event,
  organization: state.manager.organization,
  tickets: state.event.tickets,
  typeSessions: state.event.ticketTypeSessions,
});

const ActionCreators = Object.assign(
  {},
  UiActionCreators,
  ManagerActionCreators,
  EventActionCreators,
);
const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators(ActionCreators, dispatch),
});

class EventTicketingTypesListView extends Component {
  async componentDidMount () {
    const { actions, match } = this.props;
    const { eId } = match.params;

    actions.setLoading(true);

    try {
      const promises = [
        actions.getEventTickets(eId),
        actions.getEventTicketTypeSessions(eId),
      ];
      await Promise.all(promises);
    } catch (e) {
      console.error('ERROR: EventTicketingTypesListView', e);
    } finally {
      actions.setLoading(false);
    }
  }

  componentWillUnmount () {
    const { actions } = this.props;
    actions.resetSaveActions();
  }

  render () {
    const { event, organization, tickets } = this.props;

    const tableColumns = [
      {
        field: 'name',
        title: t('Ticket Name'),
        flex: '2 0 0',
        customCss: 'font-weight: 700',
      },
      {
        field: 'price',
        title: t('Price'),
        format: v => (v ? v.toLocaleString() : 'Free'),
      },
      {
        field: 'quantity',
        title: t('Qty'),
        format: v => (v !== null ? v : 'Unlimited'),
      },
      {
        field: 'quantity_sold',
        title: t('Sold'),
      },
    ];
    const tableEditUrl = r => `/${ organization.id }/events/${ event.id }/ticketing/types/${ r.id }`;
    const tableNewItemUrl = `/${ organization.id }/events/${
      event.id
    }/ticketing/types/new`;

    return (
      <ItemTable
        columns={ tableColumns }
        rows={ tickets }
        editUrl={ tableEditUrl }
        itemText={ t('ticket type') }
        newItemUrl={ tableNewItemUrl }
        searchField='name'
      />
    );
  }
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(EventTicketingTypesListView));
