import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import styled from 'styled-components';

import Button from './Button';

import * as UiActionCreators from '../../redux/ui';

const DropdownOverlay = styled.div`
  position: fixed;
  width: 100%;
  height: 100vh;
  left: 0;
  top: 0;
  background-color: rgba(0, 0, 0, 0.5);

  @media (min-width: 901px) {
    display: none;
  }
`;

const DropdownDiv = styled.div`
  position: absolute;
  outline: none;
  z-index: 999;
  ${ ({ alignRight }) => (alignRight ? 'right: 0;' : 'left: 0;') };
  top: 60px;
  border-radius: 2px;
  box-shadow: 0 3px 6px rgba(0, 0, 0, 0.16);
  background-color: white;
  & .dropdown-content {
    & a {
      text-decoration: none;
    }
  }

  @media (max-width: 900px) {
    z-index: 999;
    position: fixed;
    top: 0;
    ${ ({ contentHeight }) => (contentHeight ? null : 'bottom: 0;') };
    ${ ({ contentWidth }) => (!contentWidth ? 'left: 50px; width: calc(100% - 50px);' : null) };
    background-color: white;
    padding-top: 60px;
  }

  @media (min-width: 901px) {
    width: ${ ({ contentWidth }) => (!contentWidth ? '280px;' : 'auto') };
    max-height: 710px;
    overflow-y: scroll;
  }

  ${ props => props.customCss || '' };
`;

const Cancel = styled(Button)`
  position: absolute;
  z-index: 101;
  right: 18px;
  top: 18px;
  width: 25px;
  height: 25px;
  background-color: transparent;
  font-size: 24px !important;
  line-height: 25px !important;
  font-weight: 600;
  padding: 0;
  color: black;

  &:active,
  &:hover {
    background-color: transparent;
  }

  &:after {
    content: '✕';
  }

  @media (min-width: 901px) {
    display: none;
  }
`;

const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators(UiActionCreators, dispatch),
});

class Dropdown extends Component {
  componentDidMount () {
    const { actions } = this.props;
    actions.setScrollable(false);
  }

  componentWillUnmount () {
    const { actions } = this.props;
    actions.setScrollable(true);
  }

  render () {
    const {
      children, onCancel, refHandler, ...props
    } = this.props;

    return [
      <DropdownOverlay />,
      <DropdownDiv
        innerRef={ dropdown => (refHandler ? refHandler(dropdown) : null) }
        { ...props }
      >
        <Cancel plain onClick={ onCancel } />
        <div className='dropdown-content'>{ children }</div>
      </DropdownDiv>,
    ];
  }
}

export default connect(null, mapDispatchToProps)(Dropdown);
