import React from 'react';
import styled from 'styled-components';
import _ from 'lodash';

import Card from './cards/Card';

const INITIAL_SIZE = 12;

const CardWrapper = styled.div`
  display: flex;
  padding: 10px 30px 10px 0;
`;

const Avatar = styled.img`
  height: 36px;
  width: 36px;
  margin-right: 12px;
  border-radius: 50%;
`;

const TextWrapper = styled.div`
  line-height: 25px;
  text-transform: capitalize;
`;

const FullNameText = styled.p`
  font-weight: 700;
  font-size: 12px;
`;

const TitleText = styled.p`
  font-size: 12px;
  font-weight: 300;
  text-transform: capitalize;
`;

const StatusText = styled.p`
  font-size: 12px;
  color: ${ props => (props.isDeclined
    ? props.theme.colors.mainRed
    : props.theme.colors.mainBlue) };
  font-weight: bold;
`;

const FooterContainer = styled.div`
  display: flex;
  justify-content: center;
  padding-bottom: 30px;
`;

const PeopListWrapper = styled.div`
  display: flex;
  flex-wrap: wrap;
  padding: 20px;
`;

const FooterText = styled.p`
  font-weight: bold;
  cursor: pointer;
  border-bottom: 3px solid ${ props => props.theme.colors.mainBlue };
`;

const TabsWrapper = styled.ul`
  display: flex;
  flex: 1;
  justify-content: space-around;
  padding: 0 10px;
`;

const TabItem = styled.li`
  list-style-type: none;
  border-bottom: 3px solid
    ${ props => (props.isSelected ? props.theme.colors.mainBlue : 'transparent') };
  transition: 0.2s all;
  cursor: pointer;
  font-size: 12px;
  line-height: 45px;
`;

const FilterHeader = ({ onSelect, people, selectedOption }) => {
  const status = _.uniq(people.map(p => p.status));

  return (
    <TabsWrapper>
      <TabItem
        isSelected={ selectedOption === 'ALL' }
        key='ALL'
        onClick={ () => onSelect('ALL') }
      >
        ALL GUESTS (
        { people.length }
)
      </TabItem>
      { status.map(s => (s === undefined ? null : (
        <TabItem
          isSelected={ selectedOption === s }
          key={ s }
          onClick={ () => onSelect(s) }
        >
          { `${ s } (${ people.filter(p => p.status === s).length })` }
        </TabItem>
      ))) }
    </TabsWrapper>
  );
};

const PersonCard = ({
  person: {
    avatar, title, fullName, status,
  },
} = {}) => (
  <CardWrapper>
    <Avatar src={ avatar } />
    <TextWrapper>
      <FullNameText>{ fullName }</FullNameText>
      <TitleText>{ title }</TitleText>
      <StatusText isDeclined={ status === 'DECLINED' }>{ status }</StatusText>
    </TextWrapper>
  </CardWrapper>
);

class TabbedPeopleList extends React.Component {
  state = {
    showAll: false,
    selectedOption: 'ALL',
  };

  toggleShowAll = () => this.setState(({ showAll }) => ({
    showAll: !showAll,
  }));

  onSelect = selectedOption => this.setState({ selectedOption });

  render () {
    const { showAll, selectedOption } = this.state;
    const {
      title, people, customCss,
    } = this.props;

    const filteredPeople = selectedOption === 'ALL'
      ? people
      : people.filter(({ status }) => status === selectedOption);
    const renderedPeople = showAll
      ? filteredPeople
      : filteredPeople.slice(0, INITIAL_SIZE);

    return (
      <Card
        title={ title }
        headerComponent={ (
          <FilterHeader
            onSelect={ this.onSelect }
            people={ people }
            selectedOption={ selectedOption }
          />
) }
        customCss={ { padding: 0, width: '65%', ...customCss } }
        headerCss={ { padding: 0 } }
      >
        <React.Fragment>
          <PeopListWrapper>
            { renderedPeople.map((person, i) => (
              <PersonCard key={ `${ person.id }-${ i }` } person={ person } />
            )) }
          </PeopListWrapper>
          { !showAll
            && filteredPeople.length > INITIAL_SIZE && (
            <FooterContainer>
              <FooterText onClick={ this.toggleShowAll }>SHOW ALL</FooterText>
            </FooterContainer>
          ) }
        </React.Fragment>
      </Card>
    );
  }
}

export default TabbedPeopleList;
