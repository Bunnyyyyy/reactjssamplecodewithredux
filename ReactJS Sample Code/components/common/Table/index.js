import React, { Component, Fragment } from 'react';
import { connect } from 'react-redux';
import styled from 'styled-components';

import BulkActions from './BulkActions';
import Search from './Search';
import FilterSelector from './FilterSelector';
import FilterStatus from './FilterStatus';
import Button from '../Button';
import SelectionIndicator from './SelectionIndicator';
import AddButton from './AddButton';
import HeaderColumn from './HeaderColumn';
import HeaderConfig from './HeaderConfig';
import RowColumn from './RowColumn';
import Pagination from './Pagination';
import SortPerPage from './SortPerPage';
import Checkbox from '../Checkbox';

import { boxShadow } from '../../../utils/styles';
import i18n from '../../../../i18n';

const t = a => i18n.t([`components/common:${ a }`]);

const FLEX_GROW_MIN_WIDTH = 150;
const CHECKBOX_COLUMN_WIDTH = 55;

const TableWrapper = styled.div`
  padding-bottom: 40px;
  border-radius: 5px;
  overflow: hidden;
  min-height: 100%;
`;

const ActionsDiv = styled.div`
  display: flex;
  align-items: center;
  height: 48px;
  background-color: white;
  box-shadow: 0 0 6px 0 rgba(0, 0, 0, 0.08);
`;

const SelectAll = styled.div`
  flex: 0 0 ${ CHECKBOX_COLUMN_WIDTH }px;
  display: flex;
  align-items: center;
  justify-content: center;
  height: 48px;
`;

const HeaderDiv = styled.div`
  position: relative;
  display: flex;
  width: 100%;
  min-width: ${ props => props.minWidth }px;

  height: 48px;
  background-color: white;
  border-top: 1px solid ${ props => props.theme.colors.lightGray };
  border-bottom: 1px solid ${ props => props.theme.colors.lightGray };
  display: flex;
  align-items: center;
`;

const SearchFilterWrapper = styled.div`
  display: flex;
  height: 100%;
  flex: 0 1 calc(50% - 115px);
  align-items: center;
  padding: 0 10px;
`;

const RowDiv = styled.div`
  display: flex;
  align-items: center;
  min-width: ${ props => props.minWidth }px;
  height: 64px;

  background-color: white;
  &:nth-child(2n) {
    background-color: ${ props => props.theme.colors.bgGray };
  }
`;

const FixedContent = styled.div`
  z-index: 101;
  ${ boxShadow('6px 0 5px 0 rgba(0, 0, 0, 0.06)') };
`;

const ScrollableContent = styled.div`
  position: relative;
  width: 100%;
  z-index: 100;
  overflow-x: scroll;
  overflow-y: hidden;
`;

const ContentWrapper = styled.div`
  display: flex;
`;

const CheckboxesColumn = styled.div`
  flex: 0 0 ${ CHECKBOX_COLUMN_WIDTH }px;
  z-index: 101;
  ${ props => (props.hasShadow ? boxShadow('6px 0 5px 0 rgba(0, 0, 0, 0.06)') : '') };
`;

const CheckboxesColumnPlaceholder = styled.div`
  height: 48px;
  background-color: white;
  border-top: 1px solid ${ props => props.theme.colors.lightGray };
  border-bottom: 1px solid ${ props => props.theme.colors.lightGray };
`;

const RowCheckboxWrapper = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  height: 64px;

  background-color: white;
  &:nth-child(2n) {
    background-color: ${ props => props.theme.colors.bgGray };
  }
`;

const ShowMoreButton = Button.extend`
  position: relative;
  width: 60px;
  height: 48px;
  padding: 12px;
  margin: 8px 0;
  text-align: center;

  i {
    font-size: 24px;
    line-height: 24px;
    margin-right: 0;
    color: ${ props => props.theme.colors.mainGray };
  }

  &:hover,
  &:active {
    background: none;

    i {
      color: ${ props => props.theme.colors.mainBlue };
    }
  }
`;

const MoreActionsContainer = styled.div`
  position: absolute;
  overflow: hidden;
  display:flex;
  flex-direction: row;
  justify-content: space-around;
  align-items: center;
  right: 0;
  top: 0;
  height: 64px;
  padding: 0 10px;
  background-color: ${ props => props.theme.colors.darkBlue };
  visibility: ${ props => (props.hidden ? 'hidden' : 'visible') };
`;

const HeaderConfigWrapper = styled.div`
  height: 48px;
  background-color: white;
  border-top: 1px solid ${ props => props.theme.colors.lightGray };
  border-bottom: 1px solid ${ props => props.theme.colors.lightGray };
`;

const RightActionsColumn = styled.div`
  flex-basis: 80px;
  flex-shrink: 0;
  z-index: 101;
  ${ boxShadow('-6px 0 5px 0 rgba(0, 0, 0, 0.06)') };
`;

const IndividualActionsWrapper = styled.div`
  position: relative;
  height: 64px;
  text-align: center;
  overflow: visible;

  background-color: white;
  &:nth-child(2n) {
    background-color: ${ props => props.theme.colors.bgGray };
  }
`;

const MoreActionButton = styled.div`
  cursor: pointer;
  width: 70px;
  height: 100%;
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  color: white;
`;

const MoreActionIcon = styled.img`
  width: 22px;
  height: 22px;
`;

const MoreActionLabel = styled.h6`
  color: white;
  letter-spacing: 1px;
  margin-top: 4px;
`;

const BottomDiv = styled.div`
  display: flex;
  justify-content: space-between;
  background: white;
  padding: 0 30px;
  height: 70px;
  display: flex;
  align-items: center;
`;

const mapStateToProps = state => ({
  loading: state.ui.loading,
  tableConfigs: state.config.tableConfigs,
});

class Table extends Component {
  constructor (props) {
    super(props);

    this.state = {
      selectedRows: {},
      expandFilters: false,
      showTooltips: {},
      selectAll: false,
      showingMoreActionsIndex: null,
    };

    this.parentRefs = {};
    this.bodyRefs = {};
    this.timeout = null;

    this.resizeHandler = this.onResize.bind(this);
    this.hideMoreActionsHandler = this.hideMoreActions.bind(this);
  }

  componentDidMount () {
    this.updateShowTooltips();
    window.addEventListener('resize', this.resizeHandler);
    document.addEventListener('mousedown', this.hideMoreActionsHandler, false);
  }

  componentDidUpdate (prevProps) {
    const { rows } = this.props;
    // update tooltips when new rows are fetched
    if (prevProps.rows !== rows) {
      // only do it when mounted
      if (typeof window !== 'undefined') {
        this.updateShowTooltips();
      }
    }
  }

  componentWillUnmount () {
    window.removeEventListener('resize', this.resizeHandler);
    document.removeEventListener('mousedown', this.hideMoreActionsHandler, false);
  }

  onResize () {
    clearTimeout(this.timeout);
    this.timeout = setTimeout(this.updateShowTooltips.bind(this), 1000);
  }

  setSelectAll (selectAll) {
    const stateUpdate = { selectAll };
    if (!selectAll) stateUpdate.selectedRows = {};

    this.setState(stateUpdate);
  }

  getColumnHeaders (columns = [], fixed = false) {
    const { sortBy, sortOrder, onSort } = this.props;

    return columns.map((c) => {
      const {
        title, flex, renderFilter, field, unsortable,
      } = c;
      const style = { flex };

      let columnKey;
      let sortType = null;
      if (c.dataField) {
        sortType = 'data_field';
        columnKey = `data-column-${ c.field }`;
      } else if (c.customField) {
        sortType = 'custom_field';
        columnKey = `custom-column-${ c.field }`;
      } else {
        columnKey = `standard-column-${ c.field }`;
      }

      return (
        <HeaderColumn
          key={ `table-header-${ columnKey }` }
          fixed={ fixed }
          style={ style }
          title={ title }
          unsortable={ unsortable }
          renderFilter={ renderFilter }
          sortBy={ sortBy }
          sortOrder={ sortOrder }
          onSort={ () => {
            this.toggleAllRowsSelection(false);
            onSort(field, sortType);
          } }
          field={ field }
        />
      );
    });
  }

  getColumnRows (columns = [], rows = [], minWidth = 0, fixed = false) {
    return rows.map((r, i) => {
      const { showTooltips } = this.state;

      const RowColumns = columns.map((c, j) => {
        const {
          field,
          format,
          tooltipFormat,
          flex,
          customCss,
          bodyCss,
          noTooltip,
          wrapText,
        } = c;
        const style = { flex };

        let refKey;
        if (c.dataField) {
          refKey = `data-column-${ c.field }-${ i }`;
        } else if (c.customField) {
          refKey = `custom-column-${ c.field }-${ i }`;
        } else {
          refKey = `standard-column-${ c.field }-${ i }`;
        }

        // arguments - (value, object)
        let value = format ? format(r[field], r, i) : r[field];
        if (value === null || value === '') value = '-';

        const tooltipValue = tooltipFormat ? tooltipFormat(r[field], r) : null;

        return (
          <RowColumn
            key={ `table-row-${ refKey }` }
            fixed={ fixed }
            style={ style }
            customCss={ customCss }
            bodyCss={ bodyCss }
            value={ value }
            tooltipValue={ tooltipValue }
            tooltipCss={
              j === columns.length - 1 ? 'left: auto; right: 20px;' : ''
            }
            parentRef={ wrapper => (this.parentRefs[refKey] = wrapper) }
            bodyRef={ wrapper => (this.bodyRefs[refKey] = wrapper) }
            showTooltip={ noTooltip ? false : showTooltips[refKey] }
            wrapText={ wrapText }
          />
        );
      });

      return (
        <RowDiv key={ `table-row-${ i }` } minWidth={ minWidth }>
          { RowColumns }
        </RowDiv>
      );
    });
  }

  hideMoreActions (e) {
    if (this.moreActionsWrapper && this.moreActionsWrapper.contains(e.target)) return;
    this.setState({
      showingMoreActionsIndex: null,
    });
  }

  showMoreActions (index) {
    this.setState({
      showingMoreActionsIndex: index,
    });
  }

  updateShowTooltips () {
    const showTooltips = {};

    for (const key in this.parentRefs) {
      const parentRef = this.parentRefs[key];
      const bodyRef = this.bodyRefs[key];

      const wrapperWidth = parentRef ? parentRef.clientWidth : null;
      const bodyWidth = bodyRef ? bodyRef.clientWidth : null;

      if (wrapperWidth && bodyWidth && bodyWidth >= wrapperWidth - 30) {
        showTooltips[key] = true;
      }
    }

    this.setState({ showTooltips });
  }

  changeRowSelection (value, i) {
    let { selectedRows } = this.state;
    selectedRows = Object.assign({}, selectedRows);
    selectedRows[i] = value;

    this.setState({ selectedRows });
  }

  toggleAllRowsSelection (value) {
    const { rows } = this.props;

    const selectedRows = {};
    rows.forEach((r, i) => (selectedRows[i] = value));
    this.setState({ selectedRows });
  }

  expandFilterSelector () {
    this.setState({ expandFilters: true }, () => {
      this.setState({ expandFilters: false });
    });
  }

  render () {
    const {
      id,
      columns = [],
      tableConfigs,
      rows,
      meta,
      perPage = 10,
      search,
      filters,
      filterFields = [],
      onSetFilters,
      onChangeFrom,
      onChangePerPage,
      onChangeSearchValue,
      rowActions = [],
      bulkActions = [],
      noPagination,
      emptyText,
      bulkActionsCss,
      loading,
      searchPlaceholder = t('Quick search by ID / name / email / phone'),
      singleItemText = t('item'),
      pluralItemText = t('items'),
      addButtonText,
      addButtonActions = [],
    } = this.props;
    const {
      selectedRows,
      // HACK: use this prop to stimulate expanding of dropdown inside FilterSelector
      expandFilters,
      selectAll,
      showingMoreActionsIndex,
    } = this.state;
    let selectedRowCount = 0;
    for (const i in selectedRows) {
      if (selectedRows[i]) selectedRowCount++;
    }
    const RowSelectedText = (
      <SelectionIndicator
        selectAll={ selectAll }
        selectedRowCount={ selectedRowCount }
        search={ search }
        filters={ filters }
        singleItemText={ singleItemText }
        pluralItemText={ pluralItemText }
        meta={ meta }
        setSelectAll={ this.setSelectAll.bind(this) }
      />
    );
    const allRowsSelected = rows.length && selectedRowCount === rows.length;

    const tableConfig = tableConfigs[id] || {};
    const displayedColumns = columns.filter((c) => {
      if (c.dataField || c.customField) {
        const field = c.dataField
          ? `data-field-${ c.field }`
          : `custom-field-${ c.field }`;

        return !!tableConfig[field];
      }
      return tableConfig[c.field] !== false;
    });

    // HACK: to calculate the total count of "flex-grows"
    const fixedColumns = displayedColumns.filter(c => c.unscrollable);
    const fixedFlexGrows = fixedColumns.reduce((pv, c) => {
      const grow = c.flex ? Number(c.flex.split(' ')[0]) || 1 : 1;
      return pv + grow;
    }, 0);
    const minFixedWidth = fixedFlexGrows * FLEX_GROW_MIN_WIDTH;

    const FixedHeaderColumns = this.getColumnHeaders(fixedColumns, true);
    const FixedHeader = (
      <HeaderDiv
        hasRowActions={ rowActions.length }
        minWidth={ minFixedWidth }
      >
        { FixedHeaderColumns }
      </HeaderDiv>
    );

    const scrollableColumns = displayedColumns.filter(c => !c.unscrollable);
    const scrollableFlexGrows = scrollableColumns.reduce((pv, c) => {
      const grow = c.flex ? Number(c.flex.split(' ')[0]) || 1 : 1;
      return pv + grow;
    }, 0);
    const minScrollableWidth = scrollableFlexGrows * FLEX_GROW_MIN_WIDTH;

    const ScrollableHeaderColumns = this.getColumnHeaders(scrollableColumns);
    const ScrollableHeader = (
      <HeaderDiv
        hasRowActions={ rowActions.length }
        minWidth={ minScrollableWidth }
      >
        { ScrollableHeaderColumns }
      </HeaderDiv>
    );

    const RowCheckboxes = rows.map((r, i) => (
      <RowCheckboxWrapper>
        <Checkbox
          customCss='margin: 0 0 0 7.5px;'
          field={ `select-row-${ i }` }
          checked={ selectAll || selectedRows[i] }
          disabled={ selectAll }
          onChange={ value => this.changeRowSelection(value, i) }
        />
      </RowCheckboxWrapper>
    ));

    const IndividualActions = rows.map((r, i) => {
      const visibleRowActions = rowActions.filter(
        a => typeof a.condition !== 'function' || a.condition(r),
      );

      return (
        <IndividualActionsWrapper onClick={ () => this.showMoreActions(i) }>
          { visibleRowActions.length
            ? (
              <Fragment>
                <ShowMoreButton plain>
                  <i className='fas fa-ellipsis-v' />
                </ShowMoreButton>
                { showingMoreActionsIndex === i && (
                <MoreActionsContainer hidden={ visibleRowActions.length === 0 } innerRef={ wrapper => (this.moreActionsWrapper = wrapper) }>
                  { visibleRowActions.map(a => (
                    <MoreActionButton onClick={ a.onClick.bind(this, r) }>
                      <MoreActionIcon src={ a.icon } />
                      <MoreActionLabel>{ a.label }</MoreActionLabel>
                    </MoreActionButton>
                  )) }
                </MoreActionsContainer>
                ) }
              </Fragment>
            )
            : null }
        </IndividualActionsWrapper>
      );
    });

    const FixedRows = this.getColumnRows(
      fixedColumns,
      rows,
      minFixedWidth,
      true,
    );
    const ScrollableRows = this.getColumnRows(
      scrollableColumns,
      rows,
      minScrollableWidth,
    );

    const emptyTextStyle = {
      textAlign: 'center',
      fontSize: 16,
      fontWeight: 600,
      lineHeight: '24px',
    };

    let Content = null;
    if (!meta || meta.count) {
      Content = (
        <ContentWrapper>
          { bulkActions.length ? (
            <CheckboxesColumn hasShadow={ !fixedColumns.length }>
              <CheckboxesColumnPlaceholder />
              <div>{ RowCheckboxes }</div>
            </CheckboxesColumn>
          ) : null }
          <FixedContent>
            { FixedHeader }
            <div>{ FixedRows }</div>
          </FixedContent>
          <ScrollableContent>
            { ScrollableHeader }
            <div>{ ScrollableRows }</div>
          </ScrollableContent>
          <RightActionsColumn>
            <HeaderConfigWrapper>
              { columns.find(c => !c.compulsory) ? (
                <HeaderConfig tableId={ id } columns={ columns } />
              ) : null }
            </HeaderConfigWrapper>
            <div>{ IndividualActions }</div>
          </RightActionsColumn>
        </ContentWrapper>
      );
    } else if (!loading) {
      Content = (
        <div style={ { padding: '40px 0' } }>
          <p style={ emptyTextStyle }>{ emptyText }</p>
        </div>
      );
    }

    return (
      <TableWrapper>
        <ActionsDiv>
          { bulkActions.length ? (
            <SelectAll>
              <Checkbox
                customCss='margin: 0 0 0 7.5px;'
                field='select-all-rows'
                checked={ selectAll || allRowsSelected }
                disabled={ selectAll || !rows.length }
                onChange={ this.toggleAllRowsSelection.bind(this) }
              />
            </SelectAll>
          ) : null }
          <BulkActions
            tableState={ this.state }
            actions={ bulkActions }
            filters={ filters }
            rows={ rows }
            onSuccess={ () => this.setState({ selectedRows: {}, selectAll: false })
            }
            customCss={ bulkActionsCss }
            RowSelectedText={ selectedRowCount > 0 ? RowSelectedText : null }
          />
          <SearchFilterWrapper>
            { onChangeSearchValue && selectedRowCount === 0
              ? [
                <Search
                  placeholder={ searchPlaceholder }
                  onChangeSearchValue={ onChangeSearchValue }
                  filters={ filters }
                />,
                filterFields.length ? (
                  <FilterSelector
                    expandFilters={ expandFilters }
                    filters={ filters }
                    fields={ filterFields }
                    onSetFilters={ (val) => {
                      this.toggleAllRowsSelection(false);
                      onSetFilters(val);
                    } }
                  />
                ) : null,
              ]
              : null }
          </SearchFilterWrapper>
          { selectedRowCount === 0 && addButtonActions.length ? (
            <AddButton
              text={ addButtonText || `${ t('Add ') }${ pluralItemText }` }
              actions={ addButtonActions }
            />
          ) : null }
        </ActionsDiv>
        <FilterStatus
          filters={ filters }
          fields={ filterFields }
          onExpandFilter={ this.expandFilterSelector.bind(this) }
        />
        { Content }
        { !noPagination && meta.count ? (
          <BottomDiv>
            <SortPerPage
              perPage={ perPage }
              onChangePerPage={ (newPerPage) => {
                this.toggleAllRowsSelection(false);
                onChangePerPage(newPerPage);
              } }
            />
            <Pagination
              meta={ meta }
              perPage={ perPage }
              onChangeFrom={ (val) => {
                this.toggleAllRowsSelection(false);
                onChangeFrom(val);
              } }
            />
          </BottomDiv>
        ) : null }
      </TableWrapper>
    );
  }
}

export default connect(mapStateToProps)(Table);
