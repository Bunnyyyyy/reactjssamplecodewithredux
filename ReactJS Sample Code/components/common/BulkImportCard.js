import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import styled from 'styled-components';
import Papa from 'papaparse';
import * as ManagerActionCreators from '../../redux/manager';

import Card from '../manager/cards/Card';
import FileUpload from './FileUpload';
import Button from './Button';
import Select from './inputs/Select';

import bulkImportIcon from '../../assets/common/bulk_import.svg';

import i18n from '../../../i18n';

const t = a => i18n.t([`components/common:${ a }`]);

const HeaderButtons = styled.div`
  display: flex;
  align-items: center;
`;

const Body = styled.div`
  text-align: center;
  height: 100%;
  padding: 100px 30px;
`;

const FileUploadWrapper = styled.div`
  width: 640px;
  height: 230px;
  margin: 30px auto;
`;

const Instructions = styled.div`
  margin-top: 30px;
`;

const DownloadContainer = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  margin: 30px auto 0 auto;

  p {
    margin-right: 15px;
  }

  i {
    margin-right: 10px;
  }

  a {
    color: ${ props => props.theme.colors.mainBlue };
  }
`;

const RightArrow = styled.i`
  margin-left: 15px;
  margin-right: 15px;
`;

const HeaderError = styled.p`
  font-size: 12px;
  line-height: 14px;
  margin-right: 20px;
  color: ${ props => props.theme.colors.mainRed };
`;

const Row = styled.div`
  font-size: 12px;
  display: flex;
  color: ${ props => props.theme.colors.darkGray };
  height: 75px;
  background-color: ${ props => (props.colored ? props.theme.colors.bgGray : '') };
  align-items: center;
`;

const HeaderRow = styled.div`
  font-size: 12px;
  display: flex;
  color: #9c9c9c;
  padding-top: 20px;
  padding-bottom: 20px;
  border-bottom: 1px solid rgb(225, 232, 237);
`;

const Col = styled.div`
  display: flex;
  flex: 1;
  padding-left: 30px;
  flex-direction: column;
`;

const Preview = styled.div`
  margin-bottom: 3px;
`;

const CheckedIcon = styled.i`
  font-size: 20px;
  color: ${ props => props.theme.colors.mainBlue };
`;

const ErrorMessage = styled.p`
  font-size: 12px;
  line-height: 14px;
  color: ${ props => props.theme.colors.mainRed };
`;

class BulkImportCard extends Component {
  state = {
    page: 1,
    headers: [],
    selectedFields: [],
    rows: [],
    overallError: null,
    headerErrors: [],
  };

  // TEMP: comment out first as having unload popup on every page change is annoying
  // componentDidMount () {
  //   window.onbeforeunload = function () {
  //     return true;
  //   };
  // }

  onUpdateFile = async (file) => {
    const result = await new Promise((resolve, reject) => {
      Papa.parse(file, { complete: resolve, error: reject });
    });

    const headers = result.data[0];
    const selectedFields = this.autoMatchHeaders(headers);

    const rows = result.data
      .slice(1)
      .filter(x => x[0] && x[0] !== '' && x.filter(x1 => x1 !== '').length);

    if (!rows.length) {
      return this.setState({ overallError: 'No data in the file.' });
    }

    this.setState(
      {
        selectedFields,
        headers,
        rows,
        overallError: null,
      },
      this.validateFields,
    );
  };

  onChangeColumnField (value, index) {
    let { selectedFields } = this.state;
    selectedFields = selectedFields.slice();
    // clear old matched field first
    selectedFields.forEach((f, i) => {
      if (f === value) selectedFields[i] = null;
    });
    selectedFields[index] = value;

    this.setState({ selectedFields }, this.validateFields);
  }

  goToPage = (page) => {
    this.setState({ page });
  };

  onClickImport = async () => {
    try {
      const { onImport, fields } = this.props;
      const { selectedFields, rows } = this.state;

      const data = rows.map((row) => {
        const insertData = { data_fields: {} };
        selectedFields.forEach((header, index) => {
          const field = fields.find(f => f.value === header);

          // this should never happen
          if (!field) return;

          const dataValue = typeof field.transform !== 'undefined'
            ? field.transform(row[index])
            : row[index];

          if (dataValue && dataValue !== '') {
            if (field.custom) {
              insertData.data_fields[header] = dataValue;
            } else {
              insertData[header] = dataValue;
            }
          }
        });
        return insertData;
      });

      if (onImport) await onImport(data);
    } catch (e) {
      throw e;
    }
  };

  autoMatchHeaders (headers) {
    const { fields } = this.props;
    const selectedFields = [];

    headers.forEach((h, i) => {
      const matchingField = fields.find(f => (
        f.label
          .trim()
          .toLowerCase()
          .replace(/[ _]/g, '')
        === h
          .trim()
          .toLowerCase()
          .replace(/[ _]/g, '')
      ));

      // ensure there is no duplicated field column
      if (matchingField && selectedFields.indexOf(matchingField.value) === -1) {
        selectedFields[i] = matchingField.value;
      }
    });

    return selectedFields;
  }

  renderHeader = () => {
    const { onCancel = null } = this.props;
    const {
      page, rows, headerErrors, overallError,
    } = this.state;

    const disableImport = overallError || headerErrors.find(h => !!h);

    if (page === 1) {
      return (
        <HeaderButtons>
          <Button small gray onClick={ onCancel.bind(this) }>
            Cancel
          </Button>
          <Button
            small
            light
            disabled={ rows.length < 1 }
            onClick={ this.goToPage.bind(this, 2) }
          >
            Next
          </Button>
        </HeaderButtons>
      );
    }

    return (
      <HeaderButtons>
        { overallError ? <HeaderError>{ overallError }</HeaderError> : null }
        <Button small gray onClick={ this.goToPage.bind(this, 1) }>
          Back
        </Button>
        <Button
          small
          light
          disabled={ disableImport }
          onClick={ this.onClickImport }
        >
          Import
        </Button>
      </HeaderButtons>
    );
  };

  validateFields () {
    const { fields } = this.props;
    const { rows, selectedFields } = this.state;

    const missingRequiredFields = [];
    const headerErrors = [];

    fields.forEach((f) => {
      const selectedHeaderIndex = selectedFields.findIndex(h => h === f.value);

      if (selectedHeaderIndex < 0) {
        if (f.required && !f.custom) missingRequiredFields.push(f.label);
      } else {
        const headerInvalidRows = [];
        rows.forEach((r, i) => {
          if (
            (typeof f.validate === 'function'
              && !f.validate(r[selectedHeaderIndex]))
            || (f.required && !r[selectedHeaderIndex].length)
          ) {
            headerInvalidRows.push(i + 2);
          }
        });

        headerErrors[selectedHeaderIndex] = headerInvalidRows.length
          ? `Invalid or missing value at row(s): ${ headerInvalidRows.join(
            ', ',
          ) }`
          : null;
      }

      const overallError = missingRequiredFields.length
        ? `${ t(
          'Please match these compulsory field(s)',
        ) }: ${ missingRequiredFields.join(', ') }`
        : null;

      this.setState({
        overallError,
        headerErrors,
      });
    });
  }

  renderRows = () => {
    const { fields } = this.props;
    const {
      headers, rows, selectedFields, headerErrors,
    } = this.state;

    return headers.map((header, index) => (
      <Row colored={ index % 2 === 0 }>
        <Col>{ header }</Col>
        <Col>
          { rows.slice(0, 3).map(row => <Preview>{ row[index] }</Preview>) }
        </Col>
        <Col>
          <Select
            clearable
            placeholder={ t('No Field') }
            options={ fields }
            onChange={ value => this.onChangeColumnField(value, index) }
            value={ selectedFields[index] }
            customCss='margin-bottom: 0px;'
          />
        </Col>
        <Col>
          { headerErrors[index] ? (
            <ErrorMessage>{ headerErrors[index] }</ErrorMessage>
          ) : (
            <CheckedIcon className='fas fa-check-circle' />
          ) }
        </Col>
      </Row>
    ));
  };

  renderBody = () => {
    const { fields, sampleCsvUrl } = this.props;
    const { page, overallError } = this.state;
    const requiredFieldLabels = fields
      .filter(f => !!f.required && !f.custom)
      .map(f => f.label);
    const optionalFieldLabels = fields
      .filter(f => !f.required || f.custom)
      .map(f => f.label);

    if (page === 1) {
      return (
        <Body>
          <img alt='' src={ bulkImportIcon } />
          <Instructions>
            <p>We only support CSV file at this moment. (More is coming!)</p>
            <p>
              Please format your CSV file with a header row that contains the
              field names.
            </p>
            <p>
              Compulsory field:
              { ' ' }
              <b>{ requiredFieldLabels.join(', ') }</b>
            </p>
            <p>
              Other fields you can add:
              { ' ' }
              <b>{ optionalFieldLabels.join(', ') }</b>
            </p>
          </Instructions>
          <FileUploadWrapper>
            <FileUpload
              accept='.csv'
              onUpdate={ this.onUpdateFile.bind(this) }
              // onRemove={ this.onRemove }
              customCss='display: inline-block;'
            />
          </FileUploadWrapper>
          { overallError ? <ErrorMessage>{ overallError }</ErrorMessage> : null }
          { sampleCsvUrl ? (
            <DownloadContainer>
              <p>Not sure about the file format?</p>
              <a href={ sampleCsvUrl } target='_blank' rel='noopener noreferrer'>
                <i className='fas fa-download' />
                Download sample CSV file
              </a>
            </DownloadContainer>
          ) : null }
        </Body>
      );
    }

    return (
      <div>
        <HeaderRow>
          <Col>{ t('Column header from file') }</Col>
          <Col>{ t('Preview column data') }</Col>
          <Col>{ t('Field in Juven') }</Col>
          <Col>{ t('Matched?') }</Col>
        </HeaderRow>
        { this.renderRows() }
      </div>
    );
  };

  render () {
    const { title = t('Bulk Import') } = this.props;
    const { page } = this.state;

    const cardTitle = page === 2 ? (
      <span>
        { title }
        <RightArrow className='fas fa-arrow-right' />
        { ' ' }
        Match the field
      </span>
    ) : (
      title
    );

    return (
      <Card title={ cardTitle } headerComponent={ this.renderHeader() }>
        { this.renderBody() }
      </Card>
    );
  }
}

const ActionCreators = Object.assign({}, ManagerActionCreators);

const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators(ActionCreators, dispatch),
});

export default connect(null, mapDispatchToProps)(BulkImportCard);
