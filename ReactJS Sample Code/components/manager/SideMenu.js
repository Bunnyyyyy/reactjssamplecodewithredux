import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { withRouter } from 'react-router';
import { Link } from 'react-router-dom';
import styled, { ThemeProvider } from 'styled-components';
import ReactTooltip from 'react-tooltip';

import OrganizationToggle from './OrganizationToggle';
import CircularImage from '../common/CircularImage';
import Button from '../common/Button';

import * as ConfigActionCreators from '../../redux/config';

import logoPlaceholder from '../../assets/common/placeholder_logo.svg';
import intercomLogo from '../../assets/manager/intercom.png';
import angleLeftIcon from '../../assets/manager/menu/icon_angle_left.svg';
import angleRightIcon from '../../assets/manager/menu/icon_angle_right.svg';
import angleLeftBlueIcon from '../../assets/manager/menu/icon_angle_left_blue.svg';
import angleRightBlueIcon from '../../assets/manager/menu/icon_angle_right_blue.svg';

// circular buttons
import plusIcon from '../../assets/manager/icon-plus.svg';
import switchIcon from '../../assets/manager/icon-switch.svg';
import switchActiveIcon from '../../assets/manager/icon-switch_active.svg';
import editIcon from '../../assets/manager/icon-edit.svg';
import profileIcon from '../../assets/manager/icon-profile.svg';

// grey icons
import dashboardIcon from '../../assets/manager/menu/icon_dashboard.svg';
import eventsIcon from '../../assets/manager/menu/icon_events.svg';
import membershipIcon from '../../assets/manager/menu/icon_membership.svg';
import emailsIcon from '../../assets/manager/menu/icon_emails.svg';
import formsIcon from '../../assets/manager/menu/icon_forms.svg';
import settingsIcon from '../../assets/manager/menu/icon_settings.svg';
import fundraisingIcon from '../../assets/manager/menu/icon_fundraising.svg';

// blue icons
import dashboardBlueIcon from '../../assets/manager/menu/icon_dashboard_blue.svg';
import eventsBlueIcon from '../../assets/manager/menu/icon_events_blue.svg';
import membershipBlueIcon from '../../assets/manager/menu/icon_membership_blue.svg';
import emailsBlueIcon from '../../assets/manager/menu/icon_emails_blue.svg';
import formsBlueIcon from '../../assets/manager/menu/icon_forms_blue.svg';
import settingsBlueIcon from '../../assets/manager/menu/icon_settings_blue.svg';
import fundraisingBlueIcon from '../../assets/manager/menu/icon_fundraising_blue.svg';

// white icons
import dashboardWhiteIcon from '../../assets/manager/menu/icon_dashboard_white.svg';
import eventsWhiteIcon from '../../assets/manager/menu/icon_events_white.svg';
import membershipWhiteIcon from '../../assets/manager/menu/icon_membership_white.svg';
import emailsWhiteIcon from '../../assets/manager/menu/icon_emails_white.svg';
import formsWhiteIcon from '../../assets/manager/menu/icon_forms_white.svg';
import settingsWhiteIcon from '../../assets/manager/menu/icon_settings_white.svg';
import fundraisingWhiteIcon from '../../assets/manager/menu/icon_fundraising_white.svg';

import i18n from '../../../i18n';

const { FRONTEND_URL, INTERCOM_APP_ID } = process.env;

const t = a => i18n.t([`components/manager:${ a }`]);

const getMenuItemsData = (organization) => {
  const menus = [
    {
      title: t('Dashboard'),
      url: `/${ organization.id }`,
      match: url => (url === `/${ organization.id }` || url.indexOf('/home') > 0),
      icon: dashboardIcon,
      blueIcon: dashboardBlueIcon,
      whiteIcon: dashboardWhiteIcon,
    },
    {
      title: t('Events'),
      url: `/${ organization.id }/events`,
      icon: eventsIcon,
      blueIcon: eventsBlueIcon,
      whiteIcon: eventsWhiteIcon,
    },
    {
      title: t('Membership'),
      url: `/${ organization.id }/membership`,
      icon: membershipIcon,
      blueIcon: membershipBlueIcon,
      whiteIcon: membershipWhiteIcon,
    },
    {
      title: t('Emails'),
      url: `/${ organization.id }/emails`,
      icon: emailsIcon,
      blueIcon: emailsBlueIcon,
      whiteIcon: emailsWhiteIcon,
    },
    {
      title: t('Forms'),
      url: `/${ organization.id }/forms`,
      icon: formsIcon,
      blueIcon: formsBlueIcon,
      whiteIcon: formsWhiteIcon,
    },
    // {
    //   title: t('Deals & Benefits'),
    //   route: '/manager/deals',
    //   iconFilename: 'icon_deals',
    //   url: `/${ organization.id }/deals`
    // },
    {
      title: t('Settings'),
      url: `/${ organization.id }/settings`,
      iconWidth: 18,
      icon: settingsIcon,
      blueIcon: settingsBlueIcon,
      whiteIcon: settingsWhiteIcon,
    },
  ];

  if (organization.charity) {
    menus.splice(3, 0, {
      title: t('Fundraising'),
      url: `/${ organization.id }/fundraising`,
      icon: fundraisingIcon,
      blueIcon: fundraisingBlueIcon,
      whiteIcon: fundraisingWhiteIcon,
    });
  }

  return menus;
};

const SideMenuContainerDiv = styled.div`
  position: absolute;
  height: 100vh;
  display: flex;
  top: 0px;
  left: 0px;
  flex-direction: row;
`;

const SideMenuDiv = styled.div`
  height: calc(100vh - 60px);
  z-index: 999;
  box-shadow: 0px 3px 6px rgba(0, 0, 0, 0.1);
  background-color: white;
  width: ${ props => (props.theme.collapsed ? '60px' : '180px') };
  max-width: 180px;
  overflow-y: scroll;
  overflow-x: hidden;
  transition: all 0.5s ease-in-out;
`;

const MenuItemContent = styled.p`
  font-size: 12px;
  width: 100%;
  color: ${ props => (props.theme.selected ? 'white' : props.theme.colors.darkGray) };
  line-height: normal;
  font-weight: 600;
  white-space: nowrap;
  transition: all 0.5s ease-in-out;
  ${ props => (props.theme.collapsed ? 'display: none;' : '') };
`;

const MenuItemIconWrapper = styled.span`
  display: inline-block;
  cursor: pointer;
  width: 24px;
  height: 24px;
  margin: 0 15px 0 0;
`;

const MenuItemIcon = styled.img`
  display: ${ props => (props.theme.selected ? 'none' : 'inline') };
  height: 24px;
  width: 24px;
`;

const MenuItemBlueIcon = MenuItemIcon.extend`
  display: none;
  ${ props => (props.theme.selected ? 'display: none !important;' : '') };
`;

const MenuItemWhiteIcon = MenuItemIcon.extend`
  display: ${ props => (props.theme.selected ? 'inline' : 'none') } !important;
`;

const MenuItem = styled.div`
  display: flex;
  cursor: pointer;
  height: 44px;
  position: relative;
  width: 100%;
  flex-direction: row;
  padding: 0 18px;
  align-items: center;
  ${ props => (props.disabled
    ? `
    opacity: 0.25;
    cursor: default;
  `
    : '') } &:hover, &:active {
    & ${ MenuItemContent } {
      color: ${ props => (props.theme.selected ? 'white' : props.theme.colors.mainBlue) };
    }

    ${ MenuItemIcon }, ${ MenuItemWhiteIcon } {
      display: none;
    }

    ${ MenuItemBlueIcon } {
      display: inline;
    }
  }

  ${ props => (props.theme.selected
    ? `background-color: ${ props.theme.colors.mainBlue };`
    : '') };
`;

const ToggleItem = MenuItem.extend`
  padding: 5px 18px;
  margin-bottom: 5px;
  ${ props => (props.show ? '' : 'display: none;') };

  &:hover,
  &:active {
    ${ MenuItemIcon } {
      display: none;
    }
  }
`;

const MenuContainer = styled.ul`
  display: flex;
  flex-direction: column;
  width: 100%;
  ${ props => (props.show ? '' : 'display: none;') };
`;

const OrganizationInfoWrapper = styled.div`
  border-bottom: 1px solid ${ props => props.theme.colors.lightGray };
`;

const OrganizationName = styled.p`
  font-size: 12px;
  margin: 0 20px 20px 20px;
  text-align: center;
  ${ props => (props.theme.collapsed ? 'display: none;' : '') };
`;

const IntercomDiv = styled.div`
  cursor: pointer;
  position: absolute;
  display: flex;
  align-items:center;
  height: 60px;
  width: 100%;
  bottom: 0;
  left: 0;
  z-index: 1000;
  background: white;
  border-top: 1px solid ${ props => props.theme.colors.lightGray };
  img {
    width: 30px;
    margin-left: 15px;
  }
  span {
    white-space: nowrap;
    margin-left: 13px;
    font-size: 12px;
    font-weight: 600;
    ${ props => (props.theme.collapsed ? 'display: none;' : '') };
  }
`;

const LogoWrapper = styled.div`
  margin: 10px auto;
  text-align: center;
`;

const ActionButtons = styled.div`
  transition: all 0.5s ease-in-out;
  display: flex;
  justify-content: space-between;
  padding: 0 20px;
  margin: 20px 0;

  a {
    border-radius: 50%;
    height: 30px;
  }
`;

const CircularButton = Button.extend`
  border-radius: 50%;
  border: 1px solid ${ props => props.theme.colors.lightGray };
  width: 30px;
  height: 30px;
  padding: 2px;
  margin: 0;
  font-size: auto;
  line-height: auto;

  ${ props => (props.active ? `background-color: ${ props.theme.colors.mainBlue } !important;` : '') }

  &:hover {
    background-color: ${ props => (props.active ? props.theme.colors.mainBlue : props.theme.colors.bgGray) } !important;
  }

  & img {
    width: 100%;
  }
`;

const mapStateToProps = state => ({
  organizations: state.manager.organizations,
  organization: state.manager.organization,
  user: state.me.user,
  collapsed: state.config.collapseSideMenu,
});

const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators(ConfigActionCreators, dispatch),
});

class SideMenuContainer extends Component {
  constructor (props) {
    super(props);

    this.state = {
      showOrganizationToggle: false,
    };
  }

  componentDidMount () {
    const { user } = this.props;
    if (
      typeof window !== 'undefined'
      && window.Intercom
    ) {
      const intercomSettings = {
        app_id: INTERCOM_APP_ID,
        alignment: 'left',
        hide_default_launcher: true,
      };

      if (user && user.id) {
        intercomSettings.name = `${ user.first_name } ${ user.last_name }`;
        intercomSettings.email = `${ user.email }`;
      }
      window.Intercom('boot', intercomSettings);
    }
  }

  getMenuItemsData () {
    const { organization } = this.props;
    return getMenuItemsData(organization);
  }

  getMenuItems (collapsed) {
    const { location } = this.props;

    return this.getMenuItemsData().map((itemData, index) => {
      const {
        title, url, match, icon, blueIcon, whiteIcon, disabled,
      } = itemData;
      const isSelected = match
        ? !!match(location.pathname)
        : location.pathname.indexOf(url) > -1;

      return (
        <ThemeProvider theme={ { selected: isSelected } }>
          <li key={ `menu-item-${ index }` }>
            <Link to={ url } disabled={ disabled }>
              <MenuItem disabled={ disabled }>
                <MenuItemIconWrapper data-tip={ collapsed ? title : '' } data-place='right' data-effect='solid'>
                  <MenuItemIcon src={ icon } />
                  <MenuItemBlueIcon src={ blueIcon } />
                  <MenuItemWhiteIcon src={ whiteIcon } />
                </MenuItemIconWrapper>
                <ReactTooltip />
                <MenuItemContent>{ title }</MenuItemContent>
              </MenuItem>
            </Link>
          </li>
        </ThemeProvider>
      );
    });
  }

  toggleOrganizationMenu () {
    let { showOrganizationToggle } = this.state;
    showOrganizationToggle = !showOrganizationToggle;

    if (showOrganizationToggle) {
      this.setState({ showOrganizationToggle }, () => {
        this.toggle.focus();
      });
    } else {
      setTimeout(() => this.setState({ showOrganizationToggle }), 200);
    }
  }

  render () {
    const {
      actions, organization, collapsed, organizations,
    } = this.props;
    const { showOrganizationToggle } = this.state;

    const arrowIcon = collapsed ? angleRightIcon : angleLeftIcon;
    const arrowIconBlue = collapsed ? angleRightBlueIcon : angleLeftBlueIcon;

    return (
      <ThemeProvider theme={ { collapsed } }>
        <SideMenuContainerDiv>
          <SideMenuDiv className={ showOrganizationToggle ? 'show' : null }>
            <OrganizationInfoWrapper>
              <LogoWrapper>
                <Link to={ `/${ organization.id }/settings` }>
                  <CircularImage
                    noPadding
                    size={ collapsed ? 36 : 60 }
                    src={ organization.logo_image_url || logoPlaceholder }
                    description={ organization.name }
                    hoverText={ t('Edit') }
                  />
                </Link>
              </LogoWrapper>
              <OrganizationName>{ organization.name }</OrganizationName>
              {
                !collapsed
                && (
                  <ActionButtons>
                    <Link to='/new'>
                      <CircularButton plain data-tip={ t('Create new organization') } data-place='right' data-effect='solid'>
                        <img alt={ t('Create new organization') } src={ plusIcon } />
                      </CircularButton>
                    </Link>
                    {
                      organizations
                      && organizations.length > 1
                      && (
                      <CircularButton
                        plain
                        data-tip={ t('Switch organization') }
                        data-place='right'
                        data-effect='solid'
                        onClick={ this.toggleOrganizationMenu.bind(this) }
                        active={ showOrganizationToggle }
                      >
                        <img alt={ t('Switch organization') } src={ showOrganizationToggle ? switchActiveIcon : switchIcon } />
                      </CircularButton>
                      )
                    }
                    <Link to={ `/${ organization.id }/settings` }>
                      <CircularButton plain data-tip={ t('Edit profile') } data-place='right' data-effect='solid'>
                        <img alt={ t('Edit profile') } src={ editIcon } />
                      </CircularButton>
                    </Link>
                    <a target='_blank' rel='noopener noreferrer' href={ `${ FRONTEND_URL }/${ organization.id }` }>
                      <CircularButton plain data-tip={ t('View organization profile') } data-place='right' data-effect='solid'>
                        <img alt={ t('View organization profile') } src={ profileIcon } />
                      </CircularButton>
                    </a>
                    <ReactTooltip />
                  </ActionButtons>
                )
              }
            </OrganizationInfoWrapper>
            <MenuContainer show={ !showOrganizationToggle }>{ this.getMenuItems(collapsed) }</MenuContainer>
            <ToggleItem onClick={ actions.toogleSidebarMenu } show={ !showOrganizationToggle }>
              <MenuItemIconWrapper data-tip={ collapsed ? t('Expand') : '' } data-place='right' data-effect='solid'>
                <MenuItemIcon src={ arrowIcon } />
                <MenuItemBlueIcon src={ arrowIconBlue } />
              </MenuItemIconWrapper>
              <ReactTooltip />
              <MenuItemContent>{ t('Collapse') }</MenuItemContent>
            </ToggleItem>
            <OrganizationToggle
              refHandler={ toggle => (this.toggle = toggle) }
              show={ showOrganizationToggle }
              onSelect={ this.toggleOrganizationMenu.bind(this) }
            />
          </SideMenuDiv>
          <IntercomDiv onClick={ () => window.Intercom('show') }>
            <img alt='' src={ intercomLogo } />
            <span>{ t('Support') }</span>
          </IntercomDiv>
        </SideMenuContainerDiv>
      </ThemeProvider>
    );
  }
}

export default withRouter(
  connect(mapStateToProps, mapDispatchToProps)(SideMenuContainer),
);
