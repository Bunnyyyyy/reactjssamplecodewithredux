import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { withRouter } from 'react-router';
import styled from 'styled-components';
import { Formik } from 'formik';
import * as Yup from 'yup';
import moment from 'moment';

import { formatDate, formatTime } from '../../../../../utils';
import { DAYS_OF_MONTH, DAYS_OF_WEEK } from '../../../../../utils/dates';
import currencies from '../../../../../utils/currencies';

import * as UiActionCreators from '../../../../../redux/ui';
import * as ManagerActionCreators from '../../../../../redux/manager';
import * as EventActionCreators from '../../../../../redux/event';

import Button from '../../../../../components/common/Button';
import ConfirmModal from '../../../../../components/common/ConfirmModal';
import BiOption from '../../../../../components/common/inputs/BiOption';
import Card from '../../../../../components/manager/cards/Card';
import FormikSaveButtons from '../../../../../components/common/inputs/FormikSaveButtons';
import Input from '../../../../../components/common/Input';
import Select from '../../../../../components/common/inputs/Select';
import MultiSelect from '../../../../../components/common/MultiSelect';
import DatePicker from '../../../../../components/common/DatePicker';
import TimePicker from '../../../../../components/common/TimePicker';
import Options from '../../../../../components/common/inputs/Options';
import Checkbox from '../../../../../components/common/Checkbox';
import InvalidMessage from '../../../../../components/common/InvalidMessage';
import UnsavedPrompt from '../../../../../components/forms/UnsavedPrompt';

import { t as translate } from '../../../../../../i18n';

const t = a => translate(a, 'manager/events/ticketing/types/edit');

const comboSelectCss = `
  padding-right: 0;
  .select__container {
    border-right: 1px solid rgb(47, 46, 46, .25);
  }
`;

const comboInputCss = `
input {
  padding-left: 10px;
}
`;

const TrashButton = Button.extend`
  color: ${ props => props.theme.colors.mainGray };
  width: 30px;
  margin-left: 30px;

  &:hover,
  &:active {
    color: ${ props => props.theme.colors.mainRed };
  }

  & i {
    margin-right: 0;
  }
`;

const ButtonActions = styled.div`
  display: flex;
  align-items: center;
  padding: 10px 0;
  border-top: 1px solid ${ props => props.theme.colors.lightGray };
`;

const StyledForm = styled.form`
  display: flex;
  flex-wrap: wrap;
  padding: 10px 10px 10px 30px;

  > div,
  > input {
    flex: 100% 0 0;
    margin-right: 0;
    padding-right: 20px;
  }
`;

const PRICE_TYPE_OPTIONS = [
  {
    value: false,
    label: 'Paid',
  },
  {
    value: true,
    label: 'Free',
  },
];

const QUANTITY_TYPE_OPTIONS = [
  {
    value: true,
    label: 'Unlimited',
  },
  {
    value: false,
    label: 'Fixed',
  },
];

const START_SELLING_AT_OPTIONS = [
  { label: t('Immediately'), value: true },
  { label: t('Customize Date'), value: false },
];

const END_SELLING_AT_OPTIONS = [
  { label: t('When Session Starts'), value: true },
  { label: t('Customize Date'), value: false },
];

const mapStateToProps = state => ({
  events: state.manager.events,
  event: state.manager.event,
  organization: state.manager.organization,
  stripeAccount: state.manager.stripeAccount,
  tickets: state.event.tickets,
  ticketType: state.event.ticket,
  dates: state.event.dates,
});

const ActionCreators = Object.assign(
  {},
  UiActionCreators,
  ManagerActionCreators,
  EventActionCreators,
);
const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators(ActionCreators, dispatch),
});

class EventTicketingTypesEditView extends Component {
  constructor (props) {
    super(props);

    this.state = {
      isDeleting: false,
    };
  }

  async componentDidMount () {
    const { actions, match } = this.props;
    const { eId, tkId } = match.params;

    actions.setLoading(true);

    try {
      const promises = [
        actions.getEventTickets(eId),
        actions.getEventDates(eId),
      ];
      if (tkId) {
        promises.push(actions.getEventTicket(eId, tkId));
      } else {
        actions.setEventTicket({});
      }

      await Promise.all(promises);
    } catch (e) {
      console.error('ERROR: EventTicketingTypesEditView', e);
    } finally {
      actions.setLoading(false);
    }
  }

  componentWillUnmount () {
    const { actions } = this.props;
    actions.resetSaveActions();
  }

  async onDelete () {
    const {
      actions, history, ticketType, organization, event,
    } = this.props;
    const eId = event.id;
    const tkId = ticketType.id;

    if (eId && tkId) {
      await Promise.all([actions.removeEventTicketsType(eId, tkId)]);
    }
    history.push(`/${ organization.id }/events/${ event.id }/ticketing/types`);
  }

  async onCancel () {
    try {
      const { organization, history, event } = this.props;
      history.push(
        `/${ organization.id }/events/${ event.id }/ticketing/types`,
      );
    } catch (e) {
      throw e;
    }
  }

  async saveTicketType (state, formikBag) {
    const {
      actions,
      history,
      organization,
      event,
      tickets,
      ticketType = {},
    } = this.props;
    const { resetForm } = formikBag;
    const eId = event.id;

    try {
      const data = {
        name: state.name,
        price: state.free ? 0 : state.price,
        quantity: state.unlimited_quantity ? null : state.quantity,
        quantity_per_purchase: state.quantity_per_purchase,
        start_selling_at: state.start_selling_immediately
          ? null
          : state.start_selling_at,
        end_selling_at: state.end_selling_automatically
          ? null
          : state.end_selling_at,
        member_only: false,
        all_sessions: state.all_sessions,
        date_ids: !state.all_sessions ? state.date_ids : null,
        order: tickets.length,
      };

      if (ticketType.id) {
        await actions.updateEventTicket(eId, ticketType.id, data);
        await actions.getEventTicket(eId, ticketType.id);
        resetForm();
      } else {
        data.order = tickets.length;
        await actions.createEventTicket(eId, data);

        return history.push(
          `/${ organization.id }/events/${ event.id }/ticketing/types`,
        );
      }
    } catch (e) {
      throw e;
    }
  }

  toggleDeleting () {
    const { isDeleting } = this.state;
    this.setState({
      isDeleting: !isDeleting,
    });
  }

  render () {
    const {
      event = {},
      organization,
      stripeAccount,
      ticketType = {},
      dates,
    } = this.props;
    const { isDeleting } = this.state;

    const eventDateOptions = dates
      .filter(d => !d.recurring_interval || d.timeslots.length)
      .map((d) => {
        const startDateText = formatDate(d.start_date);
        const endDateText = d.end_date ? formatDate(d.end_date) : '';

        const startTimeText = d.timeslots.length
          ? formatTime(moment(d.timeslots[0].start_at, 'HH:mm:ss'))
          : '';

        const endTimeText = d.timeslots.length && d.timeslots[0].end_at
          ? formatTime(moment(d.timeslots[0].end_at, 'HH:mm:ss'))
          : '';

        let dateText;
        if (d.recurring_interval) {
          if (!d.timeslots.length) return null;

          dateText = `${ formatDate(d.start_date) } - ${ formatDate(d.end_date) }`;
          if (d.recurring_interval === 'monthly') {
            const monthDaysText = d.recurring_days
              .map(md => DAYS_OF_MONTH[md])
              .join(', ');
            dateText
              += d.timeslots.length > 1
                ? `, ${ t('Repeated Monthly on ') }${ monthDaysText } in ${
                  d.timeslots.length
                } ${ t('timeslots') }`
                : `, ${ t(
                  'Repeated Monthly on ',
                ) }${ monthDaysText } from ${ startTimeText }${
                  endTimeText ? ` - ${ endTimeText }` : ''
                }`;
          } else if (d.recurring_interval === 'weekly') {
            const weekdaysText = d.recurring_days
              .map(wd => DAYS_OF_WEEK[wd])
              .join(', ');
            dateText
              += d.timeslots.length > 1
                ? `, ${ t('Repeated Weekly on ') }${ weekdaysText } in ${
                  d.timeslots.length
                } timeslots`
                : `, ${ t(
                  'Repeated Weekly on ',
                ) }${ weekdaysText } from ${ startTimeText }${
                  endTimeText ? ` - ${ endTimeText }` : ''
                }`;
          } else if (d.recurring_interval === 'daily') {
            dateText
              += d.timeslots.length > 1
                ? `, ${ t('Repeated Daily in') } ${ d.timeslots.length } ${ t(
                  'timeslots',
                ) }`
                : `, ${ t('Repeated Daily from') } ${ startTimeText }${
                  endTimeText ? ` - ${ endTimeText }` : ''
                }`;
          }
        } else {
          const timeslot = d.timeslots[0];
          if (timeslot) {
            dateText = `${ startDateText }, ${ startTimeText }`;
            if (d.end_date && d.start_date !== d.end_date) {
              dateText += ` - ${ endDateText }, ${ endTimeText }`;
            }
          } else {
            dateText = `${ startDateText }`;
            if (d.end_date && d.start_date !== d.end_date) {
              dateText += ` - ${ endDateText }`;
            }
          }
        }

        return {
          label: dateText,
          value: d.id,
        };
      });

    const formInitialValues = ticketType.id
      ? Object.assign({}, ticketType)
      : { all_sessions: true };
    formInitialValues.free = ticketType.id
      ? formInitialValues.price === 0
      : false;
    formInitialValues.unlimited_quantity = ticketType.id
      ? ticketType.quantity === null
      : true;
    formInitialValues.start_selling_immediately = ticketType.id
      ? !ticketType.start_selling_at
      : true;
    formInitialValues.end_selling_automatically = ticketType.id
      ? !ticketType.end_selling_at
      : true;

    const TicketTypeFormSchema = Yup.object().shape({
      name: Yup.string()
        .nullable()
        .required(t('Please fill in a Ticket Name')),
      free: Yup.boolean().test(
        'needs-stripe-for-paid-ticket-type',
        t('You have to connect your Stripe account first before creating a paid ticket type - please go to "Settings -> Receive Payments" to connect Stripe account'),
        v => v || !!(stripeAccount && stripeAccount.id),
      ),
      price: Yup.mixed().when('free', {
        is: false,
        then: Yup.number()
          .required(t('Please fill in a Ticket Price'))
          .min(
            currencies[event.currency].minPaymentAmount,
            t('Ticket Price must be at least ')
            + currencies[event.currency].symbol
            + currencies[event.currency].minPaymentAmount,
          )
          .max(
            currencies[event.currency].maxPaymentAmount,
            t('Ticket Price must be less than ')
            + currencies[event.currency].symbol
            + currencies[event.currency].maxPaymentAmount,
          ),
        otherwise: Yup.mixed(),
      }),
      unlimited_quantity: Yup.boolean(),
      quantity: Yup.mixed().when('unlimited_quantity', {
        is: false,
        then: Yup.number()
          .required(t('Please fill in a Quantity'))
          .positive(),
        otherwise: Yup.mixed(),
      }),
      quantity_per_purchase: Yup.number().nullable(),
      start_selling_immediately: Yup.boolean(),
      start_selling_at: Yup.mixed().when('start_selling_immediately', {
        is: false,
        then: Yup.date().required(t('Please fill in a Start Date & Time')),
        otherwise: Yup.mixed(),
      }),
      end_selling_automatically: Yup.boolean(),
      end_selling_at: Yup.mixed().when('end_selling_automatically', {
        is: false,
        then: Yup.date().required(t('Please fill in a End Date & Time')),
        otherwise: Yup.mixed(),
      }),
    });

    return (
      <Card
        title={ t('Edit Your Ticket Type') }
        backRoute={ `/${ organization.id }/events/${ event.id }/ticketing/types` }
      >
        <Formik
          enableReinitialize
          initialValues={ formInitialValues }
          validationSchema={ TicketTypeFormSchema }
          onSubmit={ this.saveTicketType.bind(this) }
        >
          { (props) => {
            const {
              values, errors, setFieldValue, submitCount,
            } = props;
            // if creating a new ticket type, only validate after first submission attempt
            const validateOnChange = !!ticketType.id || submitCount > 0;

            return [
              <StyledForm>
                {
                  ticketType.id ? <UnsavedPrompt formikProps={ props } /> : null
                }
                <Input
                  name='name'
                  label={ t('Ticket Name') }
                  value={ values.name }
                  errorMessage={ errors.name }
                  onChange={ (_, value) => setFieldValue('name', value, validateOnChange)
                  }
                />
                <Select
                  name='free'
                  label={ t('Ticket Price') }
                  fullWidth
                  underlined
                  value={ values.free }
                  options={ PRICE_TYPE_OPTIONS }
                  onChange={ value => setFieldValue('free', value, validateOnChange) }
                  customCss={ `
                    ${ comboSelectCss } ${ values.free
                    ? 'flex: 100% 0 0 !important; padding-right: calc(50% + 20px) !important;'
                    : 'flex: 15% 0 0 !important; padding-right: 0 !important;' };
                  ` }
                />
                { !values.free ? (
                  <Input
                    number
                    name='price'
                    label=' '
                    value={ values.price }
                    errorMessage={ errors.price }
                    onChange={ (_, value) => setFieldValue('price', value, validateOnChange) }
                    customCss={ `
                      flex: 85% 0 0 !important;
                      padding-right: calc(50% + 20px) !important;
                      ${ comboInputCss };
                    ` }
                  />
                ) : null }
                { errors.free ? (
                  <InvalidMessage customCss='position: relative;margin-top: 20px;'>{ errors.free }</InvalidMessage>
                ) : null }
                <Select
                  name='unlimited_quantity'
                  label={ t('Quantity') }
                  fullWidth
                  underlined
                  value={ values.unlimited_quantity }
                  options={ QUANTITY_TYPE_OPTIONS }
                  onChange={ value => setFieldValue(
                    'unlimited_quantity',
                    value,
                    validateOnChange,
                  )
                  }
                  customCss={
                    values.unlimited_quantity
                      ? 'flex: 50% 0 0 !important;'
                      : 'flex: 15% 0 0 !important; padding-right: 0 !important;'
                  }
                />
                { !values.unlimited_quantity ? (
                  <Input
                    number
                    name='quantity'
                    label=' '
                    value={ values.quantity }
                    errorMessage={ errors.quantity }
                    onChange={ (_, value) => setFieldValue('quantity', value, validateOnChange)
                    }
                    customCss={ `
                      flex: 35% 0 0 !important;
                      ${ comboInputCss };
                    ` }
                  />
                ) : null }
                <Input
                  number
                  name='quantity_per_purchase'
                  label={ t('Max. Quantity Per Purchase') }
                  placeholder={ t('Unlimited') }
                  value={ values.quantity_per_purchase }
                  errorMessage={ errors.quantity_per_purchase }
                  onChange={ (_, value) => setFieldValue(
                    'quantity_per_purchase',
                    value,
                    validateOnChange,
                  )
                  }
                  customCss='flex: 50% 0 0 !important;'
                />
                <Options
                  name='start_selling_immediately'
                  label={ t('Start Selling At') }
                  value={ values.start_selling_immediately }
                  options={ START_SELLING_AT_OPTIONS.map(c => ({
                    value: c.value,
                    component: () => (
                      <BiOption>
                        <p>{ c.label }</p>
                      </BiOption>
                    ),
                  })) }
                  onChange={ value => setFieldValue(
                    'start_selling_immediately',
                    value,
                    validateOnChange,
                  )
                  }
                  customCss={
                    !values.start_selling_immediately
                      ? 'flex: 50% 0 0 !important;'
                      : 'padding-right: calc(50% + 20px) !important;'
                  }
                />
                { !values.start_selling_immediately
                  ? [
                    <DatePicker
                      label={ t('Start Date & Time') }
                      timezone={ event.timezone || null }
                      date={ values.start_selling_at }
                      errorMessage={ errors.start_selling_at }
                      onChange={ value => setFieldValue(
                        'start_selling_at',
                        value,
                        validateOnChange,
                      )
                      }
                      customCss='flex: 35% 0 0 !important;'
                    />,
                    <TimePicker
                      label=' '
                      timezone={ event.timezone || null }
                      date={ values.start_selling_at }
                      onChange={ value => setFieldValue(
                        'start_selling_at',
                        value,
                        validateOnChange,
                      )
                      }
                      customCss='flex: 15% 0 0 !important;'
                    />,
                  ]
                  : null }
                <Options
                  name='end_selling_automatically'
                  label={ t('End Selling At') }
                  value={ values.end_selling_automatically }
                  options={ END_SELLING_AT_OPTIONS.map(c => ({
                    value: c.value,
                    component: () => (
                      <BiOption>
                        <p>{ c.label }</p>
                      </BiOption>
                    ),
                  })) }
                  onChange={ value => setFieldValue(
                    'end_selling_automatically',
                    value,
                    validateOnChange,
                  )
                  }
                  customCss={
                    !values.end_selling_automatically
                      ? 'flex: 50% 0 0 !important;'
                      : 'padding-right: calc(50% + 20px) !important;'
                  }
                />
                { !values.end_selling_automatically
                  ? [
                    <DatePicker
                      label={ t('End Date & Time') }
                      timezone={ event.timezone || null }
                      date={ values.end_selling_at }
                      errorMessage={ errors.end_selling_at }
                      onChange={ value => setFieldValue(
                        'end_selling_at',
                        value,
                        validateOnChange,
                      )
                      }
                      customCss='flex: 35% 0 0 !important;'
                    />,
                    <TimePicker
                      label=' '
                      timezone={ event.timezone || null }
                      date={ values.end_selling_at }
                      onChange={ value => setFieldValue(
                        'end_selling_at',
                        value,
                        validateOnChange,
                      )
                      }
                      customCss='flex: 15% 0 0 !important;'
                    />,
                  ]
                  : null }
                <Checkbox
                  name='all_sessions'
                  checked={ values.all_sessions }
                  onChange={ value => setFieldValue('all_sessions', value, validateOnChange)
                  }
                  label={ t('Available in all dates') }
                  errorMessage={ errors.all_sessions }
                />
                { !values.all_sessions ? (
                  <MultiSelect
                    label={ t(
                      'Which dates should this ticket type be available in?',
                    ) }
                    value={ values.date_ids || [] }
                    options={ eventDateOptions }
                    onChange={ value => setFieldValue('date_ids', value, validateOnChange)
                    }
                  />
                ) : null }
              </StyledForm>,
              <ButtonActions>
                <FormikSaveButtons
                  newItem={ !ticketType.id }
                  formikProps={ props }
                />
                { ticketType.id && (
                  <TrashButton plain onClick={ this.toggleDeleting.bind(this) }>
                    <i className='fa fa-trash' aria-hidden='true' />
                  </TrashButton>
                ) }
              </ButtonActions>,
            ];
          } }
        </Formik>
        { isDeleting ? (
          <ConfirmModal
            warning
            message={ t('Are you sure you want to delete this Ticket Type?') }
            onConfirm={ this.onDelete.bind(this) }
            onCancel={ this.toggleDeleting.bind(this) }
          />
        ) : null }
      </Card>
    );
  }
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(EventTicketingTypesEditView));
