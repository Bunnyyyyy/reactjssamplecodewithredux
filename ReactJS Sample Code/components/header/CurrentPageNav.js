import React from 'react';
import { Link } from 'react-router-dom';
import styled from 'styled-components';

import { nowrap } from '../../utils/styles';
import angleRightIcon from '../../assets/manager/menu/icon_angle_right.svg';

const Wrapper = styled.div`
  flex-grow: 1;
  flex-shrink: 1;
  position: relative;
  display: flex;
  align-items: center;
  height: 56px;
  padding-right: 40px;

  h6 {
    margin-right: 20px;
  }
`;

const SectionTitleWrapper = styled.div`
  flex-shrink: 1;
  display: flex;
  align-items: center;
`;

const Title = styled.span`
  max-width: 150px;
  flex-grow: 1;
  flex-shrink: 1;
  font-size: 12px;
  line-height: 21px;
  font-weight: ${ props => (props.lastItem ? 'bold' : '600') };
  ${ props => (props.lastItem ? '' : 'color: #7d909a') }
  ${ nowrap } ${ props => (props.clickable
  ? `
    &:hover,
    &:active {
      color: ${ props.theme.colors.darkBlue };
    }
  `
  : '') };
`;

const ArrowIcon = styled.img`
  margin: 0 20px;
  width: 16px;
  height: 16px;
`;

const CurrentPageNav = ({ pages }) => (
  <Wrapper>
    { pages.map(({ title, url }, index) => {
      const lastItem = pages.length === index + 1;
      return (
        <SectionTitleWrapper>
          { url ? (
            <Link to={ url }>
              <Title clickable={ !!url }>{ title }</Title>
            </Link>
          ) : (
            <Title lastItem={ lastItem }>{ title }</Title>
          ) }
          { lastItem ? null : <ArrowIcon src={ angleRightIcon } /> }
        </SectionTitleWrapper>
      );
    }) }
  </Wrapper>
);

export default CurrentPageNav;
