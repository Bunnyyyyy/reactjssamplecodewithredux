import React from 'react';
import styled from 'styled-components';
import { DropTarget } from 'react-dnd';

import { transform } from '../../../utils/styles';

import i18n from '../../../../i18n';

const t = a => i18n.t([`components/manager:${ a }`]);

// React DnD logic
const ELEMENTS_BUILDER_ELEMENT = 'ELEMENTS_BUILDER_ELEMENT';
const source = {
  drop (props, monitor) {
    const { onDropNewElement, onDropExistingElement, order } = props;
    const { type, element, oldOrder } = monitor.getItem();

    if (type === 'new') {
      onDropNewElement(element, order);
    } else if (oldOrder !== undefined) {
      const newOrder = order > oldOrder ? order - 1 : order;
      onDropExistingElement(element, oldOrder, newOrder);
    }
  },
};
function collect (connect, monitor) {
  return {
    dropTarget: connect.dropTarget(),
    draggedItem: monitor.getItem(),
    canDrop: monitor.canDrop(),
    isOver: monitor.isOver(),
  };
}

const TargetWrapper = styled.div`
  position: absolute;
  width: 100%;
  height: 100px;
  bottom: -60px;
  left: 0;
  background-color: transparent;
  z-index: 200;

  ${ props => (props.over
    ? `
        position: relative;
        width: auto;
        height: 130px;
        bottom: auto;
        padding: 30px;
        margin: 20px 0 20px ${ props.section ? '0' : '40px' };
        background-color: ${ props.theme.colors.mainBlue };
      `
    : '') } ${ props => (props.empty
  ? `
    position: absolute;
    height: calc(100vh - ${ props.theme.newObject ? '190px' : '240px' });
    width: calc(100% - 40px);
    top: 80px;
    left: 20px;
    padding: 30px;
    margin: 0;
    background-color: ${ props.theme.colors.mainBlue };
  `
  : '') };
`;

const TargetInnerWrapper = styled.div`
  position: relative;
  width: 100%;
  height: 100%;
  padding: 0 20px;
  border: 2px dashed white;
  text-align: center;
`;

const TargetText = styled.p`
  position: relative;
  top: 50%;
  color: white;
  font-size: 20px;
  line-height: 24px;

  ${ props => (props.empty
    ? `
    font-size: 25px;
    line-height: 30px;
    font-weight: 700;
  `
    : '') } ${ transform('translateY(-50%)') };
`;

const ElementTarget = ({
  dropTarget,
  draggedItem,
  canDrop,
  isOver,
  order,
  empty,
}) => {
  if (!canDrop) return null;
  if (draggedItem && draggedItem.oldOrder !== undefined) {
    if (order === draggedItem.oldOrder || order - draggedItem.oldOrder === 1) {
      return null;
    }
  }

  return dropTarget(
    <div>
      <TargetWrapper
        section={ draggedItem.element && draggedItem.element.type === 'section' }
        over={ isOver }
        empty={ empty }
      >
        { isOver || empty ? (
          <TargetInnerWrapper>
            <TargetText empty={ empty }>
              { t('Release to drop component here') }
            </TargetText>
          </TargetInnerWrapper>
        ) : null }
      </TargetWrapper>
    </div>,
  );
};

export default DropTarget(ELEMENTS_BUILDER_ELEMENT, source, collect)(
  ElementTarget,
);
