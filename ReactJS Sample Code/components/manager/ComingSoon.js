import React from 'react';
import styled from 'styled-components';

const Div = styled.div`
  text-align: center;
`;

const EmojiP = styled.p`
  font-size: 72px;
  line-height: 1.2;
  margin: 40px 0 0 0 !important;
`;

const TextP = styled.p`
  font-size: 36px;
  font-weight: 600;
  line-height: 1.2;
`;

const ComingSoon = () => (
  <Div>
    <EmojiP>👀</EmojiP>
    <TextP>Coming Soon!</TextP>
  </Div>
);

export default ComingSoon;
