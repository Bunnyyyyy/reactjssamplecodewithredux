import React from 'react';

import NavBar from '../../common/NavBar';
import i18n from '../../../../i18n';

const t = a => i18n.t([`components/manager:${ a }`]);

const MembershipNavBar = ({ organization, inline }) => {
  const navLinks = [
    // {
    //   title: 'Overview',
    //   route: `/${ organization.id }/membership`,
    //   exact: true
    // },
    {
      title: t('Members'),
      route: `/${ organization.id }/membership`,
      exact: true,
    },
    {
      title: t('Membership Plans'),
      route: `/${ organization.id }/membership/plans`,
    },
    {
      title: t('Data Collection'),
      route: `/${ organization.id }/membership/data-collection`,
    },
  ];

  return (
    <NavBar inline={ inline } links={ navLinks } />
  );
};

export default MembershipNavBar;
