import styled from 'styled-components';
import React, { Component, Fragment } from 'react';
import {
  WhatsappShareButton,
  EmailShareButton,
} from 'react-share';
import appStoreIcon from '../../../assets/common/dl_appstore.png';
import googlePlayIcon from '../../../assets/common/dl_googleplay.png';
import helperAppIcon from '../../../assets/common/app_icon_new.png';
import qrCodeIOS from '../../../assets/common/qr-code-ios.png';
import qrCodeAndroid from '../../../assets/common/qr-code-android.png';
import whatsappIcon from '../../../assets/common/green_whatsapp.svg';
import Modal from '../../common/ModalV2';
import emailIcon from '../../../assets/common/grey_email.svg';
import i18n from '../../../../i18n';

const t = a => i18n.t([`manager/events/overview:${ a }`]);

const IOS_APP_STORE_URL = 'https://itunes.apple.com/us/app/juven-manager-app/id1438847545';
const GOOGLE_PLAY_STORE_URL = 'https://play.google.com/store/apps/details?id=com.juven.juvenmanagerapp';

const Container = styled.div`
  display: flex;
  align-items: center;
`;

const ImageButton = styled.img`
  cursor: pointer;
  width: 24px;
  height: 24px;
  margin: 5px;

  &:hover {
    opacity: 0.7;
  }
`;

const HelperImageIcon = styled.img`
  display: block;
  flex-shrink: 0;
  width: 55px;
`;

const MainMessage = styled.p`
  flex-shrink: 1;
  font-size: 12px;
  line-height: 18px;
  margin: 0 10px 0 20px;
`;

const QRCodeImage = styled.img`
  width: 150px;
  margin: 30px;
`;

const ShareButtonStyled = styled.div`
   display: flex;
   align-items: center;
`;

const ShareText = styled.p`
  margin-right: 5px;
  margin-bottom: 5px;
`;

const StoresGroup = styled.div`
  @media (min-width: 1301px) {
    display: flex;
  }
`;

const StoreIcon = styled.img`
  display: block;
  width: auto;
  height: 34px;
  cursor: pointer;

  @media (min-width: 1301px) {
    &:first-child {
      margin-right: 10px;
    }
  }

  @media (max-width: 1300px) {
    &:first-child {
      margin-bottom: 5px;
    }
  }
`;

const Reminder = styled.p`
  color: #9c9c9c;
`;

const ColoredLink = styled.a`
  color: #00a7ff;
`;

class DownloadApp extends Component {
  constructor (props) {
    super(props);
    this.state = {
      openModal: false,
    };
  }

  openModal (text) {
    this.setState({
      openModal: text,
    });
  }

  closeModal () {
    this.setState({
      openModal: false,
    });
  }

  render () {
    const { openModal } = this.state;
    const ModalFooterCss = `box-shadow: none;
        border-top: 1px solid #e1e8ed;
        display: flex;
        justify-content: center;
        align-items: center;
        width: calc(100% - 60px);
        margin-left: 30px;`;

    const url = openModal === 'IOS' ? IOS_APP_STORE_URL : GOOGLE_PLAY_STORE_URL;
    const emailSubject = `${ t('For guest check-in') }: ${ t('Download Juven Manager') }`;
    const emailBody = `${ t(
      'Hey,',
    ) } \n \n ${ t(
      'Remember to download the Juven Manager app to check-in your guest at the event! Here’s the link',
    ) }: \n ${ url }`;
    const ShareButton = [
      <ShareButtonStyled>
        <ShareText>
          { `${ t('Share') }:` }
        </ShareText>
        <WhatsappShareButton url={ url } title='Hey, Remember to download the Juven Manager app to check-in your guest at the event! Here’s the link: '>
          <ImageButton src={ whatsappIcon } />
        </WhatsappShareButton>
        <EmailShareButton url={ url } subject={ emailSubject } body={ emailBody }>
          <ImageButton src={ emailIcon } />
        </EmailShareButton>
      </ShareButtonStyled>,
    ];

    return (
      <Fragment>
        <Container>
          <HelperImageIcon src={ helperAppIcon } />
          <MainMessage>
            { ' ' }
            { t('Get') }
            <strong>
              { ' ' }
              { t('Juven manager') }
              { ' ' }
            </strong>
            { t('to help you manage guest check-in and scan tickets on your phone') }
          </MainMessage>
          <StoresGroup>
            <StoreIcon src={ appStoreIcon } onClick={ () => this.openModal('IOS') } />
            <StoreIcon src={ googlePlayIcon } onClick={ () => this.openModal('playstore') } />
          </StoresGroup>
        </Container>
        { openModal && (
          <Modal
            isOpen
            medium
            onClose={ this.closeModal.bind(this) }
            title={ openModal === 'IOS' ? t('Download Juven manager app for Apple') : t('Download Juven manager app on Android') }
            headerCss={ `
              & h4 {
                  font-size: 15px;
              }
            ` }
            bodyCss={ `
              padding-bottom: 10px;
              text-align: center;

              p, a {
                font-size: 12px;
                line-height: 18px;
              }
            ` }
            footerContent={ ShareButton }
            footerCss={ ModalFooterCss }
          >
            { openModal === 'IOS' ? <p>{ t('Scan QR code with your Apple device’s camera to install.') }</p>
              : [
                <p>
                  { t('Scan QR code with your Android device to install.') }
                </p>,
                <Reminder>
                  { t('Don’t have a QR code scanner? Search Juven manager in Google Play.') }
                </Reminder>,
              ] }
            <QRCodeImage src={ openModal === 'IOS' ? qrCodeIOS : qrCodeAndroid } />
            { openModal === 'IOS'
              ? (
                <p>
                  { t('Install on current device?') }
                  { ' ' }
                  <ColoredLink href={ url } target='_blank' rel='noopener noreferrer'>
                    { t('Go to App store') }
                    { ' ' }
                    { '>' }
                  </ColoredLink>
                </p>
              )
              : (
                <p>
                  { t('Install on current device?') }
                  { ' ' }
                  <ColoredLink href={ url } target='_blank' rel='noopener noreferrer'>
                    { t('Go to Google Play') }
                    { ' ' }
                    { '>' }
                  </ColoredLink>
                </p>
              ) }
          </Modal>
        ) }
      </Fragment>
    );
  }
}

export default DownloadApp;
