import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { withRouter } from 'react-router';
import styled from 'styled-components';
import moment from 'moment';
import * as UiActionCreators from '../../../../../redux/ui';
import * as ManagerActionCreators from '../../../../../redux/manager';
import * as EventActionCreators from '../../../../../redux/event';

import ItemTable from '../../../../../components/common/ItemTableV2';

import i18n from '../../../../../../i18n';

const t = a => i18n.t([`translations:${ a }`]);

const CustomSelect = styled.select`
  border: none;
  background-color: transparent;
  width: 50px;
  outline: none;
`;
const StatusBall = styled.div`
  width: 10px;
  height: 10px;
  border-radius: 50%;
  background-color: ${ props => (props.active ? props.theme.colors.mainGreen : props.theme.colors.lightGray) };
  margin-right: 5px;
`;

const AlignedDiv = styled.div`
  display: flex;
  align-items: center;
`;

const mapStateToProps = state => ({
  events: state.manager.events,
  event: state.manager.event,
  organization: state.manager.organization,
  tickets: state.event.tickets,
  ticketPackages: state.event.ticketPackages,
});

const ActionCreators = Object.assign(
  {},
  UiActionCreators,
  ManagerActionCreators,
  EventActionCreators,
);

const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators(ActionCreators, dispatch),
});

class EventTicketingPackagesListView extends Component {
  async componentDidMount () {
    const { actions, match } = this.props;
    const { eId } = match.params;

    actions.setLoading(true);

    try {
      const promises = [
        actions.getEventTickets(eId),
        actions.getEventTicketPackages(eId),
      ];
      await Promise.all(promises);
    } catch (e) {
      console.error('ERROR: EventTicketingPackagesListView', e);
    } finally {
      actions.setLoading(false);
    }
  }

  componentWillUnmount () {
    const { actions } = this.props;
    actions.resetSaveActions();
  }

  async onFinishSaving () {
    try {
      const { actions, event } = this.props;
      const eId = event.id;
      await Promise.all([
        actions.getOrganizationEvent(eId),
        actions.getEventTickets(eId),
        actions.getEventTicketPackages(eId),
      ]);
    } catch (e) {
      throw e;
    }
  }

  selectCurrency = currencies => (
    <CustomSelect>
      { currencies.map(data => <option value={ data.name }>{ data.name }</option>) }
    </CustomSelect>
  );

  render () {
    const {
      event = {},
      organization,
      ticketPackages,
    } = this.props;

    const tableColumns = [
      {
        field: 'name',
        title: t('Package Name'),
        flex: '3',
        customCss: 'font-weight: 700;',
      },
      {
        field: 'status',
        title: t('Status'),
        format: (_, item) => {
          let statusInput = '';
          const now = moment();
          if (now < moment(item.start_at) || now > moment(item.end_at)) {
            statusInput = 'Inactive';
          } else {
            statusInput = 'Active';
          }
          return (
            <AlignedDiv>
              <StatusBall active={ statusInput === 'Active' } />
              { statusInput }
            </AlignedDiv>
          );
        },
      },
    ];

    const tableEditUrl = item => `/${ organization.id }/events/${ event.id }/ticketing/packages/${ item.id }`;
    const tableNewItemUrl = `/${ organization.id }/events/${
      event.id
    }/ticketing/packages/new`;

    return (
      <ItemTable
        columns={ tableColumns }
        rows={ ticketPackages }
        editUrl={ tableEditUrl }
        itemText={ t('Package') }
        newItemUrl={ tableNewItemUrl }
        searchField='name'
      />
    );
  }
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(EventTicketingPackagesListView));
