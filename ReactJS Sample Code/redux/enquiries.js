import { handleApiError } from '../utils';
import { getRequest } from './index';

const defaultState = {};

export default function reducer (state = defaultState) {
  return state;
}

export function createEnquiry (data) {
  return dispatch => new Promise(async (resolve, reject) => {
    try {
      const request = getRequest(dispatch);

      const url = '/enquiries';
      await request.post(url, data);

      return resolve();
    } catch (e) {
      const error = handleApiError(e);
      return reject(error);
    }
  });
}
