import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { withRouter } from 'react-router';

import * as UiActionCreators from '../../../../redux/ui';
import * as ManagerActionCreators from '../../../../redux/manager';
import * as OrganizationActionCreators from '../../../../redux/organization';

import ManagerContainer from '../../../../components/layouts/ManagerContainer';
import Card from '../../../../components/manager/cards/Card';
import PieChart from '../../../../components/manager/charts/PieChart';
import { LineChartForGrowth, BarChartForTags } from '../../../../utils/analytics';

import { t as translate } from '../../../../../i18n';

const t = a => translate(a, 'manager/forms/overview');

const mapStateToProps = state => ({
  user: state.me.user,
  organizations: state.manager.organizations,
  organization: state.manager.organization,
  form: state.organization.form,
  respondents: state.organization.formRespondents,
  tags: state.organization.tags,
});

const ActionCreators = Object.assign(
  {},
  UiActionCreators,
  ManagerActionCreators,
  OrganizationActionCreators,
);
const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators(ActionCreators, dispatch),
});

class FormOverviewView extends Component {
  async componentDidMount () {
    const { actions, match } = this.props;
    const { oId, foId } = match.params;

    actions.setLoading(true);

    try {
      const promises = [
        actions.getOrganizationForm(oId, foId),
        actions.getOrganizationTags(oId),
        actions.getOrganizationFormRespondents(oId, foId),
      ];
      await Promise.all(promises);
    } catch (e) {
      console.error('ERROR: FormOverviewView', e);
    } finally {
      actions.setLoading(false);
    }
  }

  render () {
    const { form, respondents, tags } = this.props;

    const fieldsWithOptions = (form.fields || []).filter(
      f => Array.isArray(f.options) && f.options.length,
    );
    const OptionPieCharts = fieldsWithOptions.map((f) => {
      const dataObj = {};
      f.options.forEach(o => (dataObj[o] = 0));
      respondents.forEach((r) => {
        const fieldValue = r.data_fields[f.id] || '';
        f.options.forEach((o) => {
          if (f.multiple_options) {
            if (fieldValue.indexOf(o) > -1) {
              dataObj[o]++;
            }
          } else if (fieldValue === o) {
            dataObj[o]++;
          }
        });
      });

      const data = [];
      for (const option in dataObj) {
        data.push({
          label: option,
          value: dataObj[option],
        });
      }

      return (
        <Card title={ f.label }>
          <PieChart data={ data } />
        </Card>
      );
    });

    return (
      <ManagerContainer>
        <LineChartForGrowth
          entities={ respondents }
          title={ t('Number of Respondents') }
          type='DAY'
        />
        <BarChartForTags
          tags={ tags }
          entities={ respondents }
          label={ t('Tags in Respondents') }
        />
        { respondents.length ? OptionPieCharts : null }
      </ManagerContainer>
    );
  }
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(FormOverviewView));
