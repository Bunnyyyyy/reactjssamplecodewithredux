import React, { Component } from 'react';
import styled from 'styled-components';
import {
  StripeProvider,
  Elements,
  injectStripe,
  CardNumberElement,
  CardExpiryElement,
  CardCVCElement,
} from 'react-stripe-elements';

import Label from './Label';
import InvalidMessage from '../InvalidMessage';
import Button from '../Button';
import theme from '../../../utils/theme';

import i18n from '../../../../i18n';

const t = a => i18n.t([`translations:${ a }`]);

const { STRIPE_API_KEY } = process.env;

const InputWrapper = styled.div`
  position: relative;
  display: flex;
  align-items: center;
  width: 100%;
  padding: 20px 0;
  margin-bottom: 20px;

  ${ props => props.customCss || '' };
`;

const CardElementsWrapper = styled.div`
  display: flex;
  align-items: center;
  flex-grow: 1;
  flex-shrink: 0;

  @media (max-width: 600px) {
    flex-wrap: wrap;
  }
`;

const ElementWrapper = styled.div`
  border-bottom: 1px solid
    ${ props => (props.theme.light ? 'white' : props.theme.colors.mainGray) };
`;

const CardNumber = styled.div`
  position: relative;
  flex: 60% 0 0;
  padding-right: 30px;

  @media (max-width: 600px) {
    flex: 100% 0 0;
    padding-right: 0;
    margin-bottom: 20px;
  }
`;
const CardExpiry = styled.div`
  flex: 25% 0 0;
  padding-right: 30px;

  @media (max-width: 600px) {
    flex: 60% 0 0;
  }
`;
const CardCVC = styled.div`
  flex: 15% 0 0;

  @media (max-width: 600px) {
    flex: 40% 0 0;
  }
`;

const ActionButtonWrapper = styled.div`
  display: flex;
  margin: 25px 0 0 10px;
`;

const ActionButton = Button.extend`
  margin: 0 0 0 20px;
  line-height: 30px;

  & i {
    font-size: 20px;
    line-height: 30px;
    margin: 0;
  }

  ${ props => (props.loading
    ? `
    background-color: ${ props.theme.colors.mainBlue } !important;
    opacity: .4;
  `
    : '') };
`;

const StatusIcon = styled.i`
  position: absolute;
  bottom: 2px;
  right: 35px;
  height: 32px;
  font-size: 20px;
  line-height: 32px !important;
  color: ${ props => (props.error ? '#fa6363' : '#47c166') };
`;

const stripeElementStyle = {
  base: {
    fontFamily: '"Muli", sans-serif',
    fontSize: '15px',
    lineHeight: '32px',
    color: theme.colors.darkGray,
  },
};

class CardElements extends Component {
  componentDidMount () {
    const { onRef, onClearElementsRef } = this.props;
    onRef(this);
    onClearElementsRef(this.clearElements.bind(this));
  }

  componentWillUnmount () {
    const { onRef, onClearElementsRef } = this.props;
    onRef(undefined);
    onClearElementsRef(undefined);
  }

  clearElements () {
    this.numberElement.clear();
    this.expiryElement.clear();
    this.cvcElement.clear();
  }

  render () {
    const { onChange, valid, invalid } = this.props;

    const ValidIcon = (
      <StatusIcon className='fas fa-check-circle' aria-hidden='true' />
    );
    const InvalidIcon = (
      <StatusIcon
        error
        className='fa fa-exclamation-circle'
        aria-hidden='true'
      />
    );

    return (
      <CardElementsWrapper>
        <CardNumber>
          <Label label={ t('Card Number') } />
          <ElementWrapper>
            <CardNumberElement
              onReady={ numberElement => (this.numberElement = numberElement) }
              style={ stripeElementStyle }
              onChange={ onChange.bind(this) }
            />
          </ElementWrapper>
          { valid ? ValidIcon : invalid ? InvalidIcon : null }
        </CardNumber>
        <CardExpiry>
          <Label label={ t('Expiry Date') } />
          <ElementWrapper>
            <CardExpiryElement
              onReady={ expiryElement => (this.expiryElement = expiryElement) }
              style={ stripeElementStyle }
              onChange={ onChange.bind(this) }
            />
          </ElementWrapper>
        </CardExpiry>
        <CardCVC>
          <Label label={ t('CVC') } />
          <ElementWrapper>
            <CardCVCElement
              onReady={ cvcElement => (this.cvcElement = cvcElement) }
              style={ stripeElementStyle }
              onChange={ onChange.bind(this) }
            />
          </ElementWrapper>
        </CardCVC>
      </CardElementsWrapper>
    );
  }
}
const StripeCardElements = injectStripe(CardElements);

const STRIPE_ELEMENT_TYPES = ['cardNumber', 'cardExpiry', 'cardCvc'];

// eslint-disable-next-line
export default class CardInput extends Component {
  constructor () {
    super();

    const elementEmpties = {};
    STRIPE_ELEMENT_TYPES.forEach(type => (elementEmpties[type] = true));

    this.state = {
      elementCompletes: {},
      elementEmpties,
      elementErrors: {},
      loadingToken: false,
      token: null,
    };
  }

  // HACK: add shitty hack to clear elements from outside
  componentDidUpdate (prevProps) {
    const { clearElements } = this.props;
    if (prevProps.clearElements !== clearElements) {
      this.clearElements();
    }
  }

  async onChangeElement (element) {
    const { onUpdate } = this.props;
    let { elementCompletes, elementEmpties, elementErrors } = this.state;
    const {
      elementType, complete, empty, error,
    } = element;

    elementCompletes = Object.assign({}, elementCompletes);
    elementCompletes[elementType] = complete;

    elementEmpties = Object.assign({}, elementEmpties);
    elementEmpties[elementType] = empty;

    elementErrors = Object.assign({}, elementErrors);
    // HACK: clear server-side error
    delete elementErrors.card;
    if (error && error.code.indexOf('incomplete') === -1) {
      elementErrors[elementType] = error.message;
    } else {
      delete elementErrors[elementType];
    }

    this.setState(
      {
        elementCompletes,
        elementEmpties,
        elementErrors,
      },
      async () => {
        const allComplete = STRIPE_ELEMENT_TYPES.reduce(
          (pv, type) => pv && !!elementCompletes[type],
          true,
        );

        const token = allComplete ? await this.getStripeToken() : null;
        this.setState({ token }, async () => {
          if (onUpdate) await onUpdate(token);
        });
      },
    );
  }

  async getStripeToken () {
    let { elementErrors } = this.state;

    this.setState({ loadingToken: true });
    const { token, error } = await this.card.props.stripe.createToken();

    let cardError;
    if (error) {
      cardError = error.message;
    } else if (!token || !token.id) {
      cardError = t('There was an error when adding this card.');
    }

    if (cardError) {
      elementErrors = Object.assign({}, elementErrors);
      elementErrors.card = cardError;
      this.setState({
        elementErrors,
        loadingToken: false,
      });

      return null;
    }
    this.setState({ loadingToken: false });
    return token.id;
  }

  async clickSave () {
    const { onSave } = this.props;
    const { token } = this.state;

    try {
      if (token && onSave) await onSave(token);
      this.clearElements();
    } catch (e) {
      console.error(e);
    }
  }

  async clickCancel () {
    const { onCancel } = this.props;

    try {
      this.clearElements();
      if (onCancel) await onCancel();
    } catch (e) {
      console.error(e);
    }
  }

  render () {
    if (typeof window === 'undefined') return null;

    const {
      onSave, saveText, cancelText, customCss,
    } = this.props;
    const {
      elementEmpties = {},
      elementErrors = {},
      loadingToken,
      token,
    } = this.state;

    const allEmpty = STRIPE_ELEMENT_TYPES.reduce(
      (pv, type) => pv && !!elementEmpties[type],
      true,
    );

    let errorMessage = '';
    for (const key in elementErrors) {
      errorMessage += `${ elementErrors[key] } `;
    }

    // HACK: use onClearElementsRef to expose the clearElements method inside StripeCardElements
    return (
      <InputWrapper customCss={ customCss }>
        <StripeProvider apiKey={ STRIPE_API_KEY }>
          <Elements
            fonts={ [
              {
                cssSrc: 'https://fonts.googleapis.com/css?family=Muli:400,700',
              },
            ] }
          >
            <StripeCardElements
              onRef={ card => (this.card = card) }
              onClearElementsRef={ clearElements => (this.clearElements = clearElements)
              }
              onChange={ this.onChangeElement.bind(this) }
              valid={ !!token }
              invalid={ !!errorMessage.length }
            />
          </Elements>
        </StripeProvider>
        { onSave ? (
          <ActionButtonWrapper>
            <ActionButton
              small
              light
              loading={ loadingToken }
              disabled={ !token }
              onClick={ this.clickSave.bind(this) }
            >
              { loadingToken ? (
                <i className='fas fa-circle-notch fa-spin' />
              ) : (
                saveText || t('Save')
              ) }
            </ActionButton>
            <ActionButton
              small
              gray
              disabled={ allEmpty }
              onClick={ this.clickCancel.bind(this) }
            >
              { cancelText || t('Cancel') }
            </ActionButton>
          </ActionButtonWrapper>
        ) : null }
        { errorMessage.length ? (
          <InvalidMessage customCss='bottom: 0;'>{ errorMessage }</InvalidMessage>
        ) : null }
      </InputWrapper>
    );
  }
}
