import React from 'react';
import styled from 'styled-components';

const Wrapper = styled.ul`
  list-style-position: inside;
  list-style-type: disc;
`;

function BulletedList ({ children, attributes }) {
  return (
    <Wrapper { ...attributes }>
      { children }
    </Wrapper>
  );
}

export default BulletedList;
