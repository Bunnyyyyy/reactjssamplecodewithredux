import React from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router';
import { Route, Switch } from 'react-router-dom';
import { Helmet } from 'react-helmet';

import Header from '../../../components/header/Header';

import EventsListView from './List';
import EventsNewView from './New';
import EventIndexView from './event/Index';

import { t as translate } from '../../../../i18n';

const t = a => translate(a, 'manager/settings');

const mapStateToProps = state => ({
  organization: state.manager.organization,
});

const EventsIndexView = ({ match, organization }) => {
  const { url } = match;

  let navPages = null;
  if (url.indexOf('/new') > 0) {
    navPages = [
      {
        title: t('Events'),
        url: `/${ organization.id }/events`,
      },
      {
        title: t('New'),
      },
    ];
  } else if (url.indexOf('/events/') === -1) {
    navPages = [
      {
        title: t('Events'),
      },
    ];
  }

  const pageTitle = navPages
    .map(p => p.title)
    .reverse()
    .concat('Juven Manager')
    .join(' - ');

  return (
    <div>
      <Helmet>
        <title>{ pageTitle }</title>
        <meta property='og:title' content={ pageTitle } />
      </Helmet>
      { navPages ? <Header navPages={ navPages } /> : null }
      <Switch>
        <Route exact path='/:oId/events' component={ EventsListView } />
        <Route exact path='/:oId/events/new' component={ EventsNewView } />
        <Route path='/:oId/events/:eId' component={ EventIndexView } />
      </Switch>
    </div>
  );
};

export default withRouter(connect(mapStateToProps)(EventsIndexView));
