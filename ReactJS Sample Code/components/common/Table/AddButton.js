import React, { Component } from 'react';
import styled from 'styled-components';

import Button from '../Button';
import { boxShadow } from '../../../utils/styles';

import i18n from '../../../../i18n';

const t = a => i18n.t([`components/common:${ a }`]);

const Dropdown = styled.div`
  display: block;
  outline: none;
  position: absolute;
  z-index: 200;
  top: 5px;
  right: 10px;
  width: 200px;
  padding: 10px 0;
  border-radius: 6px;
  background-color: white;
  ${ boxShadow('0 6px 6px 0 rgba(0, 0, 0, 0.1)') };

  &:hover {
    display: block;
  }
`;

const AddItemButton = Button.extend`
  width: 105px;
  height: 30px;
  line-height: 30px;
  border-radius: 30px;
  padding: 0;
  margin: 0;
  font-size: 12px;
  line-height: 14px;
  text-transform: none;
  box-shadow: 0 0 6px 0 rgba(0, 0, 0, 0.16);

`;

const DropdownButton = Button.extend`
  height: 36px;
  width: 100%;
  font-size: 12px;
  font-weight: 400;
  text-align: left;
  text-transform: none;
  padding: 0 15px;
  margin: 0;

  &:hover,
  &:active {
    color: ${ props => props.theme.colors.mainBlue };
  }
`;

const DropdownButtonIcon = styled.div`
  display: inline-block;
  vertical-align: middle;
  height: 36px;
  width: 29px;
  padding: 6px 5px 6px 0;
`;

const AddItemWrapper = styled.div`
  position: relative;
  flex: 0 0 165px;
  height: 100%;
  padding: 9px 30px;
  border-left: 1px solid ${ props => props.theme.colors.lightGray };
`;

export default class AddButton extends Component {
  constructor (props) {
    super(props);

    this.state = {
      showingDropdown: false,
    };

    this.hideDropdownHandler = this.hideDropdown.bind(this);
  }

  componentDidMount () {
    document.addEventListener('mousedown', this.hideDropdownHandler, false);
  }

  componentWillUnmount () {
    document.removeEventListener('mousedown', this.hideDropdownHandler, false);
  }

  hideDropdown (e) {
    if (this.dropdownWrapper && this.dropdownWrapper.contains(e.target)) return;
    this.setState({ showingDropdown: false });
  }

  showDropdown () {
    this.setState({ showingDropdown: true });
  }

  render () {
    const { actions = [], text = t('Add') } = this.props;
    const { showingDropdown } = this.state;

    const AddDropdownButtons = actions.map(a => (
      <DropdownButton plain onClick={ a.function ? a.function.bind(this) : null }>
        <DropdownButtonIcon>
          { a.icon ? <img src={ a.icon } alt={ a.title } /> : null }
        </DropdownButtonIcon>
        { a.title }
      </DropdownButton>
    ));

    const AddDropdown = actions.length > 1 && showingDropdown
      ? <Dropdown innerRef={ wrapper => (this.dropdownWrapper = wrapper) }>{ AddDropdownButtons }</Dropdown>
      : null;

    const onClickAction = actions.length === 1
      ? actions[0].function.bind(this)
      : this.showDropdown.bind(this);

    return (
      <AddItemWrapper>
        <AddItemButton light disabled={ !actions.length } onClick={ onClickAction }>
          { text }
        </AddItemButton>
        { actions.length > 1 ? AddDropdown : null }
      </AddItemWrapper>
    );
  }
}
