// @flow
import React from 'react';
import styled from 'styled-components';
import { Line, defaults } from 'react-chartjs-2';

import theme from './../../../utils/theme';
import i18n from '.././../../../i18n';
const t = a => i18n.t([`components/manager:${ a }`]);

defaults.global.responsive = true;

const ChartTitle = styled.div`
  font-weight: 700;
`;

const ChartMenu = styled.i`
  color: ${ props => props.theme.colors.mainGray };
  font-size: 22px;
  cursor: pointer;
`;

const ChartHeadingRow = styled.div`
  display: flex;
  flex: 1;
  justify-content: space-between;
  align-items: center;
  margin-bottom: 5px;
`;

const ChartSummary = styled.div`
  font-size: 24px;
  font-weight: 700;
  color: ${ props => props.theme.colors.mainBlue };
`;

const SummaryIcon = styled.i`
  -webkit-text-stroke: 2px white;
  margin-left: 5px;
  color: ${ props => props.theme.colors.mainBlue };
`;

const ChartSummaryRow = styled.div`
  display: flex;
  flex: 1;
  justify-content: flex-end;
  align-items: center;
`;

const LineChartWrapper = styled.div`
  width: 100%;
  padding: 15px;
  background-color: white;
`;

const InnerWrapper = styled.div`
  height: ${ props => props.height }px;
`;

const EmptyText = styled.p`
  font-size: 20px;
  line-height: 25px;
  text-align: center;
  padding: 30px 0;
`;

const DEFAULT_CHART_HEIGHT = 320;
const CHART_OPTIONS = {
  scales: {
    xAxes: [
      {
        ticks: {
          autoSkip: false,
          fontFamily: '"Muli", sans-serif',
          fontColor: theme.colors.mainGray
        },
        gridLines: {
          display: false
        }
      }
    ],
    yAxes: [
      {
        ticks: {
          beginAtZero: true,
          // only show integer ticks
          callback: value => {
            if (Math.floor(value) === value) return value.toLocaleString();
          },
          fontFamily: '"Muli", sans-serif',
          fontColor: theme.colors.mainGray
        },
        gridLines: {
          display: false
        }
      }
    ]
  },
  maintainAspectRatio: false,
  legend: {
    display: false
  }
};

const TOOLTIP_OPTIONS = {
  mode: 'index',
  intersect: false,
  titleFontFamily: '"Muli", sans-serif',
  titleFontColor: theme.colors.darkGray,
  titleSpacing: 0,
  titleMarginBottom: 0,
  backgroundColor: 'white',
  borderColor: theme.colors.lightGray,
  borderWidth: 1,
  xPadding: 20,
  yPadding: 10,
  callbacks: {
    title: tooltipItem =>
      `${ tooltipItem[0].xLabel }: ${ tooltipItem[0].yLabel.toLocaleString() }`,
    label: () => null
  }
};

const DATASET_OPTIONS = {
  backgroundColor: theme.colors.bgGray,
  borderColor: theme.colors.mainBlue,
  borderWidth: '1px',
  pointRadius: 0
};

const LineChart = ({
  data = [],
  chartHeight = DEFAULT_CHART_HEIGHT,
  summaryValue,
  summaryDirection,
  title,
  tooltipFormat,
  emptyText = t('No data yet.')
}) => {
  if (!data.length) return <EmptyText>{ emptyText }</EmptyText>;

  const labels = data.map(d => d.label || '');
  const values = data.map(d => d.value);

  const RenderSummary = () => {
    if (!summaryValue) return null;

    const summaryNum = summaryValue.toLocaleString('en', { useGrouping: true });

    if (summaryDirection === 'up') {
      return (
        <ChartSummaryRow>
          <ChartSummary md={ 1 }>{ summaryNum }</ChartSummary>
          <SummaryIcon className='fas fa-arrow-up' />
        </ChartSummaryRow>
      );
    } else if (summaryDirection === 'down') {
      return (
        <ChartSummaryRow>
          <ChartSummary md={ 1 }>{ summaryNum }</ChartSummary>
          <SummaryIcon className='fas fa-arrow-down' />
        </ChartSummaryRow>
      );
    } else {
      return (
        <ChartSummaryRow>
          <ChartSummary md={ 1 }>{ summaryNum }</ChartSummary>
        </ChartSummaryRow>
      );
    }
  };

  const tooltipOptions =
    typeof tooltipFormat === 'function'
      ? Object.assign({}, TOOLTIP_OPTIONS, {
        callbacks: {
          title: tooltipItem => {
            const dataObj = {
              label: tooltipItem[0].xLabel,
              value: tooltipItem[0].yLabel
            };

            return tooltipFormat(dataObj);
          },
          label: () => null
        }
      })
      : TOOLTIP_OPTIONS;

  return (
    <LineChartWrapper>
      { title ? (
        <ChartHeadingRow>
          <ChartTitle>{ title }</ChartTitle>
          <ChartMenu className='fas fa-ellipsis-h' />
        </ChartHeadingRow>
      ) : null }
      <RenderSummary />
      <InnerWrapper height={ chartHeight }>
        <Line
          data={ {
            labels,
            datasets: [
              {
                data: values,
                ...DATASET_OPTIONS
              }
            ]
          } }
          options={ {
            ...CHART_OPTIONS,
            tooltips: tooltipOptions
          } }
        />
      </InnerWrapper>
    </LineChartWrapper>
  );
};

export default LineChart;
