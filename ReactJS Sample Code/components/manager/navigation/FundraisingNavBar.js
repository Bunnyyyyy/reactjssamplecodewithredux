import React from 'react';

import NavBar from '../../common/NavBar';
import i18n from '../../../../i18n';

const t = a => i18n.t([`components/manager:${ a }`]);

const FundraisingNavBar = ({ organization, inline }) => {
  const navLinks = [
    {
      title: t('Overview'),
      route: `/${ organization.id }/fundraising`,
      exact: true,
    },
    {
      title: t('One-off Donations'),
      route: `/${ organization.id }/fundraising/donations`,
    },
    {
      title: t('Monthly Donations'),
      route: `/${ organization.id }/fundraising/monthly`,
    },
    {
      title: t('Donors'),
      route: `/${ organization.id }/fundraising/donors`,
    },
    {
      title: t('Widget'),
      route: `/${ organization.id }/fundraising/widget`,
    },
    {
      title: t('Data Collection'),
      route: `/${ organization.id }/fundraising/data-collection`,
    },
    {
      title: t('Settings'),
      route: `/${ organization.id }/fundraising/settings`,
    },
  ];

  return (
    <NavBar inline={ inline } links={ navLinks } />
  );
};

export default FundraisingNavBar;
