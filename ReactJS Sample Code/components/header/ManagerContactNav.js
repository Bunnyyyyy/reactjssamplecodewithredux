import React from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import styled from 'styled-components';

import { nowrap } from './../../utils/styles';

const Wrapper = styled.div`
  flex-grow: 1;
  position: relative;
  display: flex;
  align-items: center;
  height: ${ props => (props.theme.manager ? '56px' : '78px') };
  padding-right: 40px;

  h6 {
    margin-right: 20px;
  }
`;

const SectionTitleWrapper = styled.div`
  display: flex;
  flex-shrink: 0;
  align-items: center;
`;

const SectionTitle = styled.h5`
  &:hover,
  &:active {
    color: ${ props => props.theme.colors.darkBlue };
  }
`;

const TitleArrow = styled.i`
  margin: 0 20px;
`;

const ContactName = styled.h5`
  flex-grow: 1;
  flex-shrink: 1;
  ${ nowrap };
`;

const mapStateToProps = state => ({
  organization: state.manager.organization,
  form: state.organization.form,
  member: state.organization.member,
  formRespondent: state.organization.formRespondent
});

const ManagerEventNav = ({ organization, form, member, formRespondent }) => {
  let contact = {};
  let parentLink = '/manager';
  let parentTitle = '';

  if (member.id) {
    contact = member;
    parentLink = `/${ organization.id }/community/members`;
    parentTitle = 'Members';
  } else if (formRespondent.id) {
    contact = formRespondent;
    parentLink = `/${ organization.id }/forms/${ form.id }/respondents`;
    parentTitle = form.title;
  }

  return (
    <Wrapper>
      <SectionTitleWrapper>
        <Link to={ parentLink }>
          <SectionTitle>{ parentTitle }</SectionTitle>
        </Link>
        <TitleArrow className='fas fa-arrow-right' />
      </SectionTitleWrapper>
      <ContactName>{ contact.name }</ContactName>
    </Wrapper>
  );
};

export default connect(mapStateToProps)(ManagerEventNav);
