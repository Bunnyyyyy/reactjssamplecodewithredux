import styled from 'styled-components';

export default styled.div`
  cursor: pointer;
  width: 110px;
  height: 80px;
  border-radius: 5px;
  background-color: white;
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  border: solid 2px ${ props => props.theme.colors.deadGray };
  margin-right: 20px;
  margin-bottom: 20px;
  line-height: 45px;

  &:hover,
  &:active {
    border-color: rgba(0, 167, 255);
    box-shadow: none;
  }

  & > h6 {
    text-align: center;
    font-weight: 400;
  }

  & > div {
    height: 45px;
    color: ${ props => props.theme.colors.darkGray };
    text-align: center;

    & i {
      font-size: 30px;
      line-height: 45px;
    }
  }

  ${ props => (props.selected
    ? `
      border-color: ${ props.theme.colors.mainBlue } !important;
      color: ${ props.theme.colors.mainBlue } !important;
      > div i {
        color: ${ props.theme.colors.mainBlue } !important;
      }
    `
    : '') };
  ${ props => (props.customCss ? props.customCss : '') };
`;
