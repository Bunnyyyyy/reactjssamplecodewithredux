import React from 'react';

import Bold from './tags/Bold';
import Italic from './tags/Italic';
import LinkTag from './tags/Link';
import OrderedList from './tags/OrderedList';
import ListItem from './tags/ListItem';
import BulletedList from './tags/BulletedList';
import BlockQuote from './tags/BlockQuote';

import {
  BOLD,
  ITALIC,
  UNDERLINED,
  P,
  H5,
  QUOTE,
  A,
  OL,
  UL,
  LI,
} from './constants';

const BLOCK_TAGS = {
  P, LI, UL, OL, H5, BLOCKQUOTE: QUOTE,
};

const MARK_TAGS = {
  strong: BOLD,
  em: ITALIC,
  u: UNDERLINED,
};

const INLINE_TAGS = {
  A,
};

export default [
  // block type
  {
    deserialize (el, next) {
      const type = BLOCK_TAGS[el.tagName];
      if (type) {
        return {
          object: 'block',
          type,
          data: {
            className: el.getAttribute('class'),
          },
          nodes: next(el.childNodes),
        };
      }
    },
    serialize (obj, children) {
      if (obj.object === 'block') {
        switch (obj.type) {
          case P:
            return <p>{ children }</p>;
          case H5:
            return <h5>{ children }</h5>;
          case QUOTE:
            return <BlockQuote>{ children }</BlockQuote>;
          case OL:
            return <OrderedList>{ children }</OrderedList>;
          case UL:
            return <BulletedList>{ children }</BulletedList>;
          case LI:
            return <ListItem>{ children }</ListItem>;
          default:
        }
      }
    },
  },
  // marks type
  {
    deserialize (el, next) {
      const mark = MARK_TAGS[el.tagName.toLowerCase()];
      if (mark) {
        return {
          object: 'mark',
          type: mark,
          nodes: next(el.childNodes),
        };
      }
    },
    serialize (obj, children) {
      if (obj.object === 'mark') {
        switch (obj.type) {
          case BOLD:
            return <Bold>{ children }</Bold>;
          case ITALIC:
            return <Italic>{ children }</Italic>;
          case UNDERLINED:
            return <u>{ children }</u>;
          default:
        }
      }
    },
  },
  // inline
  {
    deserialize (el, next) {
      const inline = INLINE_TAGS[el.tagName];
      if (inline) {
        return {
          object: 'inline',
          type: inline,
          nodes: next(el.childNodes),
        };
      }
    },
    serialize (obj, children) {
      switch (obj.type) {
        case A:
          return <LinkTag node={ obj }>{ children }</LinkTag>;
        default:
      }
    },
  },
];
