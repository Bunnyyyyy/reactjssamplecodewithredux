import React, { Component } from 'react';
import styled from 'styled-components';

import Input from '../Input';

import i18n from '../../../../i18n';

const t = a => i18n.t([`components/common:${ a }`]);

const SortPerPageDiv = styled.div`
  display: flex;
  align-self: center;
  padding: 10px 0;

  & p {
    font-size: 12px;
    line-height: 30px;
  }
`;

export default class SortPerPage extends Component {
  constructor (props) {
    super();

    this.state = {
      perPage: props.perPage,
    };

    this.timer = null;
  }

  componentWillUnmount () {
    clearTimeout(this.timer);
  }

  onChangePerPageValue (f, value) {
    const { onChangePerPage } = this.props;
    const perPage = value ? Math.min(99, Number(value)) : null;

    clearTimeout(this.timer);
    this.timer = setTimeout(() => {
      if (perPage) onChangePerPage(perPage);
    }, 500);

    this.setState({ perPage });
  }

  render () {
    const { perPage } = this.state;

    return (
      <SortPerPageDiv>
        <p>{ t('Display') }</p>
        <Input
          value={ perPage || '' }
          number
          onChange={ this.onChangePerPageValue.bind(this) }
          customCss={ `
            vertical-align: middle;
            width: 40px;
            margin: 0 10px;

            & input {
              height: 28px;
              padding: 0 5px;
              border-bottom: 1px solid ${ props => props.theme.colors.mainBlue };
              text-align: center;
              font-size: 15px;
              font-weight: normal;
              color: ${ props => props.theme.colors.darkGray };
              opacity: 1;
            }
          ` }
        />
        <p>{ t('Records per page') }</p>
      </SortPerPageDiv>
    );
  }
}
