import React from 'react';
import copy from 'copy-to-clipboard';
import styled from 'styled-components';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import {
  FacebookShareButton,
  TwitterShareButton,
  WhatsappShareButton,
  EmailShareButton,
  LinkedinShareButton,
} from 'react-share';

import * as ActionCreators from '../../../redux/ui';

import { nowrap } from '../../../utils/styles';
import theme from '../../../utils/theme';
import Button from '../../common/Button';
import Card from './Card';

import facebookIcon from '../../../assets/common/blue_facebook.svg';
import twitterIcon from '../../../assets/common/blue_twitter.svg';
import whatsappIcon from '../../../assets/common/green_whatsapp.svg';
import linkedinIcon from '../../../assets/common/blue_linkedin.svg';
import emailIcon from '../../../assets/common/grey_email.svg';

import i18n from '../../../../i18n';

const t = a => i18n.t([`manager/events/overview:${ a }`]);

const { FRONTEND_URL } = process.env;

const BodyWrapper = styled.div`
  display: flex;
`;

const Label = styled.p`
  font-size: 12px;
  padding-bottom: 5px;
`;

const CopyButton = styled(Button)`
  width: 90px;
  height: 30px;
  line-height: 28px;
  font-size: 13px;
  padding: 0;
  border: solid 1px ${ props => props.theme.colors.mainBlue };
  box-shadow: none;
  margin: 0;

  &:hover {
    border: solid 1px ${ props => props.theme.colors.darkBlue };
    background-color: white;
    color: ${ props => props.theme.colors.darkBlue };
    box-shadow: none;
  }
`;

const ShareButtonContainer = styled.div`
  display: flex;
  height: 30px;
  margin-bottom: 30px;

  > div {
    outline: none;
  }

  @media (min-width: 1201px) {
    padding-right: 45px;
  }
`;

const ImageButton = styled.img`
  cursor: pointer;
  width: 30px;
  height: 30px;

  &:hover {
    opacity: 0.7;
  }
`;

const MarginRight = styled.div`
  margin-right: 10px;
`;

const LinkWrapper = styled.div`
  width: calc(100% - 130px);
  display: inline-flex;
  padding-right: 30px !important;

  a {
    color: ${ props => props.theme.colors.mainBlue };
    ${ nowrap } &:hover {
      color: ${ props => props.theme.colors.darkBlue };
    }
  }
`;

const CopyWrapper = styled.div`
  display: inline-flex;
  width: 130px;
`;

const cardCss = `
  width: calc(50% - 15px);
  margin: 20px 30px 0 0;
  box-shadow: none;
  border-radius: 0;
  height: 100%;

  &:nth-child(2n) {
    margin-right: 0;
  }
`;

const cardBodyCss = `
  > div {
    padding-left: 30px;
    padding-right: 0;
  }
`;

const bodyCss = `
  > div {
    padding-left: 0;
    padding-right: 0;
  }
`;

const borderCss = `
  border-right: 1px solid ${ theme.colors.lightGray };
`;

const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators(ActionCreators, dispatch),
});

const ShareCard = ({
  actions, title, event, customCss,
}) => {
  const url = `${ FRONTEND_URL }/${ event.id }`;

  const emailSubject = `${ t('Hey, check out') } ${ event.name } on Juven!`;
  const emailBody = `${ t(
    'Hey, I\'ve come across an interesting event called',
  ) } ${ event.name } ${ t(
    'on Juven, and I think you may be interested! Here is the link to the event:',
  ) } ${ url }.`;

  const copyToClipBoard = () => {
    copy(url);
    actions.showNotification({
      type: 'success',
      text: t('Copied to clipboard'),
    });
  };

  return (
    <Card title={ title } customCss={ customCss } bodyCss={ cardBodyCss }>
      <BodyWrapper>
        <Card customCss={ [cardCss, borderCss] } bodyCss={ bodyCss }>
          <Label>{ t('Event URL') }</Label>
          <LinkWrapper>
            <a href={ url } target='_blank' rel='noopener noreferrer'>
              { url }
            </a>
          </LinkWrapper>
          <CopyWrapper>
            <CopyButton onClick={ copyToClipBoard }>{ t('Copy') }</CopyButton>
          </CopyWrapper>
        </Card>
        <Card customCss={ cardCss } bodyCss={ bodyCss }>
          <Label>{ t('Social media') }</Label>
          <ShareButtonContainer>
            <FacebookShareButton url={ url }>
              <ImageButton src={ facebookIcon } />
            </FacebookShareButton>
            <MarginRight />
            <TwitterShareButton url={ url }>
              <ImageButton src={ twitterIcon } />
            </TwitterShareButton>
            <MarginRight />
            <WhatsappShareButton url={ url }>
              <ImageButton src={ whatsappIcon } />
            </WhatsappShareButton>
            <MarginRight />
            <LinkedinShareButton url={ url }>
              <ImageButton src={ linkedinIcon } />
            </LinkedinShareButton>
            <MarginRight />
            <EmailShareButton url={ url } subject={ emailSubject } body={ emailBody }>
              <ImageButton src={ emailIcon } />
            </EmailShareButton>
          </ShareButtonContainer>
        </Card>
      </BodyWrapper>
    </Card>
  );
};

export default connect(null, mapDispatchToProps)(ShareCard);
