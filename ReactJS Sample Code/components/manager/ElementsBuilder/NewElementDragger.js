import React from 'react';
import styled from 'styled-components';
import { DragSource } from 'react-dnd';
import { userSelect } from '../../../utils/styles';

// React DnD logic
const ELEMENTS_BUILDER_ELEMENT = 'ELEMENTS_BUILDER_ELEMENT';
const source = {
  beginDrag (props) {
    // HACK: scroll to right when adding new element
    if (typeof window !== 'undefined') {
      window.scroll({
        left: 99999,
        behavior: 'smooth',
      });
    }

    return {
      type: 'new',
      element: props.element,
    };
  },

  endDrag () {
    return {};
  },
};
function collect (connect) {
  return {
    connectDragPreview: connect.dragPreview(),
    connectDragSource: connect.dragSource(),
  };
}

const DraggerDiv = styled.div`
  display: flex;
  height: 38px;
  margin: 15px 0;
  background-color: white;
  border-radius: 5px;
  border: 1px solid #bcd4e0;

  ${ userSelect('none') };
`;

const DraggerHandle = styled.div`
  background-color: ${ props => props.theme.colors.mainBlue };
  text-align: center;
  height: 100%;
  width: 41px;

  & i {
    line-height: 38px;
    color: white;
  }
`;

const DraggerLabel = styled.p`
  margin: 0 15px;
  line-height: 38px;
`;

const NewElementDragger = ({
  element = {},
  connectDragPreview,
  connectDragSource,
}) => connectDragPreview(
  <div>
    { connectDragSource(
      <div>
        <DraggerDiv>
          <DraggerHandle>
            <i className='fas fa-bars' />
          </DraggerHandle>
          <DraggerLabel>{ element.label }</DraggerLabel>
        </DraggerDiv>
      </div>,
    ) }
  </div>,
);

export default DragSource(ELEMENTS_BUILDER_ELEMENT, source, collect)(
  NewElementDragger,
);
