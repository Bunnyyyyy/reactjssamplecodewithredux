import React from 'react';
import styled from 'styled-components';

import { OL, UL } from '../constants';

const Icon = styled.i`
  display: inline-block;
  cursor: pointer;
  text-align: center;
  height: 44px;
  width: 40px;
  font-size: 14px;
  line-height: 44px;
  color: ${ props => (
    props.selected ? props.theme.colors.mainBlue : props.theme.colors.darkGray
  ) };

  &:hover {
    color: ${ props => props.theme.colors.mainBlue };
  }
`;

function Lists ({ onClick, selected }) {
  return [
    <Icon
      onClick={ () => onClick(OL) }
      className='fas fa-list-ol'
      selected={ selected(OL) }
    />,
    <Icon
      onClick={ () => onClick(UL) }
      className='fas fa-list-ul'
      selected={ selected(UL) }
    />,
  ];
}

export default Lists;
