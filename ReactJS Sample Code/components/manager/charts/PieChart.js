// @flow
import React from 'react';
import styled from 'styled-components';
import { Pie, defaults } from 'react-chartjs-2';
import { nowrap } from './../../../utils/styles';
import theme from './../../../utils/theme';

defaults.global.responsive = true;

const colors = [
  ['#5EC8FF'],
  ['#FFDD6B', '#5EC8FF'],
  ['#FFDD6B', '#6DE1B1', '#5EC8FF'],
  ['#FFDD6B', '#6DE1B1', '#5EC8FF', '#4A71A1'],
  ['#F0778F', '#FFDD6B', '#6DE1B1', '#5EC8FF', '#4A71A1'],
  ['#F0778F', '#FFDD6B', '#6DE1B1', '#5EC8FF', '#4A71A1', '#C097E4'],
  ['#D3D3D3', '#F0778F', '#FFDD6B', '#6DE1B1', '#5EC8FF', '#4A71A1', '#C097E4'],
  [
    '#D3D3D3',
    '#F0778F',
    '#FFAB6E',
    '#FFDD6B',
    '#6DE1B1',
    '#5EC8FF',
    '#4A71A1',
    '#C097E4'
  ],
  [
    '#D3D3D3',
    '#F0778F',
    '#FFAB6E',
    '#FFDD6B',
    '#6DE1B1',
    '#65AF91',
    '#5EC8FF',
    '#4A71A1',
    '#C097E4'
  ],
  [
    '#D3D3D3',
    '#F0778F',
    '#FFAB6E',
    '#FFDD6B',
    '#6DE1B1',
    '#65AF91',
    '#5EC8FF',
    '#4A71A1',
    '#C097E4',
    '#5C5C5C'
  ]
];

const ChartLegendContainer = styled.div`
  width: 360px;
  flex-basis: auto;
  flex-grow: 0;
  flex-shrink: 0;
  margin: 30px 60px;
`;

const ColorIcon = styled.div`
  width: 30px;
  height: 10px;
  display: inline-block;
  background-color: ${ props => props.color };
`;
const LegendLabelContainer = styled.span`
  display: flex;
  flex: 1;
  flex-shrink: 1;
  flex-direction: row;
  justify-content: space-between;
  margin-left: 10px;
  ${ nowrap };
`;
const LegendItem = styled.div`
  margin-top: 0px;
  margin-bottom: 0px;
  align-items: center !important;
  font-size: 15px;
  width: 100%;
  height: 36px;
  margin-right: -34.5px !important;
  padding-right: 10px !important;
  padding-left: 10px !important;
  ${ props => (props.colored ? 'background-color: #f2f7fa;' : '') };
`;
const LegendLabel = styled.div`
  flex-shrink: 1;
  font-weight: normal;
  line-height: 30px;
  padding-right: 10px;
  ${ nowrap };

  span {
    margin-left: 10px;
  }
`;
const LegendValue = styled.div`
  flex-shrink: 0;
  line-height: 30px;
  font-weight: bold;
`;

const Container = styled.div`
  display: flex;
  flex-wrap: wrap;
  padding: 30px;
  align-items: center;
  justify-content: space-around;
`;

const CHART_OPTIONS = {
  responsive: false,
  legend: {
    display: false
  }
};

const TOOLTIP_OPTIONS = {
  titleFontFamily: '"Muli", sans-serif',
  titleFontColor: theme.colors.darkGray,
  titleSpacing: 0,
  titleMarginBottom: 0,
  backgroundColor: 'white',
  borderColor: theme.colors.lightGray,
  borderWidth: 1,
  xPadding: 20,
  yPadding: 10,
  callbacks: {
    title: (items, data) => {
      const index = items[0].index;
      let label = data.labels[index];
      if (label.length > 15) {
        label = label.slice(0, 13) + '..';
      }
      return `${ label }: ${ data.datasets[0].data[index].toLocaleString() }`;
    },
    label: () => null
  }
};

const PieChart = ({ data = [], tooltipFormat }) => {
  const setColor = i =>
    colors[(data.length - 1) % colors.length][i % colors.length];

  const chartData = {
    labels: data.map(d => d.label),
    datasets: [
      {
        data: data.map(d => d.value),
        backgroundColor: data.map((_, i) => setColor(i)),
        borderWidth: 0
      }
    ]
  };

  const tooltipOptions =
    typeof tooltipFormat === 'function'
      ? Object.assign({}, TOOLTIP_OPTIONS, {
        callbacks: {
          title: (items, data) => {
            const index = items[0].index;
            let label = data.labels[index];
            if (label.length > 10) {
              label = label.slice(0, 9) + '..';
            }

            const dataObj = {
              label,
              value: data.datasets[0].data[index]
            };

            return tooltipFormat(dataObj);
          },
          label: () => null
        }
      })
      : TOOLTIP_OPTIONS;

  return (
    <Container>
      <Pie
        data={ chartData }
        width={ 200 }
        height={ 200 }
        options={ {
          ...CHART_OPTIONS,
          tooltips: tooltipOptions
        } }
      />
      <ChartLegendContainer>
        { data.map((x, i) => (
          <LegendItem colored={ i % 2 === 1 }>
            <LegendLabelContainer>
              <LegendLabel>
                <ColorIcon color={ setColor(i) } />
                <span>{ x.label }</span>
              </LegendLabel>
              <LegendValue>{ x.value }</LegendValue>
            </LegendLabelContainer>
          </LegendItem>
        )) }
      </ChartLegendContainer>
    </Container>
  );
};

export default PieChart;
